# Mint Energy Estimator Tool

Sass / Normalize + Bourbon + Neat / Grunt.js / Modernizr.js / jQuery


You will need bower-installer to run the bower install script
```
$ npm install bower-installer -g

```

Resolve Node dependencies
```
$ npm install

```

Resolve Bower dependencies and copy files into /app

```
$ bower install

```

Build Development files
```
$ grunt

```

Build Production files
```
$ grunt build

```

Watch for JS and CSS Changes
```
$ grunt watch

```

Clean JS and CSS Directories
```
$ grunt clean

```

Run JSHint
```
$ grunt jshint

```

Run cssLint
```
$ grunt cssLint

```

Run htmlLint
```
$ grunt htmllint

```
