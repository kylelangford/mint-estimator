<!doctype html>
<html class="no-js" lang="">

<?php include 'templates/_head.php'; ?>

<body>

  <?php include 'templates/_header.php'; ?>

  <section>
    <div class="content">
      <div class="row">

        <div class="main-content">

          <div class="progress-bar">
            <a href="javascript:void(0);" class="step-one active">1</a>
            <a href="javascript:void(0);" class="step-two">2</a>
            <a href="javascript:void(0);" class="step-three">3</a>
            <a href="javascript:void(0);" class="step-four">4</a>
            <a href="javascript:void(0);" class="step-five">5</a>
            <a href="javascript:void(0);" class="step-six">6</a>
          </div>

          <div class="login-form">
            <h2 class="title">Log In</h2>

            <form id="loginForm" autocomplete="off" action="">

              <span class="input">
                <label class="required input-label" for="loginName">
                  <span class="input-label-content">Broker Name</span>
                </label>
                <input type="text" class="inputRequired input-field" id="loginEmail" name="loginName" />
              </span>

              <span class="input">
                <label class="required input-label" for="loginPW">
                  <span class="input-label-content">Password</span>
                </label>
                <input type="password" class="inputRequired input-field" id="loginPW" name="loginPW" />
              </span>

              <span class="input btn-container">
                <button id="login-button" type="button" class="btn next login disabled">LOG IN</button>
              </span>

            </form>

          </div>
        </div>

      </div>
    </div>
  </section>

  <?php include 'templates/_footer.php'; ?>

  <script src="dist/js/scripts.js"></script>

  <script type="text/javascript">

    pageLoginValidation();

  </script>

</body>

</html>
