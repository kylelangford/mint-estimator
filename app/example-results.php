<!doctype html>
<html class="no-js" lang="">

<?php include 'templates/_head.php'; ?>

<body>

  <?php include 'templates/_header.php'; ?>

  <!-- example HTML: utility bar -->
  <div class="report-toolbar">
    <div class="row">

      <div class="title-left">
        <h1>Results</h1>
      </div>
      <div class="tools">
        <a href="" class="btn white"><span class="icon-print"></span>Print</a>
        <a href="" class="btn white"><span class="icon-envelope"></span>Email</a>
        <a href="" class="btn white"><span class="icon-download"></span>Save</a>
      </div>

    </div>
  </div>
  <!-- example HTML: Utility bar END -->

  <!-- example HTML: Account Block -->
  <div class="info-block">
    <div class="row">

      <div class="customer-information">
        <h2 class="result-title"><span class="icon-user"></span>Custom Information</h2>
        <div class="content">
          <ul>
            <li><strong>First Name:</strong> Marcus</li>
            <li><strong>Last Name:</strong> Fournier</li>
            <li><strong>Email Address:</strong> marcus.fournier@gmail.com</li>
            <li><strong>Title:</strong> Doctor</li>
            <li><strong>Phone Number:</strong> (347) 485-2847</li>
            <li><strong>Company Name:</strong> Widgets Inc.</li>
            <li><strong>Business Address:</strong> 135 Main Street</li>
            <li><strong>City:</strong> Boston</li>
            <li><strong>State:</strong> Massachusetts</li>
            <li><strong>Zip Code:</strong> 02111</li>
          </ul>
        </div>
      </div>

      <div class="account-information">
        <h2 class="result-title"><span class="icon-profile"></span>Account Information</h2>
        <div class="content">
          <ul>
            <li><strong>Company Utility:</strong> National Grid</li>
            <li><strong>Zone:</strong> NEMA</li>
            <li><strong>Rate Class:</strong> G1</li>
          </ul>
          <hr >
          <ul>
            <li><strong>Rate Comparison:</strong> Competitive Supplier</li>
            <li><strong>Hedging Strategy:</strong> Extended Winter ATC</li>
            <li><strong>Retail Adder:</strong> 0.04</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- example HTML: Account Block END -->

  <section class="tab-container">
    <div class="row">

      <ul class="tabs">
    		<li class="tab-link current" data-tab="tab-1">2013-2014</li>
    		<li class="tab-link" data-tab="tab-2">2014-2015</li>
    		<li class="tab-link" data-tab="tab-3">2015-2016</li>
    	</ul>

    	<div id="tab-1" class="tab-content current">

        <div class="inner">
          <div class="historical-savings-analysis">
            <h2 class="result-title"><span class="icon-pie-chart"></span>Historical Savings</h2>
            <table class="results-theme">
              <tr>
                <th></th>
                <th>Competitive Supplier</th>
                <th>Power Select</th>
              </tr>
              <tr>
                <th>Avg. Cents/kWh:</th>
                <td>$0.08220</td>
                <td>$0.10329</td>
              </tr>
              <tr>
                <th>Est. Charges:</th>
                <td>$18,902.71</td>
                <td>$23,753.36</td>
              </tr>
            </table>
          </div>

          <div class="historical-savings-summary">
            <h2 class="result-title"><span class="icon-cabinet"></span>Estimated Savings Summary</h2>
            <table class="results-theme">
              <tr>
                <th>Cents/kWh</th>
                <th>Total $ Savings</th>
                <th>Total % Savings</th>
              </tr>
              <tr>
                <td>$0.1033</td>
                <td>-$4,851</td>
                <td>-26%</td>
              </tr>
            </table>
          </div>
        </div>

        <div class="savings-detail-graph">

          <h2 class="result-title"><span class="icon-stats-dots"></span>Savings Detail Graph</h2>

          <script src="https://www.google.com/jsapi"></script>
              <div id="graph_566f079b68374"></div>
          <script>
            google.load('visualization', '1.0', {'packages':['corechart']});
            google.setOnLoadCallback(function() {

              // Create the data table.
              var data = google.visualization.arrayToDataTable([
                ['Month', 'Hedge Block', 'Power Select Rate', 'Competitive Supplier'],
                            [
                    'May-14',
                    0,
                    0.081211083276741,
                    0.0822            ],
                            [
                    'Jun-14',
                    0,
                    0.08501042559306,
                    0.0822            ],
                            [
                    'Jul-14',
                    0,
                    0.087439062364283,
                    0.0822            ],
                            [
                    'Aug-14',
                    0,
                    0.076943812022602,
                    0.0822            ],
                            [
                    'Sep-14',
                    0,
                    0.082753690888054,
                    0.0822            ],
                            [
                    'Oct-14',
                    0,
                    0.077719283152613,
                    0.0822            ],
                            [
                    'Nov-14',
                    0,
                    0.093422821768266,
                    0.0822            ],
                            [
                    'Dec-14',
                    0.09886858367313,
                    0.13886858367313,
                    0.0822            ],
                            [
                    'Jan-15',
                    0.09886858367313,
                    0.13886858367313,
                    0.0822            ],
                            [
                    'Feb-15',
                    0.09886858367313,
                    0.13886858367313,
                    0.0822            ],
                            [
                    'Mar-15',
                    0.09886858367313,
                    0.13886858367313,
                    0.0822            ],
                            [
                    'Apr-15',
                    0,
                    0.072064527590552,
                    0.0822            ],
                        ]);

              // Set chart options
              var options = {
                vAxis: {title: 'Cents/kWh'},
                legend: 'top',
                colors: ['#9F9F9F', '#0D76BD', '#92D050'],
                pointSize: 4,
                seriesType: 'line',
                series: {
                  0: {type: 'bars'}
                },
                width: 1200,
                height: 400
              };

              // Instantiate and draw our chart, passing in some options.
              var chart = new google.visualization.ComboChart(document.getElementById('graph_566f079b68374'));
              chart.draw(data, options);
            });
          </script>
        </div>

        <div class="savings-detail-table">

          <h2 class="result-title"><span class="icon-table2"></span>Savings Detail Table</h2>

          <table class="results-theme responsive">
            <tr>
              <th></th>
              <th>May-14</th>
              <th>Jun-14</th>
              <th>Jul-14</th>
              <th>Aug-14</th>
              <th>Sep-14</th>
              <th>Oct-14</th>
              <th>Nov-14</th>
              <th>Dec-14</th>
              <th>Jan-15</th>
              <th>Feb-15</th>
              <th>Mar-15</th>
              <th>Apr-15</th>
            </tr>
            <tr>
              <th>Competitive Supplier</th>
              <td>$0.0822</td>
              <td>$0.0822</td>
              <td>$0.0822</td>
              <td>$0.0822</td>
              <td>$0.0822</td>
              <td>$0.0822</td>
              <td>$0.0822</td>
              <td>$0.0822</td>
              <td>$0.0822</td>
              <td>$0.0822</td>
              <td>$0.0822</td>
              <td>$0.0822</td>
            </tr>
            <tr>
              <th>Power Select Rate</th>
              <td>$0.0812</td>
              <td>$0.0850</td>
              <td>$0.0874</td>
              <td>$0.0769</td>
              <td>$0.0828</td>
              <td>$0.0777</td>
              <td>$0.0934</td>
              <td>$0.1389</td>
              <td>$0.1389</td>
              <td>$0.1389</td>
              <td>$0.1389</td>
              <td>$0.0721</td>
            </tr>
            <tr>
              <th>Cost W/Competitive Supplier</th>
              <td>$1,213</td>
              <td>$1,411</td>
              <td>$1,785</td>
              <td>$1,973</td>
              <td>$1,539</td>
              <td>$1,174</td>
              <td>$1,223</td>
              <td>$1,647</td>
              <td>$1,808</td>
              <td>$1,884</td>
              <td>$1,766</td>
              <td>$1,480</td>
            </tr>
            <tr>
              <th>Cost w/Power Select</th>
              <td>$1,199</td>
              <td>$1,459</td>
              <td>$1,899</td>
              <td>$1,847</td>
              <td>$1,549</td>
              <td>$1,110</td>
              <td>$1,390</td>
              <td>$2,783</td>
              <td>$3,055</td>
              <td>$3,183</td>
              <td>$2,983</td>
              <td>$1,297</td>
            </tr>
            <tr>
              <th>Estimated Savings</th>
              <td>$15</td>
              <td>($48)</td>
              <td>($114)</td>
              <td>$126</td>
              <td>($10)</td>
              <td>$64</td>
              <td>($167)</td>
              <td>($1,136)</td>
              <td>($1,247)</td>
              <td>($1,299)</td>
              <td>($1,217)</td>
              <td>$182</td>
            </tr>
          </table>
        </div>
      </div>

      <div id="tab-2" class="tab-content">
    		 Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    	</div>

      <div id="tab-3" class="tab-content">
    		Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
    	</div>

    </div>
  </section>


  <footer>
    <div class="footer">
      <div class="row">
        <p class="copyright">Powered by Mint Energy</p>
      </div>
    </div>
  </footer>

  <script src="dist/js/scripts.js"></script>

  <script type="text/javascript">
    initTabs();
  </script>

  <!-- <script src="dist/js/lib.js"></script> -->

</body>

</html>
