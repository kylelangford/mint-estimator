<!doctype html>
<html class="no-js" lang="">

<?php include 'templates/_head.php'; ?>

<body>

  <?php include 'templates/_header.php'; ?>

  <h1>Results</h1>

  <?php require_once "../cmsb/lib/viewer_functions.php"; ?>

  <?php

    // load input from request
    $Input  = null;
    $logOutput = [];

    // alternate input loading from proposal_log
    if (@$_REQUEST['load']) {
      $proposalLog = mysql_get('chunked_proposal_log', $_REQUEST['load']);
      if ($proposalLog) {
        $_REQUEST = json_decode($proposalLog['input'], true);
        if (@$_REQUEST['step3Utility'] === 'National Grid') { $_REQUEST['step3Utility'] = 'National Grid (MA)'; }
      }
    }

    // start log record (input first)
    $logNum = mysql_insert('chunked_proposal_log', [
      'createdDate' => mysql_datetime(),
      'input'       => json_encode($_REQUEST),
    ], true);

  ?>

  <?php
    $testingLinkEncoded = htmlencode(realUrl("?load=$logNum"));
    echo "Direct link (for testing): <a href=\"$testingLinkEncoded\">$testingLinkEncoded</a><br><br><br>";

    ob_start();
  ?>

  <table>
    <tr>
      <th style="text-align: right;">First Name:&nbsp;&nbsp;</th>
      <td><?php echo htmlencode(@$_REQUEST['step2FirstName']) ?></td>
    </tr>
    <tr>
      <th style="text-align: right;">Last Name:&nbsp;&nbsp;</th>
      <td><?php echo htmlencode(@$_REQUEST['step2LastName']) ?></td>
    </tr>
    <tr>
      <th style="text-align: right;">Email:&nbsp;&nbsp;</th>
      <td><?php echo htmlencode(@$_REQUEST['step2Email']) ?></td>
    </tr>
    <tr>
      <th style="text-align: right;">Customer Utility:&nbsp;&nbsp;</th>
      <td><?php echo htmlencode(@$_REQUEST['step3Utility']) ?></td>
    </tr>
    <tr>
      <th style="text-align: right;">Pricing Zone:&nbsp;&nbsp;</th>
      <td><?php echo htmlencode(@$_REQUEST['step3Zone']) ?></td>
    </tr>
    <tr>
      <th style="text-align: right;">Rate Class:&nbsp;&nbsp;</th>
      <td><?php echo htmlencode(@$_REQUEST['step3RateClass']) ?></td>
    </tr>
  </table>

  <?php

    $errors = '';
    ini_set('display_errors', '0');
    function onError($errno, $errstr, $errfile, $errline, $errcontext) {
      if (error_reporting() === 0) { return; } // ignore @supressed errors
      global $errors;
      $errors .= json_encode([
        'errno'       => $errno,
        'errstr'      => $errstr,
        'errfile'     => $errfile,
        'errline'     => $errline,
        'errcontext'  => $errcontext,
      ]) . "\n";
    }
    set_error_handler('onError');
    try {

      $requestMapping = [
        //'' => 'step2FirstName',                     // e.g. "asdf"
        //'' => 'step2LastName',                      // e.g. "asdf"
        //'' => 'step2Email',                         // e.g. "asdf@example.com"
        //'' => 'step3RadioUserType',                 // e.g. "commercial"
        //'' => 'step3State',                         // e.g. "MA"
        'Utility' => 'step3Utility',                  // e.g. "National Grid"
        'Zone' => 'step3Zone',                        // e.g. "NEMA"
        'Rate_Class' => 'step3RateClass',             // e.g. "G1"
        //'' => 'step4PlanStart',                     // e.g. "05-01-2014"
        'Term' => 'step4PlanLength',                  // e.g. "12"
        'Rate_Comparison' => 'step5CompareRate',      // e.g. "Competitive Supplier"
        //'' => 'step5_monthly_usage',                // e.g. [ '', 22000, 22000, ... ]
        //'' => 'step5_monthly_rate',                 // e.g. [ '', 0.0822, 0.0822, ... ]
        'Hedging_Strategy' => 'step6PlanStrategy',    // e.g. "Extended Winter ATC"
        'Retail_Adder'     => 'step6RetailAdderRate', // e.g. "0.04"
      ];
      $newRequest = [];
      foreach ($requestMapping as $newKey => $oldKey) {
        $newRequest[$newKey] = @$_REQUEST[$oldKey];
      }

      $startMonth = @$_REQUEST['step4PlanStartMonth'];
      $startYear  = @$_REQUEST['step4PlanStartYear'];

      // strip hidden template fields
      array_shift($_REQUEST['step5_monthly_usage']);
      array_shift($_REQUEST['step5_monthly_rate']);

      // chunk!
      $usageChunks = array_chunk($_REQUEST['step5_monthly_usage'], 12);
      $rateChunks  = array_chunk($_REQUEST['step5_monthly_rate' ], 12);

      // iterate over chunks
      foreach (array_keys($usageChunks) as $chunkIndex) {
        $usageChunk = $usageChunks[$chunkIndex];
        $rateChunk  = $rateChunks[$chunkIndex];

        $chunkRequest = $newRequest; // shallow clone

        // determine date range for this chunk
        $chunkRequest['Start_Month'] = $startMonth . '-' . ($startYear + $chunkIndex);
        $chunkRequest['Term'] = count($usageChunk);

        // assign usage/rate data to the appropriate months
        foreach (range(0, count($usageChunk) - 1) as $monthIndex) {
          $monthNum = $startMonth + $monthIndex;
          if ($monthNum > 12) { $monthNum -= 12; }
          $chunkRequest["Usage_$monthNum"]                     = $usageChunk[ $monthIndex ];
          $chunkRequest["Competitive_Supplier_Rate_$monthNum"] = $rateChunk[  $monthIndex ];
        }

        //showme($chunkRequest);

        // process input into proposal
        $Input    = _mp_loadInputFromRequest($chunkRequest);
        $proposal = mintProposal($Input); // run mintProposal calculator

        if ($errors) {
          break;
        }

        // push data to log
        array_push($logOutput, $proposal);

        // show output
        if ($chunkIndex > 0) {
          echo "<hr>";
        }
        $pageNumberText = ($chunkIndex + 1) . " of " . count($usageChunks);
        echo "<h1>Proposal page $pageNumberText</h1>";
        _mp_test_showResults($proposal);  // show results
      }

      // update log
      if ($logNum) {
        mysql_update('chunked_proposal_log', $logNum, null, [ 'output' => json_encode($logOutput) ]);
      }

    }
    catch (Exception $e) {

      $errors .= "An error was caught by the try block\n";

    }

    $resultHTML = ob_get_clean();

  ?>

  <?php if ($errors): ?>
    <h2>An error occurred due to invalid input data.</h2>
    <p>Please go back to the previous page and enter valid input.</p>
    <p><a href="#" onclick="$('#errors').toggle(); return false;">Show error details</a></p>
    <pre id="errors" style="display: none;"><?php echo htmlencode($errors) ?></pre>
  <?php else: ?>
    <?php echo $resultHTML; ?>
  <?php endif ?>

  <?php include 'templates/_footer.php'; ?>

  <script src="dist/js/lib.js"></script>

</body>

</html>
