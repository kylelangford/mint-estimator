//
// Page
//

// Menu Setup
(function(){

  $('.navigation-menu-button').on('click' , function() {
    $('.nav').toggleClass('show');

    if($('.nav').hasClass('show')) {
      $('.nav').insertAfter('.navigation-wrapper');
    } else {
      $('.nav').appendTo('.navigation-wrapper');
    }

  });

})();

//
// Window Resize Event w/ Delay
//
$(window).resize(function () {
  waitForFinalEvent(function(){
    // console.log('Resize...');
    changeLayout();
  }, 100, "Change Layout");
});

// limit the amount of resize calls by calling the function after the resize event
var waitForFinalEvent = (function () {
  var timers = {};
  return function (callback, ms, uniqueId) {
    if (!uniqueId) {
      uniqueId = "Don't call this twice without a uniqueId";
    }
    if (timers[uniqueId]) {
      clearTimeout (timers[uniqueId]);
    }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();

// Move Sidebar depending on browser size
function changeLayout() {
  if ($(window).width() >= 860) {
    $('.nav.show').appendTo('.navigation-wrapper');
    $('.nav.show').toggleClass('show');
  }
}

function initTabs() {

	$('ul.tabs li').on( 'click', function(event) {

		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
    $("#"+tab_id).focus();

	});

}

//
// Validate
//

// Login Page Rules
function validateLoginForm(){
  var validate = $('#loginForm').validate({
    onsubmit: false,
    onkeyup: function(element){
      $(element).valid();
      checkForValid( '#loginForm' );
    },
    rules: {
      // Step 1
      loginName: {
        email: true,
        required: true
      },
      loginPW: {
        minlength: 6,
        required: true
      }
    }
  });
  return validate;
}

// Estimator Section Rules
function validateEstimatorForm(){
  var validate = $("#estimatorForm").validate({
    onsubmit: false,
    onkeyup: function(element) {
      $(element).valid();
      checkForValidEstimator();
    },
    onclick: function(element){
      var type = $(element).prop("type");
      if (type === "radio" || type === "checkbox") {
        $(element).valid();
      }
      if (type === "select-one") {
        checkForSelectChange(element);
        $(element).valid();
        checkForValidEstimator();

        if (element.id === 'step5CompareRate') {
          compareRateTo();
        }

      }
    },
    rules: {
      // Step 2
      step2FirstName: "required",
      step2LastName: "required",
      step2Email: {
        email: true,
        required: true
      },
      // Step 3
      step3RadioUserType: "required",
      step3State: "required",
      step3Utility: "required",
      step3RateClass: "required",
      // Step 4
      step4PlanStart: "required",
      step4PlanLength: "required",
      // Step 5
      step5_month1_usage: {
        required: true,
        number: true
      },
      step5Month: {
        number: true
      },
      // Step 6
      step6PlanStrategy: "required",
      step6RetailAdderRate: "required"
    },
    messages: {
      step3State: "Please select a valid state."
    }
  });
  return validate;
}

//
// Run Validation, these are called on the appropriate page
//

// Run Validation for Login Form
function pageLoginValidation() {
  "use strict";

  validateLoginForm();
  preventClick('.btn');
  checkForValid( '#loginForm' );

  // AutoFill / AutoComplete
  checkAutoFillEvent();

}

// Run Validation for Estimator Form
function pageEstimatorValidation() {
  "use strict";

  validateEstimatorForm();
  preventClick('.btn');
  checkForValidEstimator();

  // Next Event ( activeStep, targetStep )
  changeSectionNextEvent( 'step-two', 'step-three' );
  changeSectionNextEvent( 'step-three', 'step-four' );
  changeSectionNextEvent( 'step-four', 'step-five' );
  changeSectionNextEvent( 'step-five', 'step-six' );
  // Prev Event
  changeSectionBackEvent( 'step-three', 'step-two' );
  changeSectionBackEvent( 'step-four', 'step-three' );
  changeSectionBackEvent( 'step-five', 'step-four' );
  changeSectionBackEvent( 'step-six', 'step-five' );

  //
  // Special Cases
  //

  // AutoFill / AutoComplete
  checkAutoFillEvent();

  // Datepicker
  $( ".date-picker" ).datepicker();

  // Step 3 - Radio Input Event Listeners
  $('#radio02').click(function() {
     if($(this).is(':checked')) {
       toggleModal();
     }
  });

  // REPLACE -------- //
  // -----------------//
  // -----------------//
  // State Select
  $('#step3State').change(function(event){
    if ( this.value !== 'MA' && this.value !== 'CT' && this.value !== 'NH' ) {
      $('.modal-2').toggleClass("hide");
      this.selectedIndex = 0;
    }
  });

  // Close Modal 1 --- commercial / residence
  $('.modal-1 .close').click(function() {
     closeModal();
  });

  // CLose Modal 2 --- states
  $('.modal-2 .close').click(function() {
    $('.modal-2').toggleClass("hide");
  });

  // Step 3 - Modal 1 Toggle
  function toggleModal() {
    $('.modal-1').toggleClass("hide");
  }
  // -----------------//

  // Step 3 - Close Modal 1
  function closeModal() {
    toggleModal();
    $('#radio01').prop('checked', true);
    $('#radio01').valid();
  }
  // Step 5 - Repeat Field Values Event Listener
  $('#duplicateUsage').click(function(){
    duplicateFieldValues('.input-field.usage');
  });
  $('#duplicateRate').click(function(){
    duplicateFieldValues('.input-field.rate');
  });

  // Step 5 - Duplicate Field Values
  function duplicateFieldValues(fieldClass) {
    var inputValues = [];

    // Get Values
    $(fieldClass).each(function() {
      if ( $(this).val() !== '' ) {
        inputValues.push($(this).val());
      }
    });
    // if No Values
    if ( inputValues.length <= 0 ) {
      alert("There are no values entered");
    }
    // if Single Value
    if ( inputValues.length == 1 ) {
      $(fieldClass).each(function() {
        $(this).val(inputValues[0]);
        $(this).valid();
      });
    }
    // if Too Many Values
    if ( inputValues.length > 1 ) {
      if (confirm('Alert you have multiple values that will be replaced, Do you want to proceed?')) {
        $(fieldClass).each(function() {
          $(this).val(inputValues[0]);
          $(this).valid();
        });
      }
    }
  }
}

//
// Check if Form/Section is Valid
//

// If form is valid enable Button
function checkForValid( formID ) {
  var inputRequired = formID + ' .inputRequired';
  var inputValid = formID + ' .inputRequired.valid';
  var btnElem = $(formID).find('button.next');

  if ( $(inputRequired).length === $(inputValid).length) {
    enableButton(btnElem);
    return true;
  } else {
    disableButton(btnElem);
    return false;
  }
}

// Check Entire Estimator Form for changes
function checkForValidEstimator(){
  checkForValid( '#step-two' );
  checkForValid( '#step-three' );
  checkForValid( '#step-four' );
  checkForValid( '#step-five' );
  checkForValid( '#step-six' );
}

// Check For Select Change
function checkForSelectChange( element ){
  $(element).change(function() {
    $(this).valid();
    checkForValidEstimator();
    if (element.id === 'step5CompareRate') {
      compareRateTo();
    }
  });
}

// Check For AutoFill / AutoComplete
function checkAutoFillEvent() {
  $('.inputRequired').on('change blur', function() {
    $(this).valid();
    checkForValidEstimator();
  });
}

//
// Next / Prev Controls
//

// Next
function changeSectionNextEvent( activeStep, targetStep ) {
  var btnElem = $('#' + activeStep).find('.next');

  $(btnElem).click(function() {
    if (!$(btnElem).hasClass('disabled')) {
      // Hide Current
      $('#' + activeStep).addClass('hide');
      $('#' + targetStep).removeClass('hide');
      // Set Breadcrumb
      $('.' + activeStep).addClass('val'); // we know its valid
      $('.' + activeStep).removeClass('active');
      $('.' + targetStep).addClass('active');
    }
  });
}

// Prev
function changeSectionBackEvent( activeStep, targetStep ) {
  var btnElem = $('#' + activeStep).find('.prev');

  $(btnElem).click(function() {
    if (!$(btnElem).hasClass('disabled')) {
      // Hide Current
      $('#' + activeStep).addClass('hide');
      $('#' + targetStep).removeClass('hide');
      // Set Breadcrumb
      if ( checkForValid('#' + activeStep) ) {
        $('.' + activeStep).addClass('val');
      } else {
        $('.' + activeStep).removeClass('val');
      }
      $('.' + activeStep).removeClass('active');
      $('.' + targetStep).addClass('active');
    }
  });
}

// Prevent Clicks on Disabled Button Elements
function preventClick(element){
  $(element).click(function(event){
    if ($(this).hasClass('disabled')){
      event.preventDefault();
      var formID = '#' + $(this).closest('form').attr('id'); //.get(0) added
      // Will enable error messages immediately before the button element.
      $(this).prev().show();
      $(formID).valid();
    }
  });
}

// Disable Button
function disableButton(element) {
  $(element).addClass("disabled");
}

// Enable Button
function enableButton(element) {
  $(element).removeClass("disabled");
}

//
// Misc Validation Rules
//
var compareRateTo = function( element ) {
  if ( $('#step5CompareRate').val() == 'Service Competitor') {
    $('#step-five .input.rate').each(function(){
      $(this).removeClass('hide');
    });
    $('#step-five').removeClass('stretch');
  } else {
    $('#step-five .input.rate').each(function(){
      $(this).addClass('hide');
    });
    $('#step-five').addClass('stretch');
  }
};
