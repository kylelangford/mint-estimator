/*!
 * jQuery JavaScript Library v2.1.4
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright 2005, 2014 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2015-04-28T16:01Z
 */
(function(a, b) {
    if (typeof module === "object" && typeof module.exports === "object") {
        // For CommonJS and CommonJS-like environments where a proper `window`
        // is present, execute the factory and get jQuery.
        // For environments that do not have a `window` with a `document`
        // (such as Node.js), expose a factory as module.exports.
        // This accentuates the need for the creation of a real `window`.
        // e.g. var jQuery = require("jquery")(window);
        // See ticket #14549 for more info.
        module.exports = a.document ? b(a, true) : function(a) {
            if (!a.document) {
                throw new Error("jQuery requires a window with a document");
            }
            return b(a);
        };
    } else {
        b(a);
    }
})(typeof window !== "undefined" ? window : this, function(a, b) {
    // Support: Firefox 18+
    // Can't be in strict mode, several libs including ASP.NET trace
    // the stack via arguments.caller.callee and Firefox dies if
    // you try to trace through "use strict" call chains. (#13335)
    //
    var c = [];
    var d = c.slice;
    var e = c.concat;
    var f = c.push;
    var g = c.indexOf;
    var h = {};
    var i = h.toString;
    var j = h.hasOwnProperty;
    var k = {};
    var // Use the correct document accordingly with window argument (sandbox)
    l = a.document, m = "2.1.4", // Define a local copy of jQuery
    n = function(a, b) {
        // The jQuery object is actually just the init constructor 'enhanced'
        // Need init if jQuery is called (just allow error to be thrown if not included)
        return new n.fn.init(a, b);
    }, // Support: Android<4.1
    // Make sure we trim BOM and NBSP
    o = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, // Matches dashed string for camelizing
    p = /^-ms-/, q = /-([\da-z])/gi, // Used by jQuery.camelCase as callback to replace()
    r = function(a, b) {
        return b.toUpperCase();
    };
    n.fn = n.prototype = {
        // The current version of jQuery being used
        jquery: m,
        constructor: n,
        // Start with an empty selector
        selector: "",
        // The default length of a jQuery object is 0
        length: 0,
        toArray: function() {
            return d.call(this);
        },
        // Get the Nth element in the matched element set OR
        // Get the whole matched element set as a clean array
        get: function(a) {
            // Return just the one element from the set
            // Return all the elements in a clean array
            return a != null ? a < 0 ? this[a + this.length] : this[a] : d.call(this);
        },
        // Take an array of elements and push it onto the stack
        // (returning the new matched element set)
        pushStack: function(a) {
            // Build a new jQuery matched element set
            var b = n.merge(this.constructor(), a);
            // Add the old object onto the stack (as a reference)
            b.prevObject = this;
            b.context = this.context;
            // Return the newly-formed element set
            return b;
        },
        // Execute a callback for every element in the matched set.
        // (You can seed the arguments with an array of args, but this is
        // only used internally.)
        each: function(a, b) {
            return n.each(this, a, b);
        },
        map: function(a) {
            return this.pushStack(n.map(this, function(b, c) {
                return a.call(b, c, b);
            }));
        },
        slice: function() {
            return this.pushStack(d.apply(this, arguments));
        },
        first: function() {
            return this.eq(0);
        },
        last: function() {
            return this.eq(-1);
        },
        eq: function(a) {
            var b = this.length, c = +a + (a < 0 ? b : 0);
            return this.pushStack(c >= 0 && c < b ? [ this[c] ] : []);
        },
        end: function() {
            return this.prevObject || this.constructor(null);
        },
        // For internal use only.
        // Behaves like an Array's method, not like a jQuery method.
        push: f,
        sort: c.sort,
        splice: c.splice
    };
    n.extend = n.fn.extend = function() {
        var a, b, c, d, e, f, g = arguments[0] || {}, h = 1, i = arguments.length, j = false;
        // Handle a deep copy situation
        if (typeof g === "boolean") {
            j = g;
            // Skip the boolean and the target
            g = arguments[h] || {};
            h++;
        }
        // Handle case when target is a string or something (possible in deep copy)
        if (typeof g !== "object" && !n.isFunction(g)) {
            g = {};
        }
        // Extend jQuery itself if only one argument is passed
        if (h === i) {
            g = this;
            h--;
        }
        for (;h < i; h++) {
            // Only deal with non-null/undefined values
            if ((a = arguments[h]) != null) {
                // Extend the base object
                for (b in a) {
                    c = g[b];
                    d = a[b];
                    // Prevent never-ending loop
                    if (g === d) {
                        continue;
                    }
                    // Recurse if we're merging plain objects or arrays
                    if (j && d && (n.isPlainObject(d) || (e = n.isArray(d)))) {
                        if (e) {
                            e = false;
                            f = c && n.isArray(c) ? c : [];
                        } else {
                            f = c && n.isPlainObject(c) ? c : {};
                        }
                        // Never move original objects, clone them
                        g[b] = n.extend(j, f, d);
                    } else if (d !== undefined) {
                        g[b] = d;
                    }
                }
            }
        }
        // Return the modified object
        return g;
    };
    n.extend({
        // Unique for each copy of jQuery on the page
        expando: "jQuery" + (m + Math.random()).replace(/\D/g, ""),
        // Assume jQuery is ready without the ready module
        isReady: true,
        error: function(a) {
            throw new Error(a);
        },
        noop: function() {},
        isFunction: function(a) {
            return n.type(a) === "function";
        },
        isArray: Array.isArray,
        isWindow: function(a) {
            return a != null && a === a.window;
        },
        isNumeric: function(a) {
            // parseFloat NaNs numeric-cast false positives (null|true|false|"")
            // ...but misinterprets leading-number strings, particularly hex literals ("0x...")
            // subtraction forces infinities to NaN
            // adding 1 corrects loss of precision from parseFloat (#15100)
            return !n.isArray(a) && a - parseFloat(a) + 1 >= 0;
        },
        isPlainObject: function(a) {
            // Not plain objects:
            // - Any object or value whose internal [[Class]] property is not "[object Object]"
            // - DOM nodes
            // - window
            if (n.type(a) !== "object" || a.nodeType || n.isWindow(a)) {
                return false;
            }
            if (a.constructor && !j.call(a.constructor.prototype, "isPrototypeOf")) {
                return false;
            }
            // If the function hasn't returned already, we're confident that
            // |obj| is a plain object, created by {} or constructed with new Object
            return true;
        },
        isEmptyObject: function(a) {
            var b;
            for (b in a) {
                return false;
            }
            return true;
        },
        type: function(a) {
            if (a == null) {
                return a + "";
            }
            // Support: Android<4.0, iOS<6 (functionish RegExp)
            return typeof a === "object" || typeof a === "function" ? h[i.call(a)] || "object" : typeof a;
        },
        // Evaluates a script in a global context
        globalEval: function(a) {
            var b, c = eval;
            a = n.trim(a);
            if (a) {
                // If the code includes a valid, prologue position
                // strict mode pragma, execute code by injecting a
                // script tag into the document.
                if (a.indexOf("use strict") === 1) {
                    b = l.createElement("script");
                    b.text = a;
                    l.head.appendChild(b).parentNode.removeChild(b);
                } else {
                    // Otherwise, avoid the DOM node creation, insertion
                    // and removal by using an indirect global eval
                    c(a);
                }
            }
        },
        // Convert dashed to camelCase; used by the css and data modules
        // Support: IE9-11+
        // Microsoft forgot to hump their vendor prefix (#9572)
        camelCase: function(a) {
            return a.replace(p, "ms-").replace(q, r);
        },
        nodeName: function(a, b) {
            return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase();
        },
        // args is for internal usage only
        each: function(a, b, c) {
            var d, e = 0, f = a.length, g = s(a);
            if (c) {
                if (g) {
                    for (;e < f; e++) {
                        d = b.apply(a[e], c);
                        if (d === false) {
                            break;
                        }
                    }
                } else {
                    for (e in a) {
                        d = b.apply(a[e], c);
                        if (d === false) {
                            break;
                        }
                    }
                }
            } else {
                if (g) {
                    for (;e < f; e++) {
                        d = b.call(a[e], e, a[e]);
                        if (d === false) {
                            break;
                        }
                    }
                } else {
                    for (e in a) {
                        d = b.call(a[e], e, a[e]);
                        if (d === false) {
                            break;
                        }
                    }
                }
            }
            return a;
        },
        // Support: Android<4.1
        trim: function(a) {
            return a == null ? "" : (a + "").replace(o, "");
        },
        // results is for internal usage only
        makeArray: function(a, b) {
            var c = b || [];
            if (a != null) {
                if (s(Object(a))) {
                    n.merge(c, typeof a === "string" ? [ a ] : a);
                } else {
                    f.call(c, a);
                }
            }
            return c;
        },
        inArray: function(a, b, c) {
            return b == null ? -1 : g.call(b, a, c);
        },
        merge: function(a, b) {
            var c = +b.length, d = 0, e = a.length;
            for (;d < c; d++) {
                a[e++] = b[d];
            }
            a.length = e;
            return a;
        },
        grep: function(a, b, c) {
            var d, e = [], f = 0, g = a.length, h = !c;
            // Go through the array, only saving the items
            // that pass the validator function
            for (;f < g; f++) {
                d = !b(a[f], f);
                if (d !== h) {
                    e.push(a[f]);
                }
            }
            return e;
        },
        // arg is for internal usage only
        map: function(a, b, c) {
            var d, f = 0, g = a.length, h = s(a), i = [];
            // Go through the array, translating each of the items to their new values
            if (h) {
                for (;f < g; f++) {
                    d = b(a[f], f, c);
                    if (d != null) {
                        i.push(d);
                    }
                }
            } else {
                for (f in a) {
                    d = b(a[f], f, c);
                    if (d != null) {
                        i.push(d);
                    }
                }
            }
            // Flatten any nested arrays
            return e.apply([], i);
        },
        // A global GUID counter for objects
        guid: 1,
        // Bind a function to a context, optionally partially applying any
        // arguments.
        proxy: function(a, b) {
            var c, e, f;
            if (typeof b === "string") {
                c = a[b];
                b = a;
                a = c;
            }
            // Quick check to determine if target is callable, in the spec
            // this throws a TypeError, but we will just return undefined.
            if (!n.isFunction(a)) {
                return undefined;
            }
            // Simulated bind
            e = d.call(arguments, 2);
            f = function() {
                return a.apply(b || this, e.concat(d.call(arguments)));
            };
            // Set the guid of unique handler to the same of original handler, so it can be removed
            f.guid = a.guid = a.guid || n.guid++;
            return f;
        },
        now: Date.now,
        // jQuery.support is not used in Core but other projects attach their
        // properties to it so it needs to exist.
        support: k
    });
    // Populate the class2type map
    n.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(a, b) {
        h["[object " + b + "]"] = b.toLowerCase();
    });
    function s(a) {
        // Support: iOS 8.2 (not reproducible in simulator)
        // `in` check used to prevent JIT error (gh-2145)
        // hasOwn isn't used here due to false negatives
        // regarding Nodelist length in IE
        var b = "length" in a && a.length, c = n.type(a);
        if (c === "function" || n.isWindow(a)) {
            return false;
        }
        if (a.nodeType === 1 && b) {
            return true;
        }
        return c === "array" || b === 0 || typeof b === "number" && b > 0 && b - 1 in a;
    }
    var t = /*!
 * Sizzle CSS Selector Engine v2.2.0-pre
 * http://sizzlejs.com/
 *
 * Copyright 2008, 2014 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2014-12-16
 */
    function(a) {
        var b, c, d, e, f, g, h, i, j, k, l, // Local document vars
        m, n, o, p, q, r, s, t, // Instance-specific data
        u = "sizzle" + 1 * new Date(), v = a.document, w = 0, x = 0, y = ha(), z = ha(), A = ha(), B = function(a, b) {
            if (a === b) {
                l = true;
            }
            return 0;
        }, // General-purpose constants
        C = 1 << 31, // Instance methods
        D = {}.hasOwnProperty, E = [], F = E.pop, G = E.push, H = E.push, I = E.slice, // Use a stripped-down indexOf as it's faster than native
        // http://jsperf.com/thor-indexof-vs-for/5
        J = function(a, b) {
            var c = 0, d = a.length;
            for (;c < d; c++) {
                if (a[c] === b) {
                    return c;
                }
            }
            return -1;
        }, K = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", // Regular expressions
        // Whitespace characters http://www.w3.org/TR/css3-selectors/#whitespace
        L = "[\\x20\\t\\r\\n\\f]", // http://www.w3.org/TR/css3-syntax/#characters
        M = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", // Loosely modeled on CSS identifier characters
        // An unquoted value should be a CSS identifier http://www.w3.org/TR/css3-selectors/#attribute-selectors
        // Proper syntax: http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
        N = M.replace("w", "w#"), // Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
        O = "\\[" + L + "*(" + M + ")(?:" + L + // Operator (capture 2)
        "*([*^$|!~]?=)" + L + // "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
        "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + N + "))|)" + L + "*\\]", P = ":(" + M + ")(?:\\((" + // To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
        // 1. quoted (capture 3; capture 4 or capture 5)
        "('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" + // 2. simple (capture 6)
        "((?:\\\\.|[^\\\\()[\\]]|" + O + ")*)|" + // 3. anything else (capture 2)
        ".*" + ")\\)|)", // Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
        Q = new RegExp(L + "+", "g"), R = new RegExp("^" + L + "+|((?:^|[^\\\\])(?:\\\\.)*)" + L + "+$", "g"), S = new RegExp("^" + L + "*," + L + "*"), T = new RegExp("^" + L + "*([>+~]|" + L + ")" + L + "*"), U = new RegExp("=" + L + "*([^\\]'\"]*?)" + L + "*\\]", "g"), V = new RegExp(P), W = new RegExp("^" + N + "$"), X = {
            ID: new RegExp("^#(" + M + ")"),
            CLASS: new RegExp("^\\.(" + M + ")"),
            TAG: new RegExp("^(" + M.replace("w", "w*") + ")"),
            ATTR: new RegExp("^" + O),
            PSEUDO: new RegExp("^" + P),
            CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + L + "*(even|odd|(([+-]|)(\\d*)n|)" + L + "*(?:([+-]|)" + L + "*(\\d+)|))" + L + "*\\)|)", "i"),
            bool: new RegExp("^(?:" + K + ")$", "i"),
            // For use in libraries implementing .is()
            // We use this for POS matching in `select`
            needsContext: new RegExp("^" + L + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + L + "*((?:-\\d)?\\d*)" + L + "*\\)|)(?=[^-]|$)", "i")
        }, Y = /^(?:input|select|textarea|button)$/i, Z = /^h\d$/i, $ = /^[^{]+\{\s*\[native \w/, // Easily-parseable/retrievable ID or TAG or CLASS selectors
        _ = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, aa = /[+~]/, ba = /'|\\/g, // CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
        ca = new RegExp("\\\\([\\da-f]{1,6}" + L + "?|(" + L + ")|.)", "ig"), da = function(a, b, c) {
            var d = "0x" + b - 65536;
            // NaN means non-codepoint
            // Support: Firefox<24
            // Workaround erroneous numeric interpretation of +"0x"
            // BMP codepoint
            // Supplemental Plane codepoint (surrogate pair)
            return d !== d || c ? b : d < 0 ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, d & 1023 | 56320);
        }, // Used for iframes
        // See setDocument()
        // Removing the function wrapper causes a "Permission Denied"
        // error in IE
        ea = function() {
            m();
        };
        // Optimize for push.apply( _, NodeList )
        try {
            H.apply(E = I.call(v.childNodes), v.childNodes);
            // Support: Android<4.0
            // Detect silently failing push.apply
            E[v.childNodes.length].nodeType;
        } catch (fa) {
            H = {
                apply: E.length ? // Leverage slice if possible
                function(a, b) {
                    G.apply(a, I.call(b));
                } : // Support: IE<9
                // Otherwise append directly
                function(a, b) {
                    var c = a.length, d = 0;
                    // Can't trust NodeList.length
                    while (a[c++] = b[d++]) {}
                    a.length = c - 1;
                }
            };
        }
        function ga(a, b, d, e) {
            var f, h, j, k, // QSA vars
            l, o, r, s, w, x;
            if ((b ? b.ownerDocument || b : v) !== n) {
                m(b);
            }
            b = b || n;
            d = d || [];
            k = b.nodeType;
            if (typeof a !== "string" || !a || k !== 1 && k !== 9 && k !== 11) {
                return d;
            }
            if (!e && p) {
                // Try to shortcut find operations when possible (e.g., not under DocumentFragment)
                if (k !== 11 && (f = _.exec(a))) {
                    // Speed-up: Sizzle("#ID")
                    if (j = f[1]) {
                        if (k === 9) {
                            h = b.getElementById(j);
                            // Check parentNode to catch when Blackberry 4.6 returns
                            // nodes that are no longer in the document (jQuery #6963)
                            if (h && h.parentNode) {
                                // Handle the case where IE, Opera, and Webkit return items
                                // by name instead of ID
                                if (h.id === j) {
                                    d.push(h);
                                    return d;
                                }
                            } else {
                                return d;
                            }
                        } else {
                            // Context is not a document
                            if (b.ownerDocument && (h = b.ownerDocument.getElementById(j)) && t(b, h) && h.id === j) {
                                d.push(h);
                                return d;
                            }
                        }
                    } else if (f[2]) {
                        H.apply(d, b.getElementsByTagName(a));
                        return d;
                    } else if ((j = f[3]) && c.getElementsByClassName) {
                        H.apply(d, b.getElementsByClassName(j));
                        return d;
                    }
                }
                // QSA path
                if (c.qsa && (!q || !q.test(a))) {
                    s = r = u;
                    w = b;
                    x = k !== 1 && a;
                    // qSA works strangely on Element-rooted queries
                    // We can work around this by specifying an extra ID on the root
                    // and working up from there (Thanks to Andrew Dupont for the technique)
                    // IE 8 doesn't work on object elements
                    if (k === 1 && b.nodeName.toLowerCase() !== "object") {
                        o = g(a);
                        if (r = b.getAttribute("id")) {
                            s = r.replace(ba, "\\$&");
                        } else {
                            b.setAttribute("id", s);
                        }
                        s = "[id='" + s + "'] ";
                        l = o.length;
                        while (l--) {
                            o[l] = s + ra(o[l]);
                        }
                        w = aa.test(a) && pa(b.parentNode) || b;
                        x = o.join(",");
                    }
                    if (x) {
                        try {
                            H.apply(d, w.querySelectorAll(x));
                            return d;
                        } catch (y) {} finally {
                            if (!r) {
                                b.removeAttribute("id");
                            }
                        }
                    }
                }
            }
            // All others
            return i(a.replace(R, "$1"), b, d, e);
        }
        /**
 * Create key-value caches of limited size
 * @returns {Function(string, Object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
        function ha() {
            var a = [];
            function b(c, e) {
                // Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
                if (a.push(c + " ") > d.cacheLength) {
                    // Only keep the most recent entries
                    delete b[a.shift()];
                }
                return b[c + " "] = e;
            }
            return b;
        }
        /**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
        function ia(a) {
            a[u] = true;
            return a;
        }
        /**
 * Support testing using an element
 * @param {Function} fn Passed the created div and expects a boolean result
 */
        function ja(a) {
            var b = n.createElement("div");
            try {
                return !!a(b);
            } catch (c) {
                return false;
            } finally {
                // Remove from its parent by default
                if (b.parentNode) {
                    b.parentNode.removeChild(b);
                }
                // release memory in IE
                b = null;
            }
        }
        /**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
        function ka(a, b) {
            var c = a.split("|"), e = a.length;
            while (e--) {
                d.attrHandle[c[e]] = b;
            }
        }
        /**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
        function la(a, b) {
            var c = b && a, d = c && a.nodeType === 1 && b.nodeType === 1 && (~b.sourceIndex || C) - (~a.sourceIndex || C);
            // Use IE sourceIndex if available on both nodes
            if (d) {
                return d;
            }
            // Check if b follows a
            if (c) {
                while (c = c.nextSibling) {
                    if (c === b) {
                        return -1;
                    }
                }
            }
            return a ? 1 : -1;
        }
        /**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
        function ma(a) {
            return function(b) {
                var c = b.nodeName.toLowerCase();
                return c === "input" && b.type === a;
            };
        }
        /**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
        function na(a) {
            return function(b) {
                var c = b.nodeName.toLowerCase();
                return (c === "input" || c === "button") && b.type === a;
            };
        }
        /**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
        function oa(a) {
            return ia(function(b) {
                b = +b;
                return ia(function(c, d) {
                    var e, f = a([], c.length, b), g = f.length;
                    // Match elements found at the specified indexes
                    while (g--) {
                        if (c[e = f[g]]) {
                            c[e] = !(d[e] = c[e]);
                        }
                    }
                });
            });
        }
        /**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
        function pa(a) {
            return a && typeof a.getElementsByTagName !== "undefined" && a;
        }
        // Expose support vars for convenience
        c = ga.support = {};
        /**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
        f = ga.isXML = function(a) {
            // documentElement is verified for cases where it doesn't yet exist
            // (such as loading iframes in IE - #4833)
            var b = a && (a.ownerDocument || a).documentElement;
            return b ? b.nodeName !== "HTML" : false;
        };
        /**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
        m = ga.setDocument = function(a) {
            var b, e, g = a ? a.ownerDocument || a : v;
            // If no document and documentElement is available, return
            if (g === n || g.nodeType !== 9 || !g.documentElement) {
                return n;
            }
            // Set our document
            n = g;
            o = g.documentElement;
            e = g.defaultView;
            // Support: IE>8
            // If iframe document is assigned to "document" variable and if iframe has been reloaded,
            // IE will throw "permission denied" error when accessing "document" variable, see jQuery #13936
            // IE6-8 do not support the defaultView property so parent will be undefined
            if (e && e !== e.top) {
                // IE11 does not have attachEvent, so all must suffer
                if (e.addEventListener) {
                    e.addEventListener("unload", ea, false);
                } else if (e.attachEvent) {
                    e.attachEvent("onunload", ea);
                }
            }
            /* Support tests
	---------------------------------------------------------------------- */
            p = !f(g);
            /* Attributes
	---------------------------------------------------------------------- */
            // Support: IE<8
            // Verify that getAttribute really returns attributes and not properties
            // (excepting IE8 booleans)
            c.attributes = ja(function(a) {
                a.className = "i";
                return !a.getAttribute("className");
            });
            /* getElement(s)By*
	---------------------------------------------------------------------- */
            // Check if getElementsByTagName("*") returns only elements
            c.getElementsByTagName = ja(function(a) {
                a.appendChild(g.createComment(""));
                return !a.getElementsByTagName("*").length;
            });
            // Support: IE<9
            c.getElementsByClassName = $.test(g.getElementsByClassName);
            // Support: IE<10
            // Check if getElementById returns elements by name
            // The broken getElementById methods don't pick up programatically-set names,
            // so use a roundabout getElementsByName test
            c.getById = ja(function(a) {
                o.appendChild(a).id = u;
                return !g.getElementsByName || !g.getElementsByName(u).length;
            });
            // ID find and filter
            if (c.getById) {
                d.find["ID"] = function(a, b) {
                    if (typeof b.getElementById !== "undefined" && p) {
                        var c = b.getElementById(a);
                        // Check parentNode to catch when Blackberry 4.6 returns
                        // nodes that are no longer in the document #6963
                        return c && c.parentNode ? [ c ] : [];
                    }
                };
                d.filter["ID"] = function(a) {
                    var b = a.replace(ca, da);
                    return function(a) {
                        return a.getAttribute("id") === b;
                    };
                };
            } else {
                // Support: IE6/7
                // getElementById is not reliable as a find shortcut
                delete d.find["ID"];
                d.filter["ID"] = function(a) {
                    var b = a.replace(ca, da);
                    return function(a) {
                        var c = typeof a.getAttributeNode !== "undefined" && a.getAttributeNode("id");
                        return c && c.value === b;
                    };
                };
            }
            // Tag
            d.find["TAG"] = c.getElementsByTagName ? function(a, b) {
                if (typeof b.getElementsByTagName !== "undefined") {
                    return b.getElementsByTagName(a);
                } else if (c.qsa) {
                    return b.querySelectorAll(a);
                }
            } : function(a, b) {
                var c, d = [], e = 0, // By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
                f = b.getElementsByTagName(a);
                // Filter out possible comments
                if (a === "*") {
                    while (c = f[e++]) {
                        if (c.nodeType === 1) {
                            d.push(c);
                        }
                    }
                    return d;
                }
                return f;
            };
            // Class
            d.find["CLASS"] = c.getElementsByClassName && function(a, b) {
                if (p) {
                    return b.getElementsByClassName(a);
                }
            };
            /* QSA/matchesSelector
	---------------------------------------------------------------------- */
            // QSA and matchesSelector support
            // matchesSelector(:active) reports false when true (IE9/Opera 11.5)
            r = [];
            // qSa(:focus) reports false when true (Chrome 21)
            // We allow this because of a bug in IE8/9 that throws an error
            // whenever `document.activeElement` is accessed on an iframe
            // So, we allow :focus to pass through QSA all the time to avoid the IE error
            // See http://bugs.jquery.com/ticket/13378
            q = [];
            if (c.qsa = $.test(g.querySelectorAll)) {
                // Build QSA regex
                // Regex strategy adopted from Diego Perini
                ja(function(a) {
                    // Select is set to empty string on purpose
                    // This is to test IE's treatment of not explicitly
                    // setting a boolean content attribute,
                    // since its presence should be enough
                    // http://bugs.jquery.com/ticket/12359
                    o.appendChild(a).innerHTML = "<a id='" + u + "'></a>" + "<select id='" + u + "-\f]' msallowcapture=''>" + "<option selected=''></option></select>";
                    // Support: IE8, Opera 11-12.16
                    // Nothing should be selected when empty strings follow ^= or $= or *=
                    // The test attribute must be unknown in Opera but "safe" for WinRT
                    // http://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
                    if (a.querySelectorAll("[msallowcapture^='']").length) {
                        q.push("[*^$]=" + L + "*(?:''|\"\")");
                    }
                    // Support: IE8
                    // Boolean attributes and "value" are not treated correctly
                    if (!a.querySelectorAll("[selected]").length) {
                        q.push("\\[" + L + "*(?:value|" + K + ")");
                    }
                    // Support: Chrome<29, Android<4.2+, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.7+
                    if (!a.querySelectorAll("[id~=" + u + "-]").length) {
                        q.push("~=");
                    }
                    // Webkit/Opera - :checked should return selected option elements
                    // http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
                    // IE8 throws error here and will not see later tests
                    if (!a.querySelectorAll(":checked").length) {
                        q.push(":checked");
                    }
                    // Support: Safari 8+, iOS 8+
                    // https://bugs.webkit.org/show_bug.cgi?id=136851
                    // In-page `selector#id sibing-combinator selector` fails
                    if (!a.querySelectorAll("a#" + u + "+*").length) {
                        q.push(".#.+[+~]");
                    }
                });
                ja(function(a) {
                    // Support: Windows 8 Native Apps
                    // The type and name attributes are restricted during .innerHTML assignment
                    var b = g.createElement("input");
                    b.setAttribute("type", "hidden");
                    a.appendChild(b).setAttribute("name", "D");
                    // Support: IE8
                    // Enforce case-sensitivity of name attribute
                    if (a.querySelectorAll("[name=d]").length) {
                        q.push("name" + L + "*[*^$|!~]?=");
                    }
                    // FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
                    // IE8 throws error here and will not see later tests
                    if (!a.querySelectorAll(":enabled").length) {
                        q.push(":enabled", ":disabled");
                    }
                    // Opera 10-11 does not throw on post-comma invalid pseudos
                    a.querySelectorAll("*,:x");
                    q.push(",.*:");
                });
            }
            if (c.matchesSelector = $.test(s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector)) {
                ja(function(a) {
                    // Check to see if it's possible to do matchesSelector
                    // on a disconnected node (IE 9)
                    c.disconnectedMatch = s.call(a, "div");
                    // This should fail with an exception
                    // Gecko does not error, returns false instead
                    s.call(a, "[s!='']:x");
                    r.push("!=", P);
                });
            }
            q = q.length && new RegExp(q.join("|"));
            r = r.length && new RegExp(r.join("|"));
            /* Contains
	---------------------------------------------------------------------- */
            b = $.test(o.compareDocumentPosition);
            // Element contains another
            // Purposefully does not implement inclusive descendent
            // As in, an element does not contain itself
            t = b || $.test(o.contains) ? function(a, b) {
                var c = a.nodeType === 9 ? a.documentElement : a, d = b && b.parentNode;
                return a === d || !!(d && d.nodeType === 1 && (c.contains ? c.contains(d) : a.compareDocumentPosition && a.compareDocumentPosition(d) & 16));
            } : function(a, b) {
                if (b) {
                    while (b = b.parentNode) {
                        if (b === a) {
                            return true;
                        }
                    }
                }
                return false;
            };
            /* Sorting
	---------------------------------------------------------------------- */
            // Document order sorting
            B = b ? function(a, b) {
                // Flag for duplicate removal
                if (a === b) {
                    l = true;
                    return 0;
                }
                // Sort on method existence if only one input has compareDocumentPosition
                var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
                if (d) {
                    return d;
                }
                // Calculate position if both inputs belong to the same document
                d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : // Otherwise we know they are disconnected
                1;
                // Disconnected nodes
                if (d & 1 || !c.sortDetached && b.compareDocumentPosition(a) === d) {
                    // Choose the first element that is related to our preferred document
                    if (a === g || a.ownerDocument === v && t(v, a)) {
                        return -1;
                    }
                    if (b === g || b.ownerDocument === v && t(v, b)) {
                        return 1;
                    }
                    // Maintain original order
                    return k ? J(k, a) - J(k, b) : 0;
                }
                return d & 4 ? -1 : 1;
            } : function(a, b) {
                // Exit early if the nodes are identical
                if (a === b) {
                    l = true;
                    return 0;
                }
                var c, d = 0, e = a.parentNode, f = b.parentNode, h = [ a ], i = [ b ];
                // Parentless nodes are either documents or disconnected
                if (!e || !f) {
                    return a === g ? -1 : b === g ? 1 : e ? -1 : f ? 1 : k ? J(k, a) - J(k, b) : 0;
                } else if (e === f) {
                    return la(a, b);
                }
                // Otherwise we need full lists of their ancestors for comparison
                c = a;
                while (c = c.parentNode) {
                    h.unshift(c);
                }
                c = b;
                while (c = c.parentNode) {
                    i.unshift(c);
                }
                // Walk down the tree looking for a discrepancy
                while (h[d] === i[d]) {
                    d++;
                }
                // Do a sibling check if the nodes have a common ancestor
                // Otherwise nodes in our document sort first
                return d ? la(h[d], i[d]) : h[d] === v ? -1 : i[d] === v ? 1 : 0;
            };
            return g;
        };
        ga.matches = function(a, b) {
            return ga(a, null, null, b);
        };
        ga.matchesSelector = function(a, b) {
            // Set document vars if needed
            if ((a.ownerDocument || a) !== n) {
                m(a);
            }
            // Make sure that attribute selectors are quoted
            b = b.replace(U, "='$1']");
            if (c.matchesSelector && p && (!r || !r.test(b)) && (!q || !q.test(b))) {
                try {
                    var d = s.call(a, b);
                    // IE 9's matchesSelector returns false on disconnected nodes
                    if (d || c.disconnectedMatch || // As well, disconnected nodes are said to be in a document
                    // fragment in IE 9
                    a.document && a.document.nodeType !== 11) {
                        return d;
                    }
                } catch (e) {}
            }
            return ga(b, n, null, [ a ]).length > 0;
        };
        ga.contains = function(a, b) {
            // Set document vars if needed
            if ((a.ownerDocument || a) !== n) {
                m(a);
            }
            return t(a, b);
        };
        ga.attr = function(a, b) {
            // Set document vars if needed
            if ((a.ownerDocument || a) !== n) {
                m(a);
            }
            var e = d.attrHandle[b.toLowerCase()], // Don't get fooled by Object.prototype properties (jQuery #13807)
            f = e && D.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : undefined;
            return f !== undefined ? f : c.attributes || !p ? a.getAttribute(b) : (f = a.getAttributeNode(b)) && f.specified ? f.value : null;
        };
        ga.error = function(a) {
            throw new Error("Syntax error, unrecognized expression: " + a);
        };
        /**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
        ga.uniqueSort = function(a) {
            var b, d = [], e = 0, f = 0;
            // Unless we *know* we can detect duplicates, assume their presence
            l = !c.detectDuplicates;
            k = !c.sortStable && a.slice(0);
            a.sort(B);
            if (l) {
                while (b = a[f++]) {
                    if (b === a[f]) {
                        e = d.push(f);
                    }
                }
                while (e--) {
                    a.splice(d[e], 1);
                }
            }
            // Clear input after sorting to release objects
            // See https://github.com/jquery/sizzle/pull/225
            k = null;
            return a;
        };
        /**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
        e = ga.getText = function(a) {
            var b, c = "", d = 0, f = a.nodeType;
            if (!f) {
                // If no nodeType, this is expected to be an array
                while (b = a[d++]) {
                    // Do not traverse comment nodes
                    c += e(b);
                }
            } else if (f === 1 || f === 9 || f === 11) {
                // Use textContent for elements
                // innerText usage removed for consistency of new lines (jQuery #11153)
                if (typeof a.textContent === "string") {
                    return a.textContent;
                } else {
                    // Traverse its children
                    for (a = a.firstChild; a; a = a.nextSibling) {
                        c += e(a);
                    }
                }
            } else if (f === 3 || f === 4) {
                return a.nodeValue;
            }
            // Do not include comment or processing instruction nodes
            return c;
        };
        d = ga.selectors = {
            // Can be adjusted by the user
            cacheLength: 50,
            createPseudo: ia,
            match: X,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: true
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: true
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(a) {
                    a[1] = a[1].replace(ca, da);
                    // Move the given value to match[3] whether quoted or unquoted
                    a[3] = (a[3] || a[4] || a[5] || "").replace(ca, da);
                    if (a[2] === "~=") {
                        a[3] = " " + a[3] + " ";
                    }
                    return a.slice(0, 4);
                },
                CHILD: function(a) {
                    /* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
                    a[1] = a[1].toLowerCase();
                    if (a[1].slice(0, 3) === "nth") {
                        // nth-* requires argument
                        if (!a[3]) {
                            ga.error(a[0]);
                        }
                        // numeric x and y parameters for Expr.filter.CHILD
                        // remember that false/true cast respectively to 0/1
                        a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * (a[3] === "even" || a[3] === "odd"));
                        a[5] = +(a[7] + a[8] || a[3] === "odd");
                    } else if (a[3]) {
                        ga.error(a[0]);
                    }
                    return a;
                },
                PSEUDO: function(a) {
                    var b, c = !a[6] && a[2];
                    if (X["CHILD"].test(a[0])) {
                        return null;
                    }
                    // Accept quoted arguments as-is
                    if (a[3]) {
                        a[2] = a[4] || a[5] || "";
                    } else if (c && V.test(c) && (// Get excess from tokenize (recursively)
                    b = g(c, true)) && (// advance to the next closing parenthesis
                    b = c.indexOf(")", c.length - b) - c.length)) {
                        // excess is a negative index
                        a[0] = a[0].slice(0, b);
                        a[2] = c.slice(0, b);
                    }
                    // Return only captures needed by the pseudo filter method (type and argument)
                    return a.slice(0, 3);
                }
            },
            filter: {
                TAG: function(a) {
                    var b = a.replace(ca, da).toLowerCase();
                    return a === "*" ? function() {
                        return true;
                    } : function(a) {
                        return a.nodeName && a.nodeName.toLowerCase() === b;
                    };
                },
                CLASS: function(a) {
                    var b = y[a + " "];
                    return b || (b = new RegExp("(^|" + L + ")" + a + "(" + L + "|$)")) && y(a, function(a) {
                        return b.test(typeof a.className === "string" && a.className || typeof a.getAttribute !== "undefined" && a.getAttribute("class") || "");
                    });
                },
                ATTR: function(a, b, c) {
                    return function(d) {
                        var e = ga.attr(d, a);
                        if (e == null) {
                            return b === "!=";
                        }
                        if (!b) {
                            return true;
                        }
                        e += "";
                        return b === "=" ? e === c : b === "!=" ? e !== c : b === "^=" ? c && e.indexOf(c) === 0 : b === "*=" ? c && e.indexOf(c) > -1 : b === "$=" ? c && e.slice(-c.length) === c : b === "~=" ? (" " + e.replace(Q, " ") + " ").indexOf(c) > -1 : b === "|=" ? e === c || e.slice(0, c.length + 1) === c + "-" : false;
                    };
                },
                CHILD: function(a, b, c, d, e) {
                    var f = a.slice(0, 3) !== "nth", g = a.slice(-4) !== "last", h = b === "of-type";
                    // Shortcut for :nth-*(n)
                    return d === 1 && e === 0 ? function(a) {
                        return !!a.parentNode;
                    } : function(b, c, i) {
                        var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling", q = b.parentNode, r = h && b.nodeName.toLowerCase(), s = !i && !h;
                        if (q) {
                            // :(first|last|only)-(child|of-type)
                            if (f) {
                                while (p) {
                                    l = b;
                                    while (l = l[p]) {
                                        if (h ? l.nodeName.toLowerCase() === r : l.nodeType === 1) {
                                            return false;
                                        }
                                    }
                                    // Reverse direction for :only-* (if we haven't yet done so)
                                    o = p = a === "only" && !o && "nextSibling";
                                }
                                return true;
                            }
                            o = [ g ? q.firstChild : q.lastChild ];
                            // non-xml :nth-child(...) stores cache data on `parent`
                            if (g && s) {
                                // Seek `elem` from a previously-cached index
                                k = q[u] || (q[u] = {});
                                j = k[a] || [];
                                n = j[0] === w && j[1];
                                m = j[0] === w && j[2];
                                l = n && q.childNodes[n];
                                while (l = ++n && l && l[p] || (// Fallback to seeking `elem` from the start
                                m = n = 0) || o.pop()) {
                                    // When found, cache indexes on `parent` and break
                                    if (l.nodeType === 1 && ++m && l === b) {
                                        k[a] = [ w, n, m ];
                                        break;
                                    }
                                }
                            } else if (s && (j = (b[u] || (b[u] = {}))[a]) && j[0] === w) {
                                m = j[1];
                            } else {
                                // Use the same loop as above to seek `elem` from the start
                                while (l = ++n && l && l[p] || (m = n = 0) || o.pop()) {
                                    if ((h ? l.nodeName.toLowerCase() === r : l.nodeType === 1) && ++m) {
                                        // Cache the index of each encountered element
                                        if (s) {
                                            (l[u] || (l[u] = {}))[a] = [ w, m ];
                                        }
                                        if (l === b) {
                                            break;
                                        }
                                    }
                                }
                            }
                            // Incorporate the offset, then check against cycle size
                            m -= e;
                            return m === d || m % d === 0 && m / d >= 0;
                        }
                    };
                },
                PSEUDO: function(a, b) {
                    // pseudo-class names are case-insensitive
                    // http://www.w3.org/TR/selectors/#pseudo-classes
                    // Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
                    // Remember that setFilters inherits from pseudos
                    var c, e = d.pseudos[a] || d.setFilters[a.toLowerCase()] || ga.error("unsupported pseudo: " + a);
                    // The user may use createPseudo to indicate that
                    // arguments are needed to create the filter function
                    // just as Sizzle does
                    if (e[u]) {
                        return e(b);
                    }
                    // But maintain support for old signatures
                    if (e.length > 1) {
                        c = [ a, a, "", b ];
                        return d.setFilters.hasOwnProperty(a.toLowerCase()) ? ia(function(a, c) {
                            var d, f = e(a, b), g = f.length;
                            while (g--) {
                                d = J(a, f[g]);
                                a[d] = !(c[d] = f[g]);
                            }
                        }) : function(a) {
                            return e(a, 0, c);
                        };
                    }
                    return e;
                }
            },
            pseudos: {
                // Potentially complex pseudos
                not: ia(function(a) {
                    // Trim the selector passed to compile
                    // to avoid treating leading and trailing
                    // spaces as combinators
                    var b = [], c = [], d = h(a.replace(R, "$1"));
                    return d[u] ? ia(function(a, b, c, e) {
                        var f, g = d(a, null, e, []), h = a.length;
                        // Match elements unmatched by `matcher`
                        while (h--) {
                            if (f = g[h]) {
                                a[h] = !(b[h] = f);
                            }
                        }
                    }) : function(a, e, f) {
                        b[0] = a;
                        d(b, null, f, c);
                        // Don't keep the element (issue #299)
                        b[0] = null;
                        return !c.pop();
                    };
                }),
                has: ia(function(a) {
                    return function(b) {
                        return ga(a, b).length > 0;
                    };
                }),
                contains: ia(function(a) {
                    a = a.replace(ca, da);
                    return function(b) {
                        return (b.textContent || b.innerText || e(b)).indexOf(a) > -1;
                    };
                }),
                // "Whether an element is represented by a :lang() selector
                // is based solely on the element's language value
                // being equal to the identifier C,
                // or beginning with the identifier C immediately followed by "-".
                // The matching of C against the element's language value is performed case-insensitively.
                // The identifier C does not have to be a valid language name."
                // http://www.w3.org/TR/selectors/#lang-pseudo
                lang: ia(function(a) {
                    // lang value must be a valid identifier
                    if (!W.test(a || "")) {
                        ga.error("unsupported lang: " + a);
                    }
                    a = a.replace(ca, da).toLowerCase();
                    return function(b) {
                        var c;
                        do {
                            if (c = p ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) {
                                c = c.toLowerCase();
                                return c === a || c.indexOf(a + "-") === 0;
                            }
                        } while ((b = b.parentNode) && b.nodeType === 1);
                        return false;
                    };
                }),
                // Miscellaneous
                target: function(b) {
                    var c = a.location && a.location.hash;
                    return c && c.slice(1) === b.id;
                },
                root: function(a) {
                    return a === o;
                },
                focus: function(a) {
                    return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex);
                },
                // Boolean properties
                enabled: function(a) {
                    return a.disabled === false;
                },
                disabled: function(a) {
                    return a.disabled === true;
                },
                checked: function(a) {
                    // In CSS3, :checked should return both checked and selected elements
                    // http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
                    var b = a.nodeName.toLowerCase();
                    return b === "input" && !!a.checked || b === "option" && !!a.selected;
                },
                selected: function(a) {
                    // Accessing this property makes selected-by-default
                    // options in Safari work properly
                    if (a.parentNode) {
                        a.parentNode.selectedIndex;
                    }
                    return a.selected === true;
                },
                // Contents
                empty: function(a) {
                    // http://www.w3.org/TR/selectors/#empty-pseudo
                    // :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
                    //   but not by others (comment: 8; processing instruction: 7; etc.)
                    // nodeType < 6 works because attributes (2) do not appear as children
                    for (a = a.firstChild; a; a = a.nextSibling) {
                        if (a.nodeType < 6) {
                            return false;
                        }
                    }
                    return true;
                },
                parent: function(a) {
                    return !d.pseudos["empty"](a);
                },
                // Element/input types
                header: function(a) {
                    return Z.test(a.nodeName);
                },
                input: function(a) {
                    return Y.test(a.nodeName);
                },
                button: function(a) {
                    var b = a.nodeName.toLowerCase();
                    return b === "input" && a.type === "button" || b === "button";
                },
                text: function(a) {
                    var b;
                    // Support: IE<8
                    // New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
                    return a.nodeName.toLowerCase() === "input" && a.type === "text" && ((b = a.getAttribute("type")) == null || b.toLowerCase() === "text");
                },
                // Position-in-collection
                first: oa(function() {
                    return [ 0 ];
                }),
                last: oa(function(a, b) {
                    return [ b - 1 ];
                }),
                eq: oa(function(a, b, c) {
                    return [ c < 0 ? c + b : c ];
                }),
                even: oa(function(a, b) {
                    var c = 0;
                    for (;c < b; c += 2) {
                        a.push(c);
                    }
                    return a;
                }),
                odd: oa(function(a, b) {
                    var c = 1;
                    for (;c < b; c += 2) {
                        a.push(c);
                    }
                    return a;
                }),
                lt: oa(function(a, b, c) {
                    var d = c < 0 ? c + b : c;
                    for (;--d >= 0; ) {
                        a.push(d);
                    }
                    return a;
                }),
                gt: oa(function(a, b, c) {
                    var d = c < 0 ? c + b : c;
                    for (;++d < b; ) {
                        a.push(d);
                    }
                    return a;
                })
            }
        };
        d.pseudos["nth"] = d.pseudos["eq"];
        // Add button/input type pseudos
        for (b in {
            radio: true,
            checkbox: true,
            file: true,
            password: true,
            image: true
        }) {
            d.pseudos[b] = ma(b);
        }
        for (b in {
            submit: true,
            reset: true
        }) {
            d.pseudos[b] = na(b);
        }
        // Easy API for creating new setFilters
        function qa() {}
        qa.prototype = d.filters = d.pseudos;
        d.setFilters = new qa();
        g = ga.tokenize = function(a, b) {
            var c, e, f, g, h, i, j, k = z[a + " "];
            if (k) {
                return b ? 0 : k.slice(0);
            }
            h = a;
            i = [];
            j = d.preFilter;
            while (h) {
                // Comma and first run
                if (!c || (e = S.exec(h))) {
                    if (e) {
                        // Don't consume trailing commas as valid
                        h = h.slice(e[0].length) || h;
                    }
                    i.push(f = []);
                }
                c = false;
                // Combinators
                if (e = T.exec(h)) {
                    c = e.shift();
                    f.push({
                        value: c,
                        // Cast descendant combinators to space
                        type: e[0].replace(R, " ")
                    });
                    h = h.slice(c.length);
                }
                // Filters
                for (g in d.filter) {
                    if ((e = X[g].exec(h)) && (!j[g] || (e = j[g](e)))) {
                        c = e.shift();
                        f.push({
                            value: c,
                            type: g,
                            matches: e
                        });
                        h = h.slice(c.length);
                    }
                }
                if (!c) {
                    break;
                }
            }
            // Return the length of the invalid excess
            // if we're just parsing
            // Otherwise, throw an error or return tokens
            // Cache the tokens
            return b ? h.length : h ? ga.error(a) : z(a, i).slice(0);
        };
        function ra(a) {
            var b = 0, c = a.length, d = "";
            for (;b < c; b++) {
                d += a[b].value;
            }
            return d;
        }
        function sa(a, b, c) {
            var d = b.dir, e = c && d === "parentNode", f = x++;
            // Check against closest ancestor/preceding element
            // Check against all ancestor/preceding elements
            return b.first ? function(b, c, f) {
                while (b = b[d]) {
                    if (b.nodeType === 1 || e) {
                        return a(b, c, f);
                    }
                }
            } : function(b, c, g) {
                var h, i, j = [ w, f ];
                // We can't set arbitrary data on XML nodes, so they don't benefit from dir caching
                if (g) {
                    while (b = b[d]) {
                        if (b.nodeType === 1 || e) {
                            if (a(b, c, g)) {
                                return true;
                            }
                        }
                    }
                } else {
                    while (b = b[d]) {
                        if (b.nodeType === 1 || e) {
                            i = b[u] || (b[u] = {});
                            if ((h = i[d]) && h[0] === w && h[1] === f) {
                                // Assign to newCache so results back-propagate to previous elements
                                return j[2] = h[2];
                            } else {
                                // Reuse newcache so results back-propagate to previous elements
                                i[d] = j;
                                // A match means we're done; a fail means we have to keep checking
                                if (j[2] = a(b, c, g)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            };
        }
        function ta(a) {
            return a.length > 1 ? function(b, c, d) {
                var e = a.length;
                while (e--) {
                    if (!a[e](b, c, d)) {
                        return false;
                    }
                }
                return true;
            } : a[0];
        }
        function ua(a, b, c) {
            var d = 0, e = b.length;
            for (;d < e; d++) {
                ga(a, b[d], c);
            }
            return c;
        }
        function va(a, b, c, d, e) {
            var f, g = [], h = 0, i = a.length, j = b != null;
            for (;h < i; h++) {
                if (f = a[h]) {
                    if (!c || c(f, d, e)) {
                        g.push(f);
                        if (j) {
                            b.push(h);
                        }
                    }
                }
            }
            return g;
        }
        function wa(a, b, c, d, e, f) {
            if (d && !d[u]) {
                d = wa(d);
            }
            if (e && !e[u]) {
                e = wa(e, f);
            }
            return ia(function(f, g, h, i) {
                var j, k, l, m = [], n = [], o = g.length, // Get initial elements from seed or context
                p = f || ua(b || "*", h.nodeType ? [ h ] : h, []), // Prefilter to get matcher input, preserving a map for seed-results synchronization
                q = a && (f || !b) ? va(p, m, a, h, i) : p, r = c ? // If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
                e || (f ? a : o || d) ? // ...intermediate processing is necessary
                [] : // ...otherwise use results directly
                g : q;
                // Find primary matches
                if (c) {
                    c(q, r, h, i);
                }
                // Apply postFilter
                if (d) {
                    j = va(r, n);
                    d(j, [], h, i);
                    // Un-match failing elements by moving them back to matcherIn
                    k = j.length;
                    while (k--) {
                        if (l = j[k]) {
                            r[n[k]] = !(q[n[k]] = l);
                        }
                    }
                }
                if (f) {
                    if (e || a) {
                        if (e) {
                            // Get the final matcherOut by condensing this intermediate into postFinder contexts
                            j = [];
                            k = r.length;
                            while (k--) {
                                if (l = r[k]) {
                                    // Restore matcherIn since elem is not yet a final match
                                    j.push(q[k] = l);
                                }
                            }
                            e(null, r = [], j, i);
                        }
                        // Move matched elements from seed to results to keep them synchronized
                        k = r.length;
                        while (k--) {
                            if ((l = r[k]) && (j = e ? J(f, l) : m[k]) > -1) {
                                f[j] = !(g[j] = l);
                            }
                        }
                    }
                } else {
                    r = va(r === g ? r.splice(o, r.length) : r);
                    if (e) {
                        e(null, g, r, i);
                    } else {
                        H.apply(g, r);
                    }
                }
            });
        }
        function xa(a) {
            var b, c, e, f = a.length, g = d.relative[a[0].type], h = g || d.relative[" "], i = g ? 1 : 0, // The foundational matcher ensures that elements are reachable from top-level context(s)
            k = sa(function(a) {
                return a === b;
            }, h, true), l = sa(function(a) {
                return J(b, a) > -1;
            }, h, true), m = [ function(a, c, d) {
                var e = !g && (d || c !== j) || ((b = c).nodeType ? k(a, c, d) : l(a, c, d));
                // Avoid hanging onto element (issue #299)
                b = null;
                return e;
            } ];
            for (;i < f; i++) {
                if (c = d.relative[a[i].type]) {
                    m = [ sa(ta(m), c) ];
                } else {
                    c = d.filter[a[i].type].apply(null, a[i].matches);
                    // Return special upon seeing a positional matcher
                    if (c[u]) {
                        // Find the next relative operator (if any) for proper handling
                        e = ++i;
                        for (;e < f; e++) {
                            if (d.relative[a[e].type]) {
                                break;
                            }
                        }
                        // If the preceding token was a descendant combinator, insert an implicit any-element `*`
                        return wa(i > 1 && ta(m), i > 1 && ra(a.slice(0, i - 1).concat({
                            value: a[i - 2].type === " " ? "*" : ""
                        })).replace(R, "$1"), c, i < e && xa(a.slice(i, e)), e < f && xa(a = a.slice(e)), e < f && ra(a));
                    }
                    m.push(c);
                }
            }
            return ta(m);
        }
        function ya(a, b) {
            var c = b.length > 0, e = a.length > 0, f = function(f, g, h, i, k) {
                var l, m, o, p = 0, q = "0", r = f && [], s = [], t = j, // We must always have either seed elements or outermost context
                u = f || e && d.find["TAG"]("*", k), // Use integer dirruns iff this is the outermost matcher
                v = w += t == null ? 1 : Math.random() || .1, x = u.length;
                if (k) {
                    j = g !== n && g;
                }
                // Add elements passing elementMatchers directly to results
                // Keep `i` a string if there are no elements so `matchedCount` will be "00" below
                // Support: IE<9, Safari
                // Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
                for (;q !== x && (l = u[q]) != null; q++) {
                    if (e && l) {
                        m = 0;
                        while (o = a[m++]) {
                            if (o(l, g, h)) {
                                i.push(l);
                                break;
                            }
                        }
                        if (k) {
                            w = v;
                        }
                    }
                    // Track unmatched elements for set filters
                    if (c) {
                        // They will have gone through all possible matchers
                        if (l = !o && l) {
                            p--;
                        }
                        // Lengthen the array for every element, matched or not
                        if (f) {
                            r.push(l);
                        }
                    }
                }
                // Apply set filters to unmatched elements
                p += q;
                if (c && q !== p) {
                    m = 0;
                    while (o = b[m++]) {
                        o(r, s, g, h);
                    }
                    if (f) {
                        // Reintegrate element matches to eliminate the need for sorting
                        if (p > 0) {
                            while (q--) {
                                if (!(r[q] || s[q])) {
                                    s[q] = F.call(i);
                                }
                            }
                        }
                        // Discard index placeholder values to get only actual matches
                        s = va(s);
                    }
                    // Add matches to results
                    H.apply(i, s);
                    // Seedless set matches succeeding multiple successful matchers stipulate sorting
                    if (k && !f && s.length > 0 && p + b.length > 1) {
                        ga.uniqueSort(i);
                    }
                }
                // Override manipulation of globals by nested matchers
                if (k) {
                    w = v;
                    j = t;
                }
                return r;
            };
            return c ? ia(f) : f;
        }
        h = ga.compile = function(a, b) {
            var c, d = [], e = [], f = A[a + " "];
            if (!f) {
                // Generate a function of recursive functions that can be used to check each element
                if (!b) {
                    b = g(a);
                }
                c = b.length;
                while (c--) {
                    f = xa(b[c]);
                    if (f[u]) {
                        d.push(f);
                    } else {
                        e.push(f);
                    }
                }
                // Cache the compiled function
                f = A(a, ya(e, d));
                // Save selector and tokenization
                f.selector = a;
            }
            return f;
        };
        /**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
        i = ga.select = function(a, b, e, f) {
            var i, j, k, l, m, n = typeof a === "function" && a, o = !f && g(a = n.selector || a);
            e = e || [];
            // Try to minimize operations if there is no seed and only one group
            if (o.length === 1) {
                // Take a shortcut and set the context if the root selector is an ID
                j = o[0] = o[0].slice(0);
                if (j.length > 2 && (k = j[0]).type === "ID" && c.getById && b.nodeType === 9 && p && d.relative[j[1].type]) {
                    b = (d.find["ID"](k.matches[0].replace(ca, da), b) || [])[0];
                    if (!b) {
                        return e;
                    } else if (n) {
                        b = b.parentNode;
                    }
                    a = a.slice(j.shift().value.length);
                }
                // Fetch a seed set for right-to-left matching
                i = X["needsContext"].test(a) ? 0 : j.length;
                while (i--) {
                    k = j[i];
                    // Abort if we hit a combinator
                    if (d.relative[l = k.type]) {
                        break;
                    }
                    if (m = d.find[l]) {
                        // Search, expanding context for leading sibling combinators
                        if (f = m(k.matches[0].replace(ca, da), aa.test(j[0].type) && pa(b.parentNode) || b)) {
                            // If seed is empty or no tokens remain, we can return early
                            j.splice(i, 1);
                            a = f.length && ra(j);
                            if (!a) {
                                H.apply(e, f);
                                return e;
                            }
                            break;
                        }
                    }
                }
            }
            // Compile and execute a filtering function if one is not provided
            // Provide `match` to avoid retokenization if we modified the selector above
            (n || h(a, o))(f, b, !p, e, aa.test(a) && pa(b.parentNode) || b);
            return e;
        };
        // One-time assignments
        // Sort stability
        c.sortStable = u.split("").sort(B).join("") === u;
        // Support: Chrome 14-35+
        // Always assume duplicates if they aren't passed to the comparison function
        c.detectDuplicates = !!l;
        // Initialize against the default document
        m();
        // Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
        // Detached nodes confoundingly follow *each other*
        c.sortDetached = ja(function(a) {
            // Should return 1, but returns 4 (following)
            return a.compareDocumentPosition(n.createElement("div")) & 1;
        });
        // Support: IE<8
        // Prevent attribute/property "interpolation"
        // http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
        if (!ja(function(a) {
            a.innerHTML = "<a href='#'></a>";
            return a.firstChild.getAttribute("href") === "#";
        })) {
            ka("type|href|height|width", function(a, b, c) {
                if (!c) {
                    return a.getAttribute(b, b.toLowerCase() === "type" ? 1 : 2);
                }
            });
        }
        // Support: IE<9
        // Use defaultValue in place of getAttribute("value")
        if (!c.attributes || !ja(function(a) {
            a.innerHTML = "<input/>";
            a.firstChild.setAttribute("value", "");
            return a.firstChild.getAttribute("value") === "";
        })) {
            ka("value", function(a, b, c) {
                if (!c && a.nodeName.toLowerCase() === "input") {
                    return a.defaultValue;
                }
            });
        }
        // Support: IE<9
        // Use getAttributeNode to fetch booleans when getAttribute lies
        if (!ja(function(a) {
            return a.getAttribute("disabled") == null;
        })) {
            ka(K, function(a, b, c) {
                var d;
                if (!c) {
                    return a[b] === true ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null;
                }
            });
        }
        return ga;
    }(a);
    n.find = t;
    n.expr = t.selectors;
    n.expr[":"] = n.expr.pseudos;
    n.unique = t.uniqueSort;
    n.text = t.getText;
    n.isXMLDoc = t.isXML;
    n.contains = t.contains;
    var u = n.expr.match.needsContext;
    var v = /^<(\w+)\s*\/?>(?:<\/\1>|)$/;
    var w = /^.[^:#\[\.,]*$/;
    // Implement the identical functionality for filter and not
    function x(a, b, c) {
        if (n.isFunction(b)) {
            return n.grep(a, function(a, d) {
                /* jshint -W018 */
                return !!b.call(a, d, a) !== c;
            });
        }
        if (b.nodeType) {
            return n.grep(a, function(a) {
                return a === b !== c;
            });
        }
        if (typeof b === "string") {
            if (w.test(b)) {
                return n.filter(b, a, c);
            }
            b = n.filter(b, a);
        }
        return n.grep(a, function(a) {
            return g.call(b, a) >= 0 !== c;
        });
    }
    n.filter = function(a, b, c) {
        var d = b[0];
        if (c) {
            a = ":not(" + a + ")";
        }
        return b.length === 1 && d.nodeType === 1 ? n.find.matchesSelector(d, a) ? [ d ] : [] : n.find.matches(a, n.grep(b, function(a) {
            return a.nodeType === 1;
        }));
    };
    n.fn.extend({
        find: function(a) {
            var b, c = this.length, d = [], e = this;
            if (typeof a !== "string") {
                return this.pushStack(n(a).filter(function() {
                    for (b = 0; b < c; b++) {
                        if (n.contains(e[b], this)) {
                            return true;
                        }
                    }
                }));
            }
            for (b = 0; b < c; b++) {
                n.find(a, e[b], d);
            }
            // Needed because $( selector, context ) becomes $( context ).find( selector )
            d = this.pushStack(c > 1 ? n.unique(d) : d);
            d.selector = this.selector ? this.selector + " " + a : a;
            return d;
        },
        filter: function(a) {
            return this.pushStack(x(this, a || [], false));
        },
        not: function(a) {
            return this.pushStack(x(this, a || [], true));
        },
        is: function(a) {
            // If this is a positional/relative selector, check membership in the returned set
            // so $("p:first").is("p:last") won't return true for a doc with two "p".
            return !!x(this, typeof a === "string" && u.test(a) ? n(a) : a || [], false).length;
        }
    });
    // Initialize a jQuery object
    // A central reference to the root jQuery(document)
    var y, // A simple way to check for HTML strings
    // Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
    // Strict HTML recognition (#11290: must start with <)
    z = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, A = n.fn.init = function(a, b) {
        var c, d;
        // HANDLE: $(""), $(null), $(undefined), $(false)
        if (!a) {
            return this;
        }
        // Handle HTML strings
        if (typeof a === "string") {
            if (a[0] === "<" && a[a.length - 1] === ">" && a.length >= 3) {
                // Assume that strings that start and end with <> are HTML and skip the regex check
                c = [ null, a, null ];
            } else {
                c = z.exec(a);
            }
            // Match html or make sure no context is specified for #id
            if (c && (c[1] || !b)) {
                // HANDLE: $(html) -> $(array)
                if (c[1]) {
                    b = b instanceof n ? b[0] : b;
                    // Option to run scripts is true for back-compat
                    // Intentionally let the error be thrown if parseHTML is not present
                    n.merge(this, n.parseHTML(c[1], b && b.nodeType ? b.ownerDocument || b : l, true));
                    // HANDLE: $(html, props)
                    if (v.test(c[1]) && n.isPlainObject(b)) {
                        for (c in b) {
                            // Properties of context are called as methods if possible
                            if (n.isFunction(this[c])) {
                                this[c](b[c]);
                            } else {
                                this.attr(c, b[c]);
                            }
                        }
                    }
                    return this;
                } else {
                    d = l.getElementById(c[2]);
                    // Support: Blackberry 4.6
                    // gEBID returns nodes no longer in the document (#6963)
                    if (d && d.parentNode) {
                        // Inject the element directly into the jQuery object
                        this.length = 1;
                        this[0] = d;
                    }
                    this.context = l;
                    this.selector = a;
                    return this;
                }
            } else if (!b || b.jquery) {
                return (b || y).find(a);
            } else {
                return this.constructor(b).find(a);
            }
        } else if (a.nodeType) {
            this.context = this[0] = a;
            this.length = 1;
            return this;
        } else if (n.isFunction(a)) {
            // Execute immediately if ready is not present
            return typeof y.ready !== "undefined" ? y.ready(a) : a(n);
        }
        if (a.selector !== undefined) {
            this.selector = a.selector;
            this.context = a.context;
        }
        return n.makeArray(a, this);
    };
    // Give the init function the jQuery prototype for later instantiation
    A.prototype = n.fn;
    // Initialize central reference
    y = n(l);
    var B = /^(?:parents|prev(?:Until|All))/, // Methods guaranteed to produce a unique set when starting from a unique set
    C = {
        children: true,
        contents: true,
        next: true,
        prev: true
    };
    n.extend({
        dir: function(a, b, c) {
            var d = [], e = c !== undefined;
            while ((a = a[b]) && a.nodeType !== 9) {
                if (a.nodeType === 1) {
                    if (e && n(a).is(c)) {
                        break;
                    }
                    d.push(a);
                }
            }
            return d;
        },
        sibling: function(a, b) {
            var c = [];
            for (;a; a = a.nextSibling) {
                if (a.nodeType === 1 && a !== b) {
                    c.push(a);
                }
            }
            return c;
        }
    });
    n.fn.extend({
        has: function(a) {
            var b = n(a, this), c = b.length;
            return this.filter(function() {
                var a = 0;
                for (;a < c; a++) {
                    if (n.contains(this, b[a])) {
                        return true;
                    }
                }
            });
        },
        closest: function(a, b) {
            var c, d = 0, e = this.length, f = [], g = u.test(a) || typeof a !== "string" ? n(a, b || this.context) : 0;
            for (;d < e; d++) {
                for (c = this[d]; c && c !== b; c = c.parentNode) {
                    // Always skip document fragments
                    if (c.nodeType < 11 && (g ? g.index(c) > -1 : // Don't pass non-elements to Sizzle
                    c.nodeType === 1 && n.find.matchesSelector(c, a))) {
                        f.push(c);
                        break;
                    }
                }
            }
            return this.pushStack(f.length > 1 ? n.unique(f) : f);
        },
        // Determine the position of an element within the set
        index: function(a) {
            // No argument, return index in parent
            if (!a) {
                return this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
            }
            // Index in selector
            if (typeof a === "string") {
                return g.call(n(a), this[0]);
            }
            // Locate the position of the desired element
            // If it receives a jQuery object, the first element is used
            return g.call(this, a.jquery ? a[0] : a);
        },
        add: function(a, b) {
            return this.pushStack(n.unique(n.merge(this.get(), n(a, b))));
        },
        addBack: function(a) {
            return this.add(a == null ? this.prevObject : this.prevObject.filter(a));
        }
    });
    function D(a, b) {
        while ((a = a[b]) && a.nodeType !== 1) {}
        return a;
    }
    n.each({
        parent: function(a) {
            var b = a.parentNode;
            return b && b.nodeType !== 11 ? b : null;
        },
        parents: function(a) {
            return n.dir(a, "parentNode");
        },
        parentsUntil: function(a, b, c) {
            return n.dir(a, "parentNode", c);
        },
        next: function(a) {
            return D(a, "nextSibling");
        },
        prev: function(a) {
            return D(a, "previousSibling");
        },
        nextAll: function(a) {
            return n.dir(a, "nextSibling");
        },
        prevAll: function(a) {
            return n.dir(a, "previousSibling");
        },
        nextUntil: function(a, b, c) {
            return n.dir(a, "nextSibling", c);
        },
        prevUntil: function(a, b, c) {
            return n.dir(a, "previousSibling", c);
        },
        siblings: function(a) {
            return n.sibling((a.parentNode || {}).firstChild, a);
        },
        children: function(a) {
            return n.sibling(a.firstChild);
        },
        contents: function(a) {
            return a.contentDocument || n.merge([], a.childNodes);
        }
    }, function(a, b) {
        n.fn[a] = function(c, d) {
            var e = n.map(this, b, c);
            if (a.slice(-5) !== "Until") {
                d = c;
            }
            if (d && typeof d === "string") {
                e = n.filter(d, e);
            }
            if (this.length > 1) {
                // Remove duplicates
                if (!C[a]) {
                    n.unique(e);
                }
                // Reverse order for parents* and prev-derivatives
                if (B.test(a)) {
                    e.reverse();
                }
            }
            return this.pushStack(e);
        };
    });
    var E = /\S+/g;
    // String to Object options format cache
    var F = {};
    // Convert String-formatted options into Object-formatted ones and store in cache
    function G(a) {
        var b = F[a] = {};
        n.each(a.match(E) || [], function(a, c) {
            b[c] = true;
        });
        return b;
    }
    /*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
    n.Callbacks = function(a) {
        // Convert options from String-formatted to Object-formatted if needed
        // (we check in cache first)
        a = typeof a === "string" ? F[a] || G(a) : n.extend({}, a);
        var // Last fire value (for non-forgettable lists)
        b, // Flag to know if list was already fired
        c, // Flag to know if list is currently firing
        d, // First callback to fire (used internally by add and fireWith)
        e, // End of the loop when firing
        f, // Index of currently firing callback (modified by remove if needed)
        g, // Actual callback list
        h = [], // Stack of fire calls for repeatable lists
        i = !a.once && [], // Fire callbacks
        j = function(l) {
            b = a.memory && l;
            c = true;
            g = e || 0;
            e = 0;
            f = h.length;
            d = true;
            for (;h && g < f; g++) {
                if (h[g].apply(l[0], l[1]) === false && a.stopOnFalse) {
                    b = false;
                    // To prevent further calls using add
                    break;
                }
            }
            d = false;
            if (h) {
                if (i) {
                    if (i.length) {
                        j(i.shift());
                    }
                } else if (b) {
                    h = [];
                } else {
                    k.disable();
                }
            }
        }, // Actual Callbacks object
        k = {
            // Add a callback or a collection of callbacks to the list
            add: function() {
                if (h) {
                    // First, we save the current length
                    var c = h.length;
                    (function g(b) {
                        n.each(b, function(b, c) {
                            var d = n.type(c);
                            if (d === "function") {
                                if (!a.unique || !k.has(c)) {
                                    h.push(c);
                                }
                            } else if (c && c.length && d !== "string") {
                                // Inspect recursively
                                g(c);
                            }
                        });
                    })(arguments);
                    // Do we need to add the callbacks to the
                    // current firing batch?
                    if (d) {
                        f = h.length;
                    } else if (b) {
                        e = c;
                        j(b);
                    }
                }
                return this;
            },
            // Remove a callback from the list
            remove: function() {
                if (h) {
                    n.each(arguments, function(a, b) {
                        var c;
                        while ((c = n.inArray(b, h, c)) > -1) {
                            h.splice(c, 1);
                            // Handle firing indexes
                            if (d) {
                                if (c <= f) {
                                    f--;
                                }
                                if (c <= g) {
                                    g--;
                                }
                            }
                        }
                    });
                }
                return this;
            },
            // Check if a given callback is in the list.
            // If no argument is given, return whether or not list has callbacks attached.
            has: function(a) {
                return a ? n.inArray(a, h) > -1 : !!(h && h.length);
            },
            // Remove all callbacks from the list
            empty: function() {
                h = [];
                f = 0;
                return this;
            },
            // Have the list do nothing anymore
            disable: function() {
                h = i = b = undefined;
                return this;
            },
            // Is it disabled?
            disabled: function() {
                return !h;
            },
            // Lock the list in its current state
            lock: function() {
                i = undefined;
                if (!b) {
                    k.disable();
                }
                return this;
            },
            // Is it locked?
            locked: function() {
                return !i;
            },
            // Call all callbacks with the given context and arguments
            fireWith: function(a, b) {
                if (h && (!c || i)) {
                    b = b || [];
                    b = [ a, b.slice ? b.slice() : b ];
                    if (d) {
                        i.push(b);
                    } else {
                        j(b);
                    }
                }
                return this;
            },
            // Call all the callbacks with the given arguments
            fire: function() {
                k.fireWith(this, arguments);
                return this;
            },
            // To know if the callbacks have already been called at least once
            fired: function() {
                return !!c;
            }
        };
        return k;
    };
    n.extend({
        Deferred: function(a) {
            var b = [ // action, add listener, listener list, final state
            [ "resolve", "done", n.Callbacks("once memory"), "resolved" ], [ "reject", "fail", n.Callbacks("once memory"), "rejected" ], [ "notify", "progress", n.Callbacks("memory") ] ], c = "pending", d = {
                state: function() {
                    return c;
                },
                always: function() {
                    e.done(arguments).fail(arguments);
                    return this;
                },
                then: function() {
                    var a = arguments;
                    return n.Deferred(function(c) {
                        n.each(b, function(b, f) {
                            var g = n.isFunction(a[b]) && a[b];
                            // deferred[ done | fail | progress ] for forwarding actions to newDefer
                            e[f[1]](function() {
                                var a = g && g.apply(this, arguments);
                                if (a && n.isFunction(a.promise)) {
                                    a.promise().done(c.resolve).fail(c.reject).progress(c.notify);
                                } else {
                                    c[f[0] + "With"](this === d ? c.promise() : this, g ? [ a ] : arguments);
                                }
                            });
                        });
                        a = null;
                    }).promise();
                },
                // Get a promise for this deferred
                // If obj is provided, the promise aspect is added to the object
                promise: function(a) {
                    return a != null ? n.extend(a, d) : d;
                }
            }, e = {};
            // Keep pipe for back-compat
            d.pipe = d.then;
            // Add list-specific methods
            n.each(b, function(a, f) {
                var g = f[2], h = f[3];
                // promise[ done | fail | progress ] = list.add
                d[f[1]] = g.add;
                // Handle state
                if (h) {
                    g.add(function() {
                        // state = [ resolved | rejected ]
                        c = h;
                    }, b[a ^ 1][2].disable, b[2][2].lock);
                }
                // deferred[ resolve | reject | notify ]
                e[f[0]] = function() {
                    e[f[0] + "With"](this === e ? d : this, arguments);
                    return this;
                };
                e[f[0] + "With"] = g.fireWith;
            });
            // Make the deferred a promise
            d.promise(e);
            // Call given func if any
            if (a) {
                a.call(e, e);
            }
            // All done!
            return e;
        },
        // Deferred helper
        when: function(a) {
            var b = 0, c = d.call(arguments), e = c.length, // the count of uncompleted subordinates
            f = e !== 1 || a && n.isFunction(a.promise) ? e : 0, // the master Deferred. If resolveValues consist of only a single Deferred, just use that.
            g = f === 1 ? a : n.Deferred(), // Update function for both resolve and progress values
            h = function(a, b, c) {
                return function(e) {
                    b[a] = this;
                    c[a] = arguments.length > 1 ? d.call(arguments) : e;
                    if (c === i) {
                        g.notifyWith(b, c);
                    } else if (!--f) {
                        g.resolveWith(b, c);
                    }
                };
            }, i, j, k;
            // Add listeners to Deferred subordinates; treat others as resolved
            if (e > 1) {
                i = new Array(e);
                j = new Array(e);
                k = new Array(e);
                for (;b < e; b++) {
                    if (c[b] && n.isFunction(c[b].promise)) {
                        c[b].promise().done(h(b, k, c)).fail(g.reject).progress(h(b, j, i));
                    } else {
                        --f;
                    }
                }
            }
            // If we're not waiting on anything, resolve the master
            if (!f) {
                g.resolveWith(k, c);
            }
            return g.promise();
        }
    });
    // The deferred used on DOM ready
    var H;
    n.fn.ready = function(a) {
        // Add the callback
        n.ready.promise().done(a);
        return this;
    };
    n.extend({
        // Is the DOM ready to be used? Set to true once it occurs.
        isReady: false,
        // A counter to track how many items to wait for before
        // the ready event fires. See #6781
        readyWait: 1,
        // Hold (or release) the ready event
        holdReady: function(a) {
            if (a) {
                n.readyWait++;
            } else {
                n.ready(true);
            }
        },
        // Handle when the DOM is ready
        ready: function(a) {
            // Abort if there are pending holds or we're already ready
            if (a === true ? --n.readyWait : n.isReady) {
                return;
            }
            // Remember that the DOM is ready
            n.isReady = true;
            // If a normal DOM Ready event fired, decrement, and wait if need be
            if (a !== true && --n.readyWait > 0) {
                return;
            }
            // If there are functions bound, to execute
            H.resolveWith(l, [ n ]);
            // Trigger any bound ready events
            if (n.fn.triggerHandler) {
                n(l).triggerHandler("ready");
                n(l).off("ready");
            }
        }
    });
    /**
 * The ready event handler and self cleanup method
 */
    function I() {
        l.removeEventListener("DOMContentLoaded", I, false);
        a.removeEventListener("load", I, false);
        n.ready();
    }
    n.ready.promise = function(b) {
        if (!H) {
            H = n.Deferred();
            // Catch cases where $(document).ready() is called after the browser event has already occurred.
            // We once tried to use readyState "interactive" here, but it caused issues like the one
            // discovered by ChrisS here: http://bugs.jquery.com/ticket/12282#comment:15
            if (l.readyState === "complete") {
                // Handle it asynchronously to allow scripts the opportunity to delay ready
                setTimeout(n.ready);
            } else {
                // Use the handy event callback
                l.addEventListener("DOMContentLoaded", I, false);
                // A fallback to window.onload, that will always work
                a.addEventListener("load", I, false);
            }
        }
        return H.promise(b);
    };
    // Kick off the DOM ready check even if the user does not
    n.ready.promise();
    // Multifunctional method to get and set values of a collection
    // The value/s can optionally be executed if it's a function
    var J = n.access = function(a, b, c, d, e, f, g) {
        var h = 0, i = a.length, j = c == null;
        // Sets many values
        if (n.type(c) === "object") {
            e = true;
            for (h in c) {
                n.access(a, b, h, c[h], true, f, g);
            }
        } else if (d !== undefined) {
            e = true;
            if (!n.isFunction(d)) {
                g = true;
            }
            if (j) {
                // Bulk operations run against the entire set
                if (g) {
                    b.call(a, d);
                    b = null;
                } else {
                    j = b;
                    b = function(a, b, c) {
                        return j.call(n(a), c);
                    };
                }
            }
            if (b) {
                for (;h < i; h++) {
                    b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
                }
            }
        }
        // Gets
        return e ? a : j ? b.call(a) : i ? b(a[0], c) : f;
    };
    /**
 * Determines whether an object can have data
 */
    n.acceptData = function(a) {
        // Accepts only:
        //  - Node
        //    - Node.ELEMENT_NODE
        //    - Node.DOCUMENT_NODE
        //  - Object
        //    - Any
        /* jshint -W018 */
        return a.nodeType === 1 || a.nodeType === 9 || !+a.nodeType;
    };
    function K() {
        // Support: Android<4,
        // Old WebKit does not have Object.preventExtensions/freeze method,
        // return new empty object instead with no [[set]] accessor
        Object.defineProperty(this.cache = {}, 0, {
            get: function() {
                return {};
            }
        });
        this.expando = n.expando + K.uid++;
    }
    K.uid = 1;
    K.accepts = n.acceptData;
    K.prototype = {
        key: function(a) {
            // We can accept data for non-element nodes in modern browsers,
            // but we should not, see #8335.
            // Always return the key for a frozen object.
            if (!K.accepts(a)) {
                return 0;
            }
            var b = {}, // Check if the owner object already has a cache key
            c = a[this.expando];
            // If not, create one
            if (!c) {
                c = K.uid++;
                // Secure it in a non-enumerable, non-writable property
                try {
                    b[this.expando] = {
                        value: c
                    };
                    Object.defineProperties(a, b);
                } catch (d) {
                    b[this.expando] = c;
                    n.extend(a, b);
                }
            }
            // Ensure the cache object
            if (!this.cache[c]) {
                this.cache[c] = {};
            }
            return c;
        },
        set: function(a, b, c) {
            var d, // There may be an unlock assigned to this node,
            // if there is no entry for this "owner", create one inline
            // and set the unlock as though an owner entry had always existed
            e = this.key(a), f = this.cache[e];
            // Handle: [ owner, key, value ] args
            if (typeof b === "string") {
                f[b] = c;
            } else {
                // Fresh assignments by object are shallow copied
                if (n.isEmptyObject(f)) {
                    n.extend(this.cache[e], b);
                } else {
                    for (d in b) {
                        f[d] = b[d];
                    }
                }
            }
            return f;
        },
        get: function(a, b) {
            // Either a valid cache is found, or will be created.
            // New caches will be created and the unlock returned,
            // allowing direct access to the newly created
            // empty data object. A valid owner object must be provided.
            var c = this.cache[this.key(a)];
            return b === undefined ? c : c[b];
        },
        access: function(a, b, c) {
            var d;
            // In cases where either:
            //
            //   1. No key was specified
            //   2. A string key was specified, but no value provided
            //
            // Take the "read" path and allow the get method to determine
            // which value to return, respectively either:
            //
            //   1. The entire cache object
            //   2. The data stored at the key
            //
            if (b === undefined || b && typeof b === "string" && c === undefined) {
                d = this.get(a, b);
                return d !== undefined ? d : this.get(a, n.camelCase(b));
            }
            // [*]When the key is not a string, or both a key and value
            // are specified, set or extend (existing objects) with either:
            //
            //   1. An object of properties
            //   2. A key and value
            //
            this.set(a, b, c);
            // Since the "set" path can have two possible entry points
            // return the expected data based on which path was taken[*]
            return c !== undefined ? c : b;
        },
        remove: function(a, b) {
            var c, d, e, f = this.key(a), g = this.cache[f];
            if (b === undefined) {
                this.cache[f] = {};
            } else {
                // Support array or space separated string of keys
                if (n.isArray(b)) {
                    // If "name" is an array of keys...
                    // When data is initially created, via ("key", "val") signature,
                    // keys will be converted to camelCase.
                    // Since there is no way to tell _how_ a key was added, remove
                    // both plain key and camelCase key. #12786
                    // This will only penalize the array argument path.
                    d = b.concat(b.map(n.camelCase));
                } else {
                    e = n.camelCase(b);
                    // Try the string as a key before any manipulation
                    if (b in g) {
                        d = [ b, e ];
                    } else {
                        // If a key with the spaces exists, use it.
                        // Otherwise, create an array by matching non-whitespace
                        d = e;
                        d = d in g ? [ d ] : d.match(E) || [];
                    }
                }
                c = d.length;
                while (c--) {
                    delete g[d[c]];
                }
            }
        },
        hasData: function(a) {
            return !n.isEmptyObject(this.cache[a[this.expando]] || {});
        },
        discard: function(a) {
            if (a[this.expando]) {
                delete this.cache[a[this.expando]];
            }
        }
    };
    var L = new K();
    var M = new K();
    //	Implementation Summary
    //
    //	1. Enforce API surface and semantic compatibility with 1.9.x branch
    //	2. Improve the module's maintainability by reducing the storage
    //		paths to a single mechanism.
    //	3. Use the same single mechanism to support "private" and "user" data.
    //	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
    //	5. Avoid exposing implementation details on user objects (eg. expando properties)
    //	6. Provide a clear path for implementation upgrade to WeakMap in 2014
    var N = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, O = /([A-Z])/g;
    function P(a, b, c) {
        var d;
        // If nothing was found internally, try to fetch any
        // data from the HTML5 data-* attribute
        if (c === undefined && a.nodeType === 1) {
            d = "data-" + b.replace(O, "-$1").toLowerCase();
            c = a.getAttribute(d);
            if (typeof c === "string") {
                try {
                    c = c === "true" ? true : c === "false" ? false : c === "null" ? null : // Only convert to a number if it doesn't change the string
                    +c + "" === c ? +c : N.test(c) ? n.parseJSON(c) : c;
                } catch (e) {}
                // Make sure we set the data so it isn't changed later
                M.set(a, b, c);
            } else {
                c = undefined;
            }
        }
        return c;
    }
    n.extend({
        hasData: function(a) {
            return M.hasData(a) || L.hasData(a);
        },
        data: function(a, b, c) {
            return M.access(a, b, c);
        },
        removeData: function(a, b) {
            M.remove(a, b);
        },
        // TODO: Now that all calls to _data and _removeData have been replaced
        // with direct calls to data_priv methods, these can be deprecated.
        _data: function(a, b, c) {
            return L.access(a, b, c);
        },
        _removeData: function(a, b) {
            L.remove(a, b);
        }
    });
    n.fn.extend({
        data: function(a, b) {
            var c, d, e, f = this[0], g = f && f.attributes;
            // Gets all values
            if (a === undefined) {
                if (this.length) {
                    e = M.get(f);
                    if (f.nodeType === 1 && !L.get(f, "hasDataAttrs")) {
                        c = g.length;
                        while (c--) {
                            // Support: IE11+
                            // The attrs elements can be null (#14894)
                            if (g[c]) {
                                d = g[c].name;
                                if (d.indexOf("data-") === 0) {
                                    d = n.camelCase(d.slice(5));
                                    P(f, d, e[d]);
                                }
                            }
                        }
                        L.set(f, "hasDataAttrs", true);
                    }
                }
                return e;
            }
            // Sets multiple values
            if (typeof a === "object") {
                return this.each(function() {
                    M.set(this, a);
                });
            }
            return J(this, function(b) {
                var c, d = n.camelCase(a);
                // The calling jQuery object (element matches) is not empty
                // (and therefore has an element appears at this[ 0 ]) and the
                // `value` parameter was not undefined. An empty jQuery object
                // will result in `undefined` for elem = this[ 0 ] which will
                // throw an exception if an attempt to read a data cache is made.
                if (f && b === undefined) {
                    // Attempt to get data from the cache
                    // with the key as-is
                    c = M.get(f, a);
                    if (c !== undefined) {
                        return c;
                    }
                    // Attempt to get data from the cache
                    // with the key camelized
                    c = M.get(f, d);
                    if (c !== undefined) {
                        return c;
                    }
                    // Attempt to "discover" the data in
                    // HTML5 custom data-* attrs
                    c = P(f, d, undefined);
                    if (c !== undefined) {
                        return c;
                    }
                    // We tried really hard, but the data doesn't exist.
                    return;
                }
                // Set the data...
                this.each(function() {
                    // First, attempt to store a copy or reference of any
                    // data that might've been store with a camelCased key.
                    var c = M.get(this, d);
                    // For HTML5 data-* attribute interop, we have to
                    // store property names with dashes in a camelCase form.
                    // This might not apply to all properties...*
                    M.set(this, d, b);
                    // *... In the case of properties that might _actually_
                    // have dashes, we need to also store a copy of that
                    // unchanged property.
                    if (a.indexOf("-") !== -1 && c !== undefined) {
                        M.set(this, a, b);
                    }
                });
            }, null, b, arguments.length > 1, null, true);
        },
        removeData: function(a) {
            return this.each(function() {
                M.remove(this, a);
            });
        }
    });
    n.extend({
        queue: function(a, b, c) {
            var d;
            if (a) {
                b = (b || "fx") + "queue";
                d = L.get(a, b);
                // Speed up dequeue by getting out quickly if this is just a lookup
                if (c) {
                    if (!d || n.isArray(c)) {
                        d = L.access(a, b, n.makeArray(c));
                    } else {
                        d.push(c);
                    }
                }
                return d || [];
            }
        },
        dequeue: function(a, b) {
            b = b || "fx";
            var c = n.queue(a, b), d = c.length, e = c.shift(), f = n._queueHooks(a, b), g = function() {
                n.dequeue(a, b);
            };
            // If the fx queue is dequeued, always remove the progress sentinel
            if (e === "inprogress") {
                e = c.shift();
                d--;
            }
            if (e) {
                // Add a progress sentinel to prevent the fx queue from being
                // automatically dequeued
                if (b === "fx") {
                    c.unshift("inprogress");
                }
                // Clear up the last queue stop function
                delete f.stop;
                e.call(a, g, f);
            }
            if (!d && f) {
                f.empty.fire();
            }
        },
        // Not public - generate a queueHooks object, or return the current one
        _queueHooks: function(a, b) {
            var c = b + "queueHooks";
            return L.get(a, c) || L.access(a, c, {
                empty: n.Callbacks("once memory").add(function() {
                    L.remove(a, [ b + "queue", c ]);
                })
            });
        }
    });
    n.fn.extend({
        queue: function(a, b) {
            var c = 2;
            if (typeof a !== "string") {
                b = a;
                a = "fx";
                c--;
            }
            if (arguments.length < c) {
                return n.queue(this[0], a);
            }
            return b === undefined ? this : this.each(function() {
                var c = n.queue(this, a, b);
                // Ensure a hooks for this queue
                n._queueHooks(this, a);
                if (a === "fx" && c[0] !== "inprogress") {
                    n.dequeue(this, a);
                }
            });
        },
        dequeue: function(a) {
            return this.each(function() {
                n.dequeue(this, a);
            });
        },
        clearQueue: function(a) {
            return this.queue(a || "fx", []);
        },
        // Get a promise resolved when queues of a certain type
        // are emptied (fx is the type by default)
        promise: function(a, b) {
            var c, d = 1, e = n.Deferred(), f = this, g = this.length, h = function() {
                if (!--d) {
                    e.resolveWith(f, [ f ]);
                }
            };
            if (typeof a !== "string") {
                b = a;
                a = undefined;
            }
            a = a || "fx";
            while (g--) {
                c = L.get(f[g], a + "queueHooks");
                if (c && c.empty) {
                    d++;
                    c.empty.add(h);
                }
            }
            h();
            return e.promise(b);
        }
    });
    var Q = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source;
    var R = [ "Top", "Right", "Bottom", "Left" ];
    var S = function(a, b) {
        // isHidden might be called from jQuery#filter function;
        // in that case, element will be second argument
        a = b || a;
        return n.css(a, "display") === "none" || !n.contains(a.ownerDocument, a);
    };
    var T = /^(?:checkbox|radio)$/i;
    (function() {
        var a = l.createDocumentFragment(), b = a.appendChild(l.createElement("div")), c = l.createElement("input");
        // Support: Safari<=5.1
        // Check state lost if the name is set (#11217)
        // Support: Windows Web Apps (WWA)
        // `name` and `type` must use .setAttribute for WWA (#14901)
        c.setAttribute("type", "radio");
        c.setAttribute("checked", "checked");
        c.setAttribute("name", "t");
        b.appendChild(c);
        // Support: Safari<=5.1, Android<4.2
        // Older WebKit doesn't clone checked state correctly in fragments
        k.checkClone = b.cloneNode(true).cloneNode(true).lastChild.checked;
        // Support: IE<=11+
        // Make sure textarea (and checkbox) defaultValue is properly cloned
        b.innerHTML = "<textarea>x</textarea>";
        k.noCloneChecked = !!b.cloneNode(true).lastChild.defaultValue;
    })();
    var U = typeof undefined;
    k.focusinBubbles = "onfocusin" in a;
    var V = /^key/, W = /^(?:mouse|pointer|contextmenu)|click/, X = /^(?:focusinfocus|focusoutblur)$/, Y = /^([^.]*)(?:\.(.+)|)$/;
    function Z() {
        return true;
    }
    function $() {
        return false;
    }
    function _() {
        try {
            return l.activeElement;
        } catch (a) {}
    }
    /*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
    n.event = {
        global: {},
        add: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, o, p, q, r = L.get(a);
            // Don't attach events to noData or text/comment nodes (but allow plain objects)
            if (!r) {
                return;
            }
            // Caller can pass in an object of custom data in lieu of the handler
            if (c.handler) {
                f = c;
                c = f.handler;
                e = f.selector;
            }
            // Make sure that the handler has a unique ID, used to find/remove it later
            if (!c.guid) {
                c.guid = n.guid++;
            }
            // Init the element's event structure and main handler, if this is the first
            if (!(i = r.events)) {
                i = r.events = {};
            }
            if (!(g = r.handle)) {
                g = r.handle = function(b) {
                    // Discard the second event of a jQuery.event.trigger() and
                    // when an event is called after a page has unloaded
                    return typeof n !== U && n.event.triggered !== b.type ? n.event.dispatch.apply(a, arguments) : undefined;
                };
            }
            // Handle multiple events separated by a space
            b = (b || "").match(E) || [ "" ];
            j = b.length;
            while (j--) {
                h = Y.exec(b[j]) || [];
                o = q = h[1];
                p = (h[2] || "").split(".").sort();
                // There *must* be a type, no attaching namespace-only handlers
                if (!o) {
                    continue;
                }
                // If event changes its type, use the special event handlers for the changed type
                l = n.event.special[o] || {};
                // If selector defined, determine special event api type, otherwise given type
                o = (e ? l.delegateType : l.bindType) || o;
                // Update special based on newly reset type
                l = n.event.special[o] || {};
                // handleObj is passed to all event handlers
                k = n.extend({
                    type: o,
                    origType: q,
                    data: d,
                    handler: c,
                    guid: c.guid,
                    selector: e,
                    needsContext: e && n.expr.match.needsContext.test(e),
                    namespace: p.join(".")
                }, f);
                // Init the event handler queue if we're the first
                if (!(m = i[o])) {
                    m = i[o] = [];
                    m.delegateCount = 0;
                    // Only use addEventListener if the special events handler returns false
                    if (!l.setup || l.setup.call(a, d, p, g) === false) {
                        if (a.addEventListener) {
                            a.addEventListener(o, g, false);
                        }
                    }
                }
                if (l.add) {
                    l.add.call(a, k);
                    if (!k.handler.guid) {
                        k.handler.guid = c.guid;
                    }
                }
                // Add to the element's handler list, delegates in front
                if (e) {
                    m.splice(m.delegateCount++, 0, k);
                } else {
                    m.push(k);
                }
                // Keep track of which events have ever been used, for event optimization
                n.event.global[o] = true;
            }
        },
        // Detach an event or set of events from an element
        remove: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, o, p, q, r = L.hasData(a) && L.get(a);
            if (!r || !(i = r.events)) {
                return;
            }
            // Once for each type.namespace in types; type may be omitted
            b = (b || "").match(E) || [ "" ];
            j = b.length;
            while (j--) {
                h = Y.exec(b[j]) || [];
                o = q = h[1];
                p = (h[2] || "").split(".").sort();
                // Unbind all events (on this namespace, if provided) for the element
                if (!o) {
                    for (o in i) {
                        n.event.remove(a, o + b[j], c, d, true);
                    }
                    continue;
                }
                l = n.event.special[o] || {};
                o = (d ? l.delegateType : l.bindType) || o;
                m = i[o] || [];
                h = h[2] && new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)");
                // Remove matching events
                g = f = m.length;
                while (f--) {
                    k = m[f];
                    if ((e || q === k.origType) && (!c || c.guid === k.guid) && (!h || h.test(k.namespace)) && (!d || d === k.selector || d === "**" && k.selector)) {
                        m.splice(f, 1);
                        if (k.selector) {
                            m.delegateCount--;
                        }
                        if (l.remove) {
                            l.remove.call(a, k);
                        }
                    }
                }
                // Remove generic event handler if we removed something and no more handlers exist
                // (avoids potential for endless recursion during removal of special event handlers)
                if (g && !m.length) {
                    if (!l.teardown || l.teardown.call(a, p, r.handle) === false) {
                        n.removeEvent(a, o, r.handle);
                    }
                    delete i[o];
                }
            }
            // Remove the expando if it's no longer used
            if (n.isEmptyObject(i)) {
                delete r.handle;
                L.remove(a, "events");
            }
        },
        trigger: function(b, c, d, e) {
            var f, g, h, i, k, m, o, p = [ d || l ], q = j.call(b, "type") ? b.type : b, r = j.call(b, "namespace") ? b.namespace.split(".") : [];
            g = h = d = d || l;
            // Don't do events on text and comment nodes
            if (d.nodeType === 3 || d.nodeType === 8) {
                return;
            }
            // focus/blur morphs to focusin/out; ensure we're not firing them right now
            if (X.test(q + n.event.triggered)) {
                return;
            }
            if (q.indexOf(".") >= 0) {
                // Namespaced trigger; create a regexp to match event type in handle()
                r = q.split(".");
                q = r.shift();
                r.sort();
            }
            k = q.indexOf(":") < 0 && "on" + q;
            // Caller can pass in a jQuery.Event object, Object, or just an event type string
            b = b[n.expando] ? b : new n.Event(q, typeof b === "object" && b);
            // Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
            b.isTrigger = e ? 2 : 3;
            b.namespace = r.join(".");
            b.namespace_re = b.namespace ? new RegExp("(^|\\.)" + r.join("\\.(?:.*\\.|)") + "(\\.|$)") : null;
            // Clean up the event in case it is being reused
            b.result = undefined;
            if (!b.target) {
                b.target = d;
            }
            // Clone any incoming data and prepend the event, creating the handler arg list
            c = c == null ? [ b ] : n.makeArray(c, [ b ]);
            // Allow special events to draw outside the lines
            o = n.event.special[q] || {};
            if (!e && o.trigger && o.trigger.apply(d, c) === false) {
                return;
            }
            // Determine event propagation path in advance, per W3C events spec (#9951)
            // Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
            if (!e && !o.noBubble && !n.isWindow(d)) {
                i = o.delegateType || q;
                if (!X.test(i + q)) {
                    g = g.parentNode;
                }
                for (;g; g = g.parentNode) {
                    p.push(g);
                    h = g;
                }
                // Only add window if we got to document (e.g., not plain obj or detached DOM)
                if (h === (d.ownerDocument || l)) {
                    p.push(h.defaultView || h.parentWindow || a);
                }
            }
            // Fire handlers on the event path
            f = 0;
            while ((g = p[f++]) && !b.isPropagationStopped()) {
                b.type = f > 1 ? i : o.bindType || q;
                // jQuery handler
                m = (L.get(g, "events") || {})[b.type] && L.get(g, "handle");
                if (m) {
                    m.apply(g, c);
                }
                // Native handler
                m = k && g[k];
                if (m && m.apply && n.acceptData(g)) {
                    b.result = m.apply(g, c);
                    if (b.result === false) {
                        b.preventDefault();
                    }
                }
            }
            b.type = q;
            // If nobody prevented the default action, do it now
            if (!e && !b.isDefaultPrevented()) {
                if ((!o._default || o._default.apply(p.pop(), c) === false) && n.acceptData(d)) {
                    // Call a native DOM method on the target with the same name name as the event.
                    // Don't do default actions on window, that's where global variables be (#6170)
                    if (k && n.isFunction(d[q]) && !n.isWindow(d)) {
                        // Don't re-trigger an onFOO event when we call its FOO() method
                        h = d[k];
                        if (h) {
                            d[k] = null;
                        }
                        // Prevent re-triggering of the same event, since we already bubbled it above
                        n.event.triggered = q;
                        d[q]();
                        n.event.triggered = undefined;
                        if (h) {
                            d[k] = h;
                        }
                    }
                }
            }
            return b.result;
        },
        dispatch: function(a) {
            // Make a writable jQuery.Event from the native event object
            a = n.event.fix(a);
            var b, c, e, f, g, h = [], i = d.call(arguments), j = (L.get(this, "events") || {})[a.type] || [], k = n.event.special[a.type] || {};
            // Use the fix-ed jQuery.Event rather than the (read-only) native event
            i[0] = a;
            a.delegateTarget = this;
            // Call the preDispatch hook for the mapped type, and let it bail if desired
            if (k.preDispatch && k.preDispatch.call(this, a) === false) {
                return;
            }
            // Determine handlers
            h = n.event.handlers.call(this, a, j);
            // Run delegates first; they may want to stop propagation beneath us
            b = 0;
            while ((f = h[b++]) && !a.isPropagationStopped()) {
                a.currentTarget = f.elem;
                c = 0;
                while ((g = f.handlers[c++]) && !a.isImmediatePropagationStopped()) {
                    // Triggered event must either 1) have no namespace, or 2) have namespace(s)
                    // a subset or equal to those in the bound event (both can have no namespace).
                    if (!a.namespace_re || a.namespace_re.test(g.namespace)) {
                        a.handleObj = g;
                        a.data = g.data;
                        e = ((n.event.special[g.origType] || {}).handle || g.handler).apply(f.elem, i);
                        if (e !== undefined) {
                            if ((a.result = e) === false) {
                                a.preventDefault();
                                a.stopPropagation();
                            }
                        }
                    }
                }
            }
            // Call the postDispatch hook for the mapped type
            if (k.postDispatch) {
                k.postDispatch.call(this, a);
            }
            return a.result;
        },
        handlers: function(a, b) {
            var c, d, e, f, g = [], h = b.delegateCount, i = a.target;
            // Find delegate handlers
            // Black-hole SVG <use> instance trees (#13180)
            // Avoid non-left-click bubbling in Firefox (#3861)
            if (h && i.nodeType && (!a.button || a.type !== "click")) {
                for (;i !== this; i = i.parentNode || this) {
                    // Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
                    if (i.disabled !== true || a.type !== "click") {
                        d = [];
                        for (c = 0; c < h; c++) {
                            f = b[c];
                            // Don't conflict with Object.prototype properties (#13203)
                            e = f.selector + " ";
                            if (d[e] === undefined) {
                                d[e] = f.needsContext ? n(e, this).index(i) >= 0 : n.find(e, this, null, [ i ]).length;
                            }
                            if (d[e]) {
                                d.push(f);
                            }
                        }
                        if (d.length) {
                            g.push({
                                elem: i,
                                handlers: d
                            });
                        }
                    }
                }
            }
            // Add the remaining (directly-bound) handlers
            if (h < b.length) {
                g.push({
                    elem: this,
                    handlers: b.slice(h)
                });
            }
            return g;
        },
        // Includes some event props shared by KeyEvent and MouseEvent
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(a, b) {
                // Add which for key events
                if (a.which == null) {
                    a.which = b.charCode != null ? b.charCode : b.keyCode;
                }
                return a;
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(a, b) {
                var c, d, e, f = b.button;
                // Calculate pageX/Y if missing and clientX/Y available
                if (a.pageX == null && b.clientX != null) {
                    c = a.target.ownerDocument || l;
                    d = c.documentElement;
                    e = c.body;
                    a.pageX = b.clientX + (d && d.scrollLeft || e && e.scrollLeft || 0) - (d && d.clientLeft || e && e.clientLeft || 0);
                    a.pageY = b.clientY + (d && d.scrollTop || e && e.scrollTop || 0) - (d && d.clientTop || e && e.clientTop || 0);
                }
                // Add which for click: 1 === left; 2 === middle; 3 === right
                // Note: button is not normalized, so don't use it
                if (!a.which && f !== undefined) {
                    a.which = f & 1 ? 1 : f & 2 ? 3 : f & 4 ? 2 : 0;
                }
                return a;
            }
        },
        fix: function(a) {
            if (a[n.expando]) {
                return a;
            }
            // Create a writable copy of the event object and normalize some properties
            var b, c, d, e = a.type, f = a, g = this.fixHooks[e];
            if (!g) {
                this.fixHooks[e] = g = W.test(e) ? this.mouseHooks : V.test(e) ? this.keyHooks : {};
            }
            d = g.props ? this.props.concat(g.props) : this.props;
            a = new n.Event(f);
            b = d.length;
            while (b--) {
                c = d[b];
                a[c] = f[c];
            }
            // Support: Cordova 2.5 (WebKit) (#13255)
            // All events should have a target; Cordova deviceready doesn't
            if (!a.target) {
                a.target = l;
            }
            // Support: Safari 6.0+, Chrome<28
            // Target should not be a text node (#504, #13143)
            if (a.target.nodeType === 3) {
                a.target = a.target.parentNode;
            }
            return g.filter ? g.filter(a, f) : a;
        },
        special: {
            load: {
                // Prevent triggered image.load events from bubbling to window.load
                noBubble: true
            },
            focus: {
                // Fire native event if possible so blur/focus sequence is correct
                trigger: function() {
                    if (this !== _() && this.focus) {
                        this.focus();
                        return false;
                    }
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === _() && this.blur) {
                        this.blur();
                        return false;
                    }
                },
                delegateType: "focusout"
            },
            click: {
                // For checkbox, fire native event so checked state will be right
                trigger: function() {
                    if (this.type === "checkbox" && this.click && n.nodeName(this, "input")) {
                        this.click();
                        return false;
                    }
                },
                // For cross-browser consistency, don't fire native .click() on links
                _default: function(a) {
                    return n.nodeName(a.target, "a");
                }
            },
            beforeunload: {
                postDispatch: function(a) {
                    // Support: Firefox 20+
                    // Firefox doesn't alert if the returnValue field is not set.
                    if (a.result !== undefined && a.originalEvent) {
                        a.originalEvent.returnValue = a.result;
                    }
                }
            }
        },
        simulate: function(a, b, c, d) {
            // Piggyback on a donor event to simulate a different one.
            // Fake originalEvent to avoid donor's stopPropagation, but if the
            // simulated event prevents default then we do the same on the donor.
            var e = n.extend(new n.Event(), c, {
                type: a,
                isSimulated: true,
                originalEvent: {}
            });
            if (d) {
                n.event.trigger(e, null, b);
            } else {
                n.event.dispatch.call(b, e);
            }
            if (e.isDefaultPrevented()) {
                c.preventDefault();
            }
        }
    };
    n.removeEvent = function(a, b, c) {
        if (a.removeEventListener) {
            a.removeEventListener(b, c, false);
        }
    };
    n.Event = function(a, b) {
        // Allow instantiation without the 'new' keyword
        if (!(this instanceof n.Event)) {
            return new n.Event(a, b);
        }
        // Event object
        if (a && a.type) {
            this.originalEvent = a;
            this.type = a.type;
            // Events bubbling up the document may have been marked as prevented
            // by a handler lower down the tree; reflect the correct value.
            this.isDefaultPrevented = a.defaultPrevented || a.defaultPrevented === undefined && // Support: Android<4.0
            a.returnValue === false ? Z : $;
        } else {
            this.type = a;
        }
        // Put explicitly provided properties onto the event object
        if (b) {
            n.extend(this, b);
        }
        // Create a timestamp if incoming event doesn't have one
        this.timeStamp = a && a.timeStamp || n.now();
        // Mark it as fixed
        this[n.expando] = true;
    };
    // jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
    // http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
    n.Event.prototype = {
        isDefaultPrevented: $,
        isPropagationStopped: $,
        isImmediatePropagationStopped: $,
        preventDefault: function() {
            var a = this.originalEvent;
            this.isDefaultPrevented = Z;
            if (a && a.preventDefault) {
                a.preventDefault();
            }
        },
        stopPropagation: function() {
            var a = this.originalEvent;
            this.isPropagationStopped = Z;
            if (a && a.stopPropagation) {
                a.stopPropagation();
            }
        },
        stopImmediatePropagation: function() {
            var a = this.originalEvent;
            this.isImmediatePropagationStopped = Z;
            if (a && a.stopImmediatePropagation) {
                a.stopImmediatePropagation();
            }
            this.stopPropagation();
        }
    };
    // Create mouseenter/leave events using mouseover/out and event-time checks
    // Support: Chrome 15+
    n.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(a, b) {
        n.event.special[a] = {
            delegateType: b,
            bindType: b,
            handle: function(a) {
                var c, d = this, e = a.relatedTarget, f = a.handleObj;
                // For mousenter/leave call the handler if related is outside the target.
                // NB: No relatedTarget if the mouse left/entered the browser window
                if (!e || e !== d && !n.contains(d, e)) {
                    a.type = f.origType;
                    c = f.handler.apply(this, arguments);
                    a.type = b;
                }
                return c;
            }
        };
    });
    // Support: Firefox, Chrome, Safari
    // Create "bubbling" focus and blur events
    if (!k.focusinBubbles) {
        n.each({
            focus: "focusin",
            blur: "focusout"
        }, function(a, b) {
            // Attach a single capturing handler on the document while someone wants focusin/focusout
            var c = function(a) {
                n.event.simulate(b, a.target, n.event.fix(a), true);
            };
            n.event.special[b] = {
                setup: function() {
                    var d = this.ownerDocument || this, e = L.access(d, b);
                    if (!e) {
                        d.addEventListener(a, c, true);
                    }
                    L.access(d, b, (e || 0) + 1);
                },
                teardown: function() {
                    var d = this.ownerDocument || this, e = L.access(d, b) - 1;
                    if (!e) {
                        d.removeEventListener(a, c, true);
                        L.remove(d, b);
                    } else {
                        L.access(d, b, e);
                    }
                }
            };
        });
    }
    n.fn.extend({
        on: function(a, b, c, d, /*INTERNAL*/ e) {
            var f, g;
            // Types can be a map of types/handlers
            if (typeof a === "object") {
                // ( types-Object, selector, data )
                if (typeof b !== "string") {
                    // ( types-Object, data )
                    c = c || b;
                    b = undefined;
                }
                for (g in a) {
                    this.on(g, b, c, a[g], e);
                }
                return this;
            }
            if (c == null && d == null) {
                // ( types, fn )
                d = b;
                c = b = undefined;
            } else if (d == null) {
                if (typeof b === "string") {
                    // ( types, selector, fn )
                    d = c;
                    c = undefined;
                } else {
                    // ( types, data, fn )
                    d = c;
                    c = b;
                    b = undefined;
                }
            }
            if (d === false) {
                d = $;
            } else if (!d) {
                return this;
            }
            if (e === 1) {
                f = d;
                d = function(a) {
                    // Can use an empty set, since event contains the info
                    n().off(a);
                    return f.apply(this, arguments);
                };
                // Use same guid so caller can remove using origFn
                d.guid = f.guid || (f.guid = n.guid++);
            }
            return this.each(function() {
                n.event.add(this, a, d, c, b);
            });
        },
        one: function(a, b, c, d) {
            return this.on(a, b, c, d, 1);
        },
        off: function(a, b, c) {
            var d, e;
            if (a && a.preventDefault && a.handleObj) {
                // ( event )  dispatched jQuery.Event
                d = a.handleObj;
                n(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler);
                return this;
            }
            if (typeof a === "object") {
                // ( types-object [, selector] )
                for (e in a) {
                    this.off(e, b, a[e]);
                }
                return this;
            }
            if (b === false || typeof b === "function") {
                // ( types [, fn] )
                c = b;
                b = undefined;
            }
            if (c === false) {
                c = $;
            }
            return this.each(function() {
                n.event.remove(this, a, c, b);
            });
        },
        trigger: function(a, b) {
            return this.each(function() {
                n.event.trigger(a, b, this);
            });
        },
        triggerHandler: function(a, b) {
            var c = this[0];
            if (c) {
                return n.event.trigger(a, b, c, true);
            }
        }
    });
    var aa = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, ba = /<([\w:]+)/, ca = /<|&#?\w+;/, da = /<(?:script|style|link)/i, // checked="checked" or checked
    ea = /checked\s*(?:[^=]|=\s*.checked.)/i, fa = /^$|\/(?:java|ecma)script/i, ga = /^true\/(.*)/, ha = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, // We have to close these tags to support XHTML (#13200)
    ia = {
        // Support: IE9
        option: [ 1, "<select multiple='multiple'>", "</select>" ],
        thead: [ 1, "<table>", "</table>" ],
        col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
        tr: [ 2, "<table><tbody>", "</tbody></table>" ],
        td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],
        _default: [ 0, "", "" ]
    };
    // Support: IE9
    ia.optgroup = ia.option;
    ia.tbody = ia.tfoot = ia.colgroup = ia.caption = ia.thead;
    ia.th = ia.td;
    // Support: 1.x compatibility
    // Manipulating tables requires a tbody
    function ja(a, b) {
        return n.nodeName(a, "table") && n.nodeName(b.nodeType !== 11 ? b : b.firstChild, "tr") ? a.getElementsByTagName("tbody")[0] || a.appendChild(a.ownerDocument.createElement("tbody")) : a;
    }
    // Replace/restore the type attribute of script elements for safe DOM manipulation
    function ka(a) {
        a.type = (a.getAttribute("type") !== null) + "/" + a.type;
        return a;
    }
    function la(a) {
        var b = ga.exec(a.type);
        if (b) {
            a.type = b[1];
        } else {
            a.removeAttribute("type");
        }
        return a;
    }
    // Mark scripts as having already been evaluated
    function ma(a, b) {
        var c = 0, d = a.length;
        for (;c < d; c++) {
            L.set(a[c], "globalEval", !b || L.get(b[c], "globalEval"));
        }
    }
    function na(a, b) {
        var c, d, e, f, g, h, i, j;
        if (b.nodeType !== 1) {
            return;
        }
        // 1. Copy private data: events, handlers, etc.
        if (L.hasData(a)) {
            f = L.access(a);
            g = L.set(b, f);
            j = f.events;
            if (j) {
                delete g.handle;
                g.events = {};
                for (e in j) {
                    for (c = 0, d = j[e].length; c < d; c++) {
                        n.event.add(b, e, j[e][c]);
                    }
                }
            }
        }
        // 2. Copy user data
        if (M.hasData(a)) {
            h = M.access(a);
            i = n.extend({}, h);
            M.set(b, i);
        }
    }
    function oa(a, b) {
        var c = a.getElementsByTagName ? a.getElementsByTagName(b || "*") : a.querySelectorAll ? a.querySelectorAll(b || "*") : [];
        return b === undefined || b && n.nodeName(a, b) ? n.merge([ a ], c) : c;
    }
    // Fix IE bugs, see support tests
    function pa(a, b) {
        var c = b.nodeName.toLowerCase();
        // Fails to persist the checked state of a cloned checkbox or radio button.
        if (c === "input" && T.test(a.type)) {
            b.checked = a.checked;
        } else if (c === "input" || c === "textarea") {
            b.defaultValue = a.defaultValue;
        }
    }
    n.extend({
        clone: function(a, b, c) {
            var d, e, f, g, h = a.cloneNode(true), i = n.contains(a.ownerDocument, a);
            // Fix IE cloning issues
            if (!k.noCloneChecked && (a.nodeType === 1 || a.nodeType === 11) && !n.isXMLDoc(a)) {
                // We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2
                g = oa(h);
                f = oa(a);
                for (d = 0, e = f.length; d < e; d++) {
                    pa(f[d], g[d]);
                }
            }
            // Copy the events from the original to the clone
            if (b) {
                if (c) {
                    f = f || oa(a);
                    g = g || oa(h);
                    for (d = 0, e = f.length; d < e; d++) {
                        na(f[d], g[d]);
                    }
                } else {
                    na(a, h);
                }
            }
            // Preserve script evaluation history
            g = oa(h, "script");
            if (g.length > 0) {
                ma(g, !i && oa(a, "script"));
            }
            // Return the cloned set
            return h;
        },
        buildFragment: function(a, b, c, d) {
            var e, f, g, h, i, j, k = b.createDocumentFragment(), l = [], m = 0, o = a.length;
            for (;m < o; m++) {
                e = a[m];
                if (e || e === 0) {
                    // Add nodes directly
                    if (n.type(e) === "object") {
                        // Support: QtWebKit, PhantomJS
                        // push.apply(_, arraylike) throws on ancient WebKit
                        n.merge(l, e.nodeType ? [ e ] : e);
                    } else if (!ca.test(e)) {
                        l.push(b.createTextNode(e));
                    } else {
                        f = f || k.appendChild(b.createElement("div"));
                        // Deserialize a standard representation
                        g = (ba.exec(e) || [ "", "" ])[1].toLowerCase();
                        h = ia[g] || ia._default;
                        f.innerHTML = h[1] + e.replace(aa, "<$1></$2>") + h[2];
                        // Descend through wrappers to the right content
                        j = h[0];
                        while (j--) {
                            f = f.lastChild;
                        }
                        // Support: QtWebKit, PhantomJS
                        // push.apply(_, arraylike) throws on ancient WebKit
                        n.merge(l, f.childNodes);
                        // Remember the top-level container
                        f = k.firstChild;
                        // Ensure the created nodes are orphaned (#12392)
                        f.textContent = "";
                    }
                }
            }
            // Remove wrapper from fragment
            k.textContent = "";
            m = 0;
            while (e = l[m++]) {
                // #4087 - If origin and destination elements are the same, and this is
                // that element, do not do anything
                if (d && n.inArray(e, d) !== -1) {
                    continue;
                }
                i = n.contains(e.ownerDocument, e);
                // Append to fragment
                f = oa(k.appendChild(e), "script");
                // Preserve script evaluation history
                if (i) {
                    ma(f);
                }
                // Capture executables
                if (c) {
                    j = 0;
                    while (e = f[j++]) {
                        if (fa.test(e.type || "")) {
                            c.push(e);
                        }
                    }
                }
            }
            return k;
        },
        cleanData: function(a) {
            var b, c, d, e, f = n.event.special, g = 0;
            for (;(c = a[g]) !== undefined; g++) {
                if (n.acceptData(c)) {
                    e = c[L.expando];
                    if (e && (b = L.cache[e])) {
                        if (b.events) {
                            for (d in b.events) {
                                if (f[d]) {
                                    n.event.remove(c, d);
                                } else {
                                    n.removeEvent(c, d, b.handle);
                                }
                            }
                        }
                        if (L.cache[e]) {
                            // Discard any remaining `private` data
                            delete L.cache[e];
                        }
                    }
                }
                // Discard any remaining `user` data
                delete M.cache[c[M.expando]];
            }
        }
    });
    n.fn.extend({
        text: function(a) {
            return J(this, function(a) {
                return a === undefined ? n.text(this) : this.empty().each(function() {
                    if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                        this.textContent = a;
                    }
                });
            }, null, a, arguments.length);
        },
        append: function() {
            return this.domManip(arguments, function(a) {
                if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                    var b = ja(this, a);
                    b.appendChild(a);
                }
            });
        },
        prepend: function() {
            return this.domManip(arguments, function(a) {
                if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                    var b = ja(this, a);
                    b.insertBefore(a, b.firstChild);
                }
            });
        },
        before: function() {
            return this.domManip(arguments, function(a) {
                if (this.parentNode) {
                    this.parentNode.insertBefore(a, this);
                }
            });
        },
        after: function() {
            return this.domManip(arguments, function(a) {
                if (this.parentNode) {
                    this.parentNode.insertBefore(a, this.nextSibling);
                }
            });
        },
        remove: function(a, b) {
            var c, d = a ? n.filter(a, this) : this, e = 0;
            for (;(c = d[e]) != null; e++) {
                if (!b && c.nodeType === 1) {
                    n.cleanData(oa(c));
                }
                if (c.parentNode) {
                    if (b && n.contains(c.ownerDocument, c)) {
                        ma(oa(c, "script"));
                    }
                    c.parentNode.removeChild(c);
                }
            }
            return this;
        },
        empty: function() {
            var a, b = 0;
            for (;(a = this[b]) != null; b++) {
                if (a.nodeType === 1) {
                    // Prevent memory leaks
                    n.cleanData(oa(a, false));
                    // Remove any remaining nodes
                    a.textContent = "";
                }
            }
            return this;
        },
        clone: function(a, b) {
            a = a == null ? false : a;
            b = b == null ? a : b;
            return this.map(function() {
                return n.clone(this, a, b);
            });
        },
        html: function(a) {
            return J(this, function(a) {
                var b = this[0] || {}, c = 0, d = this.length;
                if (a === undefined && b.nodeType === 1) {
                    return b.innerHTML;
                }
                // See if we can take a shortcut and just use innerHTML
                if (typeof a === "string" && !da.test(a) && !ia[(ba.exec(a) || [ "", "" ])[1].toLowerCase()]) {
                    a = a.replace(aa, "<$1></$2>");
                    try {
                        for (;c < d; c++) {
                            b = this[c] || {};
                            // Remove element nodes and prevent memory leaks
                            if (b.nodeType === 1) {
                                n.cleanData(oa(b, false));
                                b.innerHTML = a;
                            }
                        }
                        b = 0;
                    } catch (e) {}
                }
                if (b) {
                    this.empty().append(a);
                }
            }, null, a, arguments.length);
        },
        replaceWith: function() {
            var a = arguments[0];
            // Make the changes, replacing each context element with the new content
            this.domManip(arguments, function(b) {
                a = this.parentNode;
                n.cleanData(oa(this));
                if (a) {
                    a.replaceChild(b, this);
                }
            });
            // Force removal if there was no new content (e.g., from empty arguments)
            return a && (a.length || a.nodeType) ? this : this.remove();
        },
        detach: function(a) {
            return this.remove(a, true);
        },
        domManip: function(a, b) {
            // Flatten any nested arrays
            a = e.apply([], a);
            var c, d, f, g, h, i, j = 0, l = this.length, m = this, o = l - 1, p = a[0], q = n.isFunction(p);
            // We can't cloneNode fragments that contain checked, in WebKit
            if (q || l > 1 && typeof p === "string" && !k.checkClone && ea.test(p)) {
                return this.each(function(c) {
                    var d = m.eq(c);
                    if (q) {
                        a[0] = p.call(this, c, d.html());
                    }
                    d.domManip(a, b);
                });
            }
            if (l) {
                c = n.buildFragment(a, this[0].ownerDocument, false, this);
                d = c.firstChild;
                if (c.childNodes.length === 1) {
                    c = d;
                }
                if (d) {
                    f = n.map(oa(c, "script"), ka);
                    g = f.length;
                    // Use the original fragment for the last item instead of the first because it can end up
                    // being emptied incorrectly in certain situations (#8070).
                    for (;j < l; j++) {
                        h = c;
                        if (j !== o) {
                            h = n.clone(h, true, true);
                            // Keep references to cloned scripts for later restoration
                            if (g) {
                                // Support: QtWebKit
                                // jQuery.merge because push.apply(_, arraylike) throws
                                n.merge(f, oa(h, "script"));
                            }
                        }
                        b.call(this[j], h, j);
                    }
                    if (g) {
                        i = f[f.length - 1].ownerDocument;
                        // Reenable scripts
                        n.map(f, la);
                        // Evaluate executable scripts on first document insertion
                        for (j = 0; j < g; j++) {
                            h = f[j];
                            if (fa.test(h.type || "") && !L.access(h, "globalEval") && n.contains(i, h)) {
                                if (h.src) {
                                    // Optional AJAX dependency, but won't run scripts if not present
                                    if (n._evalUrl) {
                                        n._evalUrl(h.src);
                                    }
                                } else {
                                    n.globalEval(h.textContent.replace(ha, ""));
                                }
                            }
                        }
                    }
                }
            }
            return this;
        }
    });
    n.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(a, b) {
        n.fn[a] = function(a) {
            var c, d = [], e = n(a), g = e.length - 1, h = 0;
            for (;h <= g; h++) {
                c = h === g ? this : this.clone(true);
                n(e[h])[b](c);
                // Support: QtWebKit
                // .get() because push.apply(_, arraylike) throws
                f.apply(d, c.get());
            }
            return this.pushStack(d);
        };
    });
    var qa, ra = {};
    /**
 * Retrieve the actual display of a element
 * @param {String} name nodeName of the element
 * @param {Object} doc Document object
 */
    // Called only from within defaultDisplay
    function sa(b, c) {
        var d, e = n(c.createElement(b)).appendTo(c.body), // getDefaultComputedStyle might be reliably used only on attached element
        f = a.getDefaultComputedStyle && (d = a.getDefaultComputedStyle(e[0])) ? // Use of this method is a temporary fix (more like optimization) until something better comes along,
        // since it was removed from specification and supported only in FF
        d.display : n.css(e[0], "display");
        // We don't have any data stored on the element,
        // so use "detach" method as fast way to get rid of the element
        e.detach();
        return f;
    }
    /**
 * Try to determine the default display value of an element
 * @param {String} nodeName
 */
    function ta(a) {
        var b = l, c = ra[a];
        if (!c) {
            c = sa(a, b);
            // If the simple way fails, read from inside an iframe
            if (c === "none" || !c) {
                // Use the already-created iframe if possible
                qa = (qa || n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement);
                // Always write a new HTML skeleton so Webkit and Firefox don't choke on reuse
                b = qa[0].contentDocument;
                // Support: IE
                b.write();
                b.close();
                c = sa(a, b);
                qa.detach();
            }
            // Store the correct default display
            ra[a] = c;
        }
        return c;
    }
    var ua = /^margin/;
    var va = new RegExp("^(" + Q + ")(?!px)[a-z%]+$", "i");
    var wa = function(b) {
        // Support: IE<=11+, Firefox<=30+ (#15098, #14150)
        // IE throws on elements created in popups
        // FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
        if (b.ownerDocument.defaultView.opener) {
            return b.ownerDocument.defaultView.getComputedStyle(b, null);
        }
        return a.getComputedStyle(b, null);
    };
    function xa(a, b, c) {
        var d, e, f, g, h = a.style;
        c = c || wa(a);
        // Support: IE9
        // getPropertyValue is only needed for .css('filter') (#12537)
        if (c) {
            g = c.getPropertyValue(b) || c[b];
        }
        if (c) {
            if (g === "" && !n.contains(a.ownerDocument, a)) {
                g = n.style(a, b);
            }
            // Support: iOS < 6
            // A tribute to the "awesome hack by Dean Edwards"
            // iOS < 6 (at least) returns percentage for a larger set of values, but width seems to be reliably pixels
            // this is against the CSSOM draft spec: http://dev.w3.org/csswg/cssom/#resolved-values
            if (va.test(g) && ua.test(b)) {
                // Remember the original values
                d = h.width;
                e = h.minWidth;
                f = h.maxWidth;
                // Put in the new values to get a computed value out
                h.minWidth = h.maxWidth = h.width = g;
                g = c.width;
                // Revert the changed values
                h.width = d;
                h.minWidth = e;
                h.maxWidth = f;
            }
        }
        // Support: IE
        // IE returns zIndex value as an integer.
        return g !== undefined ? g + "" : g;
    }
    function ya(a, b) {
        // Define the hook, we'll check on the first run if it's really needed.
        return {
            get: function() {
                if (a()) {
                    // Hook not needed (or it's not possible to use it due
                    // to missing dependency), remove it.
                    delete this.get;
                    return;
                }
                // Hook needed; redefine it so that the support test is not executed again.
                return (this.get = b).apply(this, arguments);
            }
        };
    }
    (function() {
        var b, c, d = l.documentElement, e = l.createElement("div"), f = l.createElement("div");
        if (!f.style) {
            return;
        }
        // Support: IE9-11+
        // Style of cloned element affects source element cloned (#8908)
        f.style.backgroundClip = "content-box";
        f.cloneNode(true).style.backgroundClip = "";
        k.clearCloneStyle = f.style.backgroundClip === "content-box";
        e.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;" + "position:absolute";
        e.appendChild(f);
        // Executing both pixelPosition & boxSizingReliable tests require only one layout
        // so they're executed at the same time to save the second computation.
        function g() {
            f.style.cssText = // Support: Firefox<29, Android 2.3
            // Vendor-prefix box-sizing
            "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;" + "box-sizing:border-box;display:block;margin-top:1%;top:1%;" + "border:1px;padding:1px;width:4px;position:absolute";
            f.innerHTML = "";
            d.appendChild(e);
            var g = a.getComputedStyle(f, null);
            b = g.top !== "1%";
            c = g.width === "4px";
            d.removeChild(e);
        }
        // Support: node.js jsdom
        // Don't assume that getComputedStyle is a property of the global object
        if (a.getComputedStyle) {
            n.extend(k, {
                pixelPosition: function() {
                    // This test is executed only once but we still do memoizing
                    // since we can use the boxSizingReliable pre-computing.
                    // No need to check if the test was already performed, though.
                    g();
                    return b;
                },
                boxSizingReliable: function() {
                    if (c == null) {
                        g();
                    }
                    return c;
                },
                reliableMarginRight: function() {
                    // Support: Android 2.3
                    // Check if div with explicit width and no margin-right incorrectly
                    // gets computed margin-right based on width of container. (#3333)
                    // WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
                    // This support function is only executed once so no memoizing is needed.
                    var b, c = f.appendChild(l.createElement("div"));
                    // Reset CSS: box-sizing; display; margin; border; padding
                    c.style.cssText = f.style.cssText = // Support: Firefox<29, Android 2.3
                    // Vendor-prefix box-sizing
                    "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;" + "box-sizing:content-box;display:block;margin:0;border:0;padding:0";
                    c.style.marginRight = c.style.width = "0";
                    f.style.width = "1px";
                    d.appendChild(e);
                    b = !parseFloat(a.getComputedStyle(c, null).marginRight);
                    d.removeChild(e);
                    f.removeChild(c);
                    return b;
                }
            });
        }
    })();
    // A method for quickly swapping in/out CSS properties to get correct calculations.
    n.swap = function(a, b, c, d) {
        var e, f, g = {};
        // Remember the old values, and insert the new ones
        for (f in b) {
            g[f] = a.style[f];
            a.style[f] = b[f];
        }
        e = c.apply(a, d || []);
        // Revert the old values
        for (f in b) {
            a.style[f] = g[f];
        }
        return e;
    };
    var // Swappable if display is none or starts with table except "table", "table-cell", or "table-caption"
    // See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
    za = /^(none|table(?!-c[ea]).+)/, Aa = new RegExp("^(" + Q + ")(.*)$", "i"), Ba = new RegExp("^([+-])=(" + Q + ")", "i"), Ca = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
    }, Da = {
        letterSpacing: "0",
        fontWeight: "400"
    }, Ea = [ "Webkit", "O", "Moz", "ms" ];
    // Return a css property mapped to a potentially vendor prefixed property
    function Fa(a, b) {
        // Shortcut for names that are not vendor prefixed
        if (b in a) {
            return b;
        }
        // Check for vendor prefixed names
        var c = b[0].toUpperCase() + b.slice(1), d = b, e = Ea.length;
        while (e--) {
            b = Ea[e] + c;
            if (b in a) {
                return b;
            }
        }
        return d;
    }
    function Ga(a, b, c) {
        var d = Aa.exec(b);
        // Guard against undefined "subtract", e.g., when used as in cssHooks
        return d ? Math.max(0, d[1] - (c || 0)) + (d[2] || "px") : b;
    }
    function Ha(a, b, c, d, e) {
        var f = c === (d ? "border" : "content") ? // If we already have the right measurement, avoid augmentation
        4 : // Otherwise initialize for horizontal or vertical properties
        b === "width" ? 1 : 0, g = 0;
        for (;f < 4; f += 2) {
            // Both box models exclude margin, so add it if we want it
            if (c === "margin") {
                g += n.css(a, c + R[f], true, e);
            }
            if (d) {
                // border-box includes padding, so remove it if we want content
                if (c === "content") {
                    g -= n.css(a, "padding" + R[f], true, e);
                }
                // At this point, extra isn't border nor margin, so remove border
                if (c !== "margin") {
                    g -= n.css(a, "border" + R[f] + "Width", true, e);
                }
            } else {
                // At this point, extra isn't content, so add padding
                g += n.css(a, "padding" + R[f], true, e);
                // At this point, extra isn't content nor padding, so add border
                if (c !== "padding") {
                    g += n.css(a, "border" + R[f] + "Width", true, e);
                }
            }
        }
        return g;
    }
    function Ia(a, b, c) {
        // Start with offset property, which is equivalent to the border-box value
        var d = true, e = b === "width" ? a.offsetWidth : a.offsetHeight, f = wa(a), g = n.css(a, "boxSizing", false, f) === "border-box";
        // Some non-html elements return undefined for offsetWidth, so check for null/undefined
        // svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
        // MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
        if (e <= 0 || e == null) {
            // Fall back to computed then uncomputed css if necessary
            e = xa(a, b, f);
            if (e < 0 || e == null) {
                e = a.style[b];
            }
            // Computed unit is not pixels. Stop here and return.
            if (va.test(e)) {
                return e;
            }
            // Check for style in case a browser which returns unreliable values
            // for getComputedStyle silently falls back to the reliable elem.style
            d = g && (k.boxSizingReliable() || e === a.style[b]);
            // Normalize "", auto, and prepare for extra
            e = parseFloat(e) || 0;
        }
        // Use the active box-sizing model to add/subtract irrelevant styles
        return e + Ha(a, b, c || (g ? "border" : "content"), d, f) + "px";
    }
    function Ja(a, b) {
        var c, d, e, f = [], g = 0, h = a.length;
        for (;g < h; g++) {
            d = a[g];
            if (!d.style) {
                continue;
            }
            f[g] = L.get(d, "olddisplay");
            c = d.style.display;
            if (b) {
                // Reset the inline display of this element to learn if it is
                // being hidden by cascaded rules or not
                if (!f[g] && c === "none") {
                    d.style.display = "";
                }
                // Set elements which have been overridden with display: none
                // in a stylesheet to whatever the default browser style is
                // for such an element
                if (d.style.display === "" && S(d)) {
                    f[g] = L.access(d, "olddisplay", ta(d.nodeName));
                }
            } else {
                e = S(d);
                if (c !== "none" || !e) {
                    L.set(d, "olddisplay", e ? c : n.css(d, "display"));
                }
            }
        }
        // Set the display of most of the elements in a second loop
        // to avoid the constant reflow
        for (g = 0; g < h; g++) {
            d = a[g];
            if (!d.style) {
                continue;
            }
            if (!b || d.style.display === "none" || d.style.display === "") {
                d.style.display = b ? f[g] || "" : "none";
            }
        }
        return a;
    }
    n.extend({
        // Add in style property hooks for overriding the default
        // behavior of getting and setting a style property
        cssHooks: {
            opacity: {
                get: function(a, b) {
                    if (b) {
                        // We should always get a number back from opacity
                        var c = xa(a, "opacity");
                        return c === "" ? "1" : c;
                    }
                }
            }
        },
        // Don't automatically add "px" to these possibly-unitless properties
        cssNumber: {
            columnCount: true,
            fillOpacity: true,
            flexGrow: true,
            flexShrink: true,
            fontWeight: true,
            lineHeight: true,
            opacity: true,
            order: true,
            orphans: true,
            widows: true,
            zIndex: true,
            zoom: true
        },
        // Add in properties whose names you wish to fix before
        // setting or getting the value
        cssProps: {
            "float": "cssFloat"
        },
        // Get and set the style property on a DOM Node
        style: function(a, b, c, d) {
            // Don't set styles on text and comment nodes
            if (!a || a.nodeType === 3 || a.nodeType === 8 || !a.style) {
                return;
            }
            // Make sure that we're working with the right name
            var e, f, g, h = n.camelCase(b), i = a.style;
            b = n.cssProps[h] || (n.cssProps[h] = Fa(i, h));
            // Gets hook for the prefixed version, then unprefixed version
            g = n.cssHooks[b] || n.cssHooks[h];
            // Check if we're setting a value
            if (c !== undefined) {
                f = typeof c;
                // Convert "+=" or "-=" to relative numbers (#7345)
                if (f === "string" && (e = Ba.exec(c))) {
                    c = (e[1] + 1) * e[2] + parseFloat(n.css(a, b));
                    // Fixes bug #9237
                    f = "number";
                }
                // Make sure that null and NaN values aren't set (#7116)
                if (c == null || c !== c) {
                    return;
                }
                // If a number, add 'px' to the (except for certain CSS properties)
                if (f === "number" && !n.cssNumber[h]) {
                    c += "px";
                }
                // Support: IE9-11+
                // background-* props affect original clone's values
                if (!k.clearCloneStyle && c === "" && b.indexOf("background") === 0) {
                    i[b] = "inherit";
                }
                // If a hook was provided, use that value, otherwise just set the specified value
                if (!g || !("set" in g) || (c = g.set(a, c, d)) !== undefined) {
                    i[b] = c;
                }
            } else {
                // If a hook was provided get the non-computed value from there
                if (g && "get" in g && (e = g.get(a, false, d)) !== undefined) {
                    return e;
                }
                // Otherwise just get the value from the style object
                return i[b];
            }
        },
        css: function(a, b, c, d) {
            var e, f, g, h = n.camelCase(b);
            // Make sure that we're working with the right name
            b = n.cssProps[h] || (n.cssProps[h] = Fa(a.style, h));
            // Try prefixed name followed by the unprefixed name
            g = n.cssHooks[b] || n.cssHooks[h];
            // If a hook was provided get the computed value from there
            if (g && "get" in g) {
                e = g.get(a, true, c);
            }
            // Otherwise, if a way to get the computed value exists, use that
            if (e === undefined) {
                e = xa(a, b, d);
            }
            // Convert "normal" to computed value
            if (e === "normal" && b in Da) {
                e = Da[b];
            }
            // Make numeric if forced or a qualifier was provided and val looks numeric
            if (c === "" || c) {
                f = parseFloat(e);
                return c === true || n.isNumeric(f) ? f || 0 : e;
            }
            return e;
        }
    });
    n.each([ "height", "width" ], function(a, b) {
        n.cssHooks[b] = {
            get: function(a, c, d) {
                if (c) {
                    // Certain elements can have dimension info if we invisibly show them
                    // but it must have a current display style that would benefit
                    return za.test(n.css(a, "display")) && a.offsetWidth === 0 ? n.swap(a, Ca, function() {
                        return Ia(a, b, d);
                    }) : Ia(a, b, d);
                }
            },
            set: function(a, c, d) {
                var e = d && wa(a);
                return Ga(a, c, d ? Ha(a, b, d, n.css(a, "boxSizing", false, e) === "border-box", e) : 0);
            }
        };
    });
    // Support: Android 2.3
    n.cssHooks.marginRight = ya(k.reliableMarginRight, function(a, b) {
        if (b) {
            return n.swap(a, {
                display: "inline-block"
            }, xa, [ a, "marginRight" ]);
        }
    });
    // These hooks are used by animate to expand properties
    n.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(a, b) {
        n.cssHooks[a + b] = {
            expand: function(c) {
                var d = 0, e = {}, // Assumes a single number if not a string
                f = typeof c === "string" ? c.split(" ") : [ c ];
                for (;d < 4; d++) {
                    e[a + R[d] + b] = f[d] || f[d - 2] || f[0];
                }
                return e;
            }
        };
        if (!ua.test(a)) {
            n.cssHooks[a + b].set = Ga;
        }
    });
    n.fn.extend({
        css: function(a, b) {
            return J(this, function(a, b, c) {
                var d, e, f = {}, g = 0;
                if (n.isArray(b)) {
                    d = wa(a);
                    e = b.length;
                    for (;g < e; g++) {
                        f[b[g]] = n.css(a, b[g], false, d);
                    }
                    return f;
                }
                return c !== undefined ? n.style(a, b, c) : n.css(a, b);
            }, a, b, arguments.length > 1);
        },
        show: function() {
            return Ja(this, true);
        },
        hide: function() {
            return Ja(this);
        },
        toggle: function(a) {
            if (typeof a === "boolean") {
                return a ? this.show() : this.hide();
            }
            return this.each(function() {
                if (S(this)) {
                    n(this).show();
                } else {
                    n(this).hide();
                }
            });
        }
    });
    function Ka(a, b, c, d, e) {
        return new Ka.prototype.init(a, b, c, d, e);
    }
    n.Tween = Ka;
    Ka.prototype = {
        constructor: Ka,
        init: function(a, b, c, d, e, f) {
            this.elem = a;
            this.prop = c;
            this.easing = e || "swing";
            this.options = b;
            this.start = this.now = this.cur();
            this.end = d;
            this.unit = f || (n.cssNumber[c] ? "" : "px");
        },
        cur: function() {
            var a = Ka.propHooks[this.prop];
            return a && a.get ? a.get(this) : Ka.propHooks._default.get(this);
        },
        run: function(a) {
            var b, c = Ka.propHooks[this.prop];
            if (this.options.duration) {
                this.pos = b = n.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration);
            } else {
                this.pos = b = a;
            }
            this.now = (this.end - this.start) * b + this.start;
            if (this.options.step) {
                this.options.step.call(this.elem, this.now, this);
            }
            if (c && c.set) {
                c.set(this);
            } else {
                Ka.propHooks._default.set(this);
            }
            return this;
        }
    };
    Ka.prototype.init.prototype = Ka.prototype;
    Ka.propHooks = {
        _default: {
            get: function(a) {
                var b;
                if (a.elem[a.prop] != null && (!a.elem.style || a.elem.style[a.prop] == null)) {
                    return a.elem[a.prop];
                }
                // Passing an empty string as a 3rd parameter to .css will automatically
                // attempt a parseFloat and fallback to a string if the parse fails.
                // Simple values such as "10px" are parsed to Float;
                // complex values such as "rotate(1rad)" are returned as-is.
                b = n.css(a.elem, a.prop, "");
                // Empty strings, null, undefined and "auto" are converted to 0.
                return !b || b === "auto" ? 0 : b;
            },
            set: function(a) {
                // Use step hook for back compat.
                // Use cssHook if its there.
                // Use .style if available and use plain properties where available.
                if (n.fx.step[a.prop]) {
                    n.fx.step[a.prop](a);
                } else if (a.elem.style && (a.elem.style[n.cssProps[a.prop]] != null || n.cssHooks[a.prop])) {
                    n.style(a.elem, a.prop, a.now + a.unit);
                } else {
                    a.elem[a.prop] = a.now;
                }
            }
        }
    };
    // Support: IE9
    // Panic based approach to setting things on disconnected nodes
    Ka.propHooks.scrollTop = Ka.propHooks.scrollLeft = {
        set: function(a) {
            if (a.elem.nodeType && a.elem.parentNode) {
                a.elem[a.prop] = a.now;
            }
        }
    };
    n.easing = {
        linear: function(a) {
            return a;
        },
        swing: function(a) {
            return .5 - Math.cos(a * Math.PI) / 2;
        }
    };
    n.fx = Ka.prototype.init;
    // Back Compat <1.8 extension point
    n.fx.step = {};
    var La, Ma, Na = /^(?:toggle|show|hide)$/, Oa = new RegExp("^(?:([+-])=|)(" + Q + ")([a-z%]*)$", "i"), Pa = /queueHooks$/, Qa = [ Va ], Ra = {
        "*": [ function(a, b) {
            var c = this.createTween(a, b), d = c.cur(), e = Oa.exec(b), f = e && e[3] || (n.cssNumber[a] ? "" : "px"), // Starting value computation is required for potential unit mismatches
            g = (n.cssNumber[a] || f !== "px" && +d) && Oa.exec(n.css(c.elem, a)), h = 1, i = 20;
            if (g && g[3] !== f) {
                // Trust units reported by jQuery.css
                f = f || g[3];
                // Make sure we update the tween properties later on
                e = e || [];
                // Iteratively approximate from a nonzero starting point
                g = +d || 1;
                do {
                    // If previous iteration zeroed out, double until we get *something*.
                    // Use string for doubling so we don't accidentally see scale as unchanged below
                    h = h || ".5";
                    // Adjust and apply
                    g = g / h;
                    n.style(c.elem, a, g + f);
                } while (h !== (h = c.cur() / d) && h !== 1 && --i);
            }
            // Update tween properties
            if (e) {
                g = c.start = +g || +d || 0;
                c.unit = f;
                // If a +=/-= token was provided, we're doing a relative animation
                c.end = e[1] ? g + (e[1] + 1) * e[2] : +e[2];
            }
            return c;
        } ]
    };
    // Animations created synchronously will run synchronously
    function Sa() {
        setTimeout(function() {
            La = undefined;
        });
        return La = n.now();
    }
    // Generate parameters to create a standard animation
    function Ta(a, b) {
        var c, d = 0, e = {
            height: a
        };
        // If we include width, step value is 1 to do all cssExpand values,
        // otherwise step value is 2 to skip over Left and Right
        b = b ? 1 : 0;
        for (;d < 4; d += 2 - b) {
            c = R[d];
            e["margin" + c] = e["padding" + c] = a;
        }
        if (b) {
            e.opacity = e.width = a;
        }
        return e;
    }
    function Ua(a, b, c) {
        var d, e = (Ra[b] || []).concat(Ra["*"]), f = 0, g = e.length;
        for (;f < g; f++) {
            if (d = e[f].call(c, b, a)) {
                // We're done with this property
                return d;
            }
        }
    }
    function Va(a, b, c) {
        /* jshint validthis: true */
        var d, e, f, g, h, i, j, k, l = this, m = {}, o = a.style, p = a.nodeType && S(a), q = L.get(a, "fxshow");
        // Handle queue: false promises
        if (!c.queue) {
            h = n._queueHooks(a, "fx");
            if (h.unqueued == null) {
                h.unqueued = 0;
                i = h.empty.fire;
                h.empty.fire = function() {
                    if (!h.unqueued) {
                        i();
                    }
                };
            }
            h.unqueued++;
            l.always(function() {
                // Ensure the complete handler is called before this completes
                l.always(function() {
                    h.unqueued--;
                    if (!n.queue(a, "fx").length) {
                        h.empty.fire();
                    }
                });
            });
        }
        // Height/width overflow pass
        if (a.nodeType === 1 && ("height" in b || "width" in b)) {
            // Make sure that nothing sneaks out
            // Record all 3 overflow attributes because IE9-10 do not
            // change the overflow attribute when overflowX and
            // overflowY are set to the same value
            c.overflow = [ o.overflow, o.overflowX, o.overflowY ];
            // Set display property to inline-block for height/width
            // animations on inline elements that are having width/height animated
            j = n.css(a, "display");
            // Test default display if display is currently "none"
            k = j === "none" ? L.get(a, "olddisplay") || ta(a.nodeName) : j;
            if (k === "inline" && n.css(a, "float") === "none") {
                o.display = "inline-block";
            }
        }
        if (c.overflow) {
            o.overflow = "hidden";
            l.always(function() {
                o.overflow = c.overflow[0];
                o.overflowX = c.overflow[1];
                o.overflowY = c.overflow[2];
            });
        }
        // show/hide pass
        for (d in b) {
            e = b[d];
            if (Na.exec(e)) {
                delete b[d];
                f = f || e === "toggle";
                if (e === (p ? "hide" : "show")) {
                    // If there is dataShow left over from a stopped hide or show and we are going to proceed with show, we should pretend to be hidden
                    if (e === "show" && q && q[d] !== undefined) {
                        p = true;
                    } else {
                        continue;
                    }
                }
                m[d] = q && q[d] || n.style(a, d);
            } else {
                j = undefined;
            }
        }
        if (!n.isEmptyObject(m)) {
            if (q) {
                if ("hidden" in q) {
                    p = q.hidden;
                }
            } else {
                q = L.access(a, "fxshow", {});
            }
            // Store state if its toggle - enables .stop().toggle() to "reverse"
            if (f) {
                q.hidden = !p;
            }
            if (p) {
                n(a).show();
            } else {
                l.done(function() {
                    n(a).hide();
                });
            }
            l.done(function() {
                var b;
                L.remove(a, "fxshow");
                for (b in m) {
                    n.style(a, b, m[b]);
                }
            });
            for (d in m) {
                g = Ua(p ? q[d] : 0, d, l);
                if (!(d in q)) {
                    q[d] = g.start;
                    if (p) {
                        g.end = g.start;
                        g.start = d === "width" || d === "height" ? 1 : 0;
                    }
                }
            }
        } else if ((j === "none" ? ta(a.nodeName) : j) === "inline") {
            o.display = j;
        }
    }
    function Wa(a, b) {
        var c, d, e, f, g;
        // camelCase, specialEasing and expand cssHook pass
        for (c in a) {
            d = n.camelCase(c);
            e = b[d];
            f = a[c];
            if (n.isArray(f)) {
                e = f[1];
                f = a[c] = f[0];
            }
            if (c !== d) {
                a[d] = f;
                delete a[c];
            }
            g = n.cssHooks[d];
            if (g && "expand" in g) {
                f = g.expand(f);
                delete a[d];
                // Not quite $.extend, this won't overwrite existing keys.
                // Reusing 'index' because we have the correct "name"
                for (c in f) {
                    if (!(c in a)) {
                        a[c] = f[c];
                        b[c] = e;
                    }
                }
            } else {
                b[d] = e;
            }
        }
    }
    function Xa(a, b, c) {
        var d, e, f = 0, g = Qa.length, h = n.Deferred().always(function() {
            // Don't match elem in the :animated selector
            delete i.elem;
        }), i = function() {
            if (e) {
                return false;
            }
            var b = La || Sa(), c = Math.max(0, j.startTime + j.duration - b), // Support: Android 2.3
            // Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
            d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length;
            for (;g < i; g++) {
                j.tweens[g].run(f);
            }
            h.notifyWith(a, [ j, f, c ]);
            if (f < 1 && i) {
                return c;
            } else {
                h.resolveWith(a, [ j ]);
                return false;
            }
        }, j = h.promise({
            elem: a,
            props: n.extend({}, b),
            opts: n.extend(true, {
                specialEasing: {}
            }, c),
            originalProperties: b,
            originalOptions: c,
            startTime: La || Sa(),
            duration: c.duration,
            tweens: [],
            createTween: function(b, c) {
                var d = n.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
                j.tweens.push(d);
                return d;
            },
            stop: function(b) {
                var c = 0, // If we are going to the end, we want to run all the tweens
                // otherwise we skip this part
                d = b ? j.tweens.length : 0;
                if (e) {
                    return this;
                }
                e = true;
                for (;c < d; c++) {
                    j.tweens[c].run(1);
                }
                // Resolve when we played the last frame; otherwise, reject
                if (b) {
                    h.resolveWith(a, [ j, b ]);
                } else {
                    h.rejectWith(a, [ j, b ]);
                }
                return this;
            }
        }), k = j.props;
        Wa(k, j.opts.specialEasing);
        for (;f < g; f++) {
            d = Qa[f].call(j, a, k, j.opts);
            if (d) {
                return d;
            }
        }
        n.map(k, Ua, j);
        if (n.isFunction(j.opts.start)) {
            j.opts.start.call(a, j);
        }
        n.fx.timer(n.extend(i, {
            elem: a,
            anim: j,
            queue: j.opts.queue
        }));
        // attach callbacks from options
        return j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always);
    }
    n.Animation = n.extend(Xa, {
        tweener: function(a, b) {
            if (n.isFunction(a)) {
                b = a;
                a = [ "*" ];
            } else {
                a = a.split(" ");
            }
            var c, d = 0, e = a.length;
            for (;d < e; d++) {
                c = a[d];
                Ra[c] = Ra[c] || [];
                Ra[c].unshift(b);
            }
        },
        prefilter: function(a, b) {
            if (b) {
                Qa.unshift(a);
            } else {
                Qa.push(a);
            }
        }
    });
    n.speed = function(a, b, c) {
        var d = a && typeof a === "object" ? n.extend({}, a) : {
            complete: c || !c && b || n.isFunction(a) && a,
            duration: a,
            easing: c && b || b && !n.isFunction(b) && b
        };
        d.duration = n.fx.off ? 0 : typeof d.duration === "number" ? d.duration : d.duration in n.fx.speeds ? n.fx.speeds[d.duration] : n.fx.speeds._default;
        // Normalize opt.queue - true/undefined/null -> "fx"
        if (d.queue == null || d.queue === true) {
            d.queue = "fx";
        }
        // Queueing
        d.old = d.complete;
        d.complete = function() {
            if (n.isFunction(d.old)) {
                d.old.call(this);
            }
            if (d.queue) {
                n.dequeue(this, d.queue);
            }
        };
        return d;
    };
    n.fn.extend({
        fadeTo: function(a, b, c, d) {
            // Show any hidden elements after setting opacity to 0
            return this.filter(S).css("opacity", 0).show().end().animate({
                opacity: b
            }, a, c, d);
        },
        animate: function(a, b, c, d) {
            var e = n.isEmptyObject(a), f = n.speed(b, c, d), g = function() {
                // Operate on a copy of prop so per-property easing won't be lost
                var b = Xa(this, n.extend({}, a), f);
                // Empty animations, or finishing resolves immediately
                if (e || L.get(this, "finish")) {
                    b.stop(true);
                }
            };
            g.finish = g;
            return e || f.queue === false ? this.each(g) : this.queue(f.queue, g);
        },
        stop: function(a, b, c) {
            var d = function(a) {
                var b = a.stop;
                delete a.stop;
                b(c);
            };
            if (typeof a !== "string") {
                c = b;
                b = a;
                a = undefined;
            }
            if (b && a !== false) {
                this.queue(a || "fx", []);
            }
            return this.each(function() {
                var b = true, e = a != null && a + "queueHooks", f = n.timers, g = L.get(this);
                if (e) {
                    if (g[e] && g[e].stop) {
                        d(g[e]);
                    }
                } else {
                    for (e in g) {
                        if (g[e] && g[e].stop && Pa.test(e)) {
                            d(g[e]);
                        }
                    }
                }
                for (e = f.length; e--; ) {
                    if (f[e].elem === this && (a == null || f[e].queue === a)) {
                        f[e].anim.stop(c);
                        b = false;
                        f.splice(e, 1);
                    }
                }
                // Start the next in the queue if the last step wasn't forced.
                // Timers currently will call their complete callbacks, which
                // will dequeue but only if they were gotoEnd.
                if (b || !c) {
                    n.dequeue(this, a);
                }
            });
        },
        finish: function(a) {
            if (a !== false) {
                a = a || "fx";
            }
            return this.each(function() {
                var b, c = L.get(this), d = c[a + "queue"], e = c[a + "queueHooks"], f = n.timers, g = d ? d.length : 0;
                // Enable finishing flag on private data
                c.finish = true;
                // Empty the queue first
                n.queue(this, a, []);
                if (e && e.stop) {
                    e.stop.call(this, true);
                }
                // Look for any active animations, and finish them
                for (b = f.length; b--; ) {
                    if (f[b].elem === this && f[b].queue === a) {
                        f[b].anim.stop(true);
                        f.splice(b, 1);
                    }
                }
                // Look for any animations in the old queue and finish them
                for (b = 0; b < g; b++) {
                    if (d[b] && d[b].finish) {
                        d[b].finish.call(this);
                    }
                }
                // Turn off finishing flag
                delete c.finish;
            });
        }
    });
    n.each([ "toggle", "show", "hide" ], function(a, b) {
        var c = n.fn[b];
        n.fn[b] = function(a, d, e) {
            return a == null || typeof a === "boolean" ? c.apply(this, arguments) : this.animate(Ta(b, true), a, d, e);
        };
    });
    // Generate shortcuts for custom animations
    n.each({
        slideDown: Ta("show"),
        slideUp: Ta("hide"),
        slideToggle: Ta("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(a, b) {
        n.fn[a] = function(a, c, d) {
            return this.animate(b, a, c, d);
        };
    });
    n.timers = [];
    n.fx.tick = function() {
        var a, b = 0, c = n.timers;
        La = n.now();
        for (;b < c.length; b++) {
            a = c[b];
            // Checks the timer has not already been removed
            if (!a() && c[b] === a) {
                c.splice(b--, 1);
            }
        }
        if (!c.length) {
            n.fx.stop();
        }
        La = undefined;
    };
    n.fx.timer = function(a) {
        n.timers.push(a);
        if (a()) {
            n.fx.start();
        } else {
            n.timers.pop();
        }
    };
    n.fx.interval = 13;
    n.fx.start = function() {
        if (!Ma) {
            Ma = setInterval(n.fx.tick, n.fx.interval);
        }
    };
    n.fx.stop = function() {
        clearInterval(Ma);
        Ma = null;
    };
    n.fx.speeds = {
        slow: 600,
        fast: 200,
        // Default speed
        _default: 400
    };
    // Based off of the plugin by Clint Helfers, with permission.
    // http://blindsignals.com/index.php/2009/07/jquery-delay/
    n.fn.delay = function(a, b) {
        a = n.fx ? n.fx.speeds[a] || a : a;
        b = b || "fx";
        return this.queue(b, function(b, c) {
            var d = setTimeout(b, a);
            c.stop = function() {
                clearTimeout(d);
            };
        });
    };
    (function() {
        var a = l.createElement("input"), b = l.createElement("select"), c = b.appendChild(l.createElement("option"));
        a.type = "checkbox";
        // Support: iOS<=5.1, Android<=4.2+
        // Default value for a checkbox should be "on"
        k.checkOn = a.value !== "";
        // Support: IE<=11+
        // Must access selectedIndex to make default options select
        k.optSelected = c.selected;
        // Support: Android<=2.3
        // Options inside disabled selects are incorrectly marked as disabled
        b.disabled = true;
        k.optDisabled = !c.disabled;
        // Support: IE<=11+
        // An input loses its value after becoming a radio
        a = l.createElement("input");
        a.value = "t";
        a.type = "radio";
        k.radioValue = a.value === "t";
    })();
    var Ya, Za, $a = n.expr.attrHandle;
    n.fn.extend({
        attr: function(a, b) {
            return J(this, n.attr, a, b, arguments.length > 1);
        },
        removeAttr: function(a) {
            return this.each(function() {
                n.removeAttr(this, a);
            });
        }
    });
    n.extend({
        attr: function(a, b, c) {
            var d, e, f = a.nodeType;
            // don't get/set attributes on text, comment and attribute nodes
            if (!a || f === 3 || f === 8 || f === 2) {
                return;
            }
            // Fallback to prop when attributes are not supported
            if (typeof a.getAttribute === U) {
                return n.prop(a, b, c);
            }
            // All attributes are lowercase
            // Grab necessary hook if one is defined
            if (f !== 1 || !n.isXMLDoc(a)) {
                b = b.toLowerCase();
                d = n.attrHooks[b] || (n.expr.match.bool.test(b) ? Za : Ya);
            }
            if (c !== undefined) {
                if (c === null) {
                    n.removeAttr(a, b);
                } else if (d && "set" in d && (e = d.set(a, c, b)) !== undefined) {
                    return e;
                } else {
                    a.setAttribute(b, c + "");
                    return c;
                }
            } else if (d && "get" in d && (e = d.get(a, b)) !== null) {
                return e;
            } else {
                e = n.find.attr(a, b);
                // Non-existent attributes return null, we normalize to undefined
                return e == null ? undefined : e;
            }
        },
        removeAttr: function(a, b) {
            var c, d, e = 0, f = b && b.match(E);
            if (f && a.nodeType === 1) {
                while (c = f[e++]) {
                    d = n.propFix[c] || c;
                    // Boolean attributes get special treatment (#10870)
                    if (n.expr.match.bool.test(c)) {
                        // Set corresponding property to false
                        a[d] = false;
                    }
                    a.removeAttribute(c);
                }
            }
        },
        attrHooks: {
            type: {
                set: function(a, b) {
                    if (!k.radioValue && b === "radio" && n.nodeName(a, "input")) {
                        var c = a.value;
                        a.setAttribute("type", b);
                        if (c) {
                            a.value = c;
                        }
                        return b;
                    }
                }
            }
        }
    });
    // Hooks for boolean attributes
    Za = {
        set: function(a, b, c) {
            if (b === false) {
                // Remove boolean attributes when set to false
                n.removeAttr(a, c);
            } else {
                a.setAttribute(c, c);
            }
            return c;
        }
    };
    n.each(n.expr.match.bool.source.match(/\w+/g), function(a, b) {
        var c = $a[b] || n.find.attr;
        $a[b] = function(a, b, d) {
            var e, f;
            if (!d) {
                // Avoid an infinite loop by temporarily removing this function from the getter
                f = $a[b];
                $a[b] = e;
                e = c(a, b, d) != null ? b.toLowerCase() : null;
                $a[b] = f;
            }
            return e;
        };
    });
    var _a = /^(?:input|select|textarea|button)$/i;
    n.fn.extend({
        prop: function(a, b) {
            return J(this, n.prop, a, b, arguments.length > 1);
        },
        removeProp: function(a) {
            return this.each(function() {
                delete this[n.propFix[a] || a];
            });
        }
    });
    n.extend({
        propFix: {
            "for": "htmlFor",
            "class": "className"
        },
        prop: function(a, b, c) {
            var d, e, f, g = a.nodeType;
            // Don't get/set properties on text, comment and attribute nodes
            if (!a || g === 3 || g === 8 || g === 2) {
                return;
            }
            f = g !== 1 || !n.isXMLDoc(a);
            if (f) {
                // Fix name and attach hooks
                b = n.propFix[b] || b;
                e = n.propHooks[b];
            }
            if (c !== undefined) {
                return e && "set" in e && (d = e.set(a, c, b)) !== undefined ? d : a[b] = c;
            } else {
                return e && "get" in e && (d = e.get(a, b)) !== null ? d : a[b];
            }
        },
        propHooks: {
            tabIndex: {
                get: function(a) {
                    return a.hasAttribute("tabindex") || _a.test(a.nodeName) || a.href ? a.tabIndex : -1;
                }
            }
        }
    });
    if (!k.optSelected) {
        n.propHooks.selected = {
            get: function(a) {
                var b = a.parentNode;
                if (b && b.parentNode) {
                    b.parentNode.selectedIndex;
                }
                return null;
            }
        };
    }
    n.each([ "tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable" ], function() {
        n.propFix[this.toLowerCase()] = this;
    });
    var ab = /[\t\r\n\f]/g;
    n.fn.extend({
        addClass: function(a) {
            var b, c, d, e, f, g, h = typeof a === "string" && a, i = 0, j = this.length;
            if (n.isFunction(a)) {
                return this.each(function(b) {
                    n(this).addClass(a.call(this, b, this.className));
                });
            }
            if (h) {
                // The disjunction here is for better compressibility (see removeClass)
                b = (a || "").match(E) || [];
                for (;i < j; i++) {
                    c = this[i];
                    d = c.nodeType === 1 && (c.className ? (" " + c.className + " ").replace(ab, " ") : " ");
                    if (d) {
                        f = 0;
                        while (e = b[f++]) {
                            if (d.indexOf(" " + e + " ") < 0) {
                                d += e + " ";
                            }
                        }
                        // only assign if different to avoid unneeded rendering.
                        g = n.trim(d);
                        if (c.className !== g) {
                            c.className = g;
                        }
                    }
                }
            }
            return this;
        },
        removeClass: function(a) {
            var b, c, d, e, f, g, h = arguments.length === 0 || typeof a === "string" && a, i = 0, j = this.length;
            if (n.isFunction(a)) {
                return this.each(function(b) {
                    n(this).removeClass(a.call(this, b, this.className));
                });
            }
            if (h) {
                b = (a || "").match(E) || [];
                for (;i < j; i++) {
                    c = this[i];
                    // This expression is here for better compressibility (see addClass)
                    d = c.nodeType === 1 && (c.className ? (" " + c.className + " ").replace(ab, " ") : "");
                    if (d) {
                        f = 0;
                        while (e = b[f++]) {
                            // Remove *all* instances
                            while (d.indexOf(" " + e + " ") >= 0) {
                                d = d.replace(" " + e + " ", " ");
                            }
                        }
                        // Only assign if different to avoid unneeded rendering.
                        g = a ? n.trim(d) : "";
                        if (c.className !== g) {
                            c.className = g;
                        }
                    }
                }
            }
            return this;
        },
        toggleClass: function(a, b) {
            var c = typeof a;
            if (typeof b === "boolean" && c === "string") {
                return b ? this.addClass(a) : this.removeClass(a);
            }
            if (n.isFunction(a)) {
                return this.each(function(c) {
                    n(this).toggleClass(a.call(this, c, this.className, b), b);
                });
            }
            return this.each(function() {
                if (c === "string") {
                    // Toggle individual class names
                    var b, d = 0, e = n(this), f = a.match(E) || [];
                    while (b = f[d++]) {
                        // Check each className given, space separated list
                        if (e.hasClass(b)) {
                            e.removeClass(b);
                        } else {
                            e.addClass(b);
                        }
                    }
                } else if (c === U || c === "boolean") {
                    if (this.className) {
                        // store className if set
                        L.set(this, "__className__", this.className);
                    }
                    // If the element has a class name or if we're passed `false`,
                    // then remove the whole classname (if there was one, the above saved it).
                    // Otherwise bring back whatever was previously saved (if anything),
                    // falling back to the empty string if nothing was stored.
                    this.className = this.className || a === false ? "" : L.get(this, "__className__") || "";
                }
            });
        },
        hasClass: function(a) {
            var b = " " + a + " ", c = 0, d = this.length;
            for (;c < d; c++) {
                if (this[c].nodeType === 1 && (" " + this[c].className + " ").replace(ab, " ").indexOf(b) >= 0) {
                    return true;
                }
            }
            return false;
        }
    });
    var bb = /\r/g;
    n.fn.extend({
        val: function(a) {
            var b, c, d, e = this[0];
            if (!arguments.length) {
                if (e) {
                    b = n.valHooks[e.type] || n.valHooks[e.nodeName.toLowerCase()];
                    if (b && "get" in b && (c = b.get(e, "value")) !== undefined) {
                        return c;
                    }
                    c = e.value;
                    // Handle most common string cases
                    // Handle cases where value is null/undef or number
                    return typeof c === "string" ? c.replace(bb, "") : c == null ? "" : c;
                }
                return;
            }
            d = n.isFunction(a);
            return this.each(function(c) {
                var e;
                if (this.nodeType !== 1) {
                    return;
                }
                if (d) {
                    e = a.call(this, c, n(this).val());
                } else {
                    e = a;
                }
                // Treat null/undefined as ""; convert numbers to string
                if (e == null) {
                    e = "";
                } else if (typeof e === "number") {
                    e += "";
                } else if (n.isArray(e)) {
                    e = n.map(e, function(a) {
                        return a == null ? "" : a + "";
                    });
                }
                b = n.valHooks[this.type] || n.valHooks[this.nodeName.toLowerCase()];
                // If set returns undefined, fall back to normal setting
                if (!b || !("set" in b) || b.set(this, e, "value") === undefined) {
                    this.value = e;
                }
            });
        }
    });
    n.extend({
        valHooks: {
            option: {
                get: function(a) {
                    var b = n.find.attr(a, "value");
                    // Support: IE10-11+
                    // option.text throws exceptions (#14686, #14858)
                    return b != null ? b : n.trim(n.text(a));
                }
            },
            select: {
                get: function(a) {
                    var b, c, d = a.options, e = a.selectedIndex, f = a.type === "select-one" || e < 0, g = f ? null : [], h = f ? e + 1 : d.length, i = e < 0 ? h : f ? e : 0;
                    // Loop through all the selected options
                    for (;i < h; i++) {
                        c = d[i];
                        // IE6-9 doesn't update selected after form reset (#2551)
                        if ((c.selected || i === e) && (// Don't return options that are disabled or in a disabled optgroup
                        k.optDisabled ? !c.disabled : c.getAttribute("disabled") === null) && (!c.parentNode.disabled || !n.nodeName(c.parentNode, "optgroup"))) {
                            // Get the specific value for the option
                            b = n(c).val();
                            // We don't need an array for one selects
                            if (f) {
                                return b;
                            }
                            // Multi-Selects return an array
                            g.push(b);
                        }
                    }
                    return g;
                },
                set: function(a, b) {
                    var c, d, e = a.options, f = n.makeArray(b), g = e.length;
                    while (g--) {
                        d = e[g];
                        if (d.selected = n.inArray(d.value, f) >= 0) {
                            c = true;
                        }
                    }
                    // Force browsers to behave consistently when non-matching value is set
                    if (!c) {
                        a.selectedIndex = -1;
                    }
                    return f;
                }
            }
        }
    });
    // Radios and checkboxes getter/setter
    n.each([ "radio", "checkbox" ], function() {
        n.valHooks[this] = {
            set: function(a, b) {
                if (n.isArray(b)) {
                    return a.checked = n.inArray(n(a).val(), b) >= 0;
                }
            }
        };
        if (!k.checkOn) {
            n.valHooks[this].get = function(a) {
                return a.getAttribute("value") === null ? "on" : a.value;
            };
        }
    });
    // Return jQuery for attributes-only inclusion
    n.each(("blur focus focusin focusout load resize scroll unload click dblclick " + "mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " + "change select submit keydown keypress keyup error contextmenu").split(" "), function(a, b) {
        // Handle event binding
        n.fn[b] = function(a, c) {
            return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b);
        };
    });
    n.fn.extend({
        hover: function(a, b) {
            return this.mouseenter(a).mouseleave(b || a);
        },
        bind: function(a, b, c) {
            return this.on(a, null, b, c);
        },
        unbind: function(a, b) {
            return this.off(a, null, b);
        },
        delegate: function(a, b, c, d) {
            return this.on(b, a, c, d);
        },
        undelegate: function(a, b, c) {
            // ( namespace ) or ( selector, types [, fn] )
            return arguments.length === 1 ? this.off(a, "**") : this.off(b, a || "**", c);
        }
    });
    var cb = n.now();
    var db = /\?/;
    // Support: Android 2.3
    // Workaround failure to string-cast null input
    n.parseJSON = function(a) {
        return JSON.parse(a + "");
    };
    // Cross-browser xml parsing
    n.parseXML = function(a) {
        var b, c;
        if (!a || typeof a !== "string") {
            return null;
        }
        // Support: IE9
        try {
            c = new DOMParser();
            b = c.parseFromString(a, "text/xml");
        } catch (d) {
            b = undefined;
        }
        if (!b || b.getElementsByTagName("parsererror").length) {
            n.error("Invalid XML: " + a);
        }
        return b;
    };
    var eb = /#.*$/, fb = /([?&])_=[^&]*/, gb = /^(.*?):[ \t]*([^\r\n]*)$/gm, // #7653, #8125, #8152: local protocol detection
    hb = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, ib = /^(?:GET|HEAD)$/, jb = /^\/\//, kb = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, /* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
    lb = {}, /* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
    mb = {}, // Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
    nb = "*/".concat("*"), // Document location
    ob = a.location.href, // Segment location into parts
    pb = kb.exec(ob.toLowerCase()) || [];
    // Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
    function qb(a) {
        // dataTypeExpression is optional and defaults to "*"
        return function(b, c) {
            if (typeof b !== "string") {
                c = b;
                b = "*";
            }
            var d, e = 0, f = b.toLowerCase().match(E) || [];
            if (n.isFunction(c)) {
                // For each dataType in the dataTypeExpression
                while (d = f[e++]) {
                    // Prepend if requested
                    if (d[0] === "+") {
                        d = d.slice(1) || "*";
                        (a[d] = a[d] || []).unshift(c);
                    } else {
                        (a[d] = a[d] || []).push(c);
                    }
                }
            }
        };
    }
    // Base inspection function for prefilters and transports
    function rb(a, b, c, d) {
        var e = {}, f = a === mb;
        function g(h) {
            var i;
            e[h] = true;
            n.each(a[h] || [], function(a, h) {
                var j = h(b, c, d);
                if (typeof j === "string" && !f && !e[j]) {
                    b.dataTypes.unshift(j);
                    g(j);
                    return false;
                } else if (f) {
                    return !(i = j);
                }
            });
            return i;
        }
        return g(b.dataTypes[0]) || !e["*"] && g("*");
    }
    // A special extend for ajax options
    // that takes "flat" options (not to be deep extended)
    // Fixes #9887
    function sb(a, b) {
        var c, d, e = n.ajaxSettings.flatOptions || {};
        for (c in b) {
            if (b[c] !== undefined) {
                (e[c] ? a : d || (d = {}))[c] = b[c];
            }
        }
        if (d) {
            n.extend(true, a, d);
        }
        return a;
    }
    /* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
    function tb(a, b, c) {
        var d, e, f, g, h = a.contents, i = a.dataTypes;
        // Remove auto dataType and get content-type in the process
        while (i[0] === "*") {
            i.shift();
            if (d === undefined) {
                d = a.mimeType || b.getResponseHeader("Content-Type");
            }
        }
        // Check if we're dealing with a known content-type
        if (d) {
            for (e in h) {
                if (h[e] && h[e].test(d)) {
                    i.unshift(e);
                    break;
                }
            }
        }
        // Check to see if we have a response for the expected dataType
        if (i[0] in c) {
            f = i[0];
        } else {
            // Try convertible dataTypes
            for (e in c) {
                if (!i[0] || a.converters[e + " " + i[0]]) {
                    f = e;
                    break;
                }
                if (!g) {
                    g = e;
                }
            }
            // Or just use first one
            f = f || g;
        }
        // If we found a dataType
        // We add the dataType to the list if needed
        // and return the corresponding response
        if (f) {
            if (f !== i[0]) {
                i.unshift(f);
            }
            return c[f];
        }
    }
    /* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
    function ub(a, b, c, d) {
        var e, f, g, h, i, j = {}, // Work with a copy of dataTypes in case we need to modify it for conversion
        k = a.dataTypes.slice();
        // Create converters map with lowercased keys
        if (k[1]) {
            for (g in a.converters) {
                j[g.toLowerCase()] = a.converters[g];
            }
        }
        f = k.shift();
        // Convert to each sequential dataType
        while (f) {
            if (a.responseFields[f]) {
                c[a.responseFields[f]] = b;
            }
            // Apply the dataFilter if provided
            if (!i && d && a.dataFilter) {
                b = a.dataFilter(b, a.dataType);
            }
            i = f;
            f = k.shift();
            if (f) {
                // There's only work to do if current dataType is non-auto
                if (f === "*") {
                    f = i;
                } else if (i !== "*" && i !== f) {
                    // Seek a direct converter
                    g = j[i + " " + f] || j["* " + f];
                    // If none found, seek a pair
                    if (!g) {
                        for (e in j) {
                            // If conv2 outputs current
                            h = e.split(" ");
                            if (h[1] === f) {
                                // If prev can be converted to accepted input
                                g = j[i + " " + h[0]] || j["* " + h[0]];
                                if (g) {
                                    // Condense equivalence converters
                                    if (g === true) {
                                        g = j[e];
                                    } else if (j[e] !== true) {
                                        f = h[0];
                                        k.unshift(h[1]);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    // Apply converter (if not an equivalence)
                    if (g !== true) {
                        // Unless errors are allowed to bubble, catch and return them
                        if (g && a["throws"]) {
                            b = g(b);
                        } else {
                            try {
                                b = g(b);
                            } catch (l) {
                                return {
                                    state: "parsererror",
                                    error: g ? l : "No conversion from " + i + " to " + f
                                };
                            }
                        }
                    }
                }
            }
        }
        return {
            state: "success",
            data: b
        };
    }
    n.extend({
        // Counter for holding the number of active queries
        active: 0,
        // Last-Modified header cache for next request
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: ob,
            type: "GET",
            isLocal: hb.test(pb[1]),
            global: true,
            processData: true,
            async: true,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            /*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/
            accepts: {
                "*": nb,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            // Data converters
            // Keys separate source (or catchall "*") and destination types with a single space
            converters: {
                // Convert anything to text
                "* text": String,
                // Text to html (true = no transformation)
                "text html": true,
                // Evaluate text as a json expression
                "text json": n.parseJSON,
                // Parse text as xml
                "text xml": n.parseXML
            },
            // For options that shouldn't be deep extended:
            // you can add your own custom options here if
            // and when you create one that shouldn't be
            // deep extended (see ajaxExtend)
            flatOptions: {
                url: true,
                context: true
            }
        },
        // Creates a full fledged settings object into target
        // with both ajaxSettings and settings fields.
        // If target is omitted, writes into ajaxSettings.
        ajaxSetup: function(a, b) {
            // Building a settings object
            // Extending ajaxSettings
            return b ? sb(sb(a, n.ajaxSettings), b) : sb(n.ajaxSettings, a);
        },
        ajaxPrefilter: qb(lb),
        ajaxTransport: qb(mb),
        // Main method
        ajax: function(a, b) {
            // If url is an object, simulate pre-1.5 signature
            if (typeof a === "object") {
                b = a;
                a = undefined;
            }
            // Force options to be an object
            b = b || {};
            var c, // URL without anti-cache param
            d, // Response headers
            e, f, // timeout handle
            g, // Cross-domain detection vars
            h, // To know if global events are to be dispatched
            i, // Loop variable
            j, // Create the final options object
            k = n.ajaxSetup({}, b), // Callbacks context
            l = k.context || k, // Context for global events is callbackContext if it is a DOM node or jQuery collection
            m = k.context && (l.nodeType || l.jquery) ? n(l) : n.event, // Deferreds
            o = n.Deferred(), p = n.Callbacks("once memory"), // Status-dependent callbacks
            q = k.statusCode || {}, // Headers (they are sent all at once)
            r = {}, s = {}, // The jqXHR state
            t = 0, // Default abort message
            u = "canceled", // Fake xhr
            v = {
                readyState: 0,
                // Builds headers hashtable if needed
                getResponseHeader: function(a) {
                    var b;
                    if (t === 2) {
                        if (!f) {
                            f = {};
                            while (b = gb.exec(e)) {
                                f[b[1].toLowerCase()] = b[2];
                            }
                        }
                        b = f[a.toLowerCase()];
                    }
                    return b == null ? null : b;
                },
                // Raw string
                getAllResponseHeaders: function() {
                    return t === 2 ? e : null;
                },
                // Caches the header
                setRequestHeader: function(a, b) {
                    var c = a.toLowerCase();
                    if (!t) {
                        a = s[c] = s[c] || a;
                        r[a] = b;
                    }
                    return this;
                },
                // Overrides response content-type header
                overrideMimeType: function(a) {
                    if (!t) {
                        k.mimeType = a;
                    }
                    return this;
                },
                // Status-dependent callbacks
                statusCode: function(a) {
                    var b;
                    if (a) {
                        if (t < 2) {
                            for (b in a) {
                                // Lazy-add the new callback in a way that preserves old ones
                                q[b] = [ q[b], a[b] ];
                            }
                        } else {
                            // Execute the appropriate callbacks
                            v.always(a[v.status]);
                        }
                    }
                    return this;
                },
                // Cancel the request
                abort: function(a) {
                    var b = a || u;
                    if (c) {
                        c.abort(b);
                    }
                    x(0, b);
                    return this;
                }
            };
            // Attach deferreds
            o.promise(v).complete = p.add;
            v.success = v.done;
            v.error = v.fail;
            // Remove hash character (#7531: and string promotion)
            // Add protocol if not provided (prefilters might expect it)
            // Handle falsy url in the settings object (#10093: consistency with old signature)
            // We also use the url parameter if available
            k.url = ((a || k.url || ob) + "").replace(eb, "").replace(jb, pb[1] + "//");
            // Alias method option to type as per ticket #12004
            k.type = b.method || b.type || k.method || k.type;
            // Extract dataTypes list
            k.dataTypes = n.trim(k.dataType || "*").toLowerCase().match(E) || [ "" ];
            // A cross-domain request is in order when we have a protocol:host:port mismatch
            if (k.crossDomain == null) {
                h = kb.exec(k.url.toLowerCase());
                k.crossDomain = !!(h && (h[1] !== pb[1] || h[2] !== pb[2] || (h[3] || (h[1] === "http:" ? "80" : "443")) !== (pb[3] || (pb[1] === "http:" ? "80" : "443"))));
            }
            // Convert data if not already a string
            if (k.data && k.processData && typeof k.data !== "string") {
                k.data = n.param(k.data, k.traditional);
            }
            // Apply prefilters
            rb(lb, k, b, v);
            // If request was aborted inside a prefilter, stop there
            if (t === 2) {
                return v;
            }
            // We can fire global events as of now if asked to
            // Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
            i = n.event && k.global;
            // Watch for a new set of requests
            if (i && n.active++ === 0) {
                n.event.trigger("ajaxStart");
            }
            // Uppercase the type
            k.type = k.type.toUpperCase();
            // Determine if request has content
            k.hasContent = !ib.test(k.type);
            // Save the URL in case we're toying with the If-Modified-Since
            // and/or If-None-Match header later on
            d = k.url;
            // More options handling for requests with no content
            if (!k.hasContent) {
                // If data is available, append data to url
                if (k.data) {
                    d = k.url += (db.test(d) ? "&" : "?") + k.data;
                    // #9682: remove data so that it's not used in an eventual retry
                    delete k.data;
                }
                // Add anti-cache in url if needed
                if (k.cache === false) {
                    k.url = fb.test(d) ? // If there is already a '_' parameter, set its value
                    d.replace(fb, "$1_=" + cb++) : // Otherwise add one to the end
                    d + (db.test(d) ? "&" : "?") + "_=" + cb++;
                }
            }
            // Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
            if (k.ifModified) {
                if (n.lastModified[d]) {
                    v.setRequestHeader("If-Modified-Since", n.lastModified[d]);
                }
                if (n.etag[d]) {
                    v.setRequestHeader("If-None-Match", n.etag[d]);
                }
            }
            // Set the correct header, if data is being sent
            if (k.data && k.hasContent && k.contentType !== false || b.contentType) {
                v.setRequestHeader("Content-Type", k.contentType);
            }
            // Set the Accepts header for the server, depending on the dataType
            v.setRequestHeader("Accept", k.dataTypes[0] && k.accepts[k.dataTypes[0]] ? k.accepts[k.dataTypes[0]] + (k.dataTypes[0] !== "*" ? ", " + nb + "; q=0.01" : "") : k.accepts["*"]);
            // Check for headers option
            for (j in k.headers) {
                v.setRequestHeader(j, k.headers[j]);
            }
            // Allow custom headers/mimetypes and early abort
            if (k.beforeSend && (k.beforeSend.call(l, v, k) === false || t === 2)) {
                // Abort if not done already and return
                return v.abort();
            }
            // Aborting is no longer a cancellation
            u = "abort";
            // Install callbacks on deferreds
            for (j in {
                success: 1,
                error: 1,
                complete: 1
            }) {
                v[j](k[j]);
            }
            // Get transport
            c = rb(mb, k, b, v);
            // If no transport, we auto-abort
            if (!c) {
                x(-1, "No Transport");
            } else {
                v.readyState = 1;
                // Send global event
                if (i) {
                    m.trigger("ajaxSend", [ v, k ]);
                }
                // Timeout
                if (k.async && k.timeout > 0) {
                    g = setTimeout(function() {
                        v.abort("timeout");
                    }, k.timeout);
                }
                try {
                    t = 1;
                    c.send(r, x);
                } catch (w) {
                    // Propagate exception as error if not done
                    if (t < 2) {
                        x(-1, w);
                    } else {
                        throw w;
                    }
                }
            }
            // Callback for when everything is done
            function x(a, b, f, h) {
                var j, r, s, u, w, x = b;
                // Called once
                if (t === 2) {
                    return;
                }
                // State is "done" now
                t = 2;
                // Clear timeout if it exists
                if (g) {
                    clearTimeout(g);
                }
                // Dereference transport for early garbage collection
                // (no matter how long the jqXHR object will be used)
                c = undefined;
                // Cache response headers
                e = h || "";
                // Set readyState
                v.readyState = a > 0 ? 4 : 0;
                // Determine if successful
                j = a >= 200 && a < 300 || a === 304;
                // Get response data
                if (f) {
                    u = tb(k, v, f);
                }
                // Convert no matter what (that way responseXXX fields are always set)
                u = ub(k, u, v, j);
                // If successful, handle type chaining
                if (j) {
                    // Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
                    if (k.ifModified) {
                        w = v.getResponseHeader("Last-Modified");
                        if (w) {
                            n.lastModified[d] = w;
                        }
                        w = v.getResponseHeader("etag");
                        if (w) {
                            n.etag[d] = w;
                        }
                    }
                    // if no content
                    if (a === 204 || k.type === "HEAD") {
                        x = "nocontent";
                    } else if (a === 304) {
                        x = "notmodified";
                    } else {
                        x = u.state;
                        r = u.data;
                        s = u.error;
                        j = !s;
                    }
                } else {
                    // Extract error from statusText and normalize for non-aborts
                    s = x;
                    if (a || !x) {
                        x = "error";
                        if (a < 0) {
                            a = 0;
                        }
                    }
                }
                // Set data for the fake xhr object
                v.status = a;
                v.statusText = (b || x) + "";
                // Success/Error
                if (j) {
                    o.resolveWith(l, [ r, x, v ]);
                } else {
                    o.rejectWith(l, [ v, x, s ]);
                }
                // Status-dependent callbacks
                v.statusCode(q);
                q = undefined;
                if (i) {
                    m.trigger(j ? "ajaxSuccess" : "ajaxError", [ v, k, j ? r : s ]);
                }
                // Complete
                p.fireWith(l, [ v, x ]);
                if (i) {
                    m.trigger("ajaxComplete", [ v, k ]);
                    // Handle the global AJAX counter
                    if (!--n.active) {
                        n.event.trigger("ajaxStop");
                    }
                }
            }
            return v;
        },
        getJSON: function(a, b, c) {
            return n.get(a, b, c, "json");
        },
        getScript: function(a, b) {
            return n.get(a, undefined, b, "script");
        }
    });
    n.each([ "get", "post" ], function(a, b) {
        n[b] = function(a, c, d, e) {
            // Shift arguments if data argument was omitted
            if (n.isFunction(c)) {
                e = e || d;
                d = c;
                c = undefined;
            }
            return n.ajax({
                url: a,
                type: b,
                dataType: e,
                data: c,
                success: d
            });
        };
    });
    n._evalUrl = function(a) {
        return n.ajax({
            url: a,
            type: "GET",
            dataType: "script",
            async: false,
            global: false,
            "throws": true
        });
    };
    n.fn.extend({
        wrapAll: function(a) {
            var b;
            if (n.isFunction(a)) {
                return this.each(function(b) {
                    n(this).wrapAll(a.call(this, b));
                });
            }
            if (this[0]) {
                // The elements to wrap the target around
                b = n(a, this[0].ownerDocument).eq(0).clone(true);
                if (this[0].parentNode) {
                    b.insertBefore(this[0]);
                }
                b.map(function() {
                    var a = this;
                    while (a.firstElementChild) {
                        a = a.firstElementChild;
                    }
                    return a;
                }).append(this);
            }
            return this;
        },
        wrapInner: function(a) {
            if (n.isFunction(a)) {
                return this.each(function(b) {
                    n(this).wrapInner(a.call(this, b));
                });
            }
            return this.each(function() {
                var b = n(this), c = b.contents();
                if (c.length) {
                    c.wrapAll(a);
                } else {
                    b.append(a);
                }
            });
        },
        wrap: function(a) {
            var b = n.isFunction(a);
            return this.each(function(c) {
                n(this).wrapAll(b ? a.call(this, c) : a);
            });
        },
        unwrap: function() {
            return this.parent().each(function() {
                if (!n.nodeName(this, "body")) {
                    n(this).replaceWith(this.childNodes);
                }
            }).end();
        }
    });
    n.expr.filters.hidden = function(a) {
        // Support: Opera <= 12.12
        // Opera reports offsetWidths and offsetHeights less than zero on some elements
        return a.offsetWidth <= 0 && a.offsetHeight <= 0;
    };
    n.expr.filters.visible = function(a) {
        return !n.expr.filters.hidden(a);
    };
    var vb = /%20/g, wb = /\[\]$/, xb = /\r?\n/g, yb = /^(?:submit|button|image|reset|file)$/i, zb = /^(?:input|select|textarea|keygen)/i;
    function Ab(a, b, c, d) {
        var e;
        if (n.isArray(b)) {
            // Serialize array item.
            n.each(b, function(b, e) {
                if (c || wb.test(a)) {
                    // Treat each array item as a scalar.
                    d(a, e);
                } else {
                    // Item is non-scalar (array or object), encode its numeric index.
                    Ab(a + "[" + (typeof e === "object" ? b : "") + "]", e, c, d);
                }
            });
        } else if (!c && n.type(b) === "object") {
            // Serialize object item.
            for (e in b) {
                Ab(a + "[" + e + "]", b[e], c, d);
            }
        } else {
            // Serialize scalar item.
            d(a, b);
        }
    }
    // Serialize an array of form elements or a set of
    // key/values into a query string
    n.param = function(a, b) {
        var c, d = [], e = function(a, b) {
            // If value is a function, invoke it and return its value
            b = n.isFunction(b) ? b() : b == null ? "" : b;
            d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(b);
        };
        // Set traditional to true for jQuery <= 1.3.2 behavior.
        if (b === undefined) {
            b = n.ajaxSettings && n.ajaxSettings.traditional;
        }
        // If an array was passed in, assume that it is an array of form elements.
        if (n.isArray(a) || a.jquery && !n.isPlainObject(a)) {
            // Serialize the form elements
            n.each(a, function() {
                e(this.name, this.value);
            });
        } else {
            // If traditional, encode the "old" way (the way 1.3.2 or older
            // did it), otherwise encode params recursively.
            for (c in a) {
                Ab(c, a[c], b, e);
            }
        }
        // Return the resulting serialization
        return d.join("&").replace(vb, "+");
    };
    n.fn.extend({
        serialize: function() {
            return n.param(this.serializeArray());
        },
        serializeArray: function() {
            return this.map(function() {
                // Can add propHook for "elements" to filter or add form elements
                var a = n.prop(this, "elements");
                return a ? n.makeArray(a) : this;
            }).filter(function() {
                var a = this.type;
                // Use .is( ":disabled" ) so that fieldset[disabled] works
                return this.name && !n(this).is(":disabled") && zb.test(this.nodeName) && !yb.test(a) && (this.checked || !T.test(a));
            }).map(function(a, b) {
                var c = n(this).val();
                return c == null ? null : n.isArray(c) ? n.map(c, function(a) {
                    return {
                        name: b.name,
                        value: a.replace(xb, "\r\n")
                    };
                }) : {
                    name: b.name,
                    value: c.replace(xb, "\r\n")
                };
            }).get();
        }
    });
    n.ajaxSettings.xhr = function() {
        try {
            return new XMLHttpRequest();
        } catch (a) {}
    };
    var Bb = 0, Cb = {}, Db = {
        // file protocol always yields status code 0, assume 200
        0: 200,
        // Support: IE9
        // #1450: sometimes IE returns 1223 when it should be 204
        1223: 204
    }, Eb = n.ajaxSettings.xhr();
    // Support: IE9
    // Open requests must be manually aborted on unload (#5280)
    // See https://support.microsoft.com/kb/2856746 for more info
    if (a.attachEvent) {
        a.attachEvent("onunload", function() {
            for (var a in Cb) {
                Cb[a]();
            }
        });
    }
    k.cors = !!Eb && "withCredentials" in Eb;
    k.ajax = Eb = !!Eb;
    n.ajaxTransport(function(a) {
        var b;
        // Cross domain only allowed if supported through XMLHttpRequest
        if (k.cors || Eb && !a.crossDomain) {
            return {
                send: function(c, d) {
                    var e, f = a.xhr(), g = ++Bb;
                    f.open(a.type, a.url, a.async, a.username, a.password);
                    // Apply custom fields if provided
                    if (a.xhrFields) {
                        for (e in a.xhrFields) {
                            f[e] = a.xhrFields[e];
                        }
                    }
                    // Override mime type if needed
                    if (a.mimeType && f.overrideMimeType) {
                        f.overrideMimeType(a.mimeType);
                    }
                    // X-Requested-With header
                    // For cross-domain requests, seeing as conditions for a preflight are
                    // akin to a jigsaw puzzle, we simply never set it to be sure.
                    // (it can always be set on a per-request basis or even using ajaxSetup)
                    // For same-domain requests, won't change header if already provided.
                    if (!a.crossDomain && !c["X-Requested-With"]) {
                        c["X-Requested-With"] = "XMLHttpRequest";
                    }
                    // Set headers
                    for (e in c) {
                        f.setRequestHeader(e, c[e]);
                    }
                    // Callback
                    b = function(a) {
                        return function() {
                            if (b) {
                                delete Cb[g];
                                b = f.onload = f.onerror = null;
                                if (a === "abort") {
                                    f.abort();
                                } else if (a === "error") {
                                    d(// file: protocol always yields status 0; see #8605, #14207
                                    f.status, f.statusText);
                                } else {
                                    d(Db[f.status] || f.status, f.statusText, // Support: IE9
                                    // Accessing binary-data responseText throws an exception
                                    // (#11426)
                                    typeof f.responseText === "string" ? {
                                        text: f.responseText
                                    } : undefined, f.getAllResponseHeaders());
                                }
                            }
                        };
                    };
                    // Listen to events
                    f.onload = b();
                    f.onerror = b("error");
                    // Create the abort callback
                    b = Cb[g] = b("abort");
                    try {
                        // Do send the request (this may raise an exception)
                        f.send(a.hasContent && a.data || null);
                    } catch (h) {
                        // #14683: Only rethrow if this hasn't been notified as an error yet
                        if (b) {
                            throw h;
                        }
                    }
                },
                abort: function() {
                    if (b) {
                        b();
                    }
                }
            };
        }
    });
    // Install script dataType
    n.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(a) {
                n.globalEval(a);
                return a;
            }
        }
    });
    // Handle cache's special case and crossDomain
    n.ajaxPrefilter("script", function(a) {
        if (a.cache === undefined) {
            a.cache = false;
        }
        if (a.crossDomain) {
            a.type = "GET";
        }
    });
    // Bind script tag hack transport
    n.ajaxTransport("script", function(a) {
        // This transport only deals with cross domain requests
        if (a.crossDomain) {
            var b, c;
            return {
                send: function(d, e) {
                    b = n("<script>").prop({
                        async: true,
                        charset: a.scriptCharset,
                        src: a.url
                    }).on("load error", c = function(a) {
                        b.remove();
                        c = null;
                        if (a) {
                            e(a.type === "error" ? 404 : 200, a.type);
                        }
                    });
                    l.head.appendChild(b[0]);
                },
                abort: function() {
                    if (c) {
                        c();
                    }
                }
            };
        }
    });
    var Fb = [], Gb = /(=)\?(?=&|$)|\?\?/;
    // Default jsonp settings
    n.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var a = Fb.pop() || n.expando + "_" + cb++;
            this[a] = true;
            return a;
        }
    });
    // Detect, normalize options and install callbacks for jsonp requests
    n.ajaxPrefilter("json jsonp", function(b, c, d) {
        var e, f, g, h = b.jsonp !== false && (Gb.test(b.url) ? "url" : typeof b.data === "string" && !(b.contentType || "").indexOf("application/x-www-form-urlencoded") && Gb.test(b.data) && "data");
        // Handle iff the expected data type is "jsonp" or we have a parameter to set
        if (h || b.dataTypes[0] === "jsonp") {
            // Get callback name, remembering preexisting value associated with it
            e = b.jsonpCallback = n.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback;
            // Insert callback into url or form data
            if (h) {
                b[h] = b[h].replace(Gb, "$1" + e);
            } else if (b.jsonp !== false) {
                b.url += (db.test(b.url) ? "&" : "?") + b.jsonp + "=" + e;
            }
            // Use data converter to retrieve json after script execution
            b.converters["script json"] = function() {
                if (!g) {
                    n.error(e + " was not called");
                }
                return g[0];
            };
            // force json dataType
            b.dataTypes[0] = "json";
            // Install callback
            f = a[e];
            a[e] = function() {
                g = arguments;
            };
            // Clean-up function (fires after converters)
            d.always(function() {
                // Restore preexisting value
                a[e] = f;
                // Save back as free
                if (b[e]) {
                    // make sure that re-using the options doesn't screw things around
                    b.jsonpCallback = c.jsonpCallback;
                    // save the callback name for future use
                    Fb.push(e);
                }
                // Call if it was a function and we have a response
                if (g && n.isFunction(f)) {
                    f(g[0]);
                }
                g = f = undefined;
            });
            // Delegate to script
            return "script";
        }
    });
    // data: string of html
    // context (optional): If specified, the fragment will be created in this context, defaults to document
    // keepScripts (optional): If true, will include scripts passed in the html string
    n.parseHTML = function(a, b, c) {
        if (!a || typeof a !== "string") {
            return null;
        }
        if (typeof b === "boolean") {
            c = b;
            b = false;
        }
        b = b || l;
        var d = v.exec(a), e = !c && [];
        // Single tag
        if (d) {
            return [ b.createElement(d[1]) ];
        }
        d = n.buildFragment([ a ], b, e);
        if (e && e.length) {
            n(e).remove();
        }
        return n.merge([], d.childNodes);
    };
    // Keep a copy of the old load method
    var Hb = n.fn.load;
    /**
 * Load a url into a page
 */
    n.fn.load = function(a, b, c) {
        if (typeof a !== "string" && Hb) {
            return Hb.apply(this, arguments);
        }
        var d, e, f, g = this, h = a.indexOf(" ");
        if (h >= 0) {
            d = n.trim(a.slice(h));
            a = a.slice(0, h);
        }
        // If it's a function
        if (n.isFunction(b)) {
            // We assume that it's the callback
            c = b;
            b = undefined;
        } else if (b && typeof b === "object") {
            e = "POST";
        }
        // If we have elements to modify, make the request
        if (g.length > 0) {
            n.ajax({
                url: a,
                // if "type" variable is undefined, then "GET" method will be used
                type: e,
                dataType: "html",
                data: b
            }).done(function(a) {
                // Save response for use in complete callback
                f = arguments;
                g.html(d ? // If a selector was specified, locate the right elements in a dummy div
                // Exclude scripts to avoid IE 'Permission Denied' errors
                n("<div>").append(n.parseHTML(a)).find(d) : // Otherwise use the full result
                a);
            }).complete(c && function(a, b) {
                g.each(c, f || [ a.responseText, b, a ]);
            });
        }
        return this;
    };
    // Attach a bunch of functions for handling common AJAX events
    n.each([ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function(a, b) {
        n.fn[b] = function(a) {
            return this.on(b, a);
        };
    });
    n.expr.filters.animated = function(a) {
        return n.grep(n.timers, function(b) {
            return a === b.elem;
        }).length;
    };
    var Ib = a.document.documentElement;
    /**
 * Gets a window from an element
 */
    function Jb(a) {
        return n.isWindow(a) ? a : a.nodeType === 9 && a.defaultView;
    }
    n.offset = {
        setOffset: function(a, b, c) {
            var d, e, f, g, h, i, j, k = n.css(a, "position"), l = n(a), m = {};
            // Set position first, in-case top/left are set even on static elem
            if (k === "static") {
                a.style.position = "relative";
            }
            h = l.offset();
            f = n.css(a, "top");
            i = n.css(a, "left");
            j = (k === "absolute" || k === "fixed") && (f + i).indexOf("auto") > -1;
            // Need to be able to calculate position if either
            // top or left is auto and position is either absolute or fixed
            if (j) {
                d = l.position();
                g = d.top;
                e = d.left;
            } else {
                g = parseFloat(f) || 0;
                e = parseFloat(i) || 0;
            }
            if (n.isFunction(b)) {
                b = b.call(a, c, h);
            }
            if (b.top != null) {
                m.top = b.top - h.top + g;
            }
            if (b.left != null) {
                m.left = b.left - h.left + e;
            }
            if ("using" in b) {
                b.using.call(a, m);
            } else {
                l.css(m);
            }
        }
    };
    n.fn.extend({
        offset: function(a) {
            if (arguments.length) {
                return a === undefined ? this : this.each(function(b) {
                    n.offset.setOffset(this, a, b);
                });
            }
            var b, c, d = this[0], e = {
                top: 0,
                left: 0
            }, f = d && d.ownerDocument;
            if (!f) {
                return;
            }
            b = f.documentElement;
            // Make sure it's not a disconnected DOM node
            if (!n.contains(b, d)) {
                return e;
            }
            // Support: BlackBerry 5, iOS 3 (original iPhone)
            // If we don't have gBCR, just use 0,0 rather than error
            if (typeof d.getBoundingClientRect !== U) {
                e = d.getBoundingClientRect();
            }
            c = Jb(f);
            return {
                top: e.top + c.pageYOffset - b.clientTop,
                left: e.left + c.pageXOffset - b.clientLeft
            };
        },
        position: function() {
            if (!this[0]) {
                return;
            }
            var a, b, c = this[0], d = {
                top: 0,
                left: 0
            };
            // Fixed elements are offset from window (parentOffset = {top:0, left: 0}, because it is its only offset parent
            if (n.css(c, "position") === "fixed") {
                // Assume getBoundingClientRect is there when computed position is fixed
                b = c.getBoundingClientRect();
            } else {
                // Get *real* offsetParent
                a = this.offsetParent();
                // Get correct offsets
                b = this.offset();
                if (!n.nodeName(a[0], "html")) {
                    d = a.offset();
                }
                // Add offsetParent borders
                d.top += n.css(a[0], "borderTopWidth", true);
                d.left += n.css(a[0], "borderLeftWidth", true);
            }
            // Subtract parent offsets and element margins
            return {
                top: b.top - d.top - n.css(c, "marginTop", true),
                left: b.left - d.left - n.css(c, "marginLeft", true)
            };
        },
        offsetParent: function() {
            return this.map(function() {
                var a = this.offsetParent || Ib;
                while (a && (!n.nodeName(a, "html") && n.css(a, "position") === "static")) {
                    a = a.offsetParent;
                }
                return a || Ib;
            });
        }
    });
    // Create scrollLeft and scrollTop methods
    n.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(b, c) {
        var d = "pageYOffset" === c;
        n.fn[b] = function(e) {
            return J(this, function(b, e, f) {
                var g = Jb(b);
                if (f === undefined) {
                    return g ? g[c] : b[e];
                }
                if (g) {
                    g.scrollTo(!d ? f : a.pageXOffset, d ? f : a.pageYOffset);
                } else {
                    b[e] = f;
                }
            }, b, e, arguments.length, null);
        };
    });
    // Support: Safari<7+, Chrome<37+
    // Add the top/left cssHooks using jQuery.fn.position
    // Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
    // Blink bug: https://code.google.com/p/chromium/issues/detail?id=229280
    // getComputedStyle returns percent when specified for top/left/bottom/right;
    // rather than make the css module depend on the offset module, just check for it here
    n.each([ "top", "left" ], function(a, b) {
        n.cssHooks[b] = ya(k.pixelPosition, function(a, c) {
            if (c) {
                c = xa(a, b);
                // If curCSS returns percentage, fallback to offset
                return va.test(c) ? n(a).position()[b] + "px" : c;
            }
        });
    });
    // Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
    n.each({
        Height: "height",
        Width: "width"
    }, function(a, b) {
        n.each({
            padding: "inner" + a,
            content: b,
            "": "outer" + a
        }, function(c, d) {
            // Margin is only for outerHeight, outerWidth
            n.fn[d] = function(d, e) {
                var f = arguments.length && (c || typeof d !== "boolean"), g = c || (d === true || e === true ? "margin" : "border");
                return J(this, function(b, c, d) {
                    var e;
                    if (n.isWindow(b)) {
                        // As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there
                        // isn't a whole lot we can do. See pull request at this URL for discussion:
                        // https://github.com/jquery/jquery/pull/764
                        return b.document.documentElement["client" + a];
                    }
                    // Get document width or height
                    if (b.nodeType === 9) {
                        e = b.documentElement;
                        // Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
                        // whichever is greatest
                        return Math.max(b.body["scroll" + a], e["scroll" + a], b.body["offset" + a], e["offset" + a], e["client" + a]);
                    }
                    // Get width or height on the element, requesting but not forcing parseFloat
                    // Set width or height on the element
                    return d === undefined ? n.css(b, c, g) : n.style(b, c, d, g);
                }, b, f ? d : undefined, f, null);
            };
        });
    });
    // The number of elements contained in the matched element set
    n.fn.size = function() {
        return this.length;
    };
    n.fn.andSelf = n.fn.addBack;
    // Register as a named AMD module, since jQuery can be concatenated with other
    // files that may use define, but not via a proper concatenation script that
    // understands anonymous AMD modules. A named AMD is safest and most robust
    // way to register. Lowercase jquery is used because AMD module names are
    // derived from file names, and jQuery is normally delivered in a lowercase
    // file name. Do this after creating the global so that if an AMD module wants
    // to call noConflict to hide this version of jQuery, it will work.
    // Note that for maximum portability, libraries that are not jQuery should
    // declare themselves as anonymous modules, and avoid setting a global if an
    // AMD loader is present. jQuery is a special case. For more information, see
    // https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon
    if (typeof define === "function" && define.amd) {
        define("jquery", [], function() {
            return n;
        });
    }
    var // Map over jQuery in case of overwrite
    Kb = a.jQuery, // Map over the $ in case of overwrite
    Lb = a.$;
    n.noConflict = function(b) {
        if (a.$ === n) {
            a.$ = Lb;
        }
        if (b && a.jQuery === n) {
            a.jQuery = Kb;
        }
        return n;
    };
    // Expose jQuery and $ identifiers, even in AMD
    // (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
    // and CommonJS for browser emulators (#13566)
    if (typeof b === U) {
        a.jQuery = a.$ = n;
    }
    return n;
});

/*! jQuery UI - v1.11.4 - 2015-03-11
* http://jqueryui.com
* Includes: core.js, widget.js, mouse.js, position.js, accordion.js, autocomplete.js, button.js, datepicker.js, dialog.js, draggable.js, droppable.js, effect.js, effect-blind.js, effect-bounce.js, effect-clip.js, effect-drop.js, effect-explode.js, effect-fade.js, effect-fold.js, effect-highlight.js, effect-puff.js, effect-pulsate.js, effect-scale.js, effect-shake.js, effect-size.js, effect-slide.js, effect-transfer.js, menu.js, progressbar.js, resizable.js, selectable.js, selectmenu.js, slider.js, sortable.js, spinner.js, tabs.js, tooltip.js
* Copyright 2015 jQuery Foundation and other contributors; Licensed MIT */
(function(a) {
    if (typeof define === "function" && define.amd) {
        // AMD. Register as an anonymous module.
        define([ "jquery" ], a);
    } else {
        // Browser globals
        a(jQuery);
    }
})(function(a) {
    /*!
 * jQuery UI Core 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/category/ui-core/
 */
    // $.ui might exist from components with no dependencies, e.g., $.ui.position
    a.ui = a.ui || {};
    a.extend(a.ui, {
        version: "1.11.4",
        keyCode: {
            BACKSPACE: 8,
            COMMA: 188,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            LEFT: 37,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SPACE: 32,
            TAB: 9,
            UP: 38
        }
    });
    // plugins
    a.fn.extend({
        scrollParent: function(b) {
            var c = this.css("position"), d = c === "absolute", e = b ? /(auto|scroll|hidden)/ : /(auto|scroll)/, f = this.parents().filter(function() {
                var b = a(this);
                if (d && b.css("position") === "static") {
                    return false;
                }
                return e.test(b.css("overflow") + b.css("overflow-y") + b.css("overflow-x"));
            }).eq(0);
            return c === "fixed" || !f.length ? a(this[0].ownerDocument || document) : f;
        },
        uniqueId: function() {
            var a = 0;
            return function() {
                return this.each(function() {
                    if (!this.id) {
                        this.id = "ui-id-" + ++a;
                    }
                });
            };
        }(),
        removeUniqueId: function() {
            return this.each(function() {
                if (/^ui-id-\d+$/.test(this.id)) {
                    a(this).removeAttr("id");
                }
            });
        }
    });
    // selectors
    function b(b, d) {
        var e, f, g, h = b.nodeName.toLowerCase();
        if ("area" === h) {
            e = b.parentNode;
            f = e.name;
            if (!b.href || !f || e.nodeName.toLowerCase() !== "map") {
                return false;
            }
            g = a("img[usemap='#" + f + "']")[0];
            return !!g && c(g);
        }
        // the element and all of its ancestors must be visible
        return (/^(input|select|textarea|button|object)$/.test(h) ? !b.disabled : "a" === h ? b.href || d : d) && c(b);
    }
    function c(b) {
        return a.expr.filters.visible(b) && !a(b).parents().addBack().filter(function() {
            return a.css(this, "visibility") === "hidden";
        }).length;
    }
    a.extend(a.expr[":"], {
        data: a.expr.createPseudo ? a.expr.createPseudo(function(b) {
            return function(c) {
                return !!a.data(c, b);
            };
        }) : // support: jQuery <1.8
        function(b, c, d) {
            return !!a.data(b, d[3]);
        },
        focusable: function(c) {
            return b(c, !isNaN(a.attr(c, "tabindex")));
        },
        tabbable: function(c) {
            var d = a.attr(c, "tabindex"), e = isNaN(d);
            return (e || d >= 0) && b(c, !e);
        }
    });
    // support: jQuery <1.8
    if (!a("<a>").outerWidth(1).jquery) {
        a.each([ "Width", "Height" ], function(b, c) {
            var d = c === "Width" ? [ "Left", "Right" ] : [ "Top", "Bottom" ], e = c.toLowerCase(), f = {
                innerWidth: a.fn.innerWidth,
                innerHeight: a.fn.innerHeight,
                outerWidth: a.fn.outerWidth,
                outerHeight: a.fn.outerHeight
            };
            function g(b, c, e, f) {
                a.each(d, function() {
                    c -= parseFloat(a.css(b, "padding" + this)) || 0;
                    if (e) {
                        c -= parseFloat(a.css(b, "border" + this + "Width")) || 0;
                    }
                    if (f) {
                        c -= parseFloat(a.css(b, "margin" + this)) || 0;
                    }
                });
                return c;
            }
            a.fn["inner" + c] = function(b) {
                if (b === undefined) {
                    return f["inner" + c].call(this);
                }
                return this.each(function() {
                    a(this).css(e, g(this, b) + "px");
                });
            };
            a.fn["outer" + c] = function(b, d) {
                if (typeof b !== "number") {
                    return f["outer" + c].call(this, b);
                }
                return this.each(function() {
                    a(this).css(e, g(this, b, true, d) + "px");
                });
            };
        });
    }
    // support: jQuery <1.8
    if (!a.fn.addBack) {
        a.fn.addBack = function(a) {
            return this.add(a == null ? this.prevObject : this.prevObject.filter(a));
        };
    }
    // support: jQuery 1.6.1, 1.6.2 (http://bugs.jquery.com/ticket/9413)
    if (a("<a>").data("a-b", "a").removeData("a-b").data("a-b")) {
        a.fn.removeData = function(b) {
            return function(c) {
                if (arguments.length) {
                    return b.call(this, a.camelCase(c));
                } else {
                    return b.call(this);
                }
            };
        }(a.fn.removeData);
    }
    // deprecated
    a.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase());
    a.fn.extend({
        focus: function(b) {
            return function(c, d) {
                return typeof c === "number" ? this.each(function() {
                    var b = this;
                    setTimeout(function() {
                        a(b).focus();
                        if (d) {
                            d.call(b);
                        }
                    }, c);
                }) : b.apply(this, arguments);
            };
        }(a.fn.focus),
        disableSelection: function() {
            var a = "onselectstart" in document.createElement("div") ? "selectstart" : "mousedown";
            return function() {
                return this.bind(a + ".ui-disableSelection", function(a) {
                    a.preventDefault();
                });
            };
        }(),
        enableSelection: function() {
            return this.unbind(".ui-disableSelection");
        },
        zIndex: function(b) {
            if (b !== undefined) {
                return this.css("zIndex", b);
            }
            if (this.length) {
                var c = a(this[0]), d, e;
                while (c.length && c[0] !== document) {
                    // Ignore z-index if position is set to a value where z-index is ignored by the browser
                    // This makes behavior of this function consistent across browsers
                    // WebKit always returns auto if the element is positioned
                    d = c.css("position");
                    if (d === "absolute" || d === "relative" || d === "fixed") {
                        // IE returns 0 when zIndex is not specified
                        // other browsers return a string
                        // we ignore the case of nested elements with an explicit value of 0
                        // <div style="z-index: -10;"><div style="z-index: 0;"></div></div>
                        e = parseInt(c.css("zIndex"), 10);
                        if (!isNaN(e) && e !== 0) {
                            return e;
                        }
                    }
                    c = c.parent();
                }
            }
            return 0;
        }
    });
    // $.ui.plugin is deprecated. Use $.widget() extensions instead.
    a.ui.plugin = {
        add: function(b, c, d) {
            var e, f = a.ui[b].prototype;
            for (e in d) {
                f.plugins[e] = f.plugins[e] || [];
                f.plugins[e].push([ c, d[e] ]);
            }
        },
        call: function(a, b, c, d) {
            var e, f = a.plugins[b];
            if (!f) {
                return;
            }
            if (!d && (!a.element[0].parentNode || a.element[0].parentNode.nodeType === 11)) {
                return;
            }
            for (e = 0; e < f.length; e++) {
                if (a.options[f[e][0]]) {
                    f[e][1].apply(a.element, c);
                }
            }
        }
    };
    /*!
 * jQuery UI Widget 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/jQuery.widget/
 */
    var d = 0, e = Array.prototype.slice;
    a.cleanData = function(b) {
        return function(c) {
            var d, e, f;
            for (f = 0; (e = c[f]) != null; f++) {
                try {
                    // Only trigger remove when necessary to save time
                    d = a._data(e, "events");
                    if (d && d.remove) {
                        a(e).triggerHandler("remove");
                    }
                } catch (g) {}
            }
            b(c);
        };
    }(a.cleanData);
    a.widget = function(b, c, d) {
        var e, f, g, h, // proxiedPrototype allows the provided prototype to remain unmodified
        // so that it can be used as a mixin for multiple widgets (#8876)
        i = {}, j = b.split(".")[0];
        b = b.split(".")[1];
        e = j + "-" + b;
        if (!d) {
            d = c;
            c = a.Widget;
        }
        // create selector for plugin
        a.expr[":"][e.toLowerCase()] = function(b) {
            return !!a.data(b, e);
        };
        a[j] = a[j] || {};
        f = a[j][b];
        g = a[j][b] = function(a, b) {
            // allow instantiation without "new" keyword
            if (!this._createWidget) {
                return new g(a, b);
            }
            // allow instantiation without initializing for simple inheritance
            // must use "new" keyword (the code above always passes args)
            if (arguments.length) {
                this._createWidget(a, b);
            }
        };
        // extend with the existing constructor to carry over any static properties
        a.extend(g, f, {
            version: d.version,
            // copy the object used to create the prototype in case we need to
            // redefine the widget later
            _proto: a.extend({}, d),
            // track widgets that inherit from this widget in case this widget is
            // redefined after a widget inherits from it
            _childConstructors: []
        });
        h = new c();
        // we need to make the options hash a property directly on the new instance
        // otherwise we'll modify the options hash on the prototype that we're
        // inheriting from
        h.options = a.widget.extend({}, h.options);
        a.each(d, function(b, d) {
            if (!a.isFunction(d)) {
                i[b] = d;
                return;
            }
            i[b] = function() {
                var a = function() {
                    return c.prototype[b].apply(this, arguments);
                }, e = function(a) {
                    return c.prototype[b].apply(this, a);
                };
                return function() {
                    var b = this._super, c = this._superApply, f;
                    this._super = a;
                    this._superApply = e;
                    f = d.apply(this, arguments);
                    this._super = b;
                    this._superApply = c;
                    return f;
                };
            }();
        });
        g.prototype = a.widget.extend(h, {
            // TODO: remove support for widgetEventPrefix
            // always use the name + a colon as the prefix, e.g., draggable:start
            // don't prefix for widgets that aren't DOM-based
            widgetEventPrefix: f ? h.widgetEventPrefix || b : b
        }, i, {
            constructor: g,
            namespace: j,
            widgetName: b,
            widgetFullName: e
        });
        // If this widget is being redefined then we need to find all widgets that
        // are inheriting from it and redefine all of them so that they inherit from
        // the new version of this widget. We're essentially trying to replace one
        // level in the prototype chain.
        if (f) {
            a.each(f._childConstructors, function(b, c) {
                var d = c.prototype;
                // redefine the child widget using the same prototype that was
                // originally used, but inherit from the new version of the base
                a.widget(d.namespace + "." + d.widgetName, g, c._proto);
            });
            // remove the list of existing child constructors from the old constructor
            // so the old child constructors can be garbage collected
            delete f._childConstructors;
        } else {
            c._childConstructors.push(g);
        }
        a.widget.bridge(b, g);
        return g;
    };
    a.widget.extend = function(b) {
        var c = e.call(arguments, 1), d = 0, f = c.length, g, h;
        for (;d < f; d++) {
            for (g in c[d]) {
                h = c[d][g];
                if (c[d].hasOwnProperty(g) && h !== undefined) {
                    // Clone objects
                    if (a.isPlainObject(h)) {
                        b[g] = a.isPlainObject(b[g]) ? a.widget.extend({}, b[g], h) : // Don't extend strings, arrays, etc. with objects
                        a.widget.extend({}, h);
                    } else {
                        b[g] = h;
                    }
                }
            }
        }
        return b;
    };
    a.widget.bridge = function(b, c) {
        var d = c.prototype.widgetFullName || b;
        a.fn[b] = function(f) {
            var g = typeof f === "string", h = e.call(arguments, 1), i = this;
            if (g) {
                this.each(function() {
                    var c, e = a.data(this, d);
                    if (f === "instance") {
                        i = e;
                        return false;
                    }
                    if (!e) {
                        return a.error("cannot call methods on " + b + " prior to initialization; " + "attempted to call method '" + f + "'");
                    }
                    if (!a.isFunction(e[f]) || f.charAt(0) === "_") {
                        return a.error("no such method '" + f + "' for " + b + " widget instance");
                    }
                    c = e[f].apply(e, h);
                    if (c !== e && c !== undefined) {
                        i = c && c.jquery ? i.pushStack(c.get()) : c;
                        return false;
                    }
                });
            } else {
                // Allow multiple hashes to be passed on init
                if (h.length) {
                    f = a.widget.extend.apply(null, [ f ].concat(h));
                }
                this.each(function() {
                    var b = a.data(this, d);
                    if (b) {
                        b.option(f || {});
                        if (b._init) {
                            b._init();
                        }
                    } else {
                        a.data(this, d, new c(f, this));
                    }
                });
            }
            return i;
        };
    };
    a.Widget = function() {};
    a.Widget._childConstructors = [];
    a.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        defaultElement: "<div>",
        options: {
            disabled: false,
            // callbacks
            create: null
        },
        _createWidget: function(b, c) {
            c = a(c || this.defaultElement || this)[0];
            this.element = a(c);
            this.uuid = d++;
            this.eventNamespace = "." + this.widgetName + this.uuid;
            this.bindings = a();
            this.hoverable = a();
            this.focusable = a();
            if (c !== this) {
                a.data(c, this.widgetFullName, this);
                this._on(true, this.element, {
                    remove: function(a) {
                        if (a.target === c) {
                            this.destroy();
                        }
                    }
                });
                this.document = a(c.style ? // element within the document
                c.ownerDocument : // element is window or document
                c.document || c);
                this.window = a(this.document[0].defaultView || this.document[0].parentWindow);
            }
            this.options = a.widget.extend({}, this.options, this._getCreateOptions(), b);
            this._create();
            this._trigger("create", null, this._getCreateEventData());
            this._init();
        },
        _getCreateOptions: a.noop,
        _getCreateEventData: a.noop,
        _create: a.noop,
        _init: a.noop,
        destroy: function() {
            this._destroy();
            // we can probably remove the unbind calls in 2.0
            // all event bindings should go through this._on()
            this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(a.camelCase(this.widgetFullName));
            this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled " + "ui-state-disabled");
            // clean up events and states
            this.bindings.unbind(this.eventNamespace);
            this.hoverable.removeClass("ui-state-hover");
            this.focusable.removeClass("ui-state-focus");
        },
        _destroy: a.noop,
        widget: function() {
            return this.element;
        },
        option: function(b, c) {
            var d = b, e, f, g;
            if (arguments.length === 0) {
                // don't return a reference to the internal hash
                return a.widget.extend({}, this.options);
            }
            if (typeof b === "string") {
                // handle nested keys, e.g., "foo.bar" => { foo: { bar: ___ } }
                d = {};
                e = b.split(".");
                b = e.shift();
                if (e.length) {
                    f = d[b] = a.widget.extend({}, this.options[b]);
                    for (g = 0; g < e.length - 1; g++) {
                        f[e[g]] = f[e[g]] || {};
                        f = f[e[g]];
                    }
                    b = e.pop();
                    if (arguments.length === 1) {
                        return f[b] === undefined ? null : f[b];
                    }
                    f[b] = c;
                } else {
                    if (arguments.length === 1) {
                        return this.options[b] === undefined ? null : this.options[b];
                    }
                    d[b] = c;
                }
            }
            this._setOptions(d);
            return this;
        },
        _setOptions: function(a) {
            var b;
            for (b in a) {
                this._setOption(b, a[b]);
            }
            return this;
        },
        _setOption: function(a, b) {
            this.options[a] = b;
            if (a === "disabled") {
                this.widget().toggleClass(this.widgetFullName + "-disabled", !!b);
                // If the widget is becoming disabled, then nothing is interactive
                if (b) {
                    this.hoverable.removeClass("ui-state-hover");
                    this.focusable.removeClass("ui-state-focus");
                }
            }
            return this;
        },
        enable: function() {
            return this._setOptions({
                disabled: false
            });
        },
        disable: function() {
            return this._setOptions({
                disabled: true
            });
        },
        _on: function(b, c, d) {
            var e, f = this;
            // no suppressDisabledCheck flag, shuffle arguments
            if (typeof b !== "boolean") {
                d = c;
                c = b;
                b = false;
            }
            // no element argument, shuffle and use this.element
            if (!d) {
                d = c;
                c = this.element;
                e = this.widget();
            } else {
                c = e = a(c);
                this.bindings = this.bindings.add(c);
            }
            a.each(d, function(d, g) {
                function h() {
                    // allow widgets to customize the disabled handling
                    // - disabled as an array instead of boolean
                    // - disabled class as method for disabling individual parts
                    if (!b && (f.options.disabled === true || a(this).hasClass("ui-state-disabled"))) {
                        return;
                    }
                    return (typeof g === "string" ? f[g] : g).apply(f, arguments);
                }
                // copy the guid so direct unbinding works
                if (typeof g !== "string") {
                    h.guid = g.guid = g.guid || h.guid || a.guid++;
                }
                var i = d.match(/^([\w:-]*)\s*(.*)$/), j = i[1] + f.eventNamespace, k = i[2];
                if (k) {
                    e.delegate(k, j, h);
                } else {
                    c.bind(j, h);
                }
            });
        },
        _off: function(b, c) {
            c = (c || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace;
            b.unbind(c).undelegate(c);
            // Clear the stack to avoid memory leaks (#10056)
            this.bindings = a(this.bindings.not(b).get());
            this.focusable = a(this.focusable.not(b).get());
            this.hoverable = a(this.hoverable.not(b).get());
        },
        _delay: function(a, b) {
            function c() {
                return (typeof a === "string" ? d[a] : a).apply(d, arguments);
            }
            var d = this;
            return setTimeout(c, b || 0);
        },
        _hoverable: function(b) {
            this.hoverable = this.hoverable.add(b);
            this._on(b, {
                mouseenter: function(b) {
                    a(b.currentTarget).addClass("ui-state-hover");
                },
                mouseleave: function(b) {
                    a(b.currentTarget).removeClass("ui-state-hover");
                }
            });
        },
        _focusable: function(b) {
            this.focusable = this.focusable.add(b);
            this._on(b, {
                focusin: function(b) {
                    a(b.currentTarget).addClass("ui-state-focus");
                },
                focusout: function(b) {
                    a(b.currentTarget).removeClass("ui-state-focus");
                }
            });
        },
        _trigger: function(b, c, d) {
            var e, f, g = this.options[b];
            d = d || {};
            c = a.Event(c);
            c.type = (b === this.widgetEventPrefix ? b : this.widgetEventPrefix + b).toLowerCase();
            // the original event may come from any element
            // so we need to reset the target on the new event
            c.target = this.element[0];
            // copy original event properties over to the new event
            f = c.originalEvent;
            if (f) {
                for (e in f) {
                    if (!(e in c)) {
                        c[e] = f[e];
                    }
                }
            }
            this.element.trigger(c, d);
            return !(a.isFunction(g) && g.apply(this.element[0], [ c ].concat(d)) === false || c.isDefaultPrevented());
        }
    };
    a.each({
        show: "fadeIn",
        hide: "fadeOut"
    }, function(b, c) {
        a.Widget.prototype["_" + b] = function(d, e, f) {
            if (typeof e === "string") {
                e = {
                    effect: e
                };
            }
            var g, h = !e ? b : e === true || typeof e === "number" ? c : e.effect || c;
            e = e || {};
            if (typeof e === "number") {
                e = {
                    duration: e
                };
            }
            g = !a.isEmptyObject(e);
            e.complete = f;
            if (e.delay) {
                d.delay(e.delay);
            }
            if (g && a.effects && a.effects.effect[h]) {
                d[b](e);
            } else if (h !== b && d[h]) {
                d[h](e.duration, e.easing, f);
            } else {
                d.queue(function(c) {
                    a(this)[b]();
                    if (f) {
                        f.call(d[0]);
                    }
                    c();
                });
            }
        };
    });
    var f = a.widget;
    /*!
 * jQuery UI Mouse 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/mouse/
 */
    var g = false;
    a(document).mouseup(function() {
        g = false;
    });
    var h = a.widget("ui.mouse", {
        version: "1.11.4",
        options: {
            cancel: "input,textarea,button,select,option",
            distance: 1,
            delay: 0
        },
        _mouseInit: function() {
            var b = this;
            this.element.bind("mousedown." + this.widgetName, function(a) {
                return b._mouseDown(a);
            }).bind("click." + this.widgetName, function(c) {
                if (true === a.data(c.target, b.widgetName + ".preventClickEvent")) {
                    a.removeData(c.target, b.widgetName + ".preventClickEvent");
                    c.stopImmediatePropagation();
                    return false;
                }
            });
            this.started = false;
        },
        // TODO: make sure destroying one instance of mouse doesn't mess with
        // other instances of mouse
        _mouseDestroy: function() {
            this.element.unbind("." + this.widgetName);
            if (this._mouseMoveDelegate) {
                this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate);
            }
        },
        _mouseDown: function(b) {
            // don't let more than one widget handle mouseStart
            if (g) {
                return;
            }
            this._mouseMoved = false;
            // we may have missed mouseup (out of window)
            this._mouseStarted && this._mouseUp(b);
            this._mouseDownEvent = b;
            var c = this, d = b.which === 1, // event.target.nodeName works around a bug in IE 8 with
            // disabled inputs (#7620)
            e = typeof this.options.cancel === "string" && b.target.nodeName ? a(b.target).closest(this.options.cancel).length : false;
            if (!d || e || !this._mouseCapture(b)) {
                return true;
            }
            this.mouseDelayMet = !this.options.delay;
            if (!this.mouseDelayMet) {
                this._mouseDelayTimer = setTimeout(function() {
                    c.mouseDelayMet = true;
                }, this.options.delay);
            }
            if (this._mouseDistanceMet(b) && this._mouseDelayMet(b)) {
                this._mouseStarted = this._mouseStart(b) !== false;
                if (!this._mouseStarted) {
                    b.preventDefault();
                    return true;
                }
            }
            // Click event may never have fired (Gecko & Opera)
            if (true === a.data(b.target, this.widgetName + ".preventClickEvent")) {
                a.removeData(b.target, this.widgetName + ".preventClickEvent");
            }
            // these delegates are required to keep context
            this._mouseMoveDelegate = function(a) {
                return c._mouseMove(a);
            };
            this._mouseUpDelegate = function(a) {
                return c._mouseUp(a);
            };
            this.document.bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate);
            b.preventDefault();
            g = true;
            return true;
        },
        _mouseMove: function(b) {
            // Only check for mouseups outside the document if you've moved inside the document
            // at least once. This prevents the firing of mouseup in the case of IE<9, which will
            // fire a mousemove event if content is placed under the cursor. See #7778
            // Support: IE <9
            if (this._mouseMoved) {
                // IE mouseup check - mouseup happened when mouse was out of window
                if (a.ui.ie && (!document.documentMode || document.documentMode < 9) && !b.button) {
                    return this._mouseUp(b);
                } else if (!b.which) {
                    return this._mouseUp(b);
                }
            }
            if (b.which || b.button) {
                this._mouseMoved = true;
            }
            if (this._mouseStarted) {
                this._mouseDrag(b);
                return b.preventDefault();
            }
            if (this._mouseDistanceMet(b) && this._mouseDelayMet(b)) {
                this._mouseStarted = this._mouseStart(this._mouseDownEvent, b) !== false;
                this._mouseStarted ? this._mouseDrag(b) : this._mouseUp(b);
            }
            return !this._mouseStarted;
        },
        _mouseUp: function(b) {
            this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate);
            if (this._mouseStarted) {
                this._mouseStarted = false;
                if (b.target === this._mouseDownEvent.target) {
                    a.data(b.target, this.widgetName + ".preventClickEvent", true);
                }
                this._mouseStop(b);
            }
            g = false;
            return false;
        },
        _mouseDistanceMet: function(a) {
            return Math.max(Math.abs(this._mouseDownEvent.pageX - a.pageX), Math.abs(this._mouseDownEvent.pageY - a.pageY)) >= this.options.distance;
        },
        _mouseDelayMet: function() {
            return this.mouseDelayMet;
        },
        // These are placeholder methods, to be overriden by extending plugin
        _mouseStart: function() {},
        _mouseDrag: function() {},
        _mouseStop: function() {},
        _mouseCapture: function() {
            return true;
        }
    });
    /*!
 * jQuery UI Position 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/position/
 */
    (function() {
        a.ui = a.ui || {};
        var b, c, d = Math.max, e = Math.abs, f = Math.round, g = /left|center|right/, h = /top|center|bottom/, i = /[\+\-]\d+(\.[\d]+)?%?/, j = /^\w+/, k = /%$/, l = a.fn.position;
        function m(a, b, c) {
            return [ parseFloat(a[0]) * (k.test(a[0]) ? b / 100 : 1), parseFloat(a[1]) * (k.test(a[1]) ? c / 100 : 1) ];
        }
        function n(b, c) {
            return parseInt(a.css(b, c), 10) || 0;
        }
        function o(b) {
            var c = b[0];
            if (c.nodeType === 9) {
                return {
                    width: b.width(),
                    height: b.height(),
                    offset: {
                        top: 0,
                        left: 0
                    }
                };
            }
            if (a.isWindow(c)) {
                return {
                    width: b.width(),
                    height: b.height(),
                    offset: {
                        top: b.scrollTop(),
                        left: b.scrollLeft()
                    }
                };
            }
            if (c.preventDefault) {
                return {
                    width: 0,
                    height: 0,
                    offset: {
                        top: c.pageY,
                        left: c.pageX
                    }
                };
            }
            return {
                width: b.outerWidth(),
                height: b.outerHeight(),
                offset: b.offset()
            };
        }
        a.position = {
            scrollbarWidth: function() {
                if (b !== undefined) {
                    return b;
                }
                var c, d, e = a("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"), f = e.children()[0];
                a("body").append(e);
                c = f.offsetWidth;
                e.css("overflow", "scroll");
                d = f.offsetWidth;
                if (c === d) {
                    d = e[0].clientWidth;
                }
                e.remove();
                return b = c - d;
            },
            getScrollInfo: function(b) {
                var c = b.isWindow || b.isDocument ? "" : b.element.css("overflow-x"), d = b.isWindow || b.isDocument ? "" : b.element.css("overflow-y"), e = c === "scroll" || c === "auto" && b.width < b.element[0].scrollWidth, f = d === "scroll" || d === "auto" && b.height < b.element[0].scrollHeight;
                return {
                    width: f ? a.position.scrollbarWidth() : 0,
                    height: e ? a.position.scrollbarWidth() : 0
                };
            },
            getWithinInfo: function(b) {
                var c = a(b || window), d = a.isWindow(c[0]), e = !!c[0] && c[0].nodeType === 9;
                return {
                    element: c,
                    isWindow: d,
                    isDocument: e,
                    offset: c.offset() || {
                        left: 0,
                        top: 0
                    },
                    scrollLeft: c.scrollLeft(),
                    scrollTop: c.scrollTop(),
                    // support: jQuery 1.6.x
                    // jQuery 1.6 doesn't support .outerWidth/Height() on documents or windows
                    width: d || e ? c.width() : c.outerWidth(),
                    height: d || e ? c.height() : c.outerHeight()
                };
            }
        };
        a.fn.position = function(b) {
            if (!b || !b.of) {
                return l.apply(this, arguments);
            }
            // make a copy, we don't want to modify arguments
            b = a.extend({}, b);
            var k, p, q, r, s, t, u = a(b.of), v = a.position.getWithinInfo(b.within), w = a.position.getScrollInfo(v), x = (b.collision || "flip").split(" "), y = {};
            t = o(u);
            if (u[0].preventDefault) {
                // force left top to allow flipping
                b.at = "left top";
            }
            p = t.width;
            q = t.height;
            r = t.offset;
            // clone to reuse original targetOffset later
            s = a.extend({}, r);
            // force my and at to have valid horizontal and vertical positions
            // if a value is missing or invalid, it will be converted to center
            a.each([ "my", "at" ], function() {
                var a = (b[this] || "").split(" "), c, d;
                if (a.length === 1) {
                    a = g.test(a[0]) ? a.concat([ "center" ]) : h.test(a[0]) ? [ "center" ].concat(a) : [ "center", "center" ];
                }
                a[0] = g.test(a[0]) ? a[0] : "center";
                a[1] = h.test(a[1]) ? a[1] : "center";
                // calculate offsets
                c = i.exec(a[0]);
                d = i.exec(a[1]);
                y[this] = [ c ? c[0] : 0, d ? d[0] : 0 ];
                // reduce to just the positions without the offsets
                b[this] = [ j.exec(a[0])[0], j.exec(a[1])[0] ];
            });
            // normalize collision option
            if (x.length === 1) {
                x[1] = x[0];
            }
            if (b.at[0] === "right") {
                s.left += p;
            } else if (b.at[0] === "center") {
                s.left += p / 2;
            }
            if (b.at[1] === "bottom") {
                s.top += q;
            } else if (b.at[1] === "center") {
                s.top += q / 2;
            }
            k = m(y.at, p, q);
            s.left += k[0];
            s.top += k[1];
            return this.each(function() {
                var g, h, i = a(this), j = i.outerWidth(), l = i.outerHeight(), o = n(this, "marginLeft"), t = n(this, "marginTop"), z = j + o + n(this, "marginRight") + w.width, A = l + t + n(this, "marginBottom") + w.height, B = a.extend({}, s), C = m(y.my, i.outerWidth(), i.outerHeight());
                if (b.my[0] === "right") {
                    B.left -= j;
                } else if (b.my[0] === "center") {
                    B.left -= j / 2;
                }
                if (b.my[1] === "bottom") {
                    B.top -= l;
                } else if (b.my[1] === "center") {
                    B.top -= l / 2;
                }
                B.left += C[0];
                B.top += C[1];
                // if the browser doesn't support fractions, then round for consistent results
                if (!c) {
                    B.left = f(B.left);
                    B.top = f(B.top);
                }
                g = {
                    marginLeft: o,
                    marginTop: t
                };
                a.each([ "left", "top" ], function(c, d) {
                    if (a.ui.position[x[c]]) {
                        a.ui.position[x[c]][d](B, {
                            targetWidth: p,
                            targetHeight: q,
                            elemWidth: j,
                            elemHeight: l,
                            collisionPosition: g,
                            collisionWidth: z,
                            collisionHeight: A,
                            offset: [ k[0] + C[0], k[1] + C[1] ],
                            my: b.my,
                            at: b.at,
                            within: v,
                            elem: i
                        });
                    }
                });
                if (b.using) {
                    // adds feedback as second argument to using callback, if present
                    h = function(a) {
                        var c = r.left - B.left, f = c + p - j, g = r.top - B.top, h = g + q - l, k = {
                            target: {
                                element: u,
                                left: r.left,
                                top: r.top,
                                width: p,
                                height: q
                            },
                            element: {
                                element: i,
                                left: B.left,
                                top: B.top,
                                width: j,
                                height: l
                            },
                            horizontal: f < 0 ? "left" : c > 0 ? "right" : "center",
                            vertical: h < 0 ? "top" : g > 0 ? "bottom" : "middle"
                        };
                        if (p < j && e(c + f) < p) {
                            k.horizontal = "center";
                        }
                        if (q < l && e(g + h) < q) {
                            k.vertical = "middle";
                        }
                        if (d(e(c), e(f)) > d(e(g), e(h))) {
                            k.important = "horizontal";
                        } else {
                            k.important = "vertical";
                        }
                        b.using.call(this, a, k);
                    };
                }
                i.offset(a.extend(B, {
                    using: h
                }));
            });
        };
        a.ui.position = {
            fit: {
                left: function(a, b) {
                    var c = b.within, e = c.isWindow ? c.scrollLeft : c.offset.left, f = c.width, g = a.left - b.collisionPosition.marginLeft, h = e - g, i = g + b.collisionWidth - f - e, j;
                    // element is wider than within
                    if (b.collisionWidth > f) {
                        // element is initially over the left side of within
                        if (h > 0 && i <= 0) {
                            j = a.left + h + b.collisionWidth - f - e;
                            a.left += h - j;
                        } else if (i > 0 && h <= 0) {
                            a.left = e;
                        } else {
                            if (h > i) {
                                a.left = e + f - b.collisionWidth;
                            } else {
                                a.left = e;
                            }
                        }
                    } else if (h > 0) {
                        a.left += h;
                    } else if (i > 0) {
                        a.left -= i;
                    } else {
                        a.left = d(a.left - g, a.left);
                    }
                },
                top: function(a, b) {
                    var c = b.within, e = c.isWindow ? c.scrollTop : c.offset.top, f = b.within.height, g = a.top - b.collisionPosition.marginTop, h = e - g, i = g + b.collisionHeight - f - e, j;
                    // element is taller than within
                    if (b.collisionHeight > f) {
                        // element is initially over the top of within
                        if (h > 0 && i <= 0) {
                            j = a.top + h + b.collisionHeight - f - e;
                            a.top += h - j;
                        } else if (i > 0 && h <= 0) {
                            a.top = e;
                        } else {
                            if (h > i) {
                                a.top = e + f - b.collisionHeight;
                            } else {
                                a.top = e;
                            }
                        }
                    } else if (h > 0) {
                        a.top += h;
                    } else if (i > 0) {
                        a.top -= i;
                    } else {
                        a.top = d(a.top - g, a.top);
                    }
                }
            },
            flip: {
                left: function(a, b) {
                    var c = b.within, d = c.offset.left + c.scrollLeft, f = c.width, g = c.isWindow ? c.scrollLeft : c.offset.left, h = a.left - b.collisionPosition.marginLeft, i = h - g, j = h + b.collisionWidth - f - g, k = b.my[0] === "left" ? -b.elemWidth : b.my[0] === "right" ? b.elemWidth : 0, l = b.at[0] === "left" ? b.targetWidth : b.at[0] === "right" ? -b.targetWidth : 0, m = -2 * b.offset[0], n, o;
                    if (i < 0) {
                        n = a.left + k + l + m + b.collisionWidth - f - d;
                        if (n < 0 || n < e(i)) {
                            a.left += k + l + m;
                        }
                    } else if (j > 0) {
                        o = a.left - b.collisionPosition.marginLeft + k + l + m - g;
                        if (o > 0 || e(o) < j) {
                            a.left += k + l + m;
                        }
                    }
                },
                top: function(a, b) {
                    var c = b.within, d = c.offset.top + c.scrollTop, f = c.height, g = c.isWindow ? c.scrollTop : c.offset.top, h = a.top - b.collisionPosition.marginTop, i = h - g, j = h + b.collisionHeight - f - g, k = b.my[1] === "top", l = k ? -b.elemHeight : b.my[1] === "bottom" ? b.elemHeight : 0, m = b.at[1] === "top" ? b.targetHeight : b.at[1] === "bottom" ? -b.targetHeight : 0, n = -2 * b.offset[1], o, p;
                    if (i < 0) {
                        p = a.top + l + m + n + b.collisionHeight - f - d;
                        if (p < 0 || p < e(i)) {
                            a.top += l + m + n;
                        }
                    } else if (j > 0) {
                        o = a.top - b.collisionPosition.marginTop + l + m + n - g;
                        if (o > 0 || e(o) < j) {
                            a.top += l + m + n;
                        }
                    }
                }
            },
            flipfit: {
                left: function() {
                    a.ui.position.flip.left.apply(this, arguments);
                    a.ui.position.fit.left.apply(this, arguments);
                },
                top: function() {
                    a.ui.position.flip.top.apply(this, arguments);
                    a.ui.position.fit.top.apply(this, arguments);
                }
            }
        };
        // fraction support test
        (function() {
            var b, d, e, f, g, h = document.getElementsByTagName("body")[0], i = document.createElement("div");
            //Create a "fake body" for testing based on method used in jQuery.support
            b = document.createElement(h ? "div" : "body");
            e = {
                visibility: "hidden",
                width: 0,
                height: 0,
                border: 0,
                margin: 0,
                background: "none"
            };
            if (h) {
                a.extend(e, {
                    position: "absolute",
                    left: "-1000px",
                    top: "-1000px"
                });
            }
            for (g in e) {
                b.style[g] = e[g];
            }
            b.appendChild(i);
            d = h || document.documentElement;
            d.insertBefore(b, d.firstChild);
            i.style.cssText = "position: absolute; left: 10.7432222px;";
            f = a(i).offset().left;
            c = f > 10 && f < 11;
            b.innerHTML = "";
            d.removeChild(b);
        })();
    })();
    var i = a.ui.position;
    /*!
 * jQuery UI Accordion 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/accordion/
 */
    var j = a.widget("ui.accordion", {
        version: "1.11.4",
        options: {
            active: 0,
            animate: {},
            collapsible: false,
            event: "click",
            header: "> li > :first-child,> :not(li):even",
            heightStyle: "auto",
            icons: {
                activeHeader: "ui-icon-triangle-1-s",
                header: "ui-icon-triangle-1-e"
            },
            // callbacks
            activate: null,
            beforeActivate: null
        },
        hideProps: {
            borderTopWidth: "hide",
            borderBottomWidth: "hide",
            paddingTop: "hide",
            paddingBottom: "hide",
            height: "hide"
        },
        showProps: {
            borderTopWidth: "show",
            borderBottomWidth: "show",
            paddingTop: "show",
            paddingBottom: "show",
            height: "show"
        },
        _create: function() {
            var b = this.options;
            this.prevShow = this.prevHide = a();
            this.element.addClass("ui-accordion ui-widget ui-helper-reset").attr("role", "tablist");
            // don't allow collapsible: false and active: false / null
            if (!b.collapsible && (b.active === false || b.active == null)) {
                b.active = 0;
            }
            this._processPanels();
            // handle negative values
            if (b.active < 0) {
                b.active += this.headers.length;
            }
            this._refresh();
        },
        _getCreateEventData: function() {
            return {
                header: this.active,
                panel: !this.active.length ? a() : this.active.next()
            };
        },
        _createIcons: function() {
            var b = this.options.icons;
            if (b) {
                a("<span>").addClass("ui-accordion-header-icon ui-icon " + b.header).prependTo(this.headers);
                this.active.children(".ui-accordion-header-icon").removeClass(b.header).addClass(b.activeHeader);
                this.headers.addClass("ui-accordion-icons");
            }
        },
        _destroyIcons: function() {
            this.headers.removeClass("ui-accordion-icons").children(".ui-accordion-header-icon").remove();
        },
        _destroy: function() {
            var a;
            // clean up main element
            this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role");
            // clean up headers
            this.headers.removeClass("ui-accordion-header ui-accordion-header-active ui-state-default " + "ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-selected").removeAttr("aria-controls").removeAttr("tabIndex").removeUniqueId();
            this._destroyIcons();
            // clean up content panels
            a = this.headers.next().removeClass("ui-helper-reset ui-widget-content ui-corner-bottom " + "ui-accordion-content ui-accordion-content-active ui-state-disabled").css("display", "").removeAttr("role").removeAttr("aria-hidden").removeAttr("aria-labelledby").removeUniqueId();
            if (this.options.heightStyle !== "content") {
                a.css("height", "");
            }
        },
        _setOption: function(a, b) {
            if (a === "active") {
                // _activate() will handle invalid values and update this.options
                this._activate(b);
                return;
            }
            if (a === "event") {
                if (this.options.event) {
                    this._off(this.headers, this.options.event);
                }
                this._setupEvents(b);
            }
            this._super(a, b);
            // setting collapsible: false while collapsed; open first panel
            if (a === "collapsible" && !b && this.options.active === false) {
                this._activate(0);
            }
            if (a === "icons") {
                this._destroyIcons();
                if (b) {
                    this._createIcons();
                }
            }
            // #5332 - opacity doesn't cascade to positioned elements in IE
            // so we need to add the disabled class to the headers and panels
            if (a === "disabled") {
                this.element.toggleClass("ui-state-disabled", !!b).attr("aria-disabled", b);
                this.headers.add(this.headers.next()).toggleClass("ui-state-disabled", !!b);
            }
        },
        _keydown: function(b) {
            if (b.altKey || b.ctrlKey) {
                return;
            }
            var c = a.ui.keyCode, d = this.headers.length, e = this.headers.index(b.target), f = false;
            switch (b.keyCode) {
              case c.RIGHT:
              case c.DOWN:
                f = this.headers[(e + 1) % d];
                break;

              case c.LEFT:
              case c.UP:
                f = this.headers[(e - 1 + d) % d];
                break;

              case c.SPACE:
              case c.ENTER:
                this._eventHandler(b);
                break;

              case c.HOME:
                f = this.headers[0];
                break;

              case c.END:
                f = this.headers[d - 1];
                break;
            }
            if (f) {
                a(b.target).attr("tabIndex", -1);
                a(f).attr("tabIndex", 0);
                f.focus();
                b.preventDefault();
            }
        },
        _panelKeyDown: function(b) {
            if (b.keyCode === a.ui.keyCode.UP && b.ctrlKey) {
                a(b.currentTarget).prev().focus();
            }
        },
        refresh: function() {
            var b = this.options;
            this._processPanels();
            // was collapsed or no panel
            if (b.active === false && b.collapsible === true || !this.headers.length) {
                b.active = false;
                this.active = a();
            } else if (b.active === false) {
                this._activate(0);
            } else if (this.active.length && !a.contains(this.element[0], this.active[0])) {
                // all remaining panel are disabled
                if (this.headers.length === this.headers.find(".ui-state-disabled").length) {
                    b.active = false;
                    this.active = a();
                } else {
                    this._activate(Math.max(0, b.active - 1));
                }
            } else {
                // make sure active index is correct
                b.active = this.headers.index(this.active);
            }
            this._destroyIcons();
            this._refresh();
        },
        _processPanels: function() {
            var a = this.headers, b = this.panels;
            this.headers = this.element.find(this.options.header).addClass("ui-accordion-header ui-state-default ui-corner-all");
            this.panels = this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").filter(":not(.ui-accordion-content-active)").hide();
            // Avoid memory leaks (#10056)
            if (b) {
                this._off(a.not(this.headers));
                this._off(b.not(this.panels));
            }
        },
        _refresh: function() {
            var b, c = this.options, d = c.heightStyle, e = this.element.parent();
            this.active = this._findActive(c.active).addClass("ui-accordion-header-active ui-state-active ui-corner-top").removeClass("ui-corner-all");
            this.active.next().addClass("ui-accordion-content-active").show();
            this.headers.attr("role", "tab").each(function() {
                var b = a(this), c = b.uniqueId().attr("id"), d = b.next(), e = d.uniqueId().attr("id");
                b.attr("aria-controls", e);
                d.attr("aria-labelledby", c);
            }).next().attr("role", "tabpanel");
            this.headers.not(this.active).attr({
                "aria-selected": "false",
                "aria-expanded": "false",
                tabIndex: -1
            }).next().attr({
                "aria-hidden": "true"
            }).hide();
            // make sure at least one header is in the tab order
            if (!this.active.length) {
                this.headers.eq(0).attr("tabIndex", 0);
            } else {
                this.active.attr({
                    "aria-selected": "true",
                    "aria-expanded": "true",
                    tabIndex: 0
                }).next().attr({
                    "aria-hidden": "false"
                });
            }
            this._createIcons();
            this._setupEvents(c.event);
            if (d === "fill") {
                b = e.height();
                this.element.siblings(":visible").each(function() {
                    var c = a(this), d = c.css("position");
                    if (d === "absolute" || d === "fixed") {
                        return;
                    }
                    b -= c.outerHeight(true);
                });
                this.headers.each(function() {
                    b -= a(this).outerHeight(true);
                });
                this.headers.next().each(function() {
                    a(this).height(Math.max(0, b - a(this).innerHeight() + a(this).height()));
                }).css("overflow", "auto");
            } else if (d === "auto") {
                b = 0;
                this.headers.next().each(function() {
                    b = Math.max(b, a(this).css("height", "").height());
                }).height(b);
            }
        },
        _activate: function(b) {
            var c = this._findActive(b)[0];
            // trying to activate the already active panel
            if (c === this.active[0]) {
                return;
            }
            // trying to collapse, simulate a click on the currently active header
            c = c || this.active[0];
            this._eventHandler({
                target: c,
                currentTarget: c,
                preventDefault: a.noop
            });
        },
        _findActive: function(b) {
            return typeof b === "number" ? this.headers.eq(b) : a();
        },
        _setupEvents: function(b) {
            var c = {
                keydown: "_keydown"
            };
            if (b) {
                a.each(b.split(" "), function(a, b) {
                    c[b] = "_eventHandler";
                });
            }
            this._off(this.headers.add(this.headers.next()));
            this._on(this.headers, c);
            this._on(this.headers.next(), {
                keydown: "_panelKeyDown"
            });
            this._hoverable(this.headers);
            this._focusable(this.headers);
        },
        _eventHandler: function(b) {
            var c = this.options, d = this.active, e = a(b.currentTarget), f = e[0] === d[0], g = f && c.collapsible, h = g ? a() : e.next(), i = d.next(), j = {
                oldHeader: d,
                oldPanel: i,
                newHeader: g ? a() : e,
                newPanel: h
            };
            b.preventDefault();
            if (// click on active header, but not collapsible
            f && !c.collapsible || // allow canceling activation
            this._trigger("beforeActivate", b, j) === false) {
                return;
            }
            c.active = g ? false : this.headers.index(e);
            // when the call to ._toggle() comes after the class changes
            // it causes a very odd bug in IE 8 (see #6720)
            this.active = f ? a() : e;
            this._toggle(j);
            // switch classes
            // corner classes on the previously active header stay after the animation
            d.removeClass("ui-accordion-header-active ui-state-active");
            if (c.icons) {
                d.children(".ui-accordion-header-icon").removeClass(c.icons.activeHeader).addClass(c.icons.header);
            }
            if (!f) {
                e.removeClass("ui-corner-all").addClass("ui-accordion-header-active ui-state-active ui-corner-top");
                if (c.icons) {
                    e.children(".ui-accordion-header-icon").removeClass(c.icons.header).addClass(c.icons.activeHeader);
                }
                e.next().addClass("ui-accordion-content-active");
            }
        },
        _toggle: function(b) {
            var c = b.newPanel, d = this.prevShow.length ? this.prevShow : b.oldPanel;
            // handle activating a panel during the animation for another activation
            this.prevShow.add(this.prevHide).stop(true, true);
            this.prevShow = c;
            this.prevHide = d;
            if (this.options.animate) {
                this._animate(c, d, b);
            } else {
                d.hide();
                c.show();
                this._toggleComplete(b);
            }
            d.attr({
                "aria-hidden": "true"
            });
            d.prev().attr({
                "aria-selected": "false",
                "aria-expanded": "false"
            });
            // if we're switching panels, remove the old header from the tab order
            // if we're opening from collapsed state, remove the previous header from the tab order
            // if we're collapsing, then keep the collapsing header in the tab order
            if (c.length && d.length) {
                d.prev().attr({
                    tabIndex: -1,
                    "aria-expanded": "false"
                });
            } else if (c.length) {
                this.headers.filter(function() {
                    return parseInt(a(this).attr("tabIndex"), 10) === 0;
                }).attr("tabIndex", -1);
            }
            c.attr("aria-hidden", "false").prev().attr({
                "aria-selected": "true",
                "aria-expanded": "true",
                tabIndex: 0
            });
        },
        _animate: function(a, b, c) {
            var d, e, f, g = this, h = 0, i = a.css("box-sizing"), j = a.length && (!b.length || a.index() < b.index()), k = this.options.animate || {}, l = j && k.down || k, m = function() {
                g._toggleComplete(c);
            };
            if (typeof l === "number") {
                f = l;
            }
            if (typeof l === "string") {
                e = l;
            }
            // fall back from options to animation in case of partial down settings
            e = e || l.easing || k.easing;
            f = f || l.duration || k.duration;
            if (!b.length) {
                return a.animate(this.showProps, f, e, m);
            }
            if (!a.length) {
                return b.animate(this.hideProps, f, e, m);
            }
            d = a.show().outerHeight();
            b.animate(this.hideProps, {
                duration: f,
                easing: e,
                step: function(a, b) {
                    b.now = Math.round(a);
                }
            });
            a.hide().animate(this.showProps, {
                duration: f,
                easing: e,
                complete: m,
                step: function(a, c) {
                    c.now = Math.round(a);
                    if (c.prop !== "height") {
                        if (i === "content-box") {
                            h += c.now;
                        }
                    } else if (g.options.heightStyle !== "content") {
                        c.now = Math.round(d - b.outerHeight() - h);
                        h = 0;
                    }
                }
            });
        },
        _toggleComplete: function(a) {
            var b = a.oldPanel;
            b.removeClass("ui-accordion-content-active").prev().removeClass("ui-corner-top").addClass("ui-corner-all");
            // Work around for rendering bug in IE (#5421)
            if (b.length) {
                b.parent()[0].className = b.parent()[0].className;
            }
            this._trigger("activate", null, a);
        }
    });
    /*!
 * jQuery UI Menu 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/menu/
 */
    var k = a.widget("ui.menu", {
        version: "1.11.4",
        defaultElement: "<ul>",
        delay: 300,
        options: {
            icons: {
                submenu: "ui-icon-carat-1-e"
            },
            items: "> *",
            menus: "ul",
            position: {
                my: "left-1 top",
                at: "right top"
            },
            role: "menu",
            // callbacks
            blur: null,
            focus: null,
            select: null
        },
        _create: function() {
            this.activeMenu = this.element;
            // Flag used to prevent firing of the click handler
            // as the event bubbles up through nested menus
            this.mouseHandled = false;
            this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content").toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length).attr({
                role: this.options.role,
                tabIndex: 0
            });
            if (this.options.disabled) {
                this.element.addClass("ui-state-disabled").attr("aria-disabled", "true");
            }
            this._on({
                // Prevent focus from sticking to links inside menu after clicking
                // them (focus should always stay on UL during navigation).
                "mousedown .ui-menu-item": function(a) {
                    a.preventDefault();
                },
                "click .ui-menu-item": function(b) {
                    var c = a(b.target);
                    if (!this.mouseHandled && c.not(".ui-state-disabled").length) {
                        this.select(b);
                        // Only set the mouseHandled flag if the event will bubble, see #9469.
                        if (!b.isPropagationStopped()) {
                            this.mouseHandled = true;
                        }
                        // Open submenu on click
                        if (c.has(".ui-menu").length) {
                            this.expand(b);
                        } else if (!this.element.is(":focus") && a(this.document[0].activeElement).closest(".ui-menu").length) {
                            // Redirect focus to the menu
                            this.element.trigger("focus", [ true ]);
                            // If the active item is on the top level, let it stay active.
                            // Otherwise, blur the active item since it is no longer visible.
                            if (this.active && this.active.parents(".ui-menu").length === 1) {
                                clearTimeout(this.timer);
                            }
                        }
                    }
                },
                "mouseenter .ui-menu-item": function(b) {
                    // Ignore mouse events while typeahead is active, see #10458.
                    // Prevents focusing the wrong item when typeahead causes a scroll while the mouse
                    // is over an item in the menu
                    if (this.previousFilter) {
                        return;
                    }
                    var c = a(b.currentTarget);
                    // Remove ui-state-active class from siblings of the newly focused menu item
                    // to avoid a jump caused by adjacent elements both having a class with a border
                    c.siblings(".ui-state-active").removeClass("ui-state-active");
                    this.focus(b, c);
                },
                mouseleave: "collapseAll",
                "mouseleave .ui-menu": "collapseAll",
                focus: function(a, b) {
                    // If there's already an active item, keep it active
                    // If not, activate the first item
                    var c = this.active || this.element.find(this.options.items).eq(0);
                    if (!b) {
                        this.focus(a, c);
                    }
                },
                blur: function(b) {
                    this._delay(function() {
                        if (!a.contains(this.element[0], this.document[0].activeElement)) {
                            this.collapseAll(b);
                        }
                    });
                },
                keydown: "_keydown"
            });
            this.refresh();
            // Clicks outside of a menu collapse any open menus
            this._on(this.document, {
                click: function(a) {
                    if (this._closeOnDocumentClick(a)) {
                        this.collapseAll(a);
                    }
                    // Reset the mouseHandled flag
                    this.mouseHandled = false;
                }
            });
        },
        _destroy: function() {
            // Destroy (sub)menus
            this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeClass("ui-menu ui-widget ui-widget-content ui-menu-icons ui-front").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show();
            // Destroy menu items
            this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").removeUniqueId().removeClass("ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function() {
                var b = a(this);
                if (b.data("ui-menu-submenu-carat")) {
                    b.remove();
                }
            });
            // Destroy menu dividers
            this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content");
        },
        _keydown: function(b) {
            var c, d, e, f, g = true;
            switch (b.keyCode) {
              case a.ui.keyCode.PAGE_UP:
                this.previousPage(b);
                break;

              case a.ui.keyCode.PAGE_DOWN:
                this.nextPage(b);
                break;

              case a.ui.keyCode.HOME:
                this._move("first", "first", b);
                break;

              case a.ui.keyCode.END:
                this._move("last", "last", b);
                break;

              case a.ui.keyCode.UP:
                this.previous(b);
                break;

              case a.ui.keyCode.DOWN:
                this.next(b);
                break;

              case a.ui.keyCode.LEFT:
                this.collapse(b);
                break;

              case a.ui.keyCode.RIGHT:
                if (this.active && !this.active.is(".ui-state-disabled")) {
                    this.expand(b);
                }
                break;

              case a.ui.keyCode.ENTER:
              case a.ui.keyCode.SPACE:
                this._activate(b);
                break;

              case a.ui.keyCode.ESCAPE:
                this.collapse(b);
                break;

              default:
                g = false;
                d = this.previousFilter || "";
                e = String.fromCharCode(b.keyCode);
                f = false;
                clearTimeout(this.filterTimer);
                if (e === d) {
                    f = true;
                } else {
                    e = d + e;
                }
                c = this._filterMenuItems(e);
                c = f && c.index(this.active.next()) !== -1 ? this.active.nextAll(".ui-menu-item") : c;
                // If no matches on the current filter, reset to the last character pressed
                // to move down the menu to the first item that starts with that character
                if (!c.length) {
                    e = String.fromCharCode(b.keyCode);
                    c = this._filterMenuItems(e);
                }
                if (c.length) {
                    this.focus(b, c);
                    this.previousFilter = e;
                    this.filterTimer = this._delay(function() {
                        delete this.previousFilter;
                    }, 1e3);
                } else {
                    delete this.previousFilter;
                }
            }
            if (g) {
                b.preventDefault();
            }
        },
        _activate: function(a) {
            if (!this.active.is(".ui-state-disabled")) {
                if (this.active.is("[aria-haspopup='true']")) {
                    this.expand(a);
                } else {
                    this.select(a);
                }
            }
        },
        refresh: function() {
            var b, c, d = this, e = this.options.icons.submenu, f = this.element.find(this.options.menus);
            this.element.toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length);
            // Initialize nested menus
            f.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-front").hide().attr({
                role: this.options.role,
                "aria-hidden": "true",
                "aria-expanded": "false"
            }).each(function() {
                var b = a(this), c = b.parent(), d = a("<span>").addClass("ui-menu-icon ui-icon " + e).data("ui-menu-submenu-carat", true);
                c.attr("aria-haspopup", "true").prepend(d);
                b.attr("aria-labelledby", c.attr("id"));
            });
            b = f.add(this.element);
            c = b.find(this.options.items);
            // Initialize menu-items containing spaces and/or dashes only as dividers
            c.not(".ui-menu-item").each(function() {
                var b = a(this);
                if (d._isDivider(b)) {
                    b.addClass("ui-widget-content ui-menu-divider");
                }
            });
            // Don't refresh list items that are already adapted
            c.not(".ui-menu-item, .ui-menu-divider").addClass("ui-menu-item").uniqueId().attr({
                tabIndex: -1,
                role: this._itemRole()
            });
            // Add aria-disabled attribute to any disabled menu item
            c.filter(".ui-state-disabled").attr("aria-disabled", "true");
            // If the active item has been removed, blur the menu
            if (this.active && !a.contains(this.element[0], this.active[0])) {
                this.blur();
            }
        },
        _itemRole: function() {
            return {
                menu: "menuitem",
                listbox: "option"
            }[this.options.role];
        },
        _setOption: function(a, b) {
            if (a === "icons") {
                this.element.find(".ui-menu-icon").removeClass(this.options.icons.submenu).addClass(b.submenu);
            }
            if (a === "disabled") {
                this.element.toggleClass("ui-state-disabled", !!b).attr("aria-disabled", b);
            }
            this._super(a, b);
        },
        focus: function(a, b) {
            var c, d;
            this.blur(a, a && a.type === "focus");
            this._scrollIntoView(b);
            this.active = b.first();
            d = this.active.addClass("ui-state-focus").removeClass("ui-state-active");
            // Only update aria-activedescendant if there's a role
            // otherwise we assume focus is managed elsewhere
            if (this.options.role) {
                this.element.attr("aria-activedescendant", d.attr("id"));
            }
            // Highlight active parent menu item, if any
            this.active.parent().closest(".ui-menu-item").addClass("ui-state-active");
            if (a && a.type === "keydown") {
                this._close();
            } else {
                this.timer = this._delay(function() {
                    this._close();
                }, this.delay);
            }
            c = b.children(".ui-menu");
            if (c.length && a && /^mouse/.test(a.type)) {
                this._startOpening(c);
            }
            this.activeMenu = b.parent();
            this._trigger("focus", a, {
                item: b
            });
        },
        _scrollIntoView: function(b) {
            var c, d, e, f, g, h;
            if (this._hasScroll()) {
                c = parseFloat(a.css(this.activeMenu[0], "borderTopWidth")) || 0;
                d = parseFloat(a.css(this.activeMenu[0], "paddingTop")) || 0;
                e = b.offset().top - this.activeMenu.offset().top - c - d;
                f = this.activeMenu.scrollTop();
                g = this.activeMenu.height();
                h = b.outerHeight();
                if (e < 0) {
                    this.activeMenu.scrollTop(f + e);
                } else if (e + h > g) {
                    this.activeMenu.scrollTop(f + e - g + h);
                }
            }
        },
        blur: function(a, b) {
            if (!b) {
                clearTimeout(this.timer);
            }
            if (!this.active) {
                return;
            }
            this.active.removeClass("ui-state-focus");
            this.active = null;
            this._trigger("blur", a, {
                item: this.active
            });
        },
        _startOpening: function(a) {
            clearTimeout(this.timer);
            // Don't open if already open fixes a Firefox bug that caused a .5 pixel
            // shift in the submenu position when mousing over the carat icon
            if (a.attr("aria-hidden") !== "true") {
                return;
            }
            this.timer = this._delay(function() {
                this._close();
                this._open(a);
            }, this.delay);
        },
        _open: function(b) {
            var c = a.extend({
                of: this.active
            }, this.options.position);
            clearTimeout(this.timer);
            this.element.find(".ui-menu").not(b.parents(".ui-menu")).hide().attr("aria-hidden", "true");
            b.show().removeAttr("aria-hidden").attr("aria-expanded", "true").position(c);
        },
        collapseAll: function(b, c) {
            clearTimeout(this.timer);
            this.timer = this._delay(function() {
                // If we were passed an event, look for the submenu that contains the event
                var d = c ? this.element : a(b && b.target).closest(this.element.find(".ui-menu"));
                // If we found no valid submenu ancestor, use the main menu to close all sub menus anyway
                if (!d.length) {
                    d = this.element;
                }
                this._close(d);
                this.blur(b);
                this.activeMenu = d;
            }, this.delay);
        },
        // With no arguments, closes the currently active menu - if nothing is active
        // it closes all menus.  If passed an argument, it will search for menus BELOW
        _close: function(a) {
            if (!a) {
                a = this.active ? this.active.parent() : this.element;
            }
            a.find(".ui-menu").hide().attr("aria-hidden", "true").attr("aria-expanded", "false").end().find(".ui-state-active").not(".ui-state-focus").removeClass("ui-state-active");
        },
        _closeOnDocumentClick: function(b) {
            return !a(b.target).closest(".ui-menu").length;
        },
        _isDivider: function(a) {
            // Match hyphen, em dash, en dash
            return !/[^\-\u2014\u2013\s]/.test(a.text());
        },
        collapse: function(a) {
            var b = this.active && this.active.parent().closest(".ui-menu-item", this.element);
            if (b && b.length) {
                this._close();
                this.focus(a, b);
            }
        },
        expand: function(a) {
            var b = this.active && this.active.children(".ui-menu ").find(this.options.items).first();
            if (b && b.length) {
                this._open(b.parent());
                // Delay so Firefox will not hide activedescendant change in expanding submenu from AT
                this._delay(function() {
                    this.focus(a, b);
                });
            }
        },
        next: function(a) {
            this._move("next", "first", a);
        },
        previous: function(a) {
            this._move("prev", "last", a);
        },
        isFirstItem: function() {
            return this.active && !this.active.prevAll(".ui-menu-item").length;
        },
        isLastItem: function() {
            return this.active && !this.active.nextAll(".ui-menu-item").length;
        },
        _move: function(a, b, c) {
            var d;
            if (this.active) {
                if (a === "first" || a === "last") {
                    d = this.active[a === "first" ? "prevAll" : "nextAll"](".ui-menu-item").eq(-1);
                } else {
                    d = this.active[a + "All"](".ui-menu-item").eq(0);
                }
            }
            if (!d || !d.length || !this.active) {
                d = this.activeMenu.find(this.options.items)[b]();
            }
            this.focus(c, d);
        },
        nextPage: function(b) {
            var c, d, e;
            if (!this.active) {
                this.next(b);
                return;
            }
            if (this.isLastItem()) {
                return;
            }
            if (this._hasScroll()) {
                d = this.active.offset().top;
                e = this.element.height();
                this.active.nextAll(".ui-menu-item").each(function() {
                    c = a(this);
                    return c.offset().top - d - e < 0;
                });
                this.focus(b, c);
            } else {
                this.focus(b, this.activeMenu.find(this.options.items)[!this.active ? "first" : "last"]());
            }
        },
        previousPage: function(b) {
            var c, d, e;
            if (!this.active) {
                this.next(b);
                return;
            }
            if (this.isFirstItem()) {
                return;
            }
            if (this._hasScroll()) {
                d = this.active.offset().top;
                e = this.element.height();
                this.active.prevAll(".ui-menu-item").each(function() {
                    c = a(this);
                    return c.offset().top - d + e > 0;
                });
                this.focus(b, c);
            } else {
                this.focus(b, this.activeMenu.find(this.options.items).first());
            }
        },
        _hasScroll: function() {
            return this.element.outerHeight() < this.element.prop("scrollHeight");
        },
        select: function(b) {
            // TODO: It should never be possible to not have an active item at this
            // point, but the tests don't trigger mouseenter before click.
            this.active = this.active || a(b.target).closest(".ui-menu-item");
            var c = {
                item: this.active
            };
            if (!this.active.has(".ui-menu").length) {
                this.collapseAll(b, true);
            }
            this._trigger("select", b, c);
        },
        _filterMenuItems: function(b) {
            var c = b.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&"), d = new RegExp("^" + c, "i");
            return this.activeMenu.find(this.options.items).filter(".ui-menu-item").filter(function() {
                return d.test(a.trim(a(this).text()));
            });
        }
    });
    /*!
 * jQuery UI Autocomplete 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/autocomplete/
 */
    a.widget("ui.autocomplete", {
        version: "1.11.4",
        defaultElement: "<input>",
        options: {
            appendTo: null,
            autoFocus: false,
            delay: 300,
            minLength: 1,
            position: {
                my: "left top",
                at: "left bottom",
                collision: "none"
            },
            source: null,
            // callbacks
            change: null,
            close: null,
            focus: null,
            open: null,
            response: null,
            search: null,
            select: null
        },
        requestIndex: 0,
        pending: 0,
        _create: function() {
            // Some browsers only repeat keydown events, not keypress events,
            // so we use the suppressKeyPress flag to determine if we've already
            // handled the keydown event. #7269
            // Unfortunately the code for & in keypress is the same as the up arrow,
            // so we use the suppressKeyPressRepeat flag to avoid handling keypress
            // events when we know the keydown event was used to modify the
            // search term. #7799
            var b, c, d, e = this.element[0].nodeName.toLowerCase(), f = e === "textarea", g = e === "input";
            this.isMultiLine = // Textareas are always multi-line
            f ? true : // Inputs are always single-line, even if inside a contentEditable element
            // IE also treats inputs as contentEditable
            g ? false : // All other element types are determined by whether or not they're contentEditable
            this.element.prop("isContentEditable");
            this.valueMethod = this.element[f || g ? "val" : "text"];
            this.isNewMenu = true;
            this.element.addClass("ui-autocomplete-input").attr("autocomplete", "off");
            this._on(this.element, {
                keydown: function(e) {
                    if (this.element.prop("readOnly")) {
                        b = true;
                        d = true;
                        c = true;
                        return;
                    }
                    b = false;
                    d = false;
                    c = false;
                    var f = a.ui.keyCode;
                    switch (e.keyCode) {
                      case f.PAGE_UP:
                        b = true;
                        this._move("previousPage", e);
                        break;

                      case f.PAGE_DOWN:
                        b = true;
                        this._move("nextPage", e);
                        break;

                      case f.UP:
                        b = true;
                        this._keyEvent("previous", e);
                        break;

                      case f.DOWN:
                        b = true;
                        this._keyEvent("next", e);
                        break;

                      case f.ENTER:
                        // when menu is open and has focus
                        if (this.menu.active) {
                            // #6055 - Opera still allows the keypress to occur
                            // which causes forms to submit
                            b = true;
                            e.preventDefault();
                            this.menu.select(e);
                        }
                        break;

                      case f.TAB:
                        if (this.menu.active) {
                            this.menu.select(e);
                        }
                        break;

                      case f.ESCAPE:
                        if (this.menu.element.is(":visible")) {
                            if (!this.isMultiLine) {
                                this._value(this.term);
                            }
                            this.close(e);
                            // Different browsers have different default behavior for escape
                            // Single press can mean undo or clear
                            // Double press in IE means clear the whole form
                            e.preventDefault();
                        }
                        break;

                      default:
                        c = true;
                        // search timeout should be triggered before the input value is changed
                        this._searchTimeout(e);
                        break;
                    }
                },
                keypress: function(d) {
                    if (b) {
                        b = false;
                        if (!this.isMultiLine || this.menu.element.is(":visible")) {
                            d.preventDefault();
                        }
                        return;
                    }
                    if (c) {
                        return;
                    }
                    // replicate some key handlers to allow them to repeat in Firefox and Opera
                    var e = a.ui.keyCode;
                    switch (d.keyCode) {
                      case e.PAGE_UP:
                        this._move("previousPage", d);
                        break;

                      case e.PAGE_DOWN:
                        this._move("nextPage", d);
                        break;

                      case e.UP:
                        this._keyEvent("previous", d);
                        break;

                      case e.DOWN:
                        this._keyEvent("next", d);
                        break;
                    }
                },
                input: function(a) {
                    if (d) {
                        d = false;
                        a.preventDefault();
                        return;
                    }
                    this._searchTimeout(a);
                },
                focus: function() {
                    this.selectedItem = null;
                    this.previous = this._value();
                },
                blur: function(a) {
                    if (this.cancelBlur) {
                        delete this.cancelBlur;
                        return;
                    }
                    clearTimeout(this.searching);
                    this.close(a);
                    this._change(a);
                }
            });
            this._initSource();
            this.menu = a("<ul>").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({
                // disable ARIA support, the live region takes care of that
                role: null
            }).hide().menu("instance");
            this._on(this.menu.element, {
                mousedown: function(b) {
                    // prevent moving focus out of the text field
                    b.preventDefault();
                    // IE doesn't prevent moving focus even with event.preventDefault()
                    // so we set a flag to know when we should ignore the blur event
                    this.cancelBlur = true;
                    this._delay(function() {
                        delete this.cancelBlur;
                    });
                    // clicking on the scrollbar causes focus to shift to the body
                    // but we can't detect a mouseup or a click immediately afterward
                    // so we have to track the next mousedown and close the menu if
                    // the user clicks somewhere outside of the autocomplete
                    var c = this.menu.element[0];
                    if (!a(b.target).closest(".ui-menu-item").length) {
                        this._delay(function() {
                            var b = this;
                            this.document.one("mousedown", function(d) {
                                if (d.target !== b.element[0] && d.target !== c && !a.contains(c, d.target)) {
                                    b.close();
                                }
                            });
                        });
                    }
                },
                menufocus: function(b, c) {
                    var d, e;
                    // support: Firefox
                    // Prevent accidental activation of menu items in Firefox (#7024 #9118)
                    if (this.isNewMenu) {
                        this.isNewMenu = false;
                        if (b.originalEvent && /^mouse/.test(b.originalEvent.type)) {
                            this.menu.blur();
                            this.document.one("mousemove", function() {
                                a(b.target).trigger(b.originalEvent);
                            });
                            return;
                        }
                    }
                    e = c.item.data("ui-autocomplete-item");
                    if (false !== this._trigger("focus", b, {
                        item: e
                    })) {
                        // use value to match what will end up in the input, if it was a key event
                        if (b.originalEvent && /^key/.test(b.originalEvent.type)) {
                            this._value(e.value);
                        }
                    }
                    // Announce the value in the liveRegion
                    d = c.item.attr("aria-label") || e.value;
                    if (d && a.trim(d).length) {
                        this.liveRegion.children().hide();
                        a("<div>").text(d).appendTo(this.liveRegion);
                    }
                },
                menuselect: function(a, b) {
                    var c = b.item.data("ui-autocomplete-item"), d = this.previous;
                    // only trigger when focus was lost (click on menu)
                    if (this.element[0] !== this.document[0].activeElement) {
                        this.element.focus();
                        this.previous = d;
                        // #6109 - IE triggers two focus events and the second
                        // is asynchronous, so we need to reset the previous
                        // term synchronously and asynchronously :-(
                        this._delay(function() {
                            this.previous = d;
                            this.selectedItem = c;
                        });
                    }
                    if (false !== this._trigger("select", a, {
                        item: c
                    })) {
                        this._value(c.value);
                    }
                    // reset the term after the select event
                    // this allows custom select handling to work properly
                    this.term = this._value();
                    this.close(a);
                    this.selectedItem = c;
                }
            });
            this.liveRegion = a("<span>", {
                role: "status",
                "aria-live": "assertive",
                "aria-relevant": "additions"
            }).addClass("ui-helper-hidden-accessible").appendTo(this.document[0].body);
            // turning off autocomplete prevents the browser from remembering the
            // value when navigating through history, so we re-enable autocomplete
            // if the page is unloaded before the widget is destroyed. #7790
            this._on(this.window, {
                beforeunload: function() {
                    this.element.removeAttr("autocomplete");
                }
            });
        },
        _destroy: function() {
            clearTimeout(this.searching);
            this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete");
            this.menu.element.remove();
            this.liveRegion.remove();
        },
        _setOption: function(a, b) {
            this._super(a, b);
            if (a === "source") {
                this._initSource();
            }
            if (a === "appendTo") {
                this.menu.element.appendTo(this._appendTo());
            }
            if (a === "disabled" && b && this.xhr) {
                this.xhr.abort();
            }
        },
        _appendTo: function() {
            var b = this.options.appendTo;
            if (b) {
                b = b.jquery || b.nodeType ? a(b) : this.document.find(b).eq(0);
            }
            if (!b || !b[0]) {
                b = this.element.closest(".ui-front");
            }
            if (!b.length) {
                b = this.document[0].body;
            }
            return b;
        },
        _initSource: function() {
            var b, c, d = this;
            if (a.isArray(this.options.source)) {
                b = this.options.source;
                this.source = function(c, d) {
                    d(a.ui.autocomplete.filter(b, c.term));
                };
            } else if (typeof this.options.source === "string") {
                c = this.options.source;
                this.source = function(b, e) {
                    if (d.xhr) {
                        d.xhr.abort();
                    }
                    d.xhr = a.ajax({
                        url: c,
                        data: b,
                        dataType: "json",
                        success: function(a) {
                            e(a);
                        },
                        error: function() {
                            e([]);
                        }
                    });
                };
            } else {
                this.source = this.options.source;
            }
        },
        _searchTimeout: function(a) {
            clearTimeout(this.searching);
            this.searching = this._delay(function() {
                // Search if the value has changed, or if the user retypes the same value (see #7434)
                var b = this.term === this._value(), c = this.menu.element.is(":visible"), d = a.altKey || a.ctrlKey || a.metaKey || a.shiftKey;
                if (!b || b && !c && !d) {
                    this.selectedItem = null;
                    this.search(null, a);
                }
            }, this.options.delay);
        },
        search: function(a, b) {
            a = a != null ? a : this._value();
            // always save the actual value, not the one passed as an argument
            this.term = this._value();
            if (a.length < this.options.minLength) {
                return this.close(b);
            }
            if (this._trigger("search", b) === false) {
                return;
            }
            return this._search(a);
        },
        _search: function(a) {
            this.pending++;
            this.element.addClass("ui-autocomplete-loading");
            this.cancelSearch = false;
            this.source({
                term: a
            }, this._response());
        },
        _response: function() {
            var b = ++this.requestIndex;
            return a.proxy(function(a) {
                if (b === this.requestIndex) {
                    this.__response(a);
                }
                this.pending--;
                if (!this.pending) {
                    this.element.removeClass("ui-autocomplete-loading");
                }
            }, this);
        },
        __response: function(a) {
            if (a) {
                a = this._normalize(a);
            }
            this._trigger("response", null, {
                content: a
            });
            if (!this.options.disabled && a && a.length && !this.cancelSearch) {
                this._suggest(a);
                this._trigger("open");
            } else {
                // use ._close() instead of .close() so we don't cancel future searches
                this._close();
            }
        },
        close: function(a) {
            this.cancelSearch = true;
            this._close(a);
        },
        _close: function(a) {
            if (this.menu.element.is(":visible")) {
                this.menu.element.hide();
                this.menu.blur();
                this.isNewMenu = true;
                this._trigger("close", a);
            }
        },
        _change: function(a) {
            if (this.previous !== this._value()) {
                this._trigger("change", a, {
                    item: this.selectedItem
                });
            }
        },
        _normalize: function(b) {
            // assume all items have the right format when the first item is complete
            if (b.length && b[0].label && b[0].value) {
                return b;
            }
            return a.map(b, function(b) {
                if (typeof b === "string") {
                    return {
                        label: b,
                        value: b
                    };
                }
                return a.extend({}, b, {
                    label: b.label || b.value,
                    value: b.value || b.label
                });
            });
        },
        _suggest: function(b) {
            var c = this.menu.element.empty();
            this._renderMenu(c, b);
            this.isNewMenu = true;
            this.menu.refresh();
            // size and position menu
            c.show();
            this._resizeMenu();
            c.position(a.extend({
                of: this.element
            }, this.options.position));
            if (this.options.autoFocus) {
                this.menu.next();
            }
        },
        _resizeMenu: function() {
            var a = this.menu.element;
            a.outerWidth(Math.max(// Firefox wraps long text (possibly a rounding bug)
            // so we add 1px to avoid the wrapping (#7513)
            a.width("").outerWidth() + 1, this.element.outerWidth()));
        },
        _renderMenu: function(b, c) {
            var d = this;
            a.each(c, function(a, c) {
                d._renderItemData(b, c);
            });
        },
        _renderItemData: function(a, b) {
            return this._renderItem(a, b).data("ui-autocomplete-item", b);
        },
        _renderItem: function(b, c) {
            return a("<li>").text(c.label).appendTo(b);
        },
        _move: function(a, b) {
            if (!this.menu.element.is(":visible")) {
                this.search(null, b);
                return;
            }
            if (this.menu.isFirstItem() && /^previous/.test(a) || this.menu.isLastItem() && /^next/.test(a)) {
                if (!this.isMultiLine) {
                    this._value(this.term);
                }
                this.menu.blur();
                return;
            }
            this.menu[a](b);
        },
        widget: function() {
            return this.menu.element;
        },
        _value: function() {
            return this.valueMethod.apply(this.element, arguments);
        },
        _keyEvent: function(a, b) {
            if (!this.isMultiLine || this.menu.element.is(":visible")) {
                this._move(a, b);
                // prevents moving cursor to beginning/end of the text field in some browsers
                b.preventDefault();
            }
        }
    });
    a.extend(a.ui.autocomplete, {
        escapeRegex: function(a) {
            return a.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
        },
        filter: function(b, c) {
            var d = new RegExp(a.ui.autocomplete.escapeRegex(c), "i");
            return a.grep(b, function(a) {
                return d.test(a.label || a.value || a);
            });
        }
    });
    // live region extension, adding a `messages` option
    // NOTE: This is an experimental API. We are still investigating
    // a full solution for string manipulation and internationalization.
    a.widget("ui.autocomplete", a.ui.autocomplete, {
        options: {
            messages: {
                noResults: "No search results.",
                results: function(a) {
                    return a + (a > 1 ? " results are" : " result is") + " available, use up and down arrow keys to navigate.";
                }
            }
        },
        __response: function(b) {
            var c;
            this._superApply(arguments);
            if (this.options.disabled || this.cancelSearch) {
                return;
            }
            if (b && b.length) {
                c = this.options.messages.results(b.length);
            } else {
                c = this.options.messages.noResults;
            }
            this.liveRegion.children().hide();
            a("<div>").text(c).appendTo(this.liveRegion);
        }
    });
    var l = a.ui.autocomplete;
    /*!
 * jQuery UI Button 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/button/
 */
    var m, n = "ui-button ui-widget ui-state-default ui-corner-all", o = "ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only", p = function() {
        var b = a(this);
        setTimeout(function() {
            b.find(":ui-button").button("refresh");
        }, 1);
    }, q = function(b) {
        var c = b.name, d = b.form, e = a([]);
        if (c) {
            c = c.replace(/'/g, "\\'");
            if (d) {
                e = a(d).find("[name='" + c + "'][type=radio]");
            } else {
                e = a("[name='" + c + "'][type=radio]", b.ownerDocument).filter(function() {
                    return !this.form;
                });
            }
        }
        return e;
    };
    a.widget("ui.button", {
        version: "1.11.4",
        defaultElement: "<button>",
        options: {
            disabled: null,
            text: true,
            label: null,
            icons: {
                primary: null,
                secondary: null
            }
        },
        _create: function() {
            this.element.closest("form").unbind("reset" + this.eventNamespace).bind("reset" + this.eventNamespace, p);
            if (typeof this.options.disabled !== "boolean") {
                this.options.disabled = !!this.element.prop("disabled");
            } else {
                this.element.prop("disabled", this.options.disabled);
            }
            this._determineButtonType();
            this.hasTitle = !!this.buttonElement.attr("title");
            var b = this, c = this.options, d = this.type === "checkbox" || this.type === "radio", e = !d ? "ui-state-active" : "";
            if (c.label === null) {
                c.label = this.type === "input" ? this.buttonElement.val() : this.buttonElement.html();
            }
            this._hoverable(this.buttonElement);
            this.buttonElement.addClass(n).attr("role", "button").bind("mouseenter" + this.eventNamespace, function() {
                if (c.disabled) {
                    return;
                }
                if (this === m) {
                    a(this).addClass("ui-state-active");
                }
            }).bind("mouseleave" + this.eventNamespace, function() {
                if (c.disabled) {
                    return;
                }
                a(this).removeClass(e);
            }).bind("click" + this.eventNamespace, function(a) {
                if (c.disabled) {
                    a.preventDefault();
                    a.stopImmediatePropagation();
                }
            });
            // Can't use _focusable() because the element that receives focus
            // and the element that gets the ui-state-focus class are different
            this._on({
                focus: function() {
                    this.buttonElement.addClass("ui-state-focus");
                },
                blur: function() {
                    this.buttonElement.removeClass("ui-state-focus");
                }
            });
            if (d) {
                this.element.bind("change" + this.eventNamespace, function() {
                    b.refresh();
                });
            }
            if (this.type === "checkbox") {
                this.buttonElement.bind("click" + this.eventNamespace, function() {
                    if (c.disabled) {
                        return false;
                    }
                });
            } else if (this.type === "radio") {
                this.buttonElement.bind("click" + this.eventNamespace, function() {
                    if (c.disabled) {
                        return false;
                    }
                    a(this).addClass("ui-state-active");
                    b.buttonElement.attr("aria-pressed", "true");
                    var d = b.element[0];
                    q(d).not(d).map(function() {
                        return a(this).button("widget")[0];
                    }).removeClass("ui-state-active").attr("aria-pressed", "false");
                });
            } else {
                this.buttonElement.bind("mousedown" + this.eventNamespace, function() {
                    if (c.disabled) {
                        return false;
                    }
                    a(this).addClass("ui-state-active");
                    m = this;
                    b.document.one("mouseup", function() {
                        m = null;
                    });
                }).bind("mouseup" + this.eventNamespace, function() {
                    if (c.disabled) {
                        return false;
                    }
                    a(this).removeClass("ui-state-active");
                }).bind("keydown" + this.eventNamespace, function(b) {
                    if (c.disabled) {
                        return false;
                    }
                    if (b.keyCode === a.ui.keyCode.SPACE || b.keyCode === a.ui.keyCode.ENTER) {
                        a(this).addClass("ui-state-active");
                    }
                }).bind("keyup" + this.eventNamespace + " blur" + this.eventNamespace, function() {
                    a(this).removeClass("ui-state-active");
                });
                if (this.buttonElement.is("a")) {
                    this.buttonElement.keyup(function(b) {
                        if (b.keyCode === a.ui.keyCode.SPACE) {
                            // TODO pass through original event correctly (just as 2nd argument doesn't work)
                            a(this).click();
                        }
                    });
                }
            }
            this._setOption("disabled", c.disabled);
            this._resetButton();
        },
        _determineButtonType: function() {
            var a, b, c;
            if (this.element.is("[type=checkbox]")) {
                this.type = "checkbox";
            } else if (this.element.is("[type=radio]")) {
                this.type = "radio";
            } else if (this.element.is("input")) {
                this.type = "input";
            } else {
                this.type = "button";
            }
            if (this.type === "checkbox" || this.type === "radio") {
                // we don't search against the document in case the element
                // is disconnected from the DOM
                a = this.element.parents().last();
                b = "label[for='" + this.element.attr("id") + "']";
                this.buttonElement = a.find(b);
                if (!this.buttonElement.length) {
                    a = a.length ? a.siblings() : this.element.siblings();
                    this.buttonElement = a.filter(b);
                    if (!this.buttonElement.length) {
                        this.buttonElement = a.find(b);
                    }
                }
                this.element.addClass("ui-helper-hidden-accessible");
                c = this.element.is(":checked");
                if (c) {
                    this.buttonElement.addClass("ui-state-active");
                }
                this.buttonElement.prop("aria-pressed", c);
            } else {
                this.buttonElement = this.element;
            }
        },
        widget: function() {
            return this.buttonElement;
        },
        _destroy: function() {
            this.element.removeClass("ui-helper-hidden-accessible");
            this.buttonElement.removeClass(n + " ui-state-active " + o).removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html());
            if (!this.hasTitle) {
                this.buttonElement.removeAttr("title");
            }
        },
        _setOption: function(a, b) {
            this._super(a, b);
            if (a === "disabled") {
                this.widget().toggleClass("ui-state-disabled", !!b);
                this.element.prop("disabled", !!b);
                if (b) {
                    if (this.type === "checkbox" || this.type === "radio") {
                        this.buttonElement.removeClass("ui-state-focus");
                    } else {
                        this.buttonElement.removeClass("ui-state-focus ui-state-active");
                    }
                }
                return;
            }
            this._resetButton();
        },
        refresh: function() {
            //See #8237 & #8828
            var b = this.element.is("input, button") ? this.element.is(":disabled") : this.element.hasClass("ui-button-disabled");
            if (b !== this.options.disabled) {
                this._setOption("disabled", b);
            }
            if (this.type === "radio") {
                q(this.element[0]).each(function() {
                    if (a(this).is(":checked")) {
                        a(this).button("widget").addClass("ui-state-active").attr("aria-pressed", "true");
                    } else {
                        a(this).button("widget").removeClass("ui-state-active").attr("aria-pressed", "false");
                    }
                });
            } else if (this.type === "checkbox") {
                if (this.element.is(":checked")) {
                    this.buttonElement.addClass("ui-state-active").attr("aria-pressed", "true");
                } else {
                    this.buttonElement.removeClass("ui-state-active").attr("aria-pressed", "false");
                }
            }
        },
        _resetButton: function() {
            if (this.type === "input") {
                if (this.options.label) {
                    this.element.val(this.options.label);
                }
                return;
            }
            var b = this.buttonElement.removeClass(o), c = a("<span></span>", this.document[0]).addClass("ui-button-text").html(this.options.label).appendTo(b.empty()).text(), d = this.options.icons, e = d.primary && d.secondary, f = [];
            if (d.primary || d.secondary) {
                if (this.options.text) {
                    f.push("ui-button-text-icon" + (e ? "s" : d.primary ? "-primary" : "-secondary"));
                }
                if (d.primary) {
                    b.prepend("<span class='ui-button-icon-primary ui-icon " + d.primary + "'></span>");
                }
                if (d.secondary) {
                    b.append("<span class='ui-button-icon-secondary ui-icon " + d.secondary + "'></span>");
                }
                if (!this.options.text) {
                    f.push(e ? "ui-button-icons-only" : "ui-button-icon-only");
                    if (!this.hasTitle) {
                        b.attr("title", a.trim(c));
                    }
                }
            } else {
                f.push("ui-button-text-only");
            }
            b.addClass(f.join(" "));
        }
    });
    a.widget("ui.buttonset", {
        version: "1.11.4",
        options: {
            items: "button, input[type=button], input[type=submit], input[type=reset], input[type=checkbox], input[type=radio], a, :data(ui-button)"
        },
        _create: function() {
            this.element.addClass("ui-buttonset");
        },
        _init: function() {
            this.refresh();
        },
        _setOption: function(a, b) {
            if (a === "disabled") {
                this.buttons.button("option", a, b);
            }
            this._super(a, b);
        },
        refresh: function() {
            var b = this.element.css("direction") === "rtl", c = this.element.find(this.options.items), d = c.filter(":ui-button");
            // Initialize new buttons
            c.not(":ui-button").button();
            // Refresh existing buttons
            d.button("refresh");
            this.buttons = c.map(function() {
                return a(this).button("widget")[0];
            }).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(b ? "ui-corner-right" : "ui-corner-left").end().filter(":last").addClass(b ? "ui-corner-left" : "ui-corner-right").end().end();
        },
        _destroy: function() {
            this.element.removeClass("ui-buttonset");
            this.buttons.map(function() {
                return a(this).button("widget")[0];
            }).removeClass("ui-corner-left ui-corner-right").end().button("destroy");
        }
    });
    var r = a.ui.button;
    /*!
 * jQuery UI Datepicker 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/datepicker/
 */
    a.extend(a.ui, {
        datepicker: {
            version: "1.11.4"
        }
    });
    var s;
    function t(a) {
        var b, c;
        while (a.length && a[0] !== document) {
            // Ignore z-index if position is set to a value where z-index is ignored by the browser
            // This makes behavior of this function consistent across browsers
            // WebKit always returns auto if the element is positioned
            b = a.css("position");
            if (b === "absolute" || b === "relative" || b === "fixed") {
                // IE returns 0 when zIndex is not specified
                // other browsers return a string
                // we ignore the case of nested elements with an explicit value of 0
                // <div style="z-index: -10;"><div style="z-index: 0;"></div></div>
                c = parseInt(a.css("zIndex"), 10);
                if (!isNaN(c) && c !== 0) {
                    return c;
                }
            }
            a = a.parent();
        }
        return 0;
    }
    /* Date picker manager.
   Use the singleton instance of this class, $.datepicker, to interact with the date picker.
   Settings for (groups of) date pickers are maintained in an instance object,
   allowing multiple different settings on the same page. */
    function u() {
        this._curInst = null;
        // The current instance in use
        this._keyEvent = false;
        // If the last event was a key event
        this._disabledInputs = [];
        // List of date picker inputs that have been disabled
        this._datepickerShowing = false;
        // True if the popup picker is showing , false if not
        this._inDialog = false;
        // True if showing within a "dialog", false if not
        this._mainDivId = "ui-datepicker-div";
        // The ID of the main datepicker division
        this._inlineClass = "ui-datepicker-inline";
        // The name of the inline marker class
        this._appendClass = "ui-datepicker-append";
        // The name of the append marker class
        this._triggerClass = "ui-datepicker-trigger";
        // The name of the trigger marker class
        this._dialogClass = "ui-datepicker-dialog";
        // The name of the dialog marker class
        this._disableClass = "ui-datepicker-disabled";
        // The name of the disabled covering marker class
        this._unselectableClass = "ui-datepicker-unselectable";
        // The name of the unselectable cell marker class
        this._currentClass = "ui-datepicker-current-day";
        // The name of the current day marker class
        this._dayOverClass = "ui-datepicker-days-cell-over";
        // The name of the day hover marker class
        this.regional = [];
        // Available regional settings, indexed by language code
        this.regional[""] = {
            // Default regional settings
            closeText: "Done",
            // Display text for close link
            prevText: "Prev",
            // Display text for previous month link
            nextText: "Next",
            // Display text for next month link
            currentText: "Today",
            // Display text for current month link
            monthNames: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
            // Names of months for drop-down and formatting
            monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
            // For formatting
            dayNames: [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
            // For formatting
            dayNamesShort: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
            // For formatting
            dayNamesMin: [ "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" ],
            // Column headings for days starting at Sunday
            weekHeader: "Wk",
            // Column header for week of the year
            dateFormat: "mm/dd/yy",
            // See format options on parseDate
            firstDay: 0,
            // The first day of the week, Sun = 0, Mon = 1, ...
            isRTL: false,
            // True if right-to-left language, false if left-to-right
            showMonthAfterYear: false,
            // True if the year select precedes month, false for month then year
            yearSuffix: ""
        };
        this._defaults = {
            // Global defaults for all the date picker instances
            showOn: "focus",
            // "focus" for popup on focus,
            // "button" for trigger button, or "both" for either
            showAnim: "fadeIn",
            // Name of jQuery animation for popup
            showOptions: {},
            // Options for enhanced animations
            defaultDate: null,
            // Used when field is blank: actual date,
            // +/-number for offset from today, null for today
            appendText: "",
            // Display text following the input box, e.g. showing the format
            buttonText: "...",
            // Text for trigger button
            buttonImage: "",
            // URL for trigger button image
            buttonImageOnly: false,
            // True if the image appears alone, false if it appears on a button
            hideIfNoPrevNext: false,
            // True to hide next/previous month links
            // if not applicable, false to just disable them
            navigationAsDateFormat: false,
            // True if date formatting applied to prev/today/next links
            gotoCurrent: false,
            // True if today link goes back to current selection instead
            changeMonth: false,
            // True if month can be selected directly, false if only prev/next
            changeYear: false,
            // True if year can be selected directly, false if only prev/next
            yearRange: "c-10:c+10",
            // Range of years to display in drop-down,
            // either relative to today's year (-nn:+nn), relative to currently displayed year
            // (c-nn:c+nn), absolute (nnnn:nnnn), or a combination of the above (nnnn:-n)
            showOtherMonths: false,
            // True to show dates in other months, false to leave blank
            selectOtherMonths: false,
            // True to allow selection of dates in other months, false for unselectable
            showWeek: false,
            // True to show week of the year, false to not show it
            calculateWeek: this.iso8601Week,
            // How to calculate the week of the year,
            // takes a Date and returns the number of the week for it
            shortYearCutoff: "+10",
            // Short year values < this are in the current century,
            // > this are in the previous century,
            // string value starting with "+" for current year + value
            minDate: null,
            // The earliest selectable date, or null for no limit
            maxDate: null,
            // The latest selectable date, or null for no limit
            duration: "fast",
            // Duration of display/closure
            beforeShowDay: null,
            // Function that takes a date and returns an array with
            // [0] = true if selectable, false if not, [1] = custom CSS class name(s) or "",
            // [2] = cell title (optional), e.g. $.datepicker.noWeekends
            beforeShow: null,
            // Function that takes an input field and
            // returns a set of custom settings for the date picker
            onSelect: null,
            // Define a callback function when a date is selected
            onChangeMonthYear: null,
            // Define a callback function when the month or year is changed
            onClose: null,
            // Define a callback function when the datepicker is closed
            numberOfMonths: 1,
            // Number of months to show at a time
            showCurrentAtPos: 0,
            // The position in multipe months at which to show the current month (starting at 0)
            stepMonths: 1,
            // Number of months to step back/forward
            stepBigMonths: 12,
            // Number of months to step back/forward for the big links
            altField: "",
            // Selector for an alternate field to store selected dates into
            altFormat: "",
            // The date format to use for the alternate field
            constrainInput: true,
            // The input is constrained by the current date format
            showButtonPanel: false,
            // True to show button panel, false to not show it
            autoSize: false,
            // True to size the input for the date format, false to leave as is
            disabled: false
        };
        a.extend(this._defaults, this.regional[""]);
        this.regional.en = a.extend(true, {}, this.regional[""]);
        this.regional["en-US"] = a.extend(true, {}, this.regional.en);
        this.dpDiv = v(a("<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"));
    }
    a.extend(u.prototype, {
        /* Class name added to elements to indicate already configured with a date picker. */
        markerClassName: "hasDatepicker",
        //Keep track of the maximum number of rows displayed (see #7043)
        maxRows: 4,
        // TODO rename to "widget" when switching to widget factory
        _widgetDatepicker: function() {
            return this.dpDiv;
        },
        /* Override the default settings for all instances of the date picker.
	 * @param  settings  object - the new settings to use as defaults (anonymous object)
	 * @return the manager object
	 */
        setDefaults: function(a) {
            x(this._defaults, a || {});
            return this;
        },
        /* Attach the date picker to a jQuery selection.
	 * @param  target	element - the target input field or division or span
	 * @param  settings  object - the new settings to use for this date picker instance (anonymous)
	 */
        _attachDatepicker: function(b, c) {
            var d, e, f;
            d = b.nodeName.toLowerCase();
            e = d === "div" || d === "span";
            if (!b.id) {
                this.uuid += 1;
                b.id = "dp" + this.uuid;
            }
            f = this._newInst(a(b), e);
            f.settings = a.extend({}, c || {});
            if (d === "input") {
                this._connectDatepicker(b, f);
            } else if (e) {
                this._inlineDatepicker(b, f);
            }
        },
        /* Create a new instance object. */
        _newInst: function(b, c) {
            var d = b[0].id.replace(/([^A-Za-z0-9_\-])/g, "\\\\$1");
            // escape jQuery meta chars
            return {
                id: d,
                input: b,
                // associated target
                selectedDay: 0,
                selectedMonth: 0,
                selectedYear: 0,
                // current selection
                drawMonth: 0,
                drawYear: 0,
                // month being drawn
                inline: c,
                // is datepicker inline or not
                dpDiv: !c ? this.dpDiv : // presentation div
                v(a("<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))
            };
        },
        /* Attach the date picker to an input field. */
        _connectDatepicker: function(b, c) {
            var d = a(b);
            c.append = a([]);
            c.trigger = a([]);
            if (d.hasClass(this.markerClassName)) {
                return;
            }
            this._attachments(d, c);
            d.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp);
            this._autoSize(c);
            a.data(b, "datepicker", c);
            //If disabled option is true, disable the datepicker once it has been attached to the input (see ticket #5665)
            if (c.settings.disabled) {
                this._disableDatepicker(b);
            }
        },
        /* Make attachments based on settings. */
        _attachments: function(b, c) {
            var d, e, f, g = this._get(c, "appendText"), h = this._get(c, "isRTL");
            if (c.append) {
                c.append.remove();
            }
            if (g) {
                c.append = a("<span class='" + this._appendClass + "'>" + g + "</span>");
                b[h ? "before" : "after"](c.append);
            }
            b.unbind("focus", this._showDatepicker);
            if (c.trigger) {
                c.trigger.remove();
            }
            d = this._get(c, "showOn");
            if (d === "focus" || d === "both") {
                // pop-up date picker when in the marked field
                b.focus(this._showDatepicker);
            }
            if (d === "button" || d === "both") {
                // pop-up date picker when button clicked
                e = this._get(c, "buttonText");
                f = this._get(c, "buttonImage");
                c.trigger = a(this._get(c, "buttonImageOnly") ? a("<img/>").addClass(this._triggerClass).attr({
                    src: f,
                    alt: e,
                    title: e
                }) : a("<button type='button'></button>").addClass(this._triggerClass).html(!f ? e : a("<img/>").attr({
                    src: f,
                    alt: e,
                    title: e
                })));
                b[h ? "before" : "after"](c.trigger);
                c.trigger.click(function() {
                    if (a.datepicker._datepickerShowing && a.datepicker._lastInput === b[0]) {
                        a.datepicker._hideDatepicker();
                    } else if (a.datepicker._datepickerShowing && a.datepicker._lastInput !== b[0]) {
                        a.datepicker._hideDatepicker();
                        a.datepicker._showDatepicker(b[0]);
                    } else {
                        a.datepicker._showDatepicker(b[0]);
                    }
                    return false;
                });
            }
        },
        /* Apply the maximum length for the date format. */
        _autoSize: function(a) {
            if (this._get(a, "autoSize") && !a.inline) {
                var b, c, d, e, f = new Date(2009, 12 - 1, 20), // Ensure double digits
                g = this._get(a, "dateFormat");
                if (g.match(/[DM]/)) {
                    b = function(a) {
                        c = 0;
                        d = 0;
                        for (e = 0; e < a.length; e++) {
                            if (a[e].length > c) {
                                c = a[e].length;
                                d = e;
                            }
                        }
                        return d;
                    };
                    f.setMonth(b(this._get(a, g.match(/MM/) ? "monthNames" : "monthNamesShort")));
                    f.setDate(b(this._get(a, g.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - f.getDay());
                }
                a.input.attr("size", this._formatDate(a, f).length);
            }
        },
        /* Attach an inline date picker to a div. */
        _inlineDatepicker: function(b, c) {
            var d = a(b);
            if (d.hasClass(this.markerClassName)) {
                return;
            }
            d.addClass(this.markerClassName).append(c.dpDiv);
            a.data(b, "datepicker", c);
            this._setDate(c, this._getDefaultDate(c), true);
            this._updateDatepicker(c);
            this._updateAlternate(c);
            //If disabled option is true, disable the datepicker before showing it (see ticket #5665)
            if (c.settings.disabled) {
                this._disableDatepicker(b);
            }
            // Set display:block in place of inst.dpDiv.show() which won't work on disconnected elements
            // http://bugs.jqueryui.com/ticket/7552 - A Datepicker created on a detached div has zero height
            c.dpDiv.css("display", "block");
        },
        /* Pop-up the date picker in a "dialog" box.
	 * @param  input element - ignored
	 * @param  date	string or Date - the initial date to display
	 * @param  onSelect  function - the function to call when a date is selected
	 * @param  settings  object - update the dialog date picker instance's settings (anonymous object)
	 * @param  pos int[2] - coordinates for the dialog's position within the screen or
	 *					event - with x/y coordinates or
	 *					leave empty for default (screen centre)
	 * @return the manager object
	 */
        _dialogDatepicker: function(b, c, d, e, f) {
            var g, h, i, j, k, l = this._dialogInst;
            // internal instance
            if (!l) {
                this.uuid += 1;
                g = "dp" + this.uuid;
                this._dialogInput = a("<input type='text' id='" + g + "' style='position: absolute; top: -100px; width: 0px;'/>");
                this._dialogInput.keydown(this._doKeyDown);
                a("body").append(this._dialogInput);
                l = this._dialogInst = this._newInst(this._dialogInput, false);
                l.settings = {};
                a.data(this._dialogInput[0], "datepicker", l);
            }
            x(l.settings, e || {});
            c = c && c.constructor === Date ? this._formatDate(l, c) : c;
            this._dialogInput.val(c);
            this._pos = f ? f.length ? f : [ f.pageX, f.pageY ] : null;
            if (!this._pos) {
                h = document.documentElement.clientWidth;
                i = document.documentElement.clientHeight;
                j = document.documentElement.scrollLeft || document.body.scrollLeft;
                k = document.documentElement.scrollTop || document.body.scrollTop;
                this._pos = // should use actual width/height below
                [ h / 2 - 100 + j, i / 2 - 150 + k ];
            }
            // move input on screen for focus, but hidden behind dialog
            this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px");
            l.settings.onSelect = d;
            this._inDialog = true;
            this.dpDiv.addClass(this._dialogClass);
            this._showDatepicker(this._dialogInput[0]);
            if (a.blockUI) {
                a.blockUI(this.dpDiv);
            }
            a.data(this._dialogInput[0], "datepicker", l);
            return this;
        },
        /* Detach a datepicker from its control.
	 * @param  target	element - the target input field or division or span
	 */
        _destroyDatepicker: function(b) {
            var c, d = a(b), e = a.data(b, "datepicker");
            if (!d.hasClass(this.markerClassName)) {
                return;
            }
            c = b.nodeName.toLowerCase();
            a.removeData(b, "datepicker");
            if (c === "input") {
                e.append.remove();
                e.trigger.remove();
                d.removeClass(this.markerClassName).unbind("focus", this._showDatepicker).unbind("keydown", this._doKeyDown).unbind("keypress", this._doKeyPress).unbind("keyup", this._doKeyUp);
            } else if (c === "div" || c === "span") {
                d.removeClass(this.markerClassName).empty();
            }
            if (s === e) {
                s = null;
            }
        },
        /* Enable the date picker to a jQuery selection.
	 * @param  target	element - the target input field or division or span
	 */
        _enableDatepicker: function(b) {
            var c, d, e = a(b), f = a.data(b, "datepicker");
            if (!e.hasClass(this.markerClassName)) {
                return;
            }
            c = b.nodeName.toLowerCase();
            if (c === "input") {
                b.disabled = false;
                f.trigger.filter("button").each(function() {
                    this.disabled = false;
                }).end().filter("img").css({
                    opacity: "1.0",
                    cursor: ""
                });
            } else if (c === "div" || c === "span") {
                d = e.children("." + this._inlineClass);
                d.children().removeClass("ui-state-disabled");
                d.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", false);
            }
            this._disabledInputs = a.map(this._disabledInputs, function(a) {
                return a === b ? null : a;
            });
        },
        /* Disable the date picker to a jQuery selection.
	 * @param  target	element - the target input field or division or span
	 */
        _disableDatepicker: function(b) {
            var c, d, e = a(b), f = a.data(b, "datepicker");
            if (!e.hasClass(this.markerClassName)) {
                return;
            }
            c = b.nodeName.toLowerCase();
            if (c === "input") {
                b.disabled = true;
                f.trigger.filter("button").each(function() {
                    this.disabled = true;
                }).end().filter("img").css({
                    opacity: "0.5",
                    cursor: "default"
                });
            } else if (c === "div" || c === "span") {
                d = e.children("." + this._inlineClass);
                d.children().addClass("ui-state-disabled");
                d.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", true);
            }
            this._disabledInputs = a.map(this._disabledInputs, function(a) {
                return a === b ? null : a;
            });
            // delete entry
            this._disabledInputs[this._disabledInputs.length] = b;
        },
        /* Is the first field in a jQuery collection disabled as a datepicker?
	 * @param  target	element - the target input field or division or span
	 * @return boolean - true if disabled, false if enabled
	 */
        _isDisabledDatepicker: function(a) {
            if (!a) {
                return false;
            }
            for (var b = 0; b < this._disabledInputs.length; b++) {
                if (this._disabledInputs[b] === a) {
                    return true;
                }
            }
            return false;
        },
        /* Retrieve the instance data for the target control.
	 * @param  target  element - the target input field or division or span
	 * @return  object - the associated instance data
	 * @throws  error if a jQuery problem getting data
	 */
        _getInst: function(b) {
            try {
                return a.data(b, "datepicker");
            } catch (c) {
                throw "Missing instance data for this datepicker";
            }
        },
        /* Update or retrieve the settings for a date picker attached to an input field or division.
	 * @param  target  element - the target input field or division or span
	 * @param  name	object - the new settings to update or
	 *				string - the name of the setting to change or retrieve,
	 *				when retrieving also "all" for all instance settings or
	 *				"defaults" for all global defaults
	 * @param  value   any - the new value for the setting
	 *				(omit if above is an object or to retrieve a value)
	 */
        _optionDatepicker: function(b, c, d) {
            var e, f, g, h, i = this._getInst(b);
            if (arguments.length === 2 && typeof c === "string") {
                return c === "defaults" ? a.extend({}, a.datepicker._defaults) : i ? c === "all" ? a.extend({}, i.settings) : this._get(i, c) : null;
            }
            e = c || {};
            if (typeof c === "string") {
                e = {};
                e[c] = d;
            }
            if (i) {
                if (this._curInst === i) {
                    this._hideDatepicker();
                }
                f = this._getDateDatepicker(b, true);
                g = this._getMinMaxDate(i, "min");
                h = this._getMinMaxDate(i, "max");
                x(i.settings, e);
                // reformat the old minDate/maxDate values if dateFormat changes and a new minDate/maxDate isn't provided
                if (g !== null && e.dateFormat !== undefined && e.minDate === undefined) {
                    i.settings.minDate = this._formatDate(i, g);
                }
                if (h !== null && e.dateFormat !== undefined && e.maxDate === undefined) {
                    i.settings.maxDate = this._formatDate(i, h);
                }
                if ("disabled" in e) {
                    if (e.disabled) {
                        this._disableDatepicker(b);
                    } else {
                        this._enableDatepicker(b);
                    }
                }
                this._attachments(a(b), i);
                this._autoSize(i);
                this._setDate(i, f);
                this._updateAlternate(i);
                this._updateDatepicker(i);
            }
        },
        // change method deprecated
        _changeDatepicker: function(a, b, c) {
            this._optionDatepicker(a, b, c);
        },
        /* Redraw the date picker attached to an input field or division.
	 * @param  target  element - the target input field or division or span
	 */
        _refreshDatepicker: function(a) {
            var b = this._getInst(a);
            if (b) {
                this._updateDatepicker(b);
            }
        },
        /* Set the dates for a jQuery selection.
	 * @param  target element - the target input field or division or span
	 * @param  date	Date - the new date
	 */
        _setDateDatepicker: function(a, b) {
            var c = this._getInst(a);
            if (c) {
                this._setDate(c, b);
                this._updateDatepicker(c);
                this._updateAlternate(c);
            }
        },
        /* Get the date(s) for the first entry in a jQuery selection.
	 * @param  target element - the target input field or division or span
	 * @param  noDefault boolean - true if no default date is to be used
	 * @return Date - the current date
	 */
        _getDateDatepicker: function(a, b) {
            var c = this._getInst(a);
            if (c && !c.inline) {
                this._setDateFromField(c, b);
            }
            return c ? this._getDate(c) : null;
        },
        /* Handle keystrokes. */
        _doKeyDown: function(b) {
            var c, d, e, f = a.datepicker._getInst(b.target), g = true, h = f.dpDiv.is(".ui-datepicker-rtl");
            f._keyEvent = true;
            if (a.datepicker._datepickerShowing) {
                switch (b.keyCode) {
                  case 9:
                    a.datepicker._hideDatepicker();
                    g = false;
                    break;

                  // hide on tab out
                    case 13:
                    e = a("td." + a.datepicker._dayOverClass + ":not(." + a.datepicker._currentClass + ")", f.dpDiv);
                    if (e[0]) {
                        a.datepicker._selectDay(b.target, f.selectedMonth, f.selectedYear, e[0]);
                    }
                    c = a.datepicker._get(f, "onSelect");
                    if (c) {
                        d = a.datepicker._formatDate(f);
                        // trigger custom callback
                        c.apply(f.input ? f.input[0] : null, [ d, f ]);
                    } else {
                        a.datepicker._hideDatepicker();
                    }
                    return false;

                  // don't submit the form
                    case 27:
                    a.datepicker._hideDatepicker();
                    break;

                  // hide on escape
                    case 33:
                    a.datepicker._adjustDate(b.target, b.ctrlKey ? -a.datepicker._get(f, "stepBigMonths") : -a.datepicker._get(f, "stepMonths"), "M");
                    break;

                  // previous month/year on page up/+ ctrl
                    case 34:
                    a.datepicker._adjustDate(b.target, b.ctrlKey ? +a.datepicker._get(f, "stepBigMonths") : +a.datepicker._get(f, "stepMonths"), "M");
                    break;

                  // next month/year on page down/+ ctrl
                    case 35:
                    if (b.ctrlKey || b.metaKey) {
                        a.datepicker._clearDate(b.target);
                    }
                    g = b.ctrlKey || b.metaKey;
                    break;

                  // clear on ctrl or command +end
                    case 36:
                    if (b.ctrlKey || b.metaKey) {
                        a.datepicker._gotoToday(b.target);
                    }
                    g = b.ctrlKey || b.metaKey;
                    break;

                  // current on ctrl or command +home
                    case 37:
                    if (b.ctrlKey || b.metaKey) {
                        a.datepicker._adjustDate(b.target, h ? +1 : -1, "D");
                    }
                    g = b.ctrlKey || b.metaKey;
                    // -1 day on ctrl or command +left
                    if (b.originalEvent.altKey) {
                        a.datepicker._adjustDate(b.target, b.ctrlKey ? -a.datepicker._get(f, "stepBigMonths") : -a.datepicker._get(f, "stepMonths"), "M");
                    }
                    // next month/year on alt +left on Mac
                    break;

                  case 38:
                    if (b.ctrlKey || b.metaKey) {
                        a.datepicker._adjustDate(b.target, -7, "D");
                    }
                    g = b.ctrlKey || b.metaKey;
                    break;

                  // -1 week on ctrl or command +up
                    case 39:
                    if (b.ctrlKey || b.metaKey) {
                        a.datepicker._adjustDate(b.target, h ? -1 : +1, "D");
                    }
                    g = b.ctrlKey || b.metaKey;
                    // +1 day on ctrl or command +right
                    if (b.originalEvent.altKey) {
                        a.datepicker._adjustDate(b.target, b.ctrlKey ? +a.datepicker._get(f, "stepBigMonths") : +a.datepicker._get(f, "stepMonths"), "M");
                    }
                    // next month/year on alt +right
                    break;

                  case 40:
                    if (b.ctrlKey || b.metaKey) {
                        a.datepicker._adjustDate(b.target, +7, "D");
                    }
                    g = b.ctrlKey || b.metaKey;
                    break;

                  // +1 week on ctrl or command +down
                    default:
                    g = false;
                }
            } else if (b.keyCode === 36 && b.ctrlKey) {
                // display the date picker on ctrl+home
                a.datepicker._showDatepicker(this);
            } else {
                g = false;
            }
            if (g) {
                b.preventDefault();
                b.stopPropagation();
            }
        },
        /* Filter entered characters - based on date format. */
        _doKeyPress: function(b) {
            var c, d, e = a.datepicker._getInst(b.target);
            if (a.datepicker._get(e, "constrainInput")) {
                c = a.datepicker._possibleChars(a.datepicker._get(e, "dateFormat"));
                d = String.fromCharCode(b.charCode == null ? b.keyCode : b.charCode);
                return b.ctrlKey || b.metaKey || (d < " " || !c || c.indexOf(d) > -1);
            }
        },
        /* Synchronise manual entry and field/alternate field. */
        _doKeyUp: function(b) {
            var c, d = a.datepicker._getInst(b.target);
            if (d.input.val() !== d.lastVal) {
                try {
                    c = a.datepicker.parseDate(a.datepicker._get(d, "dateFormat"), d.input ? d.input.val() : null, a.datepicker._getFormatConfig(d));
                    if (c) {
                        // only if valid
                        a.datepicker._setDateFromField(d);
                        a.datepicker._updateAlternate(d);
                        a.datepicker._updateDatepicker(d);
                    }
                } catch (e) {}
            }
            return true;
        },
        /* Pop-up the date picker for a given input field.
	 * If false returned from beforeShow event handler do not show.
	 * @param  input  element - the input field attached to the date picker or
	 *					event - if triggered by focus
	 */
        _showDatepicker: function(b) {
            b = b.target || b;
            if (b.nodeName.toLowerCase() !== "input") {
                // find from button/image trigger
                b = a("input", b.parentNode)[0];
            }
            if (a.datepicker._isDisabledDatepicker(b) || a.datepicker._lastInput === b) {
                // already here
                return;
            }
            var c, d, e, f, g, h, i;
            c = a.datepicker._getInst(b);
            if (a.datepicker._curInst && a.datepicker._curInst !== c) {
                a.datepicker._curInst.dpDiv.stop(true, true);
                if (c && a.datepicker._datepickerShowing) {
                    a.datepicker._hideDatepicker(a.datepicker._curInst.input[0]);
                }
            }
            d = a.datepicker._get(c, "beforeShow");
            e = d ? d.apply(b, [ b, c ]) : {};
            if (e === false) {
                return;
            }
            x(c.settings, e);
            c.lastVal = null;
            a.datepicker._lastInput = b;
            a.datepicker._setDateFromField(c);
            if (a.datepicker._inDialog) {
                // hide cursor
                b.value = "";
            }
            if (!a.datepicker._pos) {
                // position below input
                a.datepicker._pos = a.datepicker._findPos(b);
                a.datepicker._pos[1] += b.offsetHeight;
            }
            f = false;
            a(b).parents().each(function() {
                f |= a(this).css("position") === "fixed";
                return !f;
            });
            g = {
                left: a.datepicker._pos[0],
                top: a.datepicker._pos[1]
            };
            a.datepicker._pos = null;
            //to avoid flashes on Firefox
            c.dpDiv.empty();
            // determine sizing offscreen
            c.dpDiv.css({
                position: "absolute",
                display: "block",
                top: "-1000px"
            });
            a.datepicker._updateDatepicker(c);
            // fix width for dynamic number of date pickers
            // and adjust position before showing
            g = a.datepicker._checkOffset(c, g, f);
            c.dpDiv.css({
                position: a.datepicker._inDialog && a.blockUI ? "static" : f ? "fixed" : "absolute",
                display: "none",
                left: g.left + "px",
                top: g.top + "px"
            });
            if (!c.inline) {
                h = a.datepicker._get(c, "showAnim");
                i = a.datepicker._get(c, "duration");
                c.dpDiv.css("z-index", t(a(b)) + 1);
                a.datepicker._datepickerShowing = true;
                if (a.effects && a.effects.effect[h]) {
                    c.dpDiv.show(h, a.datepicker._get(c, "showOptions"), i);
                } else {
                    c.dpDiv[h || "show"](h ? i : null);
                }
                if (a.datepicker._shouldFocusInput(c)) {
                    c.input.focus();
                }
                a.datepicker._curInst = c;
            }
        },
        /* Generate the date picker content. */
        _updateDatepicker: function(b) {
            this.maxRows = 4;
            //Reset the max number of rows being displayed (see #7043)
            s = b;
            // for delegate hover events
            b.dpDiv.empty().append(this._generateHTML(b));
            this._attachHandlers(b);
            var c, d = this._getNumberOfMonths(b), e = d[1], f = 17, g = b.dpDiv.find("." + this._dayOverClass + " a");
            if (g.length > 0) {
                w.apply(g.get(0));
            }
            b.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width("");
            if (e > 1) {
                b.dpDiv.addClass("ui-datepicker-multi-" + e).css("width", f * e + "em");
            }
            b.dpDiv[(d[0] !== 1 || d[1] !== 1 ? "add" : "remove") + "Class"]("ui-datepicker-multi");
            b.dpDiv[(this._get(b, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl");
            if (b === a.datepicker._curInst && a.datepicker._datepickerShowing && a.datepicker._shouldFocusInput(b)) {
                b.input.focus();
            }
            // deffered render of the years select (to avoid flashes on Firefox)
            if (b.yearshtml) {
                c = b.yearshtml;
                setTimeout(function() {
                    //assure that inst.yearshtml didn't change.
                    if (c === b.yearshtml && b.yearshtml) {
                        b.dpDiv.find("select.ui-datepicker-year:first").replaceWith(b.yearshtml);
                    }
                    c = b.yearshtml = null;
                }, 0);
            }
        },
        // #6694 - don't focus the input if it's already focused
        // this breaks the change event in IE
        // Support: IE and jQuery <1.9
        _shouldFocusInput: function(a) {
            return a.input && a.input.is(":visible") && !a.input.is(":disabled") && !a.input.is(":focus");
        },
        /* Check positioning to remain on screen. */
        _checkOffset: function(b, c, d) {
            var e = b.dpDiv.outerWidth(), f = b.dpDiv.outerHeight(), g = b.input ? b.input.outerWidth() : 0, h = b.input ? b.input.outerHeight() : 0, i = document.documentElement.clientWidth + (d ? 0 : a(document).scrollLeft()), j = document.documentElement.clientHeight + (d ? 0 : a(document).scrollTop());
            c.left -= this._get(b, "isRTL") ? e - g : 0;
            c.left -= d && c.left === b.input.offset().left ? a(document).scrollLeft() : 0;
            c.top -= d && c.top === b.input.offset().top + h ? a(document).scrollTop() : 0;
            // now check if datepicker is showing outside window viewport - move to a better place if so.
            c.left -= Math.min(c.left, c.left + e > i && i > e ? Math.abs(c.left + e - i) : 0);
            c.top -= Math.min(c.top, c.top + f > j && j > f ? Math.abs(f + h) : 0);
            return c;
        },
        /* Find an object's position on the screen. */
        _findPos: function(b) {
            var c, d = this._getInst(b), e = this._get(d, "isRTL");
            while (b && (b.type === "hidden" || b.nodeType !== 1 || a.expr.filters.hidden(b))) {
                b = b[e ? "previousSibling" : "nextSibling"];
            }
            c = a(b).offset();
            return [ c.left, c.top ];
        },
        /* Hide the date picker from view.
	 * @param  input  element - the input field attached to the date picker
	 */
        _hideDatepicker: function(b) {
            var c, d, e, f, g = this._curInst;
            if (!g || b && g !== a.data(b, "datepicker")) {
                return;
            }
            if (this._datepickerShowing) {
                c = this._get(g, "showAnim");
                d = this._get(g, "duration");
                e = function() {
                    a.datepicker._tidyDialog(g);
                };
                // DEPRECATED: after BC for 1.8.x $.effects[ showAnim ] is not needed
                if (a.effects && (a.effects.effect[c] || a.effects[c])) {
                    g.dpDiv.hide(c, a.datepicker._get(g, "showOptions"), d, e);
                } else {
                    g.dpDiv[c === "slideDown" ? "slideUp" : c === "fadeIn" ? "fadeOut" : "hide"](c ? d : null, e);
                }
                if (!c) {
                    e();
                }
                this._datepickerShowing = false;
                f = this._get(g, "onClose");
                if (f) {
                    f.apply(g.input ? g.input[0] : null, [ g.input ? g.input.val() : "", g ]);
                }
                this._lastInput = null;
                if (this._inDialog) {
                    this._dialogInput.css({
                        position: "absolute",
                        left: "0",
                        top: "-100px"
                    });
                    if (a.blockUI) {
                        a.unblockUI();
                        a("body").append(this.dpDiv);
                    }
                }
                this._inDialog = false;
            }
        },
        /* Tidy up after a dialog display. */
        _tidyDialog: function(a) {
            a.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar");
        },
        /* Close date picker if clicked elsewhere. */
        _checkExternalClick: function(b) {
            if (!a.datepicker._curInst) {
                return;
            }
            var c = a(b.target), d = a.datepicker._getInst(c[0]);
            if (c[0].id !== a.datepicker._mainDivId && c.parents("#" + a.datepicker._mainDivId).length === 0 && !c.hasClass(a.datepicker.markerClassName) && !c.closest("." + a.datepicker._triggerClass).length && a.datepicker._datepickerShowing && !(a.datepicker._inDialog && a.blockUI) || c.hasClass(a.datepicker.markerClassName) && a.datepicker._curInst !== d) {
                a.datepicker._hideDatepicker();
            }
        },
        /* Adjust one of the date sub-fields. */
        _adjustDate: function(b, c, d) {
            var e = a(b), f = this._getInst(e[0]);
            if (this._isDisabledDatepicker(e[0])) {
                return;
            }
            this._adjustInstDate(f, c + (d === "M" ? this._get(f, "showCurrentAtPos") : 0), // undo positioning
            d);
            this._updateDatepicker(f);
        },
        /* Action for current link. */
        _gotoToday: function(b) {
            var c, d = a(b), e = this._getInst(d[0]);
            if (this._get(e, "gotoCurrent") && e.currentDay) {
                e.selectedDay = e.currentDay;
                e.drawMonth = e.selectedMonth = e.currentMonth;
                e.drawYear = e.selectedYear = e.currentYear;
            } else {
                c = new Date();
                e.selectedDay = c.getDate();
                e.drawMonth = e.selectedMonth = c.getMonth();
                e.drawYear = e.selectedYear = c.getFullYear();
            }
            this._notifyChange(e);
            this._adjustDate(d);
        },
        /* Action for selecting a new month/year. */
        _selectMonthYear: function(b, c, d) {
            var e = a(b), f = this._getInst(e[0]);
            f["selected" + (d === "M" ? "Month" : "Year")] = f["draw" + (d === "M" ? "Month" : "Year")] = parseInt(c.options[c.selectedIndex].value, 10);
            this._notifyChange(f);
            this._adjustDate(e);
        },
        /* Action for selecting a day. */
        _selectDay: function(b, c, d, e) {
            var f, g = a(b);
            if (a(e).hasClass(this._unselectableClass) || this._isDisabledDatepicker(g[0])) {
                return;
            }
            f = this._getInst(g[0]);
            f.selectedDay = f.currentDay = a("a", e).html();
            f.selectedMonth = f.currentMonth = c;
            f.selectedYear = f.currentYear = d;
            this._selectDate(b, this._formatDate(f, f.currentDay, f.currentMonth, f.currentYear));
        },
        /* Erase the input field and hide the date picker. */
        _clearDate: function(b) {
            var c = a(b);
            this._selectDate(c, "");
        },
        /* Update the input field with the selected date. */
        _selectDate: function(b, c) {
            var d, e = a(b), f = this._getInst(e[0]);
            c = c != null ? c : this._formatDate(f);
            if (f.input) {
                f.input.val(c);
            }
            this._updateAlternate(f);
            d = this._get(f, "onSelect");
            if (d) {
                d.apply(f.input ? f.input[0] : null, [ c, f ]);
            } else if (f.input) {
                f.input.trigger("change");
            }
            if (f.inline) {
                this._updateDatepicker(f);
            } else {
                this._hideDatepicker();
                this._lastInput = f.input[0];
                if (typeof f.input[0] !== "object") {
                    f.input.focus();
                }
                this._lastInput = null;
            }
        },
        /* Update any alternate field to synchronise with the main field. */
        _updateAlternate: function(b) {
            var c, d, e, f = this._get(b, "altField");
            if (f) {
                // update alternate field too
                c = this._get(b, "altFormat") || this._get(b, "dateFormat");
                d = this._getDate(b);
                e = this.formatDate(c, d, this._getFormatConfig(b));
                a(f).each(function() {
                    a(this).val(e);
                });
            }
        },
        /* Set as beforeShowDay function to prevent selection of weekends.
	 * @param  date  Date - the date to customise
	 * @return [boolean, string] - is this date selectable?, what is its CSS class?
	 */
        noWeekends: function(a) {
            var b = a.getDay();
            return [ b > 0 && b < 6, "" ];
        },
        /* Set as calculateWeek to determine the week of the year based on the ISO 8601 definition.
	 * @param  date  Date - the date to get the week for
	 * @return  number - the number of the week within the year that contains this date
	 */
        iso8601Week: function(a) {
            var b, c = new Date(a.getTime());
            // Find Thursday of this week starting on Monday
            c.setDate(c.getDate() + 4 - (c.getDay() || 7));
            b = c.getTime();
            c.setMonth(0);
            // Compare with Jan 1
            c.setDate(1);
            return Math.floor(Math.round((b - c) / 864e5) / 7) + 1;
        },
        /* Parse a string value into a date object.
	 * See formatDate below for the possible formats.
	 *
	 * @param  format string - the expected format of the date
	 * @param  value string - the date in the above format
	 * @param  settings Object - attributes include:
	 *					shortYearCutoff  number - the cutoff year for determining the century (optional)
	 *					dayNamesShort	string[7] - abbreviated names of the days from Sunday (optional)
	 *					dayNames		string[7] - names of the days from Sunday (optional)
	 *					monthNamesShort string[12] - abbreviated names of the months (optional)
	 *					monthNames		string[12] - names of the months (optional)
	 * @return  Date - the extracted date value or null if value is blank
	 */
        parseDate: function(b, c, d) {
            if (b == null || c == null) {
                throw "Invalid arguments";
            }
            c = typeof c === "object" ? c.toString() : c + "";
            if (c === "") {
                return null;
            }
            var e, f, g, h = 0, i = (d ? d.shortYearCutoff : null) || this._defaults.shortYearCutoff, j = typeof i !== "string" ? i : new Date().getFullYear() % 100 + parseInt(i, 10), k = (d ? d.dayNamesShort : null) || this._defaults.dayNamesShort, l = (d ? d.dayNames : null) || this._defaults.dayNames, m = (d ? d.monthNamesShort : null) || this._defaults.monthNamesShort, n = (d ? d.monthNames : null) || this._defaults.monthNames, o = -1, p = -1, q = -1, r = -1, s = false, t, // Check whether a format character is doubled
            u = function(a) {
                var c = e + 1 < b.length && b.charAt(e + 1) === a;
                if (c) {
                    e++;
                }
                return c;
            }, // Extract a number from the string value
            v = function(a) {
                var b = u(a), d = a === "@" ? 14 : a === "!" ? 20 : a === "y" && b ? 4 : a === "o" ? 3 : 2, e = a === "y" ? d : 1, f = new RegExp("^\\d{" + e + "," + d + "}"), g = c.substring(h).match(f);
                if (!g) {
                    throw "Missing number at position " + h;
                }
                h += g[0].length;
                return parseInt(g[0], 10);
            }, // Extract a name from the string value and convert to an index
            w = function(b, d, e) {
                var f = -1, g = a.map(u(b) ? e : d, function(a, b) {
                    return [ [ b, a ] ];
                }).sort(function(a, b) {
                    return -(a[1].length - b[1].length);
                });
                a.each(g, function(a, b) {
                    var d = b[1];
                    if (c.substr(h, d.length).toLowerCase() === d.toLowerCase()) {
                        f = b[0];
                        h += d.length;
                        return false;
                    }
                });
                if (f !== -1) {
                    return f + 1;
                } else {
                    throw "Unknown name at position " + h;
                }
            }, // Confirm that a literal character matches the string value
            x = function() {
                if (c.charAt(h) !== b.charAt(e)) {
                    throw "Unexpected literal at position " + h;
                }
                h++;
            };
            for (e = 0; e < b.length; e++) {
                if (s) {
                    if (b.charAt(e) === "'" && !u("'")) {
                        s = false;
                    } else {
                        x();
                    }
                } else {
                    switch (b.charAt(e)) {
                      case "d":
                        q = v("d");
                        break;

                      case "D":
                        w("D", k, l);
                        break;

                      case "o":
                        r = v("o");
                        break;

                      case "m":
                        p = v("m");
                        break;

                      case "M":
                        p = w("M", m, n);
                        break;

                      case "y":
                        o = v("y");
                        break;

                      case "@":
                        t = new Date(v("@"));
                        o = t.getFullYear();
                        p = t.getMonth() + 1;
                        q = t.getDate();
                        break;

                      case "!":
                        t = new Date((v("!") - this._ticksTo1970) / 1e4);
                        o = t.getFullYear();
                        p = t.getMonth() + 1;
                        q = t.getDate();
                        break;

                      case "'":
                        if (u("'")) {
                            x();
                        } else {
                            s = true;
                        }
                        break;

                      default:
                        x();
                    }
                }
            }
            if (h < c.length) {
                g = c.substr(h);
                if (!/^\s+/.test(g)) {
                    throw "Extra/unparsed characters found in date: " + g;
                }
            }
            if (o === -1) {
                o = new Date().getFullYear();
            } else if (o < 100) {
                o += new Date().getFullYear() - new Date().getFullYear() % 100 + (o <= j ? 0 : -100);
            }
            if (r > -1) {
                p = 1;
                q = r;
                do {
                    f = this._getDaysInMonth(o, p - 1);
                    if (q <= f) {
                        break;
                    }
                    p++;
                    q -= f;
                } while (true);
            }
            t = this._daylightSavingAdjust(new Date(o, p - 1, q));
            if (t.getFullYear() !== o || t.getMonth() + 1 !== p || t.getDate() !== q) {
                throw "Invalid date";
            }
            return t;
        },
        /* Standard date formats. */
        ATOM: "yy-mm-dd",
        // RFC 3339 (ISO 8601)
        COOKIE: "D, dd M yy",
        ISO_8601: "yy-mm-dd",
        RFC_822: "D, d M y",
        RFC_850: "DD, dd-M-y",
        RFC_1036: "D, d M y",
        RFC_1123: "D, d M yy",
        RFC_2822: "D, d M yy",
        RSS: "D, d M y",
        // RFC 822
        TICKS: "!",
        TIMESTAMP: "@",
        W3C: "yy-mm-dd",
        // ISO 8601
        _ticksTo1970: ((1970 - 1) * 365 + Math.floor(1970 / 4) - Math.floor(1970 / 100) + Math.floor(1970 / 400)) * 24 * 60 * 60 * 1e7,
        /* Format a date object into a string value.
	 * The format can be combinations of the following:
	 * d  - day of month (no leading zero)
	 * dd - day of month (two digit)
	 * o  - day of year (no leading zeros)
	 * oo - day of year (three digit)
	 * D  - day name short
	 * DD - day name long
	 * m  - month of year (no leading zero)
	 * mm - month of year (two digit)
	 * M  - month name short
	 * MM - month name long
	 * y  - year (two digit)
	 * yy - year (four digit)
	 * @ - Unix timestamp (ms since 01/01/1970)
	 * ! - Windows ticks (100ns since 01/01/0001)
	 * "..." - literal text
	 * '' - single quote
	 *
	 * @param  format string - the desired format of the date
	 * @param  date Date - the date value to format
	 * @param  settings Object - attributes include:
	 *					dayNamesShort	string[7] - abbreviated names of the days from Sunday (optional)
	 *					dayNames		string[7] - names of the days from Sunday (optional)
	 *					monthNamesShort string[12] - abbreviated names of the months (optional)
	 *					monthNames		string[12] - names of the months (optional)
	 * @return  string - the date in the above format
	 */
        formatDate: function(a, b, c) {
            if (!b) {
                return "";
            }
            var d, e = (c ? c.dayNamesShort : null) || this._defaults.dayNamesShort, f = (c ? c.dayNames : null) || this._defaults.dayNames, g = (c ? c.monthNamesShort : null) || this._defaults.monthNamesShort, h = (c ? c.monthNames : null) || this._defaults.monthNames, // Check whether a format character is doubled
            i = function(b) {
                var c = d + 1 < a.length && a.charAt(d + 1) === b;
                if (c) {
                    d++;
                }
                return c;
            }, // Format a number, with leading zero if necessary
            j = function(a, b, c) {
                var d = "" + b;
                if (i(a)) {
                    while (d.length < c) {
                        d = "0" + d;
                    }
                }
                return d;
            }, // Format a name, short or long as requested
            k = function(a, b, c, d) {
                return i(a) ? d[b] : c[b];
            }, l = "", m = false;
            if (b) {
                for (d = 0; d < a.length; d++) {
                    if (m) {
                        if (a.charAt(d) === "'" && !i("'")) {
                            m = false;
                        } else {
                            l += a.charAt(d);
                        }
                    } else {
                        switch (a.charAt(d)) {
                          case "d":
                            l += j("d", b.getDate(), 2);
                            break;

                          case "D":
                            l += k("D", b.getDay(), e, f);
                            break;

                          case "o":
                            l += j("o", Math.round((new Date(b.getFullYear(), b.getMonth(), b.getDate()).getTime() - new Date(b.getFullYear(), 0, 0).getTime()) / 864e5), 3);
                            break;

                          case "m":
                            l += j("m", b.getMonth() + 1, 2);
                            break;

                          case "M":
                            l += k("M", b.getMonth(), g, h);
                            break;

                          case "y":
                            l += i("y") ? b.getFullYear() : (b.getYear() % 100 < 10 ? "0" : "") + b.getYear() % 100;
                            break;

                          case "@":
                            l += b.getTime();
                            break;

                          case "!":
                            l += b.getTime() * 1e4 + this._ticksTo1970;
                            break;

                          case "'":
                            if (i("'")) {
                                l += "'";
                            } else {
                                m = true;
                            }
                            break;

                          default:
                            l += a.charAt(d);
                        }
                    }
                }
            }
            return l;
        },
        /* Extract all possible characters from the date format. */
        _possibleChars: function(a) {
            var b, c = "", d = false, // Check whether a format character is doubled
            e = function(c) {
                var d = b + 1 < a.length && a.charAt(b + 1) === c;
                if (d) {
                    b++;
                }
                return d;
            };
            for (b = 0; b < a.length; b++) {
                if (d) {
                    if (a.charAt(b) === "'" && !e("'")) {
                        d = false;
                    } else {
                        c += a.charAt(b);
                    }
                } else {
                    switch (a.charAt(b)) {
                      case "d":
                      case "m":
                      case "y":
                      case "@":
                        c += "0123456789";
                        break;

                      case "D":
                      case "M":
                        return null;

                      // Accept anything
                        case "'":
                        if (e("'")) {
                            c += "'";
                        } else {
                            d = true;
                        }
                        break;

                      default:
                        c += a.charAt(b);
                    }
                }
            }
            return c;
        },
        /* Get a setting value, defaulting if necessary. */
        _get: function(a, b) {
            return a.settings[b] !== undefined ? a.settings[b] : this._defaults[b];
        },
        /* Parse existing date and initialise date picker. */
        _setDateFromField: function(a, b) {
            if (a.input.val() === a.lastVal) {
                return;
            }
            var c = this._get(a, "dateFormat"), d = a.lastVal = a.input ? a.input.val() : null, e = this._getDefaultDate(a), f = e, g = this._getFormatConfig(a);
            try {
                f = this.parseDate(c, d, g) || e;
            } catch (h) {
                d = b ? "" : d;
            }
            a.selectedDay = f.getDate();
            a.drawMonth = a.selectedMonth = f.getMonth();
            a.drawYear = a.selectedYear = f.getFullYear();
            a.currentDay = d ? f.getDate() : 0;
            a.currentMonth = d ? f.getMonth() : 0;
            a.currentYear = d ? f.getFullYear() : 0;
            this._adjustInstDate(a);
        },
        /* Retrieve the default date shown on opening. */
        _getDefaultDate: function(a) {
            return this._restrictMinMax(a, this._determineDate(a, this._get(a, "defaultDate"), new Date()));
        },
        /* A date may be specified as an exact value or a relative one. */
        _determineDate: function(b, c, d) {
            var e = function(a) {
                var b = new Date();
                b.setDate(b.getDate() + a);
                return b;
            }, f = function(c) {
                try {
                    return a.datepicker.parseDate(a.datepicker._get(b, "dateFormat"), c, a.datepicker._getFormatConfig(b));
                } catch (d) {}
                var e = (c.toLowerCase().match(/^c/) ? a.datepicker._getDate(b) : null) || new Date(), f = e.getFullYear(), g = e.getMonth(), h = e.getDate(), i = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, j = i.exec(c);
                while (j) {
                    switch (j[2] || "d") {
                      case "d":
                      case "D":
                        h += parseInt(j[1], 10);
                        break;

                      case "w":
                      case "W":
                        h += parseInt(j[1], 10) * 7;
                        break;

                      case "m":
                      case "M":
                        g += parseInt(j[1], 10);
                        h = Math.min(h, a.datepicker._getDaysInMonth(f, g));
                        break;

                      case "y":
                      case "Y":
                        f += parseInt(j[1], 10);
                        h = Math.min(h, a.datepicker._getDaysInMonth(f, g));
                        break;
                    }
                    j = i.exec(c);
                }
                return new Date(f, g, h);
            }, g = c == null || c === "" ? d : typeof c === "string" ? f(c) : typeof c === "number" ? isNaN(c) ? d : e(c) : new Date(c.getTime());
            g = g && g.toString() === "Invalid Date" ? d : g;
            if (g) {
                g.setHours(0);
                g.setMinutes(0);
                g.setSeconds(0);
                g.setMilliseconds(0);
            }
            return this._daylightSavingAdjust(g);
        },
        /* Handle switch to/from daylight saving.
	 * Hours may be non-zero on daylight saving cut-over:
	 * > 12 when midnight changeover, but then cannot generate
	 * midnight datetime, so jump to 1AM, otherwise reset.
	 * @param  date  (Date) the date to check
	 * @return  (Date) the corrected date
	 */
        _daylightSavingAdjust: function(a) {
            if (!a) {
                return null;
            }
            a.setHours(a.getHours() > 12 ? a.getHours() + 2 : 0);
            return a;
        },
        /* Set the date(s) directly. */
        _setDate: function(a, b, c) {
            var d = !b, e = a.selectedMonth, f = a.selectedYear, g = this._restrictMinMax(a, this._determineDate(a, b, new Date()));
            a.selectedDay = a.currentDay = g.getDate();
            a.drawMonth = a.selectedMonth = a.currentMonth = g.getMonth();
            a.drawYear = a.selectedYear = a.currentYear = g.getFullYear();
            if ((e !== a.selectedMonth || f !== a.selectedYear) && !c) {
                this._notifyChange(a);
            }
            this._adjustInstDate(a);
            if (a.input) {
                a.input.val(d ? "" : this._formatDate(a));
            }
        },
        /* Retrieve the date(s) directly. */
        _getDate: function(a) {
            var b = !a.currentYear || a.input && a.input.val() === "" ? null : this._daylightSavingAdjust(new Date(a.currentYear, a.currentMonth, a.currentDay));
            return b;
        },
        /* Attach the onxxx handlers.  These are declared statically so
	 * they work with static code transformers like Caja.
	 */
        _attachHandlers: function(b) {
            var c = this._get(b, "stepMonths"), d = "#" + b.id.replace(/\\\\/g, "\\");
            b.dpDiv.find("[data-handler]").map(function() {
                var b = {
                    prev: function() {
                        a.datepicker._adjustDate(d, -c, "M");
                    },
                    next: function() {
                        a.datepicker._adjustDate(d, +c, "M");
                    },
                    hide: function() {
                        a.datepicker._hideDatepicker();
                    },
                    today: function() {
                        a.datepicker._gotoToday(d);
                    },
                    selectDay: function() {
                        a.datepicker._selectDay(d, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this);
                        return false;
                    },
                    selectMonth: function() {
                        a.datepicker._selectMonthYear(d, this, "M");
                        return false;
                    },
                    selectYear: function() {
                        a.datepicker._selectMonthYear(d, this, "Y");
                        return false;
                    }
                };
                a(this).bind(this.getAttribute("data-event"), b[this.getAttribute("data-handler")]);
            });
        },
        /* Generate the HTML for the current state of the date picker. */
        _generateHTML: function(a) {
            var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O = new Date(), P = this._daylightSavingAdjust(new Date(O.getFullYear(), O.getMonth(), O.getDate())), // clear time
            Q = this._get(a, "isRTL"), R = this._get(a, "showButtonPanel"), S = this._get(a, "hideIfNoPrevNext"), T = this._get(a, "navigationAsDateFormat"), U = this._getNumberOfMonths(a), V = this._get(a, "showCurrentAtPos"), W = this._get(a, "stepMonths"), X = U[0] !== 1 || U[1] !== 1, Y = this._daylightSavingAdjust(!a.currentDay ? new Date(9999, 9, 9) : new Date(a.currentYear, a.currentMonth, a.currentDay)), Z = this._getMinMaxDate(a, "min"), $ = this._getMinMaxDate(a, "max"), _ = a.drawMonth - V, aa = a.drawYear;
            if (_ < 0) {
                _ += 12;
                aa--;
            }
            if ($) {
                b = this._daylightSavingAdjust(new Date($.getFullYear(), $.getMonth() - U[0] * U[1] + 1, $.getDate()));
                b = Z && b < Z ? Z : b;
                while (this._daylightSavingAdjust(new Date(aa, _, 1)) > b) {
                    _--;
                    if (_ < 0) {
                        _ = 11;
                        aa--;
                    }
                }
            }
            a.drawMonth = _;
            a.drawYear = aa;
            c = this._get(a, "prevText");
            c = !T ? c : this.formatDate(c, this._daylightSavingAdjust(new Date(aa, _ - W, 1)), this._getFormatConfig(a));
            d = this._canAdjustMonth(a, -1, aa, _) ? "<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click'" + " title='" + c + "'><span class='ui-icon ui-icon-circle-triangle-" + (Q ? "e" : "w") + "'>" + c + "</span></a>" : S ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='" + c + "'><span class='ui-icon ui-icon-circle-triangle-" + (Q ? "e" : "w") + "'>" + c + "</span></a>";
            e = this._get(a, "nextText");
            e = !T ? e : this.formatDate(e, this._daylightSavingAdjust(new Date(aa, _ + W, 1)), this._getFormatConfig(a));
            f = this._canAdjustMonth(a, +1, aa, _) ? "<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click'" + " title='" + e + "'><span class='ui-icon ui-icon-circle-triangle-" + (Q ? "w" : "e") + "'>" + e + "</span></a>" : S ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='" + e + "'><span class='ui-icon ui-icon-circle-triangle-" + (Q ? "w" : "e") + "'>" + e + "</span></a>";
            g = this._get(a, "currentText");
            h = this._get(a, "gotoCurrent") && a.currentDay ? Y : P;
            g = !T ? g : this.formatDate(g, h, this._getFormatConfig(a));
            i = !a.inline ? "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" + this._get(a, "closeText") + "</button>" : "";
            j = R ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + (Q ? i : "") + (this._isInRange(a, h) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'" + ">" + g + "</button>" : "") + (Q ? "" : i) + "</div>" : "";
            k = parseInt(this._get(a, "firstDay"), 10);
            k = isNaN(k) ? 0 : k;
            l = this._get(a, "showWeek");
            m = this._get(a, "dayNames");
            n = this._get(a, "dayNamesMin");
            o = this._get(a, "monthNames");
            p = this._get(a, "monthNamesShort");
            q = this._get(a, "beforeShowDay");
            r = this._get(a, "showOtherMonths");
            s = this._get(a, "selectOtherMonths");
            t = this._getDefaultDate(a);
            u = "";
            v;
            for (w = 0; w < U[0]; w++) {
                x = "";
                this.maxRows = 4;
                for (y = 0; y < U[1]; y++) {
                    z = this._daylightSavingAdjust(new Date(aa, _, a.selectedDay));
                    A = " ui-corner-all";
                    B = "";
                    if (X) {
                        B += "<div class='ui-datepicker-group";
                        if (U[1] > 1) {
                            switch (y) {
                              case 0:
                                B += " ui-datepicker-group-first";
                                A = " ui-corner-" + (Q ? "right" : "left");
                                break;

                              case U[1] - 1:
                                B += " ui-datepicker-group-last";
                                A = " ui-corner-" + (Q ? "left" : "right");
                                break;

                              default:
                                B += " ui-datepicker-group-middle";
                                A = "";
                                break;
                            }
                        }
                        B += "'>";
                    }
                    B += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + A + "'>" + (/all|left/.test(A) && w === 0 ? Q ? f : d : "") + (/all|right/.test(A) && w === 0 ? Q ? d : f : "") + this._generateMonthYearHeader(a, _, aa, Z, $, w > 0 || y > 0, o, p) + // draw month headers
                    "</div><table class='ui-datepicker-calendar'><thead>" + "<tr>";
                    C = l ? "<th class='ui-datepicker-week-col'>" + this._get(a, "weekHeader") + "</th>" : "";
                    for (v = 0; v < 7; v++) {
                        // days of the week
                        D = (v + k) % 7;
                        C += "<th scope='col'" + ((v + k + 6) % 7 >= 5 ? " class='ui-datepicker-week-end'" : "") + ">" + "<span title='" + m[D] + "'>" + n[D] + "</span></th>";
                    }
                    B += C + "</tr></thead><tbody>";
                    E = this._getDaysInMonth(aa, _);
                    if (aa === a.selectedYear && _ === a.selectedMonth) {
                        a.selectedDay = Math.min(a.selectedDay, E);
                    }
                    F = (this._getFirstDayOfMonth(aa, _) - k + 7) % 7;
                    G = Math.ceil((F + E) / 7);
                    // calculate the number of rows to generate
                    H = X ? this.maxRows > G ? this.maxRows : G : G;
                    //If multiple months, use the higher number of rows (see #7043)
                    this.maxRows = H;
                    I = this._daylightSavingAdjust(new Date(aa, _, 1 - F));
                    for (J = 0; J < H; J++) {
                        // create date picker rows
                        B += "<tr>";
                        K = !l ? "" : "<td class='ui-datepicker-week-col'>" + this._get(a, "calculateWeek")(I) + "</td>";
                        for (v = 0; v < 7; v++) {
                            // create date picker days
                            L = q ? q.apply(a.input ? a.input[0] : null, [ I ]) : [ true, "" ];
                            M = I.getMonth() !== _;
                            N = M && !s || !L[0] || Z && I < Z || $ && I > $;
                            K += "<td class='" + ((v + k + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (// highlight weekends
                            M ? " ui-datepicker-other-month" : "") + (// highlight days from other months
                            I.getTime() === z.getTime() && _ === a.selectedMonth && a._keyEvent || // user pressed key
                            t.getTime() === I.getTime() && t.getTime() === z.getTime() ? // or defaultDate is current printedDate and defaultDate is selectedDate
                            " " + this._dayOverClass : "") + (// highlight selected day
                            N ? " " + this._unselectableClass + " ui-state-disabled" : "") + (// highlight unselectable days
                            M && !r ? "" : " " + L[1] + (// highlight custom dates
                            I.getTime() === Y.getTime() ? " " + this._currentClass : "") + (// highlight selected day
                            I.getTime() === P.getTime() ? " ui-datepicker-today" : "")) + "'" + (// highlight today (if different)
                            (!M || r) && L[2] ? " title='" + L[2].replace(/'/g, "&#39;") + "'" : "") + (// cell title
                            N ? "" : " data-handler='selectDay' data-event='click' data-month='" + I.getMonth() + "' data-year='" + I.getFullYear() + "'") + ">" + (// actions
                            M && !r ? "&#xa0;" : // display for other months
                            N ? "<span class='ui-state-default'>" + I.getDate() + "</span>" : "<a class='ui-state-default" + (I.getTime() === P.getTime() ? " ui-state-highlight" : "") + (I.getTime() === Y.getTime() ? " ui-state-active" : "") + (// highlight selected day
                            M ? " ui-priority-secondary" : "") + // distinguish dates from other months
                            "' href='#'>" + I.getDate() + "</a>") + "</td>";
                            // display selectable date
                            I.setDate(I.getDate() + 1);
                            I = this._daylightSavingAdjust(I);
                        }
                        B += K + "</tr>";
                    }
                    _++;
                    if (_ > 11) {
                        _ = 0;
                        aa++;
                    }
                    B += "</tbody></table>" + (X ? "</div>" + (U[0] > 0 && y === U[1] - 1 ? "<div class='ui-datepicker-row-break'></div>" : "") : "");
                    x += B;
                }
                u += x;
            }
            u += j;
            a._keyEvent = false;
            return u;
        },
        /* Generate the month and year header. */
        _generateMonthYearHeader: function(a, b, c, d, e, f, g, h) {
            var i, j, k, l, m, n, o, p, q = this._get(a, "changeMonth"), r = this._get(a, "changeYear"), s = this._get(a, "showMonthAfterYear"), t = "<div class='ui-datepicker-title'>", u = "";
            // month selection
            if (f || !q) {
                u += "<span class='ui-datepicker-month'>" + g[b] + "</span>";
            } else {
                i = d && d.getFullYear() === c;
                j = e && e.getFullYear() === c;
                u += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>";
                for (k = 0; k < 12; k++) {
                    if ((!i || k >= d.getMonth()) && (!j || k <= e.getMonth())) {
                        u += "<option value='" + k + "'" + (k === b ? " selected='selected'" : "") + ">" + h[k] + "</option>";
                    }
                }
                u += "</select>";
            }
            if (!s) {
                t += u + (f || !(q && r) ? "&#xa0;" : "");
            }
            // year selection
            if (!a.yearshtml) {
                a.yearshtml = "";
                if (f || !r) {
                    t += "<span class='ui-datepicker-year'>" + c + "</span>";
                } else {
                    // determine range of years to display
                    l = this._get(a, "yearRange").split(":");
                    m = new Date().getFullYear();
                    n = function(a) {
                        var b = a.match(/c[+\-].*/) ? c + parseInt(a.substring(1), 10) : a.match(/[+\-].*/) ? m + parseInt(a, 10) : parseInt(a, 10);
                        return isNaN(b) ? m : b;
                    };
                    o = n(l[0]);
                    p = Math.max(o, n(l[1] || ""));
                    o = d ? Math.max(o, d.getFullYear()) : o;
                    p = e ? Math.min(p, e.getFullYear()) : p;
                    a.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>";
                    for (;o <= p; o++) {
                        a.yearshtml += "<option value='" + o + "'" + (o === c ? " selected='selected'" : "") + ">" + o + "</option>";
                    }
                    a.yearshtml += "</select>";
                    t += a.yearshtml;
                    a.yearshtml = null;
                }
            }
            t += this._get(a, "yearSuffix");
            if (s) {
                t += (f || !(q && r) ? "&#xa0;" : "") + u;
            }
            t += "</div>";
            // Close datepicker_header
            return t;
        },
        /* Adjust one of the date sub-fields. */
        _adjustInstDate: function(a, b, c) {
            var d = a.drawYear + (c === "Y" ? b : 0), e = a.drawMonth + (c === "M" ? b : 0), f = Math.min(a.selectedDay, this._getDaysInMonth(d, e)) + (c === "D" ? b : 0), g = this._restrictMinMax(a, this._daylightSavingAdjust(new Date(d, e, f)));
            a.selectedDay = g.getDate();
            a.drawMonth = a.selectedMonth = g.getMonth();
            a.drawYear = a.selectedYear = g.getFullYear();
            if (c === "M" || c === "Y") {
                this._notifyChange(a);
            }
        },
        /* Ensure a date is within any min/max bounds. */
        _restrictMinMax: function(a, b) {
            var c = this._getMinMaxDate(a, "min"), d = this._getMinMaxDate(a, "max"), e = c && b < c ? c : b;
            return d && e > d ? d : e;
        },
        /* Notify change of month/year. */
        _notifyChange: function(a) {
            var b = this._get(a, "onChangeMonthYear");
            if (b) {
                b.apply(a.input ? a.input[0] : null, [ a.selectedYear, a.selectedMonth + 1, a ]);
            }
        },
        /* Determine the number of months to show. */
        _getNumberOfMonths: function(a) {
            var b = this._get(a, "numberOfMonths");
            return b == null ? [ 1, 1 ] : typeof b === "number" ? [ 1, b ] : b;
        },
        /* Determine the current maximum date - ensure no time components are set. */
        _getMinMaxDate: function(a, b) {
            return this._determineDate(a, this._get(a, b + "Date"), null);
        },
        /* Find the number of days in a given month. */
        _getDaysInMonth: function(a, b) {
            return 32 - this._daylightSavingAdjust(new Date(a, b, 32)).getDate();
        },
        /* Find the day of the week of the first of a month. */
        _getFirstDayOfMonth: function(a, b) {
            return new Date(a, b, 1).getDay();
        },
        /* Determines if we should allow a "next/prev" month display change. */
        _canAdjustMonth: function(a, b, c, d) {
            var e = this._getNumberOfMonths(a), f = this._daylightSavingAdjust(new Date(c, d + (b < 0 ? b : e[0] * e[1]), 1));
            if (b < 0) {
                f.setDate(this._getDaysInMonth(f.getFullYear(), f.getMonth()));
            }
            return this._isInRange(a, f);
        },
        /* Is the given date in the accepted range? */
        _isInRange: function(a, b) {
            var c, d, e = this._getMinMaxDate(a, "min"), f = this._getMinMaxDate(a, "max"), g = null, h = null, i = this._get(a, "yearRange");
            if (i) {
                c = i.split(":");
                d = new Date().getFullYear();
                g = parseInt(c[0], 10);
                h = parseInt(c[1], 10);
                if (c[0].match(/[+\-].*/)) {
                    g += d;
                }
                if (c[1].match(/[+\-].*/)) {
                    h += d;
                }
            }
            return (!e || b.getTime() >= e.getTime()) && (!f || b.getTime() <= f.getTime()) && (!g || b.getFullYear() >= g) && (!h || b.getFullYear() <= h);
        },
        /* Provide the configuration settings for formatting/parsing. */
        _getFormatConfig: function(a) {
            var b = this._get(a, "shortYearCutoff");
            b = typeof b !== "string" ? b : new Date().getFullYear() % 100 + parseInt(b, 10);
            return {
                shortYearCutoff: b,
                dayNamesShort: this._get(a, "dayNamesShort"),
                dayNames: this._get(a, "dayNames"),
                monthNamesShort: this._get(a, "monthNamesShort"),
                monthNames: this._get(a, "monthNames")
            };
        },
        /* Format the given date for display. */
        _formatDate: function(a, b, c, d) {
            if (!b) {
                a.currentDay = a.selectedDay;
                a.currentMonth = a.selectedMonth;
                a.currentYear = a.selectedYear;
            }
            var e = b ? typeof b === "object" ? b : this._daylightSavingAdjust(new Date(d, c, b)) : this._daylightSavingAdjust(new Date(a.currentYear, a.currentMonth, a.currentDay));
            return this.formatDate(this._get(a, "dateFormat"), e, this._getFormatConfig(a));
        }
    });
    /*
 * Bind hover events for datepicker elements.
 * Done via delegate so the binding only occurs once in the lifetime of the parent div.
 * Global datepicker_instActive, set by _updateDatepicker allows the handlers to find their way back to the active picker.
 */
    function v(b) {
        var c = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
        return b.delegate(c, "mouseout", function() {
            a(this).removeClass("ui-state-hover");
            if (this.className.indexOf("ui-datepicker-prev") !== -1) {
                a(this).removeClass("ui-datepicker-prev-hover");
            }
            if (this.className.indexOf("ui-datepicker-next") !== -1) {
                a(this).removeClass("ui-datepicker-next-hover");
            }
        }).delegate(c, "mouseover", w);
    }
    function w() {
        if (!a.datepicker._isDisabledDatepicker(s.inline ? s.dpDiv.parent()[0] : s.input[0])) {
            a(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover");
            a(this).addClass("ui-state-hover");
            if (this.className.indexOf("ui-datepicker-prev") !== -1) {
                a(this).addClass("ui-datepicker-prev-hover");
            }
            if (this.className.indexOf("ui-datepicker-next") !== -1) {
                a(this).addClass("ui-datepicker-next-hover");
            }
        }
    }
    /* jQuery extend now ignores nulls! */
    function x(b, c) {
        a.extend(b, c);
        for (var d in c) {
            if (c[d] == null) {
                b[d] = c[d];
            }
        }
        return b;
    }
    /* Invoke the datepicker functionality.
   @param  options  string - a command, optionally followed by additional parameters or
					Object - settings for attaching new datepicker functionality
   @return  jQuery object */
    a.fn.datepicker = function(b) {
        /* Verify an empty collection wasn't passed - Fixes #6976 */
        if (!this.length) {
            return this;
        }
        /* Initialise the date picker. */
        if (!a.datepicker.initialized) {
            a(document).mousedown(a.datepicker._checkExternalClick);
            a.datepicker.initialized = true;
        }
        /* Append datepicker main container to body if not exist. */
        if (a("#" + a.datepicker._mainDivId).length === 0) {
            a("body").append(a.datepicker.dpDiv);
        }
        var c = Array.prototype.slice.call(arguments, 1);
        if (typeof b === "string" && (b === "isDisabled" || b === "getDate" || b === "widget")) {
            return a.datepicker["_" + b + "Datepicker"].apply(a.datepicker, [ this[0] ].concat(c));
        }
        if (b === "option" && arguments.length === 2 && typeof arguments[1] === "string") {
            return a.datepicker["_" + b + "Datepicker"].apply(a.datepicker, [ this[0] ].concat(c));
        }
        return this.each(function() {
            typeof b === "string" ? a.datepicker["_" + b + "Datepicker"].apply(a.datepicker, [ this ].concat(c)) : a.datepicker._attachDatepicker(this, b);
        });
    };
    a.datepicker = new u();
    // singleton instance
    a.datepicker.initialized = false;
    a.datepicker.uuid = new Date().getTime();
    a.datepicker.version = "1.11.4";
    var y = a.datepicker;
    /*!
 * jQuery UI Draggable 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/draggable/
 */
    a.widget("ui.draggable", a.ui.mouse, {
        version: "1.11.4",
        widgetEventPrefix: "drag",
        options: {
            addClasses: true,
            appendTo: "parent",
            axis: false,
            connectToSortable: false,
            containment: false,
            cursor: "auto",
            cursorAt: false,
            grid: false,
            handle: false,
            helper: "original",
            iframeFix: false,
            opacity: false,
            refreshPositions: false,
            revert: false,
            revertDuration: 500,
            scope: "default",
            scroll: true,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            snap: false,
            snapMode: "both",
            snapTolerance: 20,
            stack: false,
            zIndex: false,
            // callbacks
            drag: null,
            start: null,
            stop: null
        },
        _create: function() {
            if (this.options.helper === "original") {
                this._setPositionRelative();
            }
            if (this.options.addClasses) {
                this.element.addClass("ui-draggable");
            }
            if (this.options.disabled) {
                this.element.addClass("ui-draggable-disabled");
            }
            this._setHandleClassName();
            this._mouseInit();
        },
        _setOption: function(a, b) {
            this._super(a, b);
            if (a === "handle") {
                this._removeHandleClassName();
                this._setHandleClassName();
            }
        },
        _destroy: function() {
            if ((this.helper || this.element).is(".ui-draggable-dragging")) {
                this.destroyOnClear = true;
                return;
            }
            this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled");
            this._removeHandleClassName();
            this._mouseDestroy();
        },
        _mouseCapture: function(b) {
            var c = this.options;
            this._blurActiveElement(b);
            // among others, prevent a drag on a resizable-handle
            if (this.helper || c.disabled || a(b.target).closest(".ui-resizable-handle").length > 0) {
                return false;
            }
            //Quit if we're not on a valid handle
            this.handle = this._getHandle(b);
            if (!this.handle) {
                return false;
            }
            this._blockFrames(c.iframeFix === true ? "iframe" : c.iframeFix);
            return true;
        },
        _blockFrames: function(b) {
            this.iframeBlocks = this.document.find(b).map(function() {
                var b = a(this);
                return a("<div>").css("position", "absolute").appendTo(b.parent()).outerWidth(b.outerWidth()).outerHeight(b.outerHeight()).offset(b.offset())[0];
            });
        },
        _unblockFrames: function() {
            if (this.iframeBlocks) {
                this.iframeBlocks.remove();
                delete this.iframeBlocks;
            }
        },
        _blurActiveElement: function(b) {
            var c = this.document[0];
            // Only need to blur if the event occurred on the draggable itself, see #10527
            if (!this.handleElement.is(b.target)) {
                return;
            }
            // support: IE9
            // IE9 throws an "Unspecified error" accessing document.activeElement from an <iframe>
            try {
                // Support: IE9, IE10
                // If the <body> is blurred, IE will switch windows, see #9520
                if (c.activeElement && c.activeElement.nodeName.toLowerCase() !== "body") {
                    // Blur any element that currently has focus, see #4261
                    a(c.activeElement).blur();
                }
            } catch (d) {}
        },
        _mouseStart: function(b) {
            var c = this.options;
            //Create and append the visible helper
            this.helper = this._createHelper(b);
            this.helper.addClass("ui-draggable-dragging");
            //Cache the helper size
            this._cacheHelperProportions();
            //If ddmanager is used for droppables, set the global draggable
            if (a.ui.ddmanager) {
                a.ui.ddmanager.current = this;
            }
            /*
		 * - Position generation -
		 * This block generates everything position related - it's the core of draggables.
		 */
            //Cache the margins of the original element
            this._cacheMargins();
            //Store the helper's css position
            this.cssPosition = this.helper.css("position");
            this.scrollParent = this.helper.scrollParent(true);
            this.offsetParent = this.helper.offsetParent();
            this.hasFixedAncestor = this.helper.parents().filter(function() {
                return a(this).css("position") === "fixed";
            }).length > 0;
            //The element's absolute position on the page minus margins
            this.positionAbs = this.element.offset();
            this._refreshOffsets(b);
            //Generate the original position
            this.originalPosition = this.position = this._generatePosition(b, false);
            this.originalPageX = b.pageX;
            this.originalPageY = b.pageY;
            //Adjust the mouse offset relative to the helper if "cursorAt" is supplied
            c.cursorAt && this._adjustOffsetFromHelper(c.cursorAt);
            //Set a containment if given in the options
            this._setContainment();
            //Trigger event + callbacks
            if (this._trigger("start", b) === false) {
                this._clear();
                return false;
            }
            //Recache the helper size
            this._cacheHelperProportions();
            //Prepare the droppable offsets
            if (a.ui.ddmanager && !c.dropBehaviour) {
                a.ui.ddmanager.prepareOffsets(this, b);
            }
            // Reset helper's right/bottom css if they're set and set explicit width/height instead
            // as this prevents resizing of elements with right/bottom set (see #7772)
            this._normalizeRightBottom();
            this._mouseDrag(b, true);
            //Execute the drag once - this causes the helper not to be visible before getting its correct position
            //If the ddmanager is used for droppables, inform the manager that dragging has started (see #5003)
            if (a.ui.ddmanager) {
                a.ui.ddmanager.dragStart(this, b);
            }
            return true;
        },
        _refreshOffsets: function(a) {
            this.offset = {
                top: this.positionAbs.top - this.margins.top,
                left: this.positionAbs.left - this.margins.left,
                scroll: false,
                parent: this._getParentOffset(),
                relative: this._getRelativeOffset()
            };
            this.offset.click = {
                left: a.pageX - this.offset.left,
                top: a.pageY - this.offset.top
            };
        },
        _mouseDrag: function(b, c) {
            // reset any necessary cached properties (see #5009)
            if (this.hasFixedAncestor) {
                this.offset.parent = this._getParentOffset();
            }
            //Compute the helpers position
            this.position = this._generatePosition(b, true);
            this.positionAbs = this._convertPositionTo("absolute");
            //Call plugins and callbacks and use the resulting position if something is returned
            if (!c) {
                var d = this._uiHash();
                if (this._trigger("drag", b, d) === false) {
                    this._mouseUp({});
                    return false;
                }
                this.position = d.position;
            }
            this.helper[0].style.left = this.position.left + "px";
            this.helper[0].style.top = this.position.top + "px";
            if (a.ui.ddmanager) {
                a.ui.ddmanager.drag(this, b);
            }
            return false;
        },
        _mouseStop: function(b) {
            //If we are using droppables, inform the manager about the drop
            var c = this, d = false;
            if (a.ui.ddmanager && !this.options.dropBehaviour) {
                d = a.ui.ddmanager.drop(this, b);
            }
            //if a drop comes from outside (a sortable)
            if (this.dropped) {
                d = this.dropped;
                this.dropped = false;
            }
            if (this.options.revert === "invalid" && !d || this.options.revert === "valid" && d || this.options.revert === true || a.isFunction(this.options.revert) && this.options.revert.call(this.element, d)) {
                a(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function() {
                    if (c._trigger("stop", b) !== false) {
                        c._clear();
                    }
                });
            } else {
                if (this._trigger("stop", b) !== false) {
                    this._clear();
                }
            }
            return false;
        },
        _mouseUp: function(b) {
            this._unblockFrames();
            //If the ddmanager is used for droppables, inform the manager that dragging has stopped (see #5003)
            if (a.ui.ddmanager) {
                a.ui.ddmanager.dragStop(this, b);
            }
            // Only need to focus if the event occurred on the draggable itself, see #10527
            if (this.handleElement.is(b.target)) {
                // The interaction is over; whether or not the click resulted in a drag, focus the element
                this.element.focus();
            }
            return a.ui.mouse.prototype._mouseUp.call(this, b);
        },
        cancel: function() {
            if (this.helper.is(".ui-draggable-dragging")) {
                this._mouseUp({});
            } else {
                this._clear();
            }
            return this;
        },
        _getHandle: function(b) {
            return this.options.handle ? !!a(b.target).closest(this.element.find(this.options.handle)).length : true;
        },
        _setHandleClassName: function() {
            this.handleElement = this.options.handle ? this.element.find(this.options.handle) : this.element;
            this.handleElement.addClass("ui-draggable-handle");
        },
        _removeHandleClassName: function() {
            this.handleElement.removeClass("ui-draggable-handle");
        },
        _createHelper: function(b) {
            var c = this.options, d = a.isFunction(c.helper), e = d ? a(c.helper.apply(this.element[0], [ b ])) : c.helper === "clone" ? this.element.clone().removeAttr("id") : this.element;
            if (!e.parents("body").length) {
                e.appendTo(c.appendTo === "parent" ? this.element[0].parentNode : c.appendTo);
            }
            // http://bugs.jqueryui.com/ticket/9446
            // a helper function can return the original element
            // which wouldn't have been set to relative in _create
            if (d && e[0] === this.element[0]) {
                this._setPositionRelative();
            }
            if (e[0] !== this.element[0] && !/(fixed|absolute)/.test(e.css("position"))) {
                e.css("position", "absolute");
            }
            return e;
        },
        _setPositionRelative: function() {
            if (!/^(?:r|a|f)/.test(this.element.css("position"))) {
                this.element[0].style.position = "relative";
            }
        },
        _adjustOffsetFromHelper: function(b) {
            if (typeof b === "string") {
                b = b.split(" ");
            }
            if (a.isArray(b)) {
                b = {
                    left: +b[0],
                    top: +b[1] || 0
                };
            }
            if ("left" in b) {
                this.offset.click.left = b.left + this.margins.left;
            }
            if ("right" in b) {
                this.offset.click.left = this.helperProportions.width - b.right + this.margins.left;
            }
            if ("top" in b) {
                this.offset.click.top = b.top + this.margins.top;
            }
            if ("bottom" in b) {
                this.offset.click.top = this.helperProportions.height - b.bottom + this.margins.top;
            }
        },
        _isRootNode: function(a) {
            return /(html|body)/i.test(a.tagName) || a === this.document[0];
        },
        _getParentOffset: function() {
            //Get the offsetParent and cache its position
            var b = this.offsetParent.offset(), c = this.document[0];
            // This is a special case where we need to modify a offset calculated on start, since the following happened:
            // 1. The position of the helper is absolute, so it's position is calculated based on the next positioned parent
            // 2. The actual offset parent is a child of the scroll parent, and the scroll parent isn't the document, which means that
            //    the scroll is included in the initial calculation of the offset of the parent, and never recalculated upon drag
            if (this.cssPosition === "absolute" && this.scrollParent[0] !== c && a.contains(this.scrollParent[0], this.offsetParent[0])) {
                b.left += this.scrollParent.scrollLeft();
                b.top += this.scrollParent.scrollTop();
            }
            if (this._isRootNode(this.offsetParent[0])) {
                b = {
                    top: 0,
                    left: 0
                };
            }
            return {
                top: b.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: b.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            };
        },
        _getRelativeOffset: function() {
            if (this.cssPosition !== "relative") {
                return {
                    top: 0,
                    left: 0
                };
            }
            var a = this.element.position(), b = this._isRootNode(this.scrollParent[0]);
            return {
                top: a.top - (parseInt(this.helper.css("top"), 10) || 0) + (!b ? this.scrollParent.scrollTop() : 0),
                left: a.left - (parseInt(this.helper.css("left"), 10) || 0) + (!b ? this.scrollParent.scrollLeft() : 0)
            };
        },
        _cacheMargins: function() {
            this.margins = {
                left: parseInt(this.element.css("marginLeft"), 10) || 0,
                top: parseInt(this.element.css("marginTop"), 10) || 0,
                right: parseInt(this.element.css("marginRight"), 10) || 0,
                bottom: parseInt(this.element.css("marginBottom"), 10) || 0
            };
        },
        _cacheHelperProportions: function() {
            this.helperProportions = {
                width: this.helper.outerWidth(),
                height: this.helper.outerHeight()
            };
        },
        _setContainment: function() {
            var b, c, d, e = this.options, f = this.document[0];
            this.relativeContainer = null;
            if (!e.containment) {
                this.containment = null;
                return;
            }
            if (e.containment === "window") {
                this.containment = [ a(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, a(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, a(window).scrollLeft() + a(window).width() - this.helperProportions.width - this.margins.left, a(window).scrollTop() + (a(window).height() || f.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top ];
                return;
            }
            if (e.containment === "document") {
                this.containment = [ 0, 0, a(f).width() - this.helperProportions.width - this.margins.left, (a(f).height() || f.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top ];
                return;
            }
            if (e.containment.constructor === Array) {
                this.containment = e.containment;
                return;
            }
            if (e.containment === "parent") {
                e.containment = this.helper[0].parentNode;
            }
            c = a(e.containment);
            d = c[0];
            if (!d) {
                return;
            }
            b = /(scroll|auto)/.test(c.css("overflow"));
            this.containment = [ (parseInt(c.css("borderLeftWidth"), 10) || 0) + (parseInt(c.css("paddingLeft"), 10) || 0), (parseInt(c.css("borderTopWidth"), 10) || 0) + (parseInt(c.css("paddingTop"), 10) || 0), (b ? Math.max(d.scrollWidth, d.offsetWidth) : d.offsetWidth) - (parseInt(c.css("borderRightWidth"), 10) || 0) - (parseInt(c.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (b ? Math.max(d.scrollHeight, d.offsetHeight) : d.offsetHeight) - (parseInt(c.css("borderBottomWidth"), 10) || 0) - (parseInt(c.css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom ];
            this.relativeContainer = c;
        },
        _convertPositionTo: function(a, b) {
            if (!b) {
                b = this.position;
            }
            var c = a === "absolute" ? 1 : -1, d = this._isRootNode(this.scrollParent[0]);
            return {
                top: b.top + // The absolute mouse position
                this.offset.relative.top * c + // Only for relative positioned nodes: Relative offset from element to offset parent
                this.offset.parent.top * c - // The offsetParent's offset without borders (offset + border)
                (this.cssPosition === "fixed" ? -this.offset.scroll.top : d ? 0 : this.offset.scroll.top) * c,
                left: b.left + // The absolute mouse position
                this.offset.relative.left * c + // Only for relative positioned nodes: Relative offset from element to offset parent
                this.offset.parent.left * c - // The offsetParent's offset without borders (offset + border)
                (this.cssPosition === "fixed" ? -this.offset.scroll.left : d ? 0 : this.offset.scroll.left) * c
            };
        },
        _generatePosition: function(a, b) {
            var c, d, e, f, g = this.options, h = this._isRootNode(this.scrollParent[0]), i = a.pageX, j = a.pageY;
            // Cache the scroll
            if (!h || !this.offset.scroll) {
                this.offset.scroll = {
                    top: this.scrollParent.scrollTop(),
                    left: this.scrollParent.scrollLeft()
                };
            }
            /*
		 * - Position constraining -
		 * Constrain the position to a mix of grid, containment.
		 */
            // If we are not dragging yet, we won't check for options
            if (b) {
                if (this.containment) {
                    if (this.relativeContainer) {
                        d = this.relativeContainer.offset();
                        c = [ this.containment[0] + d.left, this.containment[1] + d.top, this.containment[2] + d.left, this.containment[3] + d.top ];
                    } else {
                        c = this.containment;
                    }
                    if (a.pageX - this.offset.click.left < c[0]) {
                        i = c[0] + this.offset.click.left;
                    }
                    if (a.pageY - this.offset.click.top < c[1]) {
                        j = c[1] + this.offset.click.top;
                    }
                    if (a.pageX - this.offset.click.left > c[2]) {
                        i = c[2] + this.offset.click.left;
                    }
                    if (a.pageY - this.offset.click.top > c[3]) {
                        j = c[3] + this.offset.click.top;
                    }
                }
                if (g.grid) {
                    //Check for grid elements set to 0 to prevent divide by 0 error causing invalid argument errors in IE (see ticket #6950)
                    e = g.grid[1] ? this.originalPageY + Math.round((j - this.originalPageY) / g.grid[1]) * g.grid[1] : this.originalPageY;
                    j = c ? e - this.offset.click.top >= c[1] || e - this.offset.click.top > c[3] ? e : e - this.offset.click.top >= c[1] ? e - g.grid[1] : e + g.grid[1] : e;
                    f = g.grid[0] ? this.originalPageX + Math.round((i - this.originalPageX) / g.grid[0]) * g.grid[0] : this.originalPageX;
                    i = c ? f - this.offset.click.left >= c[0] || f - this.offset.click.left > c[2] ? f : f - this.offset.click.left >= c[0] ? f - g.grid[0] : f + g.grid[0] : f;
                }
                if (g.axis === "y") {
                    i = this.originalPageX;
                }
                if (g.axis === "x") {
                    j = this.originalPageY;
                }
            }
            return {
                top: j - // The absolute mouse position
                this.offset.click.top - // Click offset (relative to the element)
                this.offset.relative.top - // Only for relative positioned nodes: Relative offset from element to offset parent
                this.offset.parent.top + (// The offsetParent's offset without borders (offset + border)
                this.cssPosition === "fixed" ? -this.offset.scroll.top : h ? 0 : this.offset.scroll.top),
                left: i - // The absolute mouse position
                this.offset.click.left - // Click offset (relative to the element)
                this.offset.relative.left - // Only for relative positioned nodes: Relative offset from element to offset parent
                this.offset.parent.left + (// The offsetParent's offset without borders (offset + border)
                this.cssPosition === "fixed" ? -this.offset.scroll.left : h ? 0 : this.offset.scroll.left)
            };
        },
        _clear: function() {
            this.helper.removeClass("ui-draggable-dragging");
            if (this.helper[0] !== this.element[0] && !this.cancelHelperRemoval) {
                this.helper.remove();
            }
            this.helper = null;
            this.cancelHelperRemoval = false;
            if (this.destroyOnClear) {
                this.destroy();
            }
        },
        _normalizeRightBottom: function() {
            if (this.options.axis !== "y" && this.helper.css("right") !== "auto") {
                this.helper.width(this.helper.width());
                this.helper.css("right", "auto");
            }
            if (this.options.axis !== "x" && this.helper.css("bottom") !== "auto") {
                this.helper.height(this.helper.height());
                this.helper.css("bottom", "auto");
            }
        },
        // From now on bulk stuff - mainly helpers
        _trigger: function(b, c, d) {
            d = d || this._uiHash();
            a.ui.plugin.call(this, b, [ c, d, this ], true);
            // Absolute position and offset (see #6884 ) have to be recalculated after plugins
            if (/^(drag|start|stop)/.test(b)) {
                this.positionAbs = this._convertPositionTo("absolute");
                d.offset = this.positionAbs;
            }
            return a.Widget.prototype._trigger.call(this, b, c, d);
        },
        plugins: {},
        _uiHash: function() {
            return {
                helper: this.helper,
                position: this.position,
                originalPosition: this.originalPosition,
                offset: this.positionAbs
            };
        }
    });
    a.ui.plugin.add("draggable", "connectToSortable", {
        start: function(b, c, d) {
            var e = a.extend({}, c, {
                item: d.element
            });
            d.sortables = [];
            a(d.options.connectToSortable).each(function() {
                var c = a(this).sortable("instance");
                if (c && !c.options.disabled) {
                    d.sortables.push(c);
                    // refreshPositions is called at drag start to refresh the containerCache
                    // which is used in drag. This ensures it's initialized and synchronized
                    // with any changes that might have happened on the page since initialization.
                    c.refreshPositions();
                    c._trigger("activate", b, e);
                }
            });
        },
        stop: function(b, c, d) {
            var e = a.extend({}, c, {
                item: d.element
            });
            d.cancelHelperRemoval = false;
            a.each(d.sortables, function() {
                var a = this;
                if (a.isOver) {
                    a.isOver = 0;
                    // Allow this sortable to handle removing the helper
                    d.cancelHelperRemoval = true;
                    a.cancelHelperRemoval = false;
                    // Use _storedCSS To restore properties in the sortable,
                    // as this also handles revert (#9675) since the draggable
                    // may have modified them in unexpected ways (#8809)
                    a._storedCSS = {
                        position: a.placeholder.css("position"),
                        top: a.placeholder.css("top"),
                        left: a.placeholder.css("left")
                    };
                    a._mouseStop(b);
                    // Once drag has ended, the sortable should return to using
                    // its original helper, not the shared helper from draggable
                    a.options.helper = a.options._helper;
                } else {
                    // Prevent this Sortable from removing the helper.
                    // However, don't set the draggable to remove the helper
                    // either as another connected Sortable may yet handle the removal.
                    a.cancelHelperRemoval = true;
                    a._trigger("deactivate", b, e);
                }
            });
        },
        drag: function(b, c, d) {
            a.each(d.sortables, function() {
                var e = false, f = this;
                // Copy over variables that sortable's _intersectsWith uses
                f.positionAbs = d.positionAbs;
                f.helperProportions = d.helperProportions;
                f.offset.click = d.offset.click;
                if (f._intersectsWith(f.containerCache)) {
                    e = true;
                    a.each(d.sortables, function() {
                        // Copy over variables that sortable's _intersectsWith uses
                        this.positionAbs = d.positionAbs;
                        this.helperProportions = d.helperProportions;
                        this.offset.click = d.offset.click;
                        if (this !== f && this._intersectsWith(this.containerCache) && a.contains(f.element[0], this.element[0])) {
                            e = false;
                        }
                        return e;
                    });
                }
                if (e) {
                    // If it intersects, we use a little isOver variable and set it once,
                    // so that the move-in stuff gets fired only once.
                    if (!f.isOver) {
                        f.isOver = 1;
                        // Store draggable's parent in case we need to reappend to it later.
                        d._parent = c.helper.parent();
                        f.currentItem = c.helper.appendTo(f.element).data("ui-sortable-item", true);
                        // Store helper option to later restore it
                        f.options._helper = f.options.helper;
                        f.options.helper = function() {
                            return c.helper[0];
                        };
                        // Fire the start events of the sortable with our passed browser event,
                        // and our own helper (so it doesn't create a new one)
                        b.target = f.currentItem[0];
                        f._mouseCapture(b, true);
                        f._mouseStart(b, true, true);
                        // Because the browser event is way off the new appended portlet,
                        // modify necessary variables to reflect the changes
                        f.offset.click.top = d.offset.click.top;
                        f.offset.click.left = d.offset.click.left;
                        f.offset.parent.left -= d.offset.parent.left - f.offset.parent.left;
                        f.offset.parent.top -= d.offset.parent.top - f.offset.parent.top;
                        d._trigger("toSortable", b);
                        // Inform draggable that the helper is in a valid drop zone,
                        // used solely in the revert option to handle "valid/invalid".
                        d.dropped = f.element;
                        // Need to refreshPositions of all sortables in the case that
                        // adding to one sortable changes the location of the other sortables (#9675)
                        a.each(d.sortables, function() {
                            this.refreshPositions();
                        });
                        // hack so receive/update callbacks work (mostly)
                        d.currentItem = d.element;
                        f.fromOutside = d;
                    }
                    if (f.currentItem) {
                        f._mouseDrag(b);
                        // Copy the sortable's position because the draggable's can potentially reflect
                        // a relative position, while sortable is always absolute, which the dragged
                        // element has now become. (#8809)
                        c.position = f.position;
                    }
                } else {
                    // If it doesn't intersect with the sortable, and it intersected before,
                    // we fake the drag stop of the sortable, but make sure it doesn't remove
                    // the helper by using cancelHelperRemoval.
                    if (f.isOver) {
                        f.isOver = 0;
                        f.cancelHelperRemoval = true;
                        // Calling sortable's mouseStop would trigger a revert,
                        // so revert must be temporarily false until after mouseStop is called.
                        f.options._revert = f.options.revert;
                        f.options.revert = false;
                        f._trigger("out", b, f._uiHash(f));
                        f._mouseStop(b, true);
                        // restore sortable behaviors that were modfied
                        // when the draggable entered the sortable area (#9481)
                        f.options.revert = f.options._revert;
                        f.options.helper = f.options._helper;
                        if (f.placeholder) {
                            f.placeholder.remove();
                        }
                        // Restore and recalculate the draggable's offset considering the sortable
                        // may have modified them in unexpected ways. (#8809, #10669)
                        c.helper.appendTo(d._parent);
                        d._refreshOffsets(b);
                        c.position = d._generatePosition(b, true);
                        d._trigger("fromSortable", b);
                        // Inform draggable that the helper is no longer in a valid drop zone
                        d.dropped = false;
                        // Need to refreshPositions of all sortables just in case removing
                        // from one sortable changes the location of other sortables (#9675)
                        a.each(d.sortables, function() {
                            this.refreshPositions();
                        });
                    }
                }
            });
        }
    });
    a.ui.plugin.add("draggable", "cursor", {
        start: function(b, c, d) {
            var e = a("body"), f = d.options;
            if (e.css("cursor")) {
                f._cursor = e.css("cursor");
            }
            e.css("cursor", f.cursor);
        },
        stop: function(b, c, d) {
            var e = d.options;
            if (e._cursor) {
                a("body").css("cursor", e._cursor);
            }
        }
    });
    a.ui.plugin.add("draggable", "opacity", {
        start: function(b, c, d) {
            var e = a(c.helper), f = d.options;
            if (e.css("opacity")) {
                f._opacity = e.css("opacity");
            }
            e.css("opacity", f.opacity);
        },
        stop: function(b, c, d) {
            var e = d.options;
            if (e._opacity) {
                a(c.helper).css("opacity", e._opacity);
            }
        }
    });
    a.ui.plugin.add("draggable", "scroll", {
        start: function(a, b, c) {
            if (!c.scrollParentNotHidden) {
                c.scrollParentNotHidden = c.helper.scrollParent(false);
            }
            if (c.scrollParentNotHidden[0] !== c.document[0] && c.scrollParentNotHidden[0].tagName !== "HTML") {
                c.overflowOffset = c.scrollParentNotHidden.offset();
            }
        },
        drag: function(b, c, d) {
            var e = d.options, f = false, g = d.scrollParentNotHidden[0], h = d.document[0];
            if (g !== h && g.tagName !== "HTML") {
                if (!e.axis || e.axis !== "x") {
                    if (d.overflowOffset.top + g.offsetHeight - b.pageY < e.scrollSensitivity) {
                        g.scrollTop = f = g.scrollTop + e.scrollSpeed;
                    } else if (b.pageY - d.overflowOffset.top < e.scrollSensitivity) {
                        g.scrollTop = f = g.scrollTop - e.scrollSpeed;
                    }
                }
                if (!e.axis || e.axis !== "y") {
                    if (d.overflowOffset.left + g.offsetWidth - b.pageX < e.scrollSensitivity) {
                        g.scrollLeft = f = g.scrollLeft + e.scrollSpeed;
                    } else if (b.pageX - d.overflowOffset.left < e.scrollSensitivity) {
                        g.scrollLeft = f = g.scrollLeft - e.scrollSpeed;
                    }
                }
            } else {
                if (!e.axis || e.axis !== "x") {
                    if (b.pageY - a(h).scrollTop() < e.scrollSensitivity) {
                        f = a(h).scrollTop(a(h).scrollTop() - e.scrollSpeed);
                    } else if (a(window).height() - (b.pageY - a(h).scrollTop()) < e.scrollSensitivity) {
                        f = a(h).scrollTop(a(h).scrollTop() + e.scrollSpeed);
                    }
                }
                if (!e.axis || e.axis !== "y") {
                    if (b.pageX - a(h).scrollLeft() < e.scrollSensitivity) {
                        f = a(h).scrollLeft(a(h).scrollLeft() - e.scrollSpeed);
                    } else if (a(window).width() - (b.pageX - a(h).scrollLeft()) < e.scrollSensitivity) {
                        f = a(h).scrollLeft(a(h).scrollLeft() + e.scrollSpeed);
                    }
                }
            }
            if (f !== false && a.ui.ddmanager && !e.dropBehaviour) {
                a.ui.ddmanager.prepareOffsets(d, b);
            }
        }
    });
    a.ui.plugin.add("draggable", "snap", {
        start: function(b, c, d) {
            var e = d.options;
            d.snapElements = [];
            a(e.snap.constructor !== String ? e.snap.items || ":data(ui-draggable)" : e.snap).each(function() {
                var b = a(this), c = b.offset();
                if (this !== d.element[0]) {
                    d.snapElements.push({
                        item: this,
                        width: b.outerWidth(),
                        height: b.outerHeight(),
                        top: c.top,
                        left: c.left
                    });
                }
            });
        },
        drag: function(b, c, d) {
            var e, f, g, h, i, j, k, l, m, n, o = d.options, p = o.snapTolerance, q = c.offset.left, r = q + d.helperProportions.width, s = c.offset.top, t = s + d.helperProportions.height;
            for (m = d.snapElements.length - 1; m >= 0; m--) {
                i = d.snapElements[m].left - d.margins.left;
                j = i + d.snapElements[m].width;
                k = d.snapElements[m].top - d.margins.top;
                l = k + d.snapElements[m].height;
                if (r < i - p || q > j + p || t < k - p || s > l + p || !a.contains(d.snapElements[m].item.ownerDocument, d.snapElements[m].item)) {
                    if (d.snapElements[m].snapping) {
                        d.options.snap.release && d.options.snap.release.call(d.element, b, a.extend(d._uiHash(), {
                            snapItem: d.snapElements[m].item
                        }));
                    }
                    d.snapElements[m].snapping = false;
                    continue;
                }
                if (o.snapMode !== "inner") {
                    e = Math.abs(k - t) <= p;
                    f = Math.abs(l - s) <= p;
                    g = Math.abs(i - r) <= p;
                    h = Math.abs(j - q) <= p;
                    if (e) {
                        c.position.top = d._convertPositionTo("relative", {
                            top: k - d.helperProportions.height,
                            left: 0
                        }).top;
                    }
                    if (f) {
                        c.position.top = d._convertPositionTo("relative", {
                            top: l,
                            left: 0
                        }).top;
                    }
                    if (g) {
                        c.position.left = d._convertPositionTo("relative", {
                            top: 0,
                            left: i - d.helperProportions.width
                        }).left;
                    }
                    if (h) {
                        c.position.left = d._convertPositionTo("relative", {
                            top: 0,
                            left: j
                        }).left;
                    }
                }
                n = e || f || g || h;
                if (o.snapMode !== "outer") {
                    e = Math.abs(k - s) <= p;
                    f = Math.abs(l - t) <= p;
                    g = Math.abs(i - q) <= p;
                    h = Math.abs(j - r) <= p;
                    if (e) {
                        c.position.top = d._convertPositionTo("relative", {
                            top: k,
                            left: 0
                        }).top;
                    }
                    if (f) {
                        c.position.top = d._convertPositionTo("relative", {
                            top: l - d.helperProportions.height,
                            left: 0
                        }).top;
                    }
                    if (g) {
                        c.position.left = d._convertPositionTo("relative", {
                            top: 0,
                            left: i
                        }).left;
                    }
                    if (h) {
                        c.position.left = d._convertPositionTo("relative", {
                            top: 0,
                            left: j - d.helperProportions.width
                        }).left;
                    }
                }
                if (!d.snapElements[m].snapping && (e || f || g || h || n)) {
                    d.options.snap.snap && d.options.snap.snap.call(d.element, b, a.extend(d._uiHash(), {
                        snapItem: d.snapElements[m].item
                    }));
                }
                d.snapElements[m].snapping = e || f || g || h || n;
            }
        }
    });
    a.ui.plugin.add("draggable", "stack", {
        start: function(b, c, d) {
            var e, f = d.options, g = a.makeArray(a(f.stack)).sort(function(b, c) {
                return (parseInt(a(b).css("zIndex"), 10) || 0) - (parseInt(a(c).css("zIndex"), 10) || 0);
            });
            if (!g.length) {
                return;
            }
            e = parseInt(a(g[0]).css("zIndex"), 10) || 0;
            a(g).each(function(b) {
                a(this).css("zIndex", e + b);
            });
            this.css("zIndex", e + g.length);
        }
    });
    a.ui.plugin.add("draggable", "zIndex", {
        start: function(b, c, d) {
            var e = a(c.helper), f = d.options;
            if (e.css("zIndex")) {
                f._zIndex = e.css("zIndex");
            }
            e.css("zIndex", f.zIndex);
        },
        stop: function(b, c, d) {
            var e = d.options;
            if (e._zIndex) {
                a(c.helper).css("zIndex", e._zIndex);
            }
        }
    });
    var z = a.ui.draggable;
    /*!
 * jQuery UI Resizable 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/resizable/
 */
    a.widget("ui.resizable", a.ui.mouse, {
        version: "1.11.4",
        widgetEventPrefix: "resize",
        options: {
            alsoResize: false,
            animate: false,
            animateDuration: "slow",
            animateEasing: "swing",
            aspectRatio: false,
            autoHide: false,
            containment: false,
            ghost: false,
            grid: false,
            handles: "e,s,se",
            helper: false,
            maxHeight: null,
            maxWidth: null,
            minHeight: 10,
            minWidth: 10,
            // See #7960
            zIndex: 90,
            // callbacks
            resize: null,
            start: null,
            stop: null
        },
        _num: function(a) {
            return parseInt(a, 10) || 0;
        },
        _isNumber: function(a) {
            return !isNaN(parseInt(a, 10));
        },
        _hasScroll: function(b, c) {
            if (a(b).css("overflow") === "hidden") {
                return false;
            }
            var d = c && c === "left" ? "scrollLeft" : "scrollTop", e = false;
            if (b[d] > 0) {
                return true;
            }
            // TODO: determine which cases actually cause this to happen
            // if the element doesn't have the scroll set, see if it's possible to
            // set the scroll
            b[d] = 1;
            e = b[d] > 0;
            b[d] = 0;
            return e;
        },
        _create: function() {
            var b, c, d, e, f, g = this, h = this.options;
            this.element.addClass("ui-resizable");
            a.extend(this, {
                _aspectRatio: !!h.aspectRatio,
                aspectRatio: h.aspectRatio,
                originalElement: this.element,
                _proportionallyResizeElements: [],
                _helper: h.helper || h.ghost || h.animate ? h.helper || "ui-resizable-helper" : null
            });
            // Wrap the element if it cannot hold child nodes
            if (this.element[0].nodeName.match(/^(canvas|textarea|input|select|button|img)$/i)) {
                this.element.wrap(a("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({
                    position: this.element.css("position"),
                    width: this.element.outerWidth(),
                    height: this.element.outerHeight(),
                    top: this.element.css("top"),
                    left: this.element.css("left")
                }));
                this.element = this.element.parent().data("ui-resizable", this.element.resizable("instance"));
                this.elementIsWrapper = true;
                this.element.css({
                    marginLeft: this.originalElement.css("marginLeft"),
                    marginTop: this.originalElement.css("marginTop"),
                    marginRight: this.originalElement.css("marginRight"),
                    marginBottom: this.originalElement.css("marginBottom")
                });
                this.originalElement.css({
                    marginLeft: 0,
                    marginTop: 0,
                    marginRight: 0,
                    marginBottom: 0
                });
                // support: Safari
                // Prevent Safari textarea resize
                this.originalResizeStyle = this.originalElement.css("resize");
                this.originalElement.css("resize", "none");
                this._proportionallyResizeElements.push(this.originalElement.css({
                    position: "static",
                    zoom: 1,
                    display: "block"
                }));
                // support: IE9
                // avoid IE jump (hard set the margin)
                this.originalElement.css({
                    margin: this.originalElement.css("margin")
                });
                this._proportionallyResize();
            }
            this.handles = h.handles || (!a(".ui-resizable-handle", this.element).length ? "e,s,se" : {
                n: ".ui-resizable-n",
                e: ".ui-resizable-e",
                s: ".ui-resizable-s",
                w: ".ui-resizable-w",
                se: ".ui-resizable-se",
                sw: ".ui-resizable-sw",
                ne: ".ui-resizable-ne",
                nw: ".ui-resizable-nw"
            });
            this._handles = a();
            if (this.handles.constructor === String) {
                if (this.handles === "all") {
                    this.handles = "n,e,s,w,se,sw,ne,nw";
                }
                b = this.handles.split(",");
                this.handles = {};
                for (c = 0; c < b.length; c++) {
                    d = a.trim(b[c]);
                    f = "ui-resizable-" + d;
                    e = a("<div class='ui-resizable-handle " + f + "'></div>");
                    e.css({
                        zIndex: h.zIndex
                    });
                    // TODO : What's going on here?
                    if ("se" === d) {
                        e.addClass("ui-icon ui-icon-gripsmall-diagonal-se");
                    }
                    this.handles[d] = ".ui-resizable-" + d;
                    this.element.append(e);
                }
            }
            this._renderAxis = function(b) {
                var c, d, e, f;
                b = b || this.element;
                for (c in this.handles) {
                    if (this.handles[c].constructor === String) {
                        this.handles[c] = this.element.children(this.handles[c]).first().show();
                    } else if (this.handles[c].jquery || this.handles[c].nodeType) {
                        this.handles[c] = a(this.handles[c]);
                        this._on(this.handles[c], {
                            mousedown: g._mouseDown
                        });
                    }
                    if (this.elementIsWrapper && this.originalElement[0].nodeName.match(/^(textarea|input|select|button)$/i)) {
                        d = a(this.handles[c], this.element);
                        f = /sw|ne|nw|se|n|s/.test(c) ? d.outerHeight() : d.outerWidth();
                        e = [ "padding", /ne|nw|n/.test(c) ? "Top" : /se|sw|s/.test(c) ? "Bottom" : /^e$/.test(c) ? "Right" : "Left" ].join("");
                        b.css(e, f);
                        this._proportionallyResize();
                    }
                    this._handles = this._handles.add(this.handles[c]);
                }
            };
            // TODO: make renderAxis a prototype function
            this._renderAxis(this.element);
            this._handles = this._handles.add(this.element.find(".ui-resizable-handle"));
            this._handles.disableSelection();
            this._handles.mouseover(function() {
                if (!g.resizing) {
                    if (this.className) {
                        e = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i);
                    }
                    g.axis = e && e[1] ? e[1] : "se";
                }
            });
            if (h.autoHide) {
                this._handles.hide();
                a(this.element).addClass("ui-resizable-autohide").mouseenter(function() {
                    if (h.disabled) {
                        return;
                    }
                    a(this).removeClass("ui-resizable-autohide");
                    g._handles.show();
                }).mouseleave(function() {
                    if (h.disabled) {
                        return;
                    }
                    if (!g.resizing) {
                        a(this).addClass("ui-resizable-autohide");
                        g._handles.hide();
                    }
                });
            }
            this._mouseInit();
        },
        _destroy: function() {
            this._mouseDestroy();
            var b, c = function(b) {
                a(b).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove();
            };
            // TODO: Unwrap at same DOM position
            if (this.elementIsWrapper) {
                c(this.element);
                b = this.element;
                this.originalElement.css({
                    position: b.css("position"),
                    width: b.outerWidth(),
                    height: b.outerHeight(),
                    top: b.css("top"),
                    left: b.css("left")
                }).insertAfter(b);
                b.remove();
            }
            this.originalElement.css("resize", this.originalResizeStyle);
            c(this.originalElement);
            return this;
        },
        _mouseCapture: function(b) {
            var c, d, e = false;
            for (c in this.handles) {
                d = a(this.handles[c])[0];
                if (d === b.target || a.contains(d, b.target)) {
                    e = true;
                }
            }
            return !this.options.disabled && e;
        },
        _mouseStart: function(b) {
            var c, d, e, f = this.options, g = this.element;
            this.resizing = true;
            this._renderProxy();
            c = this._num(this.helper.css("left"));
            d = this._num(this.helper.css("top"));
            if (f.containment) {
                c += a(f.containment).scrollLeft() || 0;
                d += a(f.containment).scrollTop() || 0;
            }
            this.offset = this.helper.offset();
            this.position = {
                left: c,
                top: d
            };
            this.size = this._helper ? {
                width: this.helper.width(),
                height: this.helper.height()
            } : {
                width: g.width(),
                height: g.height()
            };
            this.originalSize = this._helper ? {
                width: g.outerWidth(),
                height: g.outerHeight()
            } : {
                width: g.width(),
                height: g.height()
            };
            this.sizeDiff = {
                width: g.outerWidth() - g.width(),
                height: g.outerHeight() - g.height()
            };
            this.originalPosition = {
                left: c,
                top: d
            };
            this.originalMousePosition = {
                left: b.pageX,
                top: b.pageY
            };
            this.aspectRatio = typeof f.aspectRatio === "number" ? f.aspectRatio : this.originalSize.width / this.originalSize.height || 1;
            e = a(".ui-resizable-" + this.axis).css("cursor");
            a("body").css("cursor", e === "auto" ? this.axis + "-resize" : e);
            g.addClass("ui-resizable-resizing");
            this._propagate("start", b);
            return true;
        },
        _mouseDrag: function(b) {
            var c, d, e = this.originalMousePosition, f = this.axis, g = b.pageX - e.left || 0, h = b.pageY - e.top || 0, i = this._change[f];
            this._updatePrevProperties();
            if (!i) {
                return false;
            }
            c = i.apply(this, [ b, g, h ]);
            this._updateVirtualBoundaries(b.shiftKey);
            if (this._aspectRatio || b.shiftKey) {
                c = this._updateRatio(c, b);
            }
            c = this._respectSize(c, b);
            this._updateCache(c);
            this._propagate("resize", b);
            d = this._applyChanges();
            if (!this._helper && this._proportionallyResizeElements.length) {
                this._proportionallyResize();
            }
            if (!a.isEmptyObject(d)) {
                this._updatePrevProperties();
                this._trigger("resize", b, this.ui());
                this._applyChanges();
            }
            return false;
        },
        _mouseStop: function(b) {
            this.resizing = false;
            var c, d, e, f, g, h, i, j = this.options, k = this;
            if (this._helper) {
                c = this._proportionallyResizeElements;
                d = c.length && /textarea/i.test(c[0].nodeName);
                e = d && this._hasScroll(c[0], "left") ? 0 : k.sizeDiff.height;
                f = d ? 0 : k.sizeDiff.width;
                g = {
                    width: k.helper.width() - f,
                    height: k.helper.height() - e
                };
                h = parseInt(k.element.css("left"), 10) + (k.position.left - k.originalPosition.left) || null;
                i = parseInt(k.element.css("top"), 10) + (k.position.top - k.originalPosition.top) || null;
                if (!j.animate) {
                    this.element.css(a.extend(g, {
                        top: i,
                        left: h
                    }));
                }
                k.helper.height(k.size.height);
                k.helper.width(k.size.width);
                if (this._helper && !j.animate) {
                    this._proportionallyResize();
                }
            }
            a("body").css("cursor", "auto");
            this.element.removeClass("ui-resizable-resizing");
            this._propagate("stop", b);
            if (this._helper) {
                this.helper.remove();
            }
            return false;
        },
        _updatePrevProperties: function() {
            this.prevPosition = {
                top: this.position.top,
                left: this.position.left
            };
            this.prevSize = {
                width: this.size.width,
                height: this.size.height
            };
        },
        _applyChanges: function() {
            var a = {};
            if (this.position.top !== this.prevPosition.top) {
                a.top = this.position.top + "px";
            }
            if (this.position.left !== this.prevPosition.left) {
                a.left = this.position.left + "px";
            }
            if (this.size.width !== this.prevSize.width) {
                a.width = this.size.width + "px";
            }
            if (this.size.height !== this.prevSize.height) {
                a.height = this.size.height + "px";
            }
            this.helper.css(a);
            return a;
        },
        _updateVirtualBoundaries: function(a) {
            var b, c, d, e, f, g = this.options;
            f = {
                minWidth: this._isNumber(g.minWidth) ? g.minWidth : 0,
                maxWidth: this._isNumber(g.maxWidth) ? g.maxWidth : Infinity,
                minHeight: this._isNumber(g.minHeight) ? g.minHeight : 0,
                maxHeight: this._isNumber(g.maxHeight) ? g.maxHeight : Infinity
            };
            if (this._aspectRatio || a) {
                b = f.minHeight * this.aspectRatio;
                d = f.minWidth / this.aspectRatio;
                c = f.maxHeight * this.aspectRatio;
                e = f.maxWidth / this.aspectRatio;
                if (b > f.minWidth) {
                    f.minWidth = b;
                }
                if (d > f.minHeight) {
                    f.minHeight = d;
                }
                if (c < f.maxWidth) {
                    f.maxWidth = c;
                }
                if (e < f.maxHeight) {
                    f.maxHeight = e;
                }
            }
            this._vBoundaries = f;
        },
        _updateCache: function(a) {
            this.offset = this.helper.offset();
            if (this._isNumber(a.left)) {
                this.position.left = a.left;
            }
            if (this._isNumber(a.top)) {
                this.position.top = a.top;
            }
            if (this._isNumber(a.height)) {
                this.size.height = a.height;
            }
            if (this._isNumber(a.width)) {
                this.size.width = a.width;
            }
        },
        _updateRatio: function(a) {
            var b = this.position, c = this.size, d = this.axis;
            if (this._isNumber(a.height)) {
                a.width = a.height * this.aspectRatio;
            } else if (this._isNumber(a.width)) {
                a.height = a.width / this.aspectRatio;
            }
            if (d === "sw") {
                a.left = b.left + (c.width - a.width);
                a.top = null;
            }
            if (d === "nw") {
                a.top = b.top + (c.height - a.height);
                a.left = b.left + (c.width - a.width);
            }
            return a;
        },
        _respectSize: function(a) {
            var b = this._vBoundaries, c = this.axis, d = this._isNumber(a.width) && b.maxWidth && b.maxWidth < a.width, e = this._isNumber(a.height) && b.maxHeight && b.maxHeight < a.height, f = this._isNumber(a.width) && b.minWidth && b.minWidth > a.width, g = this._isNumber(a.height) && b.minHeight && b.minHeight > a.height, h = this.originalPosition.left + this.originalSize.width, i = this.position.top + this.size.height, j = /sw|nw|w/.test(c), k = /nw|ne|n/.test(c);
            if (f) {
                a.width = b.minWidth;
            }
            if (g) {
                a.height = b.minHeight;
            }
            if (d) {
                a.width = b.maxWidth;
            }
            if (e) {
                a.height = b.maxHeight;
            }
            if (f && j) {
                a.left = h - b.minWidth;
            }
            if (d && j) {
                a.left = h - b.maxWidth;
            }
            if (g && k) {
                a.top = i - b.minHeight;
            }
            if (e && k) {
                a.top = i - b.maxHeight;
            }
            // Fixing jump error on top/left - bug #2330
            if (!a.width && !a.height && !a.left && a.top) {
                a.top = null;
            } else if (!a.width && !a.height && !a.top && a.left) {
                a.left = null;
            }
            return a;
        },
        _getPaddingPlusBorderDimensions: function(a) {
            var b = 0, c = [], d = [ a.css("borderTopWidth"), a.css("borderRightWidth"), a.css("borderBottomWidth"), a.css("borderLeftWidth") ], e = [ a.css("paddingTop"), a.css("paddingRight"), a.css("paddingBottom"), a.css("paddingLeft") ];
            for (;b < 4; b++) {
                c[b] = parseInt(d[b], 10) || 0;
                c[b] += parseInt(e[b], 10) || 0;
            }
            return {
                height: c[0] + c[2],
                width: c[1] + c[3]
            };
        },
        _proportionallyResize: function() {
            if (!this._proportionallyResizeElements.length) {
                return;
            }
            var a, b = 0, c = this.helper || this.element;
            for (;b < this._proportionallyResizeElements.length; b++) {
                a = this._proportionallyResizeElements[b];
                // TODO: Seems like a bug to cache this.outerDimensions
                // considering that we are in a loop.
                if (!this.outerDimensions) {
                    this.outerDimensions = this._getPaddingPlusBorderDimensions(a);
                }
                a.css({
                    height: c.height() - this.outerDimensions.height || 0,
                    width: c.width() - this.outerDimensions.width || 0
                });
            }
        },
        _renderProxy: function() {
            var b = this.element, c = this.options;
            this.elementOffset = b.offset();
            if (this._helper) {
                this.helper = this.helper || a("<div style='overflow:hidden;'></div>");
                this.helper.addClass(this._helper).css({
                    width: this.element.outerWidth() - 1,
                    height: this.element.outerHeight() - 1,
                    position: "absolute",
                    left: this.elementOffset.left + "px",
                    top: this.elementOffset.top + "px",
                    zIndex: ++c.zIndex
                });
                this.helper.appendTo("body").disableSelection();
            } else {
                this.helper = this.element;
            }
        },
        _change: {
            e: function(a, b) {
                return {
                    width: this.originalSize.width + b
                };
            },
            w: function(a, b) {
                var c = this.originalSize, d = this.originalPosition;
                return {
                    left: d.left + b,
                    width: c.width - b
                };
            },
            n: function(a, b, c) {
                var d = this.originalSize, e = this.originalPosition;
                return {
                    top: e.top + c,
                    height: d.height - c
                };
            },
            s: function(a, b, c) {
                return {
                    height: this.originalSize.height + c
                };
            },
            se: function(b, c, d) {
                return a.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [ b, c, d ]));
            },
            sw: function(b, c, d) {
                return a.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [ b, c, d ]));
            },
            ne: function(b, c, d) {
                return a.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [ b, c, d ]));
            },
            nw: function(b, c, d) {
                return a.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [ b, c, d ]));
            }
        },
        _propagate: function(b, c) {
            a.ui.plugin.call(this, b, [ c, this.ui() ]);
            b !== "resize" && this._trigger(b, c, this.ui());
        },
        plugins: {},
        ui: function() {
            return {
                originalElement: this.originalElement,
                element: this.element,
                helper: this.helper,
                position: this.position,
                size: this.size,
                originalSize: this.originalSize,
                originalPosition: this.originalPosition
            };
        }
    });
    /*
 * Resizable Extensions
 */
    a.ui.plugin.add("resizable", "animate", {
        stop: function(b) {
            var c = a(this).resizable("instance"), d = c.options, e = c._proportionallyResizeElements, f = e.length && /textarea/i.test(e[0].nodeName), g = f && c._hasScroll(e[0], "left") ? 0 : c.sizeDiff.height, h = f ? 0 : c.sizeDiff.width, i = {
                width: c.size.width - h,
                height: c.size.height - g
            }, j = parseInt(c.element.css("left"), 10) + (c.position.left - c.originalPosition.left) || null, k = parseInt(c.element.css("top"), 10) + (c.position.top - c.originalPosition.top) || null;
            c.element.animate(a.extend(i, k && j ? {
                top: k,
                left: j
            } : {}), {
                duration: d.animateDuration,
                easing: d.animateEasing,
                step: function() {
                    var d = {
                        width: parseInt(c.element.css("width"), 10),
                        height: parseInt(c.element.css("height"), 10),
                        top: parseInt(c.element.css("top"), 10),
                        left: parseInt(c.element.css("left"), 10)
                    };
                    if (e && e.length) {
                        a(e[0]).css({
                            width: d.width,
                            height: d.height
                        });
                    }
                    // propagating resize, and updating values for each animation step
                    c._updateCache(d);
                    c._propagate("resize", b);
                }
            });
        }
    });
    a.ui.plugin.add("resizable", "containment", {
        start: function() {
            var b, c, d, e, f, g, h, i = a(this).resizable("instance"), j = i.options, k = i.element, l = j.containment, m = l instanceof a ? l.get(0) : /parent/.test(l) ? k.parent().get(0) : l;
            if (!m) {
                return;
            }
            i.containerElement = a(m);
            if (/document/.test(l) || l === document) {
                i.containerOffset = {
                    left: 0,
                    top: 0
                };
                i.containerPosition = {
                    left: 0,
                    top: 0
                };
                i.parentData = {
                    element: a(document),
                    left: 0,
                    top: 0,
                    width: a(document).width(),
                    height: a(document).height() || document.body.parentNode.scrollHeight
                };
            } else {
                b = a(m);
                c = [];
                a([ "Top", "Right", "Left", "Bottom" ]).each(function(a, d) {
                    c[a] = i._num(b.css("padding" + d));
                });
                i.containerOffset = b.offset();
                i.containerPosition = b.position();
                i.containerSize = {
                    height: b.innerHeight() - c[3],
                    width: b.innerWidth() - c[1]
                };
                d = i.containerOffset;
                e = i.containerSize.height;
                f = i.containerSize.width;
                g = i._hasScroll(m, "left") ? m.scrollWidth : f;
                h = i._hasScroll(m) ? m.scrollHeight : e;
                i.parentData = {
                    element: m,
                    left: d.left,
                    top: d.top,
                    width: g,
                    height: h
                };
            }
        },
        resize: function(b) {
            var c, d, e, f, g = a(this).resizable("instance"), h = g.options, i = g.containerOffset, j = g.position, k = g._aspectRatio || b.shiftKey, l = {
                top: 0,
                left: 0
            }, m = g.containerElement, n = true;
            if (m[0] !== document && /static/.test(m.css("position"))) {
                l = i;
            }
            if (j.left < (g._helper ? i.left : 0)) {
                g.size.width = g.size.width + (g._helper ? g.position.left - i.left : g.position.left - l.left);
                if (k) {
                    g.size.height = g.size.width / g.aspectRatio;
                    n = false;
                }
                g.position.left = h.helper ? i.left : 0;
            }
            if (j.top < (g._helper ? i.top : 0)) {
                g.size.height = g.size.height + (g._helper ? g.position.top - i.top : g.position.top);
                if (k) {
                    g.size.width = g.size.height * g.aspectRatio;
                    n = false;
                }
                g.position.top = g._helper ? i.top : 0;
            }
            e = g.containerElement.get(0) === g.element.parent().get(0);
            f = /relative|absolute/.test(g.containerElement.css("position"));
            if (e && f) {
                g.offset.left = g.parentData.left + g.position.left;
                g.offset.top = g.parentData.top + g.position.top;
            } else {
                g.offset.left = g.element.offset().left;
                g.offset.top = g.element.offset().top;
            }
            c = Math.abs(g.sizeDiff.width + (g._helper ? g.offset.left - l.left : g.offset.left - i.left));
            d = Math.abs(g.sizeDiff.height + (g._helper ? g.offset.top - l.top : g.offset.top - i.top));
            if (c + g.size.width >= g.parentData.width) {
                g.size.width = g.parentData.width - c;
                if (k) {
                    g.size.height = g.size.width / g.aspectRatio;
                    n = false;
                }
            }
            if (d + g.size.height >= g.parentData.height) {
                g.size.height = g.parentData.height - d;
                if (k) {
                    g.size.width = g.size.height * g.aspectRatio;
                    n = false;
                }
            }
            if (!n) {
                g.position.left = g.prevPosition.left;
                g.position.top = g.prevPosition.top;
                g.size.width = g.prevSize.width;
                g.size.height = g.prevSize.height;
            }
        },
        stop: function() {
            var b = a(this).resizable("instance"), c = b.options, d = b.containerOffset, e = b.containerPosition, f = b.containerElement, g = a(b.helper), h = g.offset(), i = g.outerWidth() - b.sizeDiff.width, j = g.outerHeight() - b.sizeDiff.height;
            if (b._helper && !c.animate && /relative/.test(f.css("position"))) {
                a(this).css({
                    left: h.left - e.left - d.left,
                    width: i,
                    height: j
                });
            }
            if (b._helper && !c.animate && /static/.test(f.css("position"))) {
                a(this).css({
                    left: h.left - e.left - d.left,
                    width: i,
                    height: j
                });
            }
        }
    });
    a.ui.plugin.add("resizable", "alsoResize", {
        start: function() {
            var b = a(this).resizable("instance"), c = b.options;
            a(c.alsoResize).each(function() {
                var b = a(this);
                b.data("ui-resizable-alsoresize", {
                    width: parseInt(b.width(), 10),
                    height: parseInt(b.height(), 10),
                    left: parseInt(b.css("left"), 10),
                    top: parseInt(b.css("top"), 10)
                });
            });
        },
        resize: function(b, c) {
            var d = a(this).resizable("instance"), e = d.options, f = d.originalSize, g = d.originalPosition, h = {
                height: d.size.height - f.height || 0,
                width: d.size.width - f.width || 0,
                top: d.position.top - g.top || 0,
                left: d.position.left - g.left || 0
            };
            a(e.alsoResize).each(function() {
                var b = a(this), d = a(this).data("ui-resizable-alsoresize"), e = {}, f = b.parents(c.originalElement[0]).length ? [ "width", "height" ] : [ "width", "height", "top", "left" ];
                a.each(f, function(a, b) {
                    var c = (d[b] || 0) + (h[b] || 0);
                    if (c && c >= 0) {
                        e[b] = c || null;
                    }
                });
                b.css(e);
            });
        },
        stop: function() {
            a(this).removeData("resizable-alsoresize");
        }
    });
    a.ui.plugin.add("resizable", "ghost", {
        start: function() {
            var b = a(this).resizable("instance"), c = b.options, d = b.size;
            b.ghost = b.originalElement.clone();
            b.ghost.css({
                opacity: .25,
                display: "block",
                position: "relative",
                height: d.height,
                width: d.width,
                margin: 0,
                left: 0,
                top: 0
            }).addClass("ui-resizable-ghost").addClass(typeof c.ghost === "string" ? c.ghost : "");
            b.ghost.appendTo(b.helper);
        },
        resize: function() {
            var b = a(this).resizable("instance");
            if (b.ghost) {
                b.ghost.css({
                    position: "relative",
                    height: b.size.height,
                    width: b.size.width
                });
            }
        },
        stop: function() {
            var b = a(this).resizable("instance");
            if (b.ghost && b.helper) {
                b.helper.get(0).removeChild(b.ghost.get(0));
            }
        }
    });
    a.ui.plugin.add("resizable", "grid", {
        resize: function() {
            var b, c = a(this).resizable("instance"), d = c.options, e = c.size, f = c.originalSize, g = c.originalPosition, h = c.axis, i = typeof d.grid === "number" ? [ d.grid, d.grid ] : d.grid, j = i[0] || 1, k = i[1] || 1, l = Math.round((e.width - f.width) / j) * j, m = Math.round((e.height - f.height) / k) * k, n = f.width + l, o = f.height + m, p = d.maxWidth && d.maxWidth < n, q = d.maxHeight && d.maxHeight < o, r = d.minWidth && d.minWidth > n, s = d.minHeight && d.minHeight > o;
            d.grid = i;
            if (r) {
                n += j;
            }
            if (s) {
                o += k;
            }
            if (p) {
                n -= j;
            }
            if (q) {
                o -= k;
            }
            if (/^(se|s|e)$/.test(h)) {
                c.size.width = n;
                c.size.height = o;
            } else if (/^(ne)$/.test(h)) {
                c.size.width = n;
                c.size.height = o;
                c.position.top = g.top - m;
            } else if (/^(sw)$/.test(h)) {
                c.size.width = n;
                c.size.height = o;
                c.position.left = g.left - l;
            } else {
                if (o - k <= 0 || n - j <= 0) {
                    b = c._getPaddingPlusBorderDimensions(this);
                }
                if (o - k > 0) {
                    c.size.height = o;
                    c.position.top = g.top - m;
                } else {
                    o = k - b.height;
                    c.size.height = o;
                    c.position.top = g.top + f.height - o;
                }
                if (n - j > 0) {
                    c.size.width = n;
                    c.position.left = g.left - l;
                } else {
                    n = j - b.width;
                    c.size.width = n;
                    c.position.left = g.left + f.width - n;
                }
            }
        }
    });
    var A = a.ui.resizable;
    /*!
 * jQuery UI Dialog 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/dialog/
 */
    var B = a.widget("ui.dialog", {
        version: "1.11.4",
        options: {
            appendTo: "body",
            autoOpen: true,
            buttons: [],
            closeOnEscape: true,
            closeText: "Close",
            dialogClass: "",
            draggable: true,
            hide: null,
            height: "auto",
            maxHeight: null,
            maxWidth: null,
            minHeight: 150,
            minWidth: 150,
            modal: false,
            position: {
                my: "center",
                at: "center",
                of: window,
                collision: "fit",
                // Ensure the titlebar is always visible
                using: function(b) {
                    var c = a(this).css(b).offset().top;
                    if (c < 0) {
                        a(this).css("top", b.top - c);
                    }
                }
            },
            resizable: true,
            show: null,
            title: null,
            width: 300,
            // callbacks
            beforeClose: null,
            close: null,
            drag: null,
            dragStart: null,
            dragStop: null,
            focus: null,
            open: null,
            resize: null,
            resizeStart: null,
            resizeStop: null
        },
        sizeRelatedOptions: {
            buttons: true,
            height: true,
            maxHeight: true,
            maxWidth: true,
            minHeight: true,
            minWidth: true,
            width: true
        },
        resizableRelatedOptions: {
            maxHeight: true,
            maxWidth: true,
            minHeight: true,
            minWidth: true
        },
        _create: function() {
            this.originalCss = {
                display: this.element[0].style.display,
                width: this.element[0].style.width,
                minHeight: this.element[0].style.minHeight,
                maxHeight: this.element[0].style.maxHeight,
                height: this.element[0].style.height
            };
            this.originalPosition = {
                parent: this.element.parent(),
                index: this.element.parent().children().index(this.element)
            };
            this.originalTitle = this.element.attr("title");
            this.options.title = this.options.title || this.originalTitle;
            this._createWrapper();
            this.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(this.uiDialog);
            this._createTitlebar();
            this._createButtonPane();
            if (this.options.draggable && a.fn.draggable) {
                this._makeDraggable();
            }
            if (this.options.resizable && a.fn.resizable) {
                this._makeResizable();
            }
            this._isOpen = false;
            this._trackFocus();
        },
        _init: function() {
            if (this.options.autoOpen) {
                this.open();
            }
        },
        _appendTo: function() {
            var b = this.options.appendTo;
            if (b && (b.jquery || b.nodeType)) {
                return a(b);
            }
            return this.document.find(b || "body").eq(0);
        },
        _destroy: function() {
            var a, b = this.originalPosition;
            this._untrackInstance();
            this._destroyOverlay();
            this.element.removeUniqueId().removeClass("ui-dialog-content ui-widget-content").css(this.originalCss).detach();
            this.uiDialog.stop(true, true).remove();
            if (this.originalTitle) {
                this.element.attr("title", this.originalTitle);
            }
            a = b.parent.children().eq(b.index);
            // Don't try to place the dialog next to itself (#8613)
            if (a.length && a[0] !== this.element[0]) {
                a.before(this.element);
            } else {
                b.parent.append(this.element);
            }
        },
        widget: function() {
            return this.uiDialog;
        },
        disable: a.noop,
        enable: a.noop,
        close: function(b) {
            var c, d = this;
            if (!this._isOpen || this._trigger("beforeClose", b) === false) {
                return;
            }
            this._isOpen = false;
            this._focusedElement = null;
            this._destroyOverlay();
            this._untrackInstance();
            if (!this.opener.filter(":focusable").focus().length) {
                // support: IE9
                // IE9 throws an "Unspecified error" accessing document.activeElement from an <iframe>
                try {
                    c = this.document[0].activeElement;
                    // Support: IE9, IE10
                    // If the <body> is blurred, IE will switch windows, see #4520
                    if (c && c.nodeName.toLowerCase() !== "body") {
                        // Hiding a focused element doesn't trigger blur in WebKit
                        // so in case we have nothing to focus on, explicitly blur the active element
                        // https://bugs.webkit.org/show_bug.cgi?id=47182
                        a(c).blur();
                    }
                } catch (e) {}
            }
            this._hide(this.uiDialog, this.options.hide, function() {
                d._trigger("close", b);
            });
        },
        isOpen: function() {
            return this._isOpen;
        },
        moveToTop: function() {
            this._moveToTop();
        },
        _moveToTop: function(b, c) {
            var d = false, e = this.uiDialog.siblings(".ui-front:visible").map(function() {
                return +a(this).css("z-index");
            }).get(), f = Math.max.apply(null, e);
            if (f >= +this.uiDialog.css("z-index")) {
                this.uiDialog.css("z-index", f + 1);
                d = true;
            }
            if (d && !c) {
                this._trigger("focus", b);
            }
            return d;
        },
        open: function() {
            var b = this;
            if (this._isOpen) {
                if (this._moveToTop()) {
                    this._focusTabbable();
                }
                return;
            }
            this._isOpen = true;
            this.opener = a(this.document[0].activeElement);
            this._size();
            this._position();
            this._createOverlay();
            this._moveToTop(null, true);
            // Ensure the overlay is moved to the top with the dialog, but only when
            // opening. The overlay shouldn't move after the dialog is open so that
            // modeless dialogs opened after the modal dialog stack properly.
            if (this.overlay) {
                this.overlay.css("z-index", this.uiDialog.css("z-index") - 1);
            }
            this._show(this.uiDialog, this.options.show, function() {
                b._focusTabbable();
                b._trigger("focus");
            });
            // Track the dialog immediately upon openening in case a focus event
            // somehow occurs outside of the dialog before an element inside the
            // dialog is focused (#10152)
            this._makeFocusTarget();
            this._trigger("open");
        },
        _focusTabbable: function() {
            // Set focus to the first match:
            // 1. An element that was focused previously
            // 2. First element inside the dialog matching [autofocus]
            // 3. Tabbable element inside the content element
            // 4. Tabbable element inside the buttonpane
            // 5. The close button
            // 6. The dialog itself
            var a = this._focusedElement;
            if (!a) {
                a = this.element.find("[autofocus]");
            }
            if (!a.length) {
                a = this.element.find(":tabbable");
            }
            if (!a.length) {
                a = this.uiDialogButtonPane.find(":tabbable");
            }
            if (!a.length) {
                a = this.uiDialogTitlebarClose.filter(":tabbable");
            }
            if (!a.length) {
                a = this.uiDialog;
            }
            a.eq(0).focus();
        },
        _keepFocus: function(b) {
            function c() {
                var b = this.document[0].activeElement, c = this.uiDialog[0] === b || a.contains(this.uiDialog[0], b);
                if (!c) {
                    this._focusTabbable();
                }
            }
            b.preventDefault();
            c.call(this);
            // support: IE
            // IE <= 8 doesn't prevent moving focus even with event.preventDefault()
            // so we check again later
            this._delay(c);
        },
        _createWrapper: function() {
            this.uiDialog = a("<div>").addClass("ui-dialog ui-widget ui-widget-content ui-corner-all ui-front " + this.options.dialogClass).hide().attr({
                // Setting tabIndex makes the div focusable
                tabIndex: -1,
                role: "dialog"
            }).appendTo(this._appendTo());
            this._on(this.uiDialog, {
                keydown: function(b) {
                    if (this.options.closeOnEscape && !b.isDefaultPrevented() && b.keyCode && b.keyCode === a.ui.keyCode.ESCAPE) {
                        b.preventDefault();
                        this.close(b);
                        return;
                    }
                    // prevent tabbing out of dialogs
                    if (b.keyCode !== a.ui.keyCode.TAB || b.isDefaultPrevented()) {
                        return;
                    }
                    var c = this.uiDialog.find(":tabbable"), d = c.filter(":first"), e = c.filter(":last");
                    if ((b.target === e[0] || b.target === this.uiDialog[0]) && !b.shiftKey) {
                        this._delay(function() {
                            d.focus();
                        });
                        b.preventDefault();
                    } else if ((b.target === d[0] || b.target === this.uiDialog[0]) && b.shiftKey) {
                        this._delay(function() {
                            e.focus();
                        });
                        b.preventDefault();
                    }
                },
                mousedown: function(a) {
                    if (this._moveToTop(a)) {
                        this._focusTabbable();
                    }
                }
            });
            // We assume that any existing aria-describedby attribute means
            // that the dialog content is marked up properly
            // otherwise we brute force the content as the description
            if (!this.element.find("[aria-describedby]").length) {
                this.uiDialog.attr({
                    "aria-describedby": this.element.uniqueId().attr("id")
                });
            }
        },
        _createTitlebar: function() {
            var b;
            this.uiDialogTitlebar = a("<div>").addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(this.uiDialog);
            this._on(this.uiDialogTitlebar, {
                mousedown: function(b) {
                    // Don't prevent click on close button (#8838)
                    // Focusing a dialog that is partially scrolled out of view
                    // causes the browser to scroll it into view, preventing the click event
                    if (!a(b.target).closest(".ui-dialog-titlebar-close")) {
                        // Dialog isn't getting focus when dragging (#8063)
                        this.uiDialog.focus();
                    }
                }
            });
            // support: IE
            // Use type="button" to prevent enter keypresses in textboxes from closing the
            // dialog in IE (#9312)
            this.uiDialogTitlebarClose = a("<button type='button'></button>").button({
                label: this.options.closeText,
                icons: {
                    primary: "ui-icon-closethick"
                },
                text: false
            }).addClass("ui-dialog-titlebar-close").appendTo(this.uiDialogTitlebar);
            this._on(this.uiDialogTitlebarClose, {
                click: function(a) {
                    a.preventDefault();
                    this.close(a);
                }
            });
            b = a("<span>").uniqueId().addClass("ui-dialog-title").prependTo(this.uiDialogTitlebar);
            this._title(b);
            this.uiDialog.attr({
                "aria-labelledby": b.attr("id")
            });
        },
        _title: function(a) {
            if (!this.options.title) {
                a.html("&#160;");
            }
            a.text(this.options.title);
        },
        _createButtonPane: function() {
            this.uiDialogButtonPane = a("<div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix");
            this.uiButtonSet = a("<div>").addClass("ui-dialog-buttonset").appendTo(this.uiDialogButtonPane);
            this._createButtons();
        },
        _createButtons: function() {
            var b = this, c = this.options.buttons;
            // if we already have a button pane, remove it
            this.uiDialogButtonPane.remove();
            this.uiButtonSet.empty();
            if (a.isEmptyObject(c) || a.isArray(c) && !c.length) {
                this.uiDialog.removeClass("ui-dialog-buttons");
                return;
            }
            a.each(c, function(c, d) {
                var e, f;
                d = a.isFunction(d) ? {
                    click: d,
                    text: c
                } : d;
                // Default to a non-submitting button
                d = a.extend({
                    type: "button"
                }, d);
                // Change the context for the click callback to be the main element
                e = d.click;
                d.click = function() {
                    e.apply(b.element[0], arguments);
                };
                f = {
                    icons: d.icons,
                    text: d.showText
                };
                delete d.icons;
                delete d.showText;
                a("<button></button>", d).button(f).appendTo(b.uiButtonSet);
            });
            this.uiDialog.addClass("ui-dialog-buttons");
            this.uiDialogButtonPane.appendTo(this.uiDialog);
        },
        _makeDraggable: function() {
            var b = this, c = this.options;
            function d(a) {
                return {
                    position: a.position,
                    offset: a.offset
                };
            }
            this.uiDialog.draggable({
                cancel: ".ui-dialog-content, .ui-dialog-titlebar-close",
                handle: ".ui-dialog-titlebar",
                containment: "document",
                start: function(c, e) {
                    a(this).addClass("ui-dialog-dragging");
                    b._blockFrames();
                    b._trigger("dragStart", c, d(e));
                },
                drag: function(a, c) {
                    b._trigger("drag", a, d(c));
                },
                stop: function(e, f) {
                    var g = f.offset.left - b.document.scrollLeft(), h = f.offset.top - b.document.scrollTop();
                    c.position = {
                        my: "left top",
                        at: "left" + (g >= 0 ? "+" : "") + g + " " + "top" + (h >= 0 ? "+" : "") + h,
                        of: b.window
                    };
                    a(this).removeClass("ui-dialog-dragging");
                    b._unblockFrames();
                    b._trigger("dragStop", e, d(f));
                }
            });
        },
        _makeResizable: function() {
            var b = this, c = this.options, d = c.resizable, // .ui-resizable has position: relative defined in the stylesheet
            // but dialogs have to use absolute or fixed positioning
            e = this.uiDialog.css("position"), f = typeof d === "string" ? d : "n,e,s,w,se,sw,ne,nw";
            function g(a) {
                return {
                    originalPosition: a.originalPosition,
                    originalSize: a.originalSize,
                    position: a.position,
                    size: a.size
                };
            }
            this.uiDialog.resizable({
                cancel: ".ui-dialog-content",
                containment: "document",
                alsoResize: this.element,
                maxWidth: c.maxWidth,
                maxHeight: c.maxHeight,
                minWidth: c.minWidth,
                minHeight: this._minHeight(),
                handles: f,
                start: function(c, d) {
                    a(this).addClass("ui-dialog-resizing");
                    b._blockFrames();
                    b._trigger("resizeStart", c, g(d));
                },
                resize: function(a, c) {
                    b._trigger("resize", a, g(c));
                },
                stop: function(d, e) {
                    var f = b.uiDialog.offset(), h = f.left - b.document.scrollLeft(), i = f.top - b.document.scrollTop();
                    c.height = b.uiDialog.height();
                    c.width = b.uiDialog.width();
                    c.position = {
                        my: "left top",
                        at: "left" + (h >= 0 ? "+" : "") + h + " " + "top" + (i >= 0 ? "+" : "") + i,
                        of: b.window
                    };
                    a(this).removeClass("ui-dialog-resizing");
                    b._unblockFrames();
                    b._trigger("resizeStop", d, g(e));
                }
            }).css("position", e);
        },
        _trackFocus: function() {
            this._on(this.widget(), {
                focusin: function(b) {
                    this._makeFocusTarget();
                    this._focusedElement = a(b.target);
                }
            });
        },
        _makeFocusTarget: function() {
            this._untrackInstance();
            this._trackingInstances().unshift(this);
        },
        _untrackInstance: function() {
            var b = this._trackingInstances(), c = a.inArray(this, b);
            if (c !== -1) {
                b.splice(c, 1);
            }
        },
        _trackingInstances: function() {
            var a = this.document.data("ui-dialog-instances");
            if (!a) {
                a = [];
                this.document.data("ui-dialog-instances", a);
            }
            return a;
        },
        _minHeight: function() {
            var a = this.options;
            return a.height === "auto" ? a.minHeight : Math.min(a.minHeight, a.height);
        },
        _position: function() {
            // Need to show the dialog to get the actual offset in the position plugin
            var a = this.uiDialog.is(":visible");
            if (!a) {
                this.uiDialog.show();
            }
            this.uiDialog.position(this.options.position);
            if (!a) {
                this.uiDialog.hide();
            }
        },
        _setOptions: function(b) {
            var c = this, d = false, e = {};
            a.each(b, function(a, b) {
                c._setOption(a, b);
                if (a in c.sizeRelatedOptions) {
                    d = true;
                }
                if (a in c.resizableRelatedOptions) {
                    e[a] = b;
                }
            });
            if (d) {
                this._size();
                this._position();
            }
            if (this.uiDialog.is(":data(ui-resizable)")) {
                this.uiDialog.resizable("option", e);
            }
        },
        _setOption: function(a, b) {
            var c, d, e = this.uiDialog;
            if (a === "dialogClass") {
                e.removeClass(this.options.dialogClass).addClass(b);
            }
            if (a === "disabled") {
                return;
            }
            this._super(a, b);
            if (a === "appendTo") {
                this.uiDialog.appendTo(this._appendTo());
            }
            if (a === "buttons") {
                this._createButtons();
            }
            if (a === "closeText") {
                this.uiDialogTitlebarClose.button({
                    // Ensure that we always pass a string
                    label: "" + b
                });
            }
            if (a === "draggable") {
                c = e.is(":data(ui-draggable)");
                if (c && !b) {
                    e.draggable("destroy");
                }
                if (!c && b) {
                    this._makeDraggable();
                }
            }
            if (a === "position") {
                this._position();
            }
            if (a === "resizable") {
                // currently resizable, becoming non-resizable
                d = e.is(":data(ui-resizable)");
                if (d && !b) {
                    e.resizable("destroy");
                }
                // currently resizable, changing handles
                if (d && typeof b === "string") {
                    e.resizable("option", "handles", b);
                }
                // currently non-resizable, becoming resizable
                if (!d && b !== false) {
                    this._makeResizable();
                }
            }
            if (a === "title") {
                this._title(this.uiDialogTitlebar.find(".ui-dialog-title"));
            }
        },
        _size: function() {
            // If the user has resized the dialog, the .ui-dialog and .ui-dialog-content
            // divs will both have width and height set, so we need to reset them
            var a, b, c, d = this.options;
            // Reset content sizing
            this.element.show().css({
                width: "auto",
                minHeight: 0,
                maxHeight: "none",
                height: 0
            });
            if (d.minWidth > d.width) {
                d.width = d.minWidth;
            }
            // reset wrapper sizing
            // determine the height of all the non-content elements
            a = this.uiDialog.css({
                height: "auto",
                width: d.width
            }).outerHeight();
            b = Math.max(0, d.minHeight - a);
            c = typeof d.maxHeight === "number" ? Math.max(0, d.maxHeight - a) : "none";
            if (d.height === "auto") {
                this.element.css({
                    minHeight: b,
                    maxHeight: c,
                    height: "auto"
                });
            } else {
                this.element.height(Math.max(0, d.height - a));
            }
            if (this.uiDialog.is(":data(ui-resizable)")) {
                this.uiDialog.resizable("option", "minHeight", this._minHeight());
            }
        },
        _blockFrames: function() {
            this.iframeBlocks = this.document.find("iframe").map(function() {
                var b = a(this);
                return a("<div>").css({
                    position: "absolute",
                    width: b.outerWidth(),
                    height: b.outerHeight()
                }).appendTo(b.parent()).offset(b.offset())[0];
            });
        },
        _unblockFrames: function() {
            if (this.iframeBlocks) {
                this.iframeBlocks.remove();
                delete this.iframeBlocks;
            }
        },
        _allowInteraction: function(b) {
            if (a(b.target).closest(".ui-dialog").length) {
                return true;
            }
            // TODO: Remove hack when datepicker implements
            // the .ui-front logic (#8989)
            return !!a(b.target).closest(".ui-datepicker").length;
        },
        _createOverlay: function() {
            if (!this.options.modal) {
                return;
            }
            // We use a delay in case the overlay is created from an
            // event that we're going to be cancelling (#2804)
            var b = true;
            this._delay(function() {
                b = false;
            });
            if (!this.document.data("ui-dialog-overlays")) {
                // Prevent use of anchors and inputs
                // Using _on() for an event handler shared across many instances is
                // safe because the dialogs stack and must be closed in reverse order
                this._on(this.document, {
                    focusin: function(a) {
                        if (b) {
                            return;
                        }
                        if (!this._allowInteraction(a)) {
                            a.preventDefault();
                            this._trackingInstances()[0]._focusTabbable();
                        }
                    }
                });
            }
            this.overlay = a("<div>").addClass("ui-widget-overlay ui-front").appendTo(this._appendTo());
            this._on(this.overlay, {
                mousedown: "_keepFocus"
            });
            this.document.data("ui-dialog-overlays", (this.document.data("ui-dialog-overlays") || 0) + 1);
        },
        _destroyOverlay: function() {
            if (!this.options.modal) {
                return;
            }
            if (this.overlay) {
                var a = this.document.data("ui-dialog-overlays") - 1;
                if (!a) {
                    this.document.unbind("focusin").removeData("ui-dialog-overlays");
                } else {
                    this.document.data("ui-dialog-overlays", a);
                }
                this.overlay.remove();
                this.overlay = null;
            }
        }
    });
    /*!
 * jQuery UI Droppable 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/droppable/
 */
    a.widget("ui.droppable", {
        version: "1.11.4",
        widgetEventPrefix: "drop",
        options: {
            accept: "*",
            activeClass: false,
            addClasses: true,
            greedy: false,
            hoverClass: false,
            scope: "default",
            tolerance: "intersect",
            // callbacks
            activate: null,
            deactivate: null,
            drop: null,
            out: null,
            over: null
        },
        _create: function() {
            var b, c = this.options, d = c.accept;
            this.isover = false;
            this.isout = true;
            this.accept = a.isFunction(d) ? d : function(a) {
                return a.is(d);
            };
            this.proportions = function() {
                if (arguments.length) {
                    // Store the droppable's proportions
                    b = arguments[0];
                } else {
                    // Retrieve or derive the droppable's proportions
                    return b ? b : b = {
                        width: this.element[0].offsetWidth,
                        height: this.element[0].offsetHeight
                    };
                }
            };
            this._addToManager(c.scope);
            c.addClasses && this.element.addClass("ui-droppable");
        },
        _addToManager: function(b) {
            // Add the reference and positions to the manager
            a.ui.ddmanager.droppables[b] = a.ui.ddmanager.droppables[b] || [];
            a.ui.ddmanager.droppables[b].push(this);
        },
        _splice: function(a) {
            var b = 0;
            for (;b < a.length; b++) {
                if (a[b] === this) {
                    a.splice(b, 1);
                }
            }
        },
        _destroy: function() {
            var b = a.ui.ddmanager.droppables[this.options.scope];
            this._splice(b);
            this.element.removeClass("ui-droppable ui-droppable-disabled");
        },
        _setOption: function(b, c) {
            if (b === "accept") {
                this.accept = a.isFunction(c) ? c : function(a) {
                    return a.is(c);
                };
            } else if (b === "scope") {
                var d = a.ui.ddmanager.droppables[this.options.scope];
                this._splice(d);
                this._addToManager(c);
            }
            this._super(b, c);
        },
        _activate: function(b) {
            var c = a.ui.ddmanager.current;
            if (this.options.activeClass) {
                this.element.addClass(this.options.activeClass);
            }
            if (c) {
                this._trigger("activate", b, this.ui(c));
            }
        },
        _deactivate: function(b) {
            var c = a.ui.ddmanager.current;
            if (this.options.activeClass) {
                this.element.removeClass(this.options.activeClass);
            }
            if (c) {
                this._trigger("deactivate", b, this.ui(c));
            }
        },
        _over: function(b) {
            var c = a.ui.ddmanager.current;
            // Bail if draggable and droppable are same element
            if (!c || (c.currentItem || c.element)[0] === this.element[0]) {
                return;
            }
            if (this.accept.call(this.element[0], c.currentItem || c.element)) {
                if (this.options.hoverClass) {
                    this.element.addClass(this.options.hoverClass);
                }
                this._trigger("over", b, this.ui(c));
            }
        },
        _out: function(b) {
            var c = a.ui.ddmanager.current;
            // Bail if draggable and droppable are same element
            if (!c || (c.currentItem || c.element)[0] === this.element[0]) {
                return;
            }
            if (this.accept.call(this.element[0], c.currentItem || c.element)) {
                if (this.options.hoverClass) {
                    this.element.removeClass(this.options.hoverClass);
                }
                this._trigger("out", b, this.ui(c));
            }
        },
        _drop: function(b, c) {
            var d = c || a.ui.ddmanager.current, e = false;
            // Bail if draggable and droppable are same element
            if (!d || (d.currentItem || d.element)[0] === this.element[0]) {
                return false;
            }
            this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function() {
                var c = a(this).droppable("instance");
                if (c.options.greedy && !c.options.disabled && c.options.scope === d.options.scope && c.accept.call(c.element[0], d.currentItem || d.element) && a.ui.intersect(d, a.extend(c, {
                    offset: c.element.offset()
                }), c.options.tolerance, b)) {
                    e = true;
                    return false;
                }
            });
            if (e) {
                return false;
            }
            if (this.accept.call(this.element[0], d.currentItem || d.element)) {
                if (this.options.activeClass) {
                    this.element.removeClass(this.options.activeClass);
                }
                if (this.options.hoverClass) {
                    this.element.removeClass(this.options.hoverClass);
                }
                this._trigger("drop", b, this.ui(d));
                return this.element;
            }
            return false;
        },
        ui: function(a) {
            return {
                draggable: a.currentItem || a.element,
                helper: a.helper,
                position: a.position,
                offset: a.positionAbs
            };
        }
    });
    a.ui.intersect = function() {
        function a(a, b, c) {
            return a >= b && a < b + c;
        }
        return function(b, c, d, e) {
            if (!c.offset) {
                return false;
            }
            var f = (b.positionAbs || b.position.absolute).left + b.margins.left, g = (b.positionAbs || b.position.absolute).top + b.margins.top, h = f + b.helperProportions.width, i = g + b.helperProportions.height, j = c.offset.left, k = c.offset.top, l = j + c.proportions().width, m = k + c.proportions().height;
            switch (d) {
              case "fit":
                return j <= f && h <= l && k <= g && i <= m;

              case "intersect":
                // Right Half
                // Left Half
                // Bottom Half
                return j < f + b.helperProportions.width / 2 && h - b.helperProportions.width / 2 < l && k < g + b.helperProportions.height / 2 && i - b.helperProportions.height / 2 < m;

              // Top Half
                case "pointer":
                return a(e.pageY, k, c.proportions().height) && a(e.pageX, j, c.proportions().width);

              case "touch":
                // Top edge touching
                // Bottom edge touching
                // Left edge touching
                // Right edge touching
                return (g >= k && g <= m || i >= k && i <= m || g < k && i > m) && (f >= j && f <= l || h >= j && h <= l || f < j && h > l);

              default:
                return false;
            }
        };
    }();
    /*
	This manager tracks offsets of draggables and droppables
*/
    a.ui.ddmanager = {
        current: null,
        droppables: {
            "default": []
        },
        prepareOffsets: function(b, c) {
            var d, e, f = a.ui.ddmanager.droppables[b.options.scope] || [], g = c ? c.type : null, // workaround for #2317
            h = (b.currentItem || b.element).find(":data(ui-droppable)").addBack();
            a: for (d = 0; d < f.length; d++) {
                // No disabled and non-accepted
                if (f[d].options.disabled || b && !f[d].accept.call(f[d].element[0], b.currentItem || b.element)) {
                    continue;
                }
                // Filter out elements in the current dragged item
                for (e = 0; e < h.length; e++) {
                    if (h[e] === f[d].element[0]) {
                        f[d].proportions().height = 0;
                        continue a;
                    }
                }
                f[d].visible = f[d].element.css("display") !== "none";
                if (!f[d].visible) {
                    continue;
                }
                // Activate the droppable if used directly from draggables
                if (g === "mousedown") {
                    f[d]._activate.call(f[d], c);
                }
                f[d].offset = f[d].element.offset();
                f[d].proportions({
                    width: f[d].element[0].offsetWidth,
                    height: f[d].element[0].offsetHeight
                });
            }
        },
        drop: function(b, c) {
            var d = false;
            // Create a copy of the droppables in case the list changes during the drop (#9116)
            a.each((a.ui.ddmanager.droppables[b.options.scope] || []).slice(), function() {
                if (!this.options) {
                    return;
                }
                if (!this.options.disabled && this.visible && a.ui.intersect(b, this, this.options.tolerance, c)) {
                    d = this._drop.call(this, c) || d;
                }
                if (!this.options.disabled && this.visible && this.accept.call(this.element[0], b.currentItem || b.element)) {
                    this.isout = true;
                    this.isover = false;
                    this._deactivate.call(this, c);
                }
            });
            return d;
        },
        dragStart: function(b, c) {
            // Listen for scrolling so that if the dragging causes scrolling the position of the droppables can be recalculated (see #5003)
            b.element.parentsUntil("body").bind("scroll.droppable", function() {
                if (!b.options.refreshPositions) {
                    a.ui.ddmanager.prepareOffsets(b, c);
                }
            });
        },
        drag: function(b, c) {
            // If you have a highly dynamic page, you might try this option. It renders positions every time you move the mouse.
            if (b.options.refreshPositions) {
                a.ui.ddmanager.prepareOffsets(b, c);
            }
            // Run through all droppables and check their positions based on specific tolerance options
            a.each(a.ui.ddmanager.droppables[b.options.scope] || [], function() {
                if (this.options.disabled || this.greedyChild || !this.visible) {
                    return;
                }
                var d, e, f, g = a.ui.intersect(b, this, this.options.tolerance, c), h = !g && this.isover ? "isout" : g && !this.isover ? "isover" : null;
                if (!h) {
                    return;
                }
                if (this.options.greedy) {
                    // find droppable parents with same scope
                    e = this.options.scope;
                    f = this.element.parents(":data(ui-droppable)").filter(function() {
                        return a(this).droppable("instance").options.scope === e;
                    });
                    if (f.length) {
                        d = a(f[0]).droppable("instance");
                        d.greedyChild = h === "isover";
                    }
                }
                // we just moved into a greedy child
                if (d && h === "isover") {
                    d.isover = false;
                    d.isout = true;
                    d._out.call(d, c);
                }
                this[h] = true;
                this[h === "isout" ? "isover" : "isout"] = false;
                this[h === "isover" ? "_over" : "_out"].call(this, c);
                // we just moved out of a greedy child
                if (d && h === "isout") {
                    d.isout = false;
                    d.isover = true;
                    d._over.call(d, c);
                }
            });
        },
        dragStop: function(b, c) {
            b.element.parentsUntil("body").unbind("scroll.droppable");
            // Call prepareOffsets one final time since IE does not fire return scroll events when overflow was caused by drag (see #5003)
            if (!b.options.refreshPositions) {
                a.ui.ddmanager.prepareOffsets(b, c);
            }
        }
    };
    var C = a.ui.droppable;
    /*!
 * jQuery UI Effects 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/category/effects-core/
 */
    var D = "ui-effects-", // Create a local jQuery because jQuery Color relies on it and the
    // global may not exist with AMD and a custom build (#10199)
    E = a;
    a.effects = {
        effect: {}
    };
    /*!
 * jQuery Color Animations v2.1.2
 * https://github.com/jquery/jquery-color
 *
 * Copyright 2014 jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * Date: Wed Jan 16 08:47:09 2013 -0600
 */
    (function(a, b) {
        var c = "backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor", // plusequals test for += 100 -= 100
        d = /^([\-+])=\s*(\d+\.?\d*)/, // a set of RE's that can match strings and generate color tuples.
        e = [ {
            re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
            parse: function(a) {
                return [ a[1], a[2], a[3], a[4] ];
            }
        }, {
            re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
            parse: function(a) {
                return [ a[1] * 2.55, a[2] * 2.55, a[3] * 2.55, a[4] ];
            }
        }, {
            // this regex ignores A-F because it's compared against an already lowercased string
            re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
            parse: function(a) {
                return [ parseInt(a[1], 16), parseInt(a[2], 16), parseInt(a[3], 16) ];
            }
        }, {
            // this regex ignores A-F because it's compared against an already lowercased string
            re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
            parse: function(a) {
                return [ parseInt(a[1] + a[1], 16), parseInt(a[2] + a[2], 16), parseInt(a[3] + a[3], 16) ];
            }
        }, {
            re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
            space: "hsla",
            parse: function(a) {
                return [ a[1], a[2] / 100, a[3] / 100, a[4] ];
            }
        } ], // jQuery.Color( )
        f = a.Color = function(b, c, d, e) {
            return new a.Color.fn.parse(b, c, d, e);
        }, g = {
            rgba: {
                props: {
                    red: {
                        idx: 0,
                        type: "byte"
                    },
                    green: {
                        idx: 1,
                        type: "byte"
                    },
                    blue: {
                        idx: 2,
                        type: "byte"
                    }
                }
            },
            hsla: {
                props: {
                    hue: {
                        idx: 0,
                        type: "degrees"
                    },
                    saturation: {
                        idx: 1,
                        type: "percent"
                    },
                    lightness: {
                        idx: 2,
                        type: "percent"
                    }
                }
            }
        }, h = {
            "byte": {
                floor: true,
                max: 255
            },
            percent: {
                max: 1
            },
            degrees: {
                mod: 360,
                floor: true
            }
        }, i = f.support = {}, // element for support tests
        j = a("<p>")[0], // colors = jQuery.Color.names
        k, // local aliases of functions called often
        l = a.each;
        // determine rgba support immediately
        j.style.cssText = "background-color:rgba(1,1,1,.5)";
        i.rgba = j.style.backgroundColor.indexOf("rgba") > -1;
        // define cache name and alpha properties
        // for rgba and hsla spaces
        l(g, function(a, b) {
            b.cache = "_" + a;
            b.props.alpha = {
                idx: 3,
                type: "percent",
                def: 1
            };
        });
        function m(a, b, c) {
            var d = h[b.type] || {};
            if (a == null) {
                return c || !b.def ? null : b.def;
            }
            // ~~ is an short way of doing floor for positive numbers
            a = d.floor ? ~~a : parseFloat(a);
            // IE will pass in empty strings as value for alpha,
            // which will hit this case
            if (isNaN(a)) {
                return b.def;
            }
            if (d.mod) {
                // we add mod before modding to make sure that negatives values
                // get converted properly: -10 -> 350
                return (a + d.mod) % d.mod;
            }
            // for now all property types without mod have min and max
            return 0 > a ? 0 : d.max < a ? d.max : a;
        }
        function n(b) {
            var c = f(), d = c._rgba = [];
            b = b.toLowerCase();
            l(e, function(a, e) {
                var f, h = e.re.exec(b), i = h && e.parse(h), j = e.space || "rgba";
                if (i) {
                    f = c[j](i);
                    // if this was an rgba parse the assignment might happen twice
                    // oh well....
                    c[g[j].cache] = f[g[j].cache];
                    d = c._rgba = f._rgba;
                    // exit each( stringParsers ) here because we matched
                    return false;
                }
            });
            // Found a stringParser that handled it
            if (d.length) {
                // if this came from a parsed string, force "transparent" when alpha is 0
                // chrome, (and maybe others) return "transparent" as rgba(0,0,0,0)
                if (d.join() === "0,0,0,0") {
                    a.extend(d, k.transparent);
                }
                return c;
            }
            // named colors
            return k[b];
        }
        f.fn = a.extend(f.prototype, {
            parse: function(c, d, e, h) {
                if (c === b) {
                    this._rgba = [ null, null, null, null ];
                    return this;
                }
                if (c.jquery || c.nodeType) {
                    c = a(c).css(d);
                    d = b;
                }
                var i = this, j = a.type(c), o = this._rgba = [];
                // more than 1 argument specified - assume ( red, green, blue, alpha )
                if (d !== b) {
                    c = [ c, d, e, h ];
                    j = "array";
                }
                if (j === "string") {
                    return this.parse(n(c) || k._default);
                }
                if (j === "array") {
                    l(g.rgba.props, function(a, b) {
                        o[b.idx] = m(c[b.idx], b);
                    });
                    return this;
                }
                if (j === "object") {
                    if (c instanceof f) {
                        l(g, function(a, b) {
                            if (c[b.cache]) {
                                i[b.cache] = c[b.cache].slice();
                            }
                        });
                    } else {
                        l(g, function(b, d) {
                            var e = d.cache;
                            l(d.props, function(a, b) {
                                // if the cache doesn't exist, and we know how to convert
                                if (!i[e] && d.to) {
                                    // if the value was null, we don't need to copy it
                                    // if the key was alpha, we don't need to copy it either
                                    if (a === "alpha" || c[a] == null) {
                                        return;
                                    }
                                    i[e] = d.to(i._rgba);
                                }
                                // this is the only case where we allow nulls for ALL properties.
                                // call clamp with alwaysAllowEmpty
                                i[e][b.idx] = m(c[a], b, true);
                            });
                            // everything defined but alpha?
                            if (i[e] && a.inArray(null, i[e].slice(0, 3)) < 0) {
                                // use the default of 1
                                i[e][3] = 1;
                                if (d.from) {
                                    i._rgba = d.from(i[e]);
                                }
                            }
                        });
                    }
                    return this;
                }
            },
            is: function(a) {
                var b = f(a), c = true, d = this;
                l(g, function(a, e) {
                    var f, g = b[e.cache];
                    if (g) {
                        f = d[e.cache] || e.to && e.to(d._rgba) || [];
                        l(e.props, function(a, b) {
                            if (g[b.idx] != null) {
                                c = g[b.idx] === f[b.idx];
                                return c;
                            }
                        });
                    }
                    return c;
                });
                return c;
            },
            _space: function() {
                var a = [], b = this;
                l(g, function(c, d) {
                    if (b[d.cache]) {
                        a.push(c);
                    }
                });
                return a.pop();
            },
            transition: function(a, b) {
                var c = f(a), d = c._space(), e = g[d], i = this.alpha() === 0 ? f("transparent") : this, j = i[e.cache] || e.to(i._rgba), k = j.slice();
                c = c[e.cache];
                l(e.props, function(a, d) {
                    var e = d.idx, f = j[e], g = c[e], i = h[d.type] || {};
                    // if null, don't override start value
                    if (g === null) {
                        return;
                    }
                    // if null - use end
                    if (f === null) {
                        k[e] = g;
                    } else {
                        if (i.mod) {
                            if (g - f > i.mod / 2) {
                                f += i.mod;
                            } else if (f - g > i.mod / 2) {
                                f -= i.mod;
                            }
                        }
                        k[e] = m((g - f) * b + f, d);
                    }
                });
                return this[d](k);
            },
            blend: function(b) {
                // if we are already opaque - return ourself
                if (this._rgba[3] === 1) {
                    return this;
                }
                var c = this._rgba.slice(), d = c.pop(), e = f(b)._rgba;
                return f(a.map(c, function(a, b) {
                    return (1 - d) * e[b] + d * a;
                }));
            },
            toRgbaString: function() {
                var b = "rgba(", c = a.map(this._rgba, function(a, b) {
                    return a == null ? b > 2 ? 1 : 0 : a;
                });
                if (c[3] === 1) {
                    c.pop();
                    b = "rgb(";
                }
                return b + c.join() + ")";
            },
            toHslaString: function() {
                var b = "hsla(", c = a.map(this.hsla(), function(a, b) {
                    if (a == null) {
                        a = b > 2 ? 1 : 0;
                    }
                    // catch 1 and 2
                    if (b && b < 3) {
                        a = Math.round(a * 100) + "%";
                    }
                    return a;
                });
                if (c[3] === 1) {
                    c.pop();
                    b = "hsl(";
                }
                return b + c.join() + ")";
            },
            toHexString: function(b) {
                var c = this._rgba.slice(), d = c.pop();
                if (b) {
                    c.push(~~(d * 255));
                }
                return "#" + a.map(c, function(a) {
                    // default to 0 when nulls exist
                    a = (a || 0).toString(16);
                    return a.length === 1 ? "0" + a : a;
                }).join("");
            },
            toString: function() {
                return this._rgba[3] === 0 ? "transparent" : this.toRgbaString();
            }
        });
        f.fn.parse.prototype = f.fn;
        // hsla conversions adapted from:
        // https://code.google.com/p/maashaack/source/browse/packages/graphics/trunk/src/graphics/colors/HUE2RGB.as?r=5021
        function o(a, b, c) {
            c = (c + 1) % 1;
            if (c * 6 < 1) {
                return a + (b - a) * c * 6;
            }
            if (c * 2 < 1) {
                return b;
            }
            if (c * 3 < 2) {
                return a + (b - a) * (2 / 3 - c) * 6;
            }
            return a;
        }
        g.hsla.to = function(a) {
            if (a[0] == null || a[1] == null || a[2] == null) {
                return [ null, null, null, a[3] ];
            }
            var b = a[0] / 255, c = a[1] / 255, d = a[2] / 255, e = a[3], f = Math.max(b, c, d), g = Math.min(b, c, d), h = f - g, i = f + g, j = i * .5, k, l;
            if (g === f) {
                k = 0;
            } else if (b === f) {
                k = 60 * (c - d) / h + 360;
            } else if (c === f) {
                k = 60 * (d - b) / h + 120;
            } else {
                k = 60 * (b - c) / h + 240;
            }
            // chroma (diff) == 0 means greyscale which, by definition, saturation = 0%
            // otherwise, saturation is based on the ratio of chroma (diff) to lightness (add)
            if (h === 0) {
                l = 0;
            } else if (j <= .5) {
                l = h / i;
            } else {
                l = h / (2 - i);
            }
            return [ Math.round(k) % 360, l, j, e == null ? 1 : e ];
        };
        g.hsla.from = function(a) {
            if (a[0] == null || a[1] == null || a[2] == null) {
                return [ null, null, null, a[3] ];
            }
            var b = a[0] / 360, c = a[1], d = a[2], e = a[3], f = d <= .5 ? d * (1 + c) : d + c - d * c, g = 2 * d - f;
            return [ Math.round(o(g, f, b + 1 / 3) * 255), Math.round(o(g, f, b) * 255), Math.round(o(g, f, b - 1 / 3) * 255), e ];
        };
        l(g, function(c, e) {
            var g = e.props, h = e.cache, i = e.to, j = e.from;
            // makes rgba() and hsla()
            f.fn[c] = function(c) {
                // generate a cache for this space if it doesn't exist
                if (i && !this[h]) {
                    this[h] = i(this._rgba);
                }
                if (c === b) {
                    return this[h].slice();
                }
                var d, e = a.type(c), k = e === "array" || e === "object" ? c : arguments, n = this[h].slice();
                l(g, function(a, b) {
                    var c = k[e === "object" ? a : b.idx];
                    if (c == null) {
                        c = n[b.idx];
                    }
                    n[b.idx] = m(c, b);
                });
                if (j) {
                    d = f(j(n));
                    d[h] = n;
                    return d;
                } else {
                    return f(n);
                }
            };
            // makes red() green() blue() alpha() hue() saturation() lightness()
            l(g, function(b, e) {
                // alpha is included in more than one space
                if (f.fn[b]) {
                    return;
                }
                f.fn[b] = function(f) {
                    var g = a.type(f), h = b === "alpha" ? this._hsla ? "hsla" : "rgba" : c, i = this[h](), j = i[e.idx], k;
                    if (g === "undefined") {
                        return j;
                    }
                    if (g === "function") {
                        f = f.call(this, j);
                        g = a.type(f);
                    }
                    if (f == null && e.empty) {
                        return this;
                    }
                    if (g === "string") {
                        k = d.exec(f);
                        if (k) {
                            f = j + parseFloat(k[2]) * (k[1] === "+" ? 1 : -1);
                        }
                    }
                    i[e.idx] = f;
                    return this[h](i);
                };
            });
        });
        // add cssHook and .fx.step function for each named hook.
        // accept a space separated string of properties
        f.hook = function(b) {
            var c = b.split(" ");
            l(c, function(b, c) {
                a.cssHooks[c] = {
                    set: function(b, d) {
                        var e, g, h = "";
                        if (d !== "transparent" && (a.type(d) !== "string" || (e = n(d)))) {
                            d = f(e || d);
                            if (!i.rgba && d._rgba[3] !== 1) {
                                g = c === "backgroundColor" ? b.parentNode : b;
                                while ((h === "" || h === "transparent") && g && g.style) {
                                    try {
                                        h = a.css(g, "backgroundColor");
                                        g = g.parentNode;
                                    } catch (j) {}
                                }
                                d = d.blend(h && h !== "transparent" ? h : "_default");
                            }
                            d = d.toRgbaString();
                        }
                        try {
                            b.style[c] = d;
                        } catch (j) {}
                    }
                };
                a.fx.step[c] = function(b) {
                    if (!b.colorInit) {
                        b.start = f(b.elem, c);
                        b.end = f(b.end);
                        b.colorInit = true;
                    }
                    a.cssHooks[c].set(b.elem, b.start.transition(b.end, b.pos));
                };
            });
        };
        f.hook(c);
        a.cssHooks.borderColor = {
            expand: function(a) {
                var b = {};
                l([ "Top", "Right", "Bottom", "Left" ], function(c, d) {
                    b["border" + d + "Color"] = a;
                });
                return b;
            }
        };
        // Basic color names only.
        // Usage of any of the other color names requires adding yourself or including
        // jquery.color.svg-names.js.
        k = a.Color.names = {
            // 4.1. Basic color keywords
            aqua: "#00ffff",
            black: "#000000",
            blue: "#0000ff",
            fuchsia: "#ff00ff",
            gray: "#808080",
            green: "#008000",
            lime: "#00ff00",
            maroon: "#800000",
            navy: "#000080",
            olive: "#808000",
            purple: "#800080",
            red: "#ff0000",
            silver: "#c0c0c0",
            teal: "#008080",
            white: "#ffffff",
            yellow: "#ffff00",
            // 4.2.3. "transparent" color keyword
            transparent: [ null, null, null, 0 ],
            _default: "#ffffff"
        };
    })(E);
    /******************************************************************************/
    /****************************** CLASS ANIMATIONS ******************************/
    /******************************************************************************/
    (function() {
        var b = [ "add", "remove", "toggle" ], c = {
            border: 1,
            borderBottom: 1,
            borderColor: 1,
            borderLeft: 1,
            borderRight: 1,
            borderTop: 1,
            borderWidth: 1,
            margin: 1,
            padding: 1
        };
        a.each([ "borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle" ], function(b, c) {
            a.fx.step[c] = function(a) {
                if (a.end !== "none" && !a.setAttr || a.pos === 1 && !a.setAttr) {
                    E.style(a.elem, c, a.end);
                    a.setAttr = true;
                }
            };
        });
        function d(b) {
            var c, d, e = b.ownerDocument.defaultView ? b.ownerDocument.defaultView.getComputedStyle(b, null) : b.currentStyle, f = {};
            if (e && e.length && e[0] && e[e[0]]) {
                d = e.length;
                while (d--) {
                    c = e[d];
                    if (typeof e[c] === "string") {
                        f[a.camelCase(c)] = e[c];
                    }
                }
            } else {
                for (c in e) {
                    if (typeof e[c] === "string") {
                        f[c] = e[c];
                    }
                }
            }
            return f;
        }
        function e(b, d) {
            var e = {}, f, g;
            for (f in d) {
                g = d[f];
                if (b[f] !== g) {
                    if (!c[f]) {
                        if (a.fx.step[f] || !isNaN(parseFloat(g))) {
                            e[f] = g;
                        }
                    }
                }
            }
            return e;
        }
        // support: jQuery <1.8
        if (!a.fn.addBack) {
            a.fn.addBack = function(a) {
                return this.add(a == null ? this.prevObject : this.prevObject.filter(a));
            };
        }
        a.effects.animateClass = function(c, f, g, h) {
            var i = a.speed(f, g, h);
            return this.queue(function() {
                var f = a(this), g = f.attr("class") || "", h, j = i.children ? f.find("*").addBack() : f;
                // map the animated objects to store the original styles.
                j = j.map(function() {
                    var b = a(this);
                    return {
                        el: b,
                        start: d(this)
                    };
                });
                // apply class change
                h = function() {
                    a.each(b, function(a, b) {
                        if (c[b]) {
                            f[b + "Class"](c[b]);
                        }
                    });
                };
                h();
                // map all animated objects again - calculate new styles and diff
                j = j.map(function() {
                    this.end = d(this.el[0]);
                    this.diff = e(this.start, this.end);
                    return this;
                });
                // apply original class
                f.attr("class", g);
                // map all animated objects again - this time collecting a promise
                j = j.map(function() {
                    var b = this, c = a.Deferred(), d = a.extend({}, i, {
                        queue: false,
                        complete: function() {
                            c.resolve(b);
                        }
                    });
                    this.el.animate(this.diff, d);
                    return c.promise();
                });
                // once all animations have completed:
                a.when.apply(a, j.get()).done(function() {
                    // set the final class
                    h();
                    // for each animated element,
                    // clear all css properties that were animated
                    a.each(arguments, function() {
                        var b = this.el;
                        a.each(this.diff, function(a) {
                            b.css(a, "");
                        });
                    });
                    // this is guarnteed to be there if you use jQuery.speed()
                    // it also handles dequeuing the next anim...
                    i.complete.call(f[0]);
                });
            });
        };
        a.fn.extend({
            addClass: function(b) {
                return function(c, d, e, f) {
                    return d ? a.effects.animateClass.call(this, {
                        add: c
                    }, d, e, f) : b.apply(this, arguments);
                };
            }(a.fn.addClass),
            removeClass: function(b) {
                return function(c, d, e, f) {
                    return arguments.length > 1 ? a.effects.animateClass.call(this, {
                        remove: c
                    }, d, e, f) : b.apply(this, arguments);
                };
            }(a.fn.removeClass),
            toggleClass: function(b) {
                return function(c, d, e, f, g) {
                    if (typeof d === "boolean" || d === undefined) {
                        if (!e) {
                            // without speed parameter
                            return b.apply(this, arguments);
                        } else {
                            return a.effects.animateClass.call(this, d ? {
                                add: c
                            } : {
                                remove: c
                            }, e, f, g);
                        }
                    } else {
                        // without force parameter
                        return a.effects.animateClass.call(this, {
                            toggle: c
                        }, d, e, f);
                    }
                };
            }(a.fn.toggleClass),
            switchClass: function(b, c, d, e, f) {
                return a.effects.animateClass.call(this, {
                    add: c,
                    remove: b
                }, d, e, f);
            }
        });
    })();
    /******************************************************************************/
    /*********************************** EFFECTS **********************************/
    /******************************************************************************/
    (function() {
        a.extend(a.effects, {
            version: "1.11.4",
            // Saves a set of properties in a data storage
            save: function(a, b) {
                for (var c = 0; c < b.length; c++) {
                    if (b[c] !== null) {
                        a.data(D + b[c], a[0].style[b[c]]);
                    }
                }
            },
            // Restores a set of previously saved properties from a data storage
            restore: function(a, b) {
                var c, d;
                for (d = 0; d < b.length; d++) {
                    if (b[d] !== null) {
                        c = a.data(D + b[d]);
                        // support: jQuery 1.6.2
                        // http://bugs.jquery.com/ticket/9917
                        // jQuery 1.6.2 incorrectly returns undefined for any falsy value.
                        // We can't differentiate between "" and 0 here, so we just assume
                        // empty string since it's likely to be a more common value...
                        if (c === undefined) {
                            c = "";
                        }
                        a.css(b[d], c);
                    }
                }
            },
            setMode: function(a, b) {
                if (b === "toggle") {
                    b = a.is(":hidden") ? "show" : "hide";
                }
                return b;
            },
            // Translates a [top,left] array into a baseline value
            // this should be a little more flexible in the future to handle a string & hash
            getBaseline: function(a, b) {
                var c, d;
                switch (a[0]) {
                  case "top":
                    c = 0;
                    break;

                  case "middle":
                    c = .5;
                    break;

                  case "bottom":
                    c = 1;
                    break;

                  default:
                    c = a[0] / b.height;
                }
                switch (a[1]) {
                  case "left":
                    d = 0;
                    break;

                  case "center":
                    d = .5;
                    break;

                  case "right":
                    d = 1;
                    break;

                  default:
                    d = a[1] / b.width;
                }
                return {
                    x: d,
                    y: c
                };
            },
            // Wraps the element around a wrapper that copies position properties
            createWrapper: function(b) {
                // if the element is already wrapped, return it
                if (b.parent().is(".ui-effects-wrapper")) {
                    return b.parent();
                }
                // wrap the element
                var c = {
                    width: b.outerWidth(true),
                    height: b.outerHeight(true),
                    "float": b.css("float")
                }, d = a("<div></div>").addClass("ui-effects-wrapper").css({
                    fontSize: "100%",
                    background: "transparent",
                    border: "none",
                    margin: 0,
                    padding: 0
                }), // Store the size in case width/height are defined in % - Fixes #5245
                e = {
                    width: b.width(),
                    height: b.height()
                }, f = document.activeElement;
                // support: Firefox
                // Firefox incorrectly exposes anonymous content
                // https://bugzilla.mozilla.org/show_bug.cgi?id=561664
                try {
                    f.id;
                } catch (g) {
                    f = document.body;
                }
                b.wrap(d);
                // Fixes #7595 - Elements lose focus when wrapped.
                if (b[0] === f || a.contains(b[0], f)) {
                    a(f).focus();
                }
                d = b.parent();
                //Hotfix for jQuery 1.4 since some change in wrap() seems to actually lose the reference to the wrapped element
                // transfer positioning properties to the wrapper
                if (b.css("position") === "static") {
                    d.css({
                        position: "relative"
                    });
                    b.css({
                        position: "relative"
                    });
                } else {
                    a.extend(c, {
                        position: b.css("position"),
                        zIndex: b.css("z-index")
                    });
                    a.each([ "top", "left", "bottom", "right" ], function(a, d) {
                        c[d] = b.css(d);
                        if (isNaN(parseInt(c[d], 10))) {
                            c[d] = "auto";
                        }
                    });
                    b.css({
                        position: "relative",
                        top: 0,
                        left: 0,
                        right: "auto",
                        bottom: "auto"
                    });
                }
                b.css(e);
                return d.css(c).show();
            },
            removeWrapper: function(b) {
                var c = document.activeElement;
                if (b.parent().is(".ui-effects-wrapper")) {
                    b.parent().replaceWith(b);
                    // Fixes #7595 - Elements lose focus when wrapped.
                    if (b[0] === c || a.contains(b[0], c)) {
                        a(c).focus();
                    }
                }
                return b;
            },
            setTransition: function(b, c, d, e) {
                e = e || {};
                a.each(c, function(a, c) {
                    var f = b.cssUnit(c);
                    if (f[0] > 0) {
                        e[c] = f[0] * d + f[1];
                    }
                });
                return e;
            }
        });
        // return an effect options object for the given parameters:
        function b(b, c, d, e) {
            // allow passing all options as the first parameter
            if (a.isPlainObject(b)) {
                c = b;
                b = b.effect;
            }
            // convert to an object
            b = {
                effect: b
            };
            // catch (effect, null, ...)
            if (c == null) {
                c = {};
            }
            // catch (effect, callback)
            if (a.isFunction(c)) {
                e = c;
                d = null;
                c = {};
            }
            // catch (effect, speed, ?)
            if (typeof c === "number" || a.fx.speeds[c]) {
                e = d;
                d = c;
                c = {};
            }
            // catch (effect, options, callback)
            if (a.isFunction(d)) {
                e = d;
                d = null;
            }
            // add options to effect
            if (c) {
                a.extend(b, c);
            }
            d = d || c.duration;
            b.duration = a.fx.off ? 0 : typeof d === "number" ? d : d in a.fx.speeds ? a.fx.speeds[d] : a.fx.speeds._default;
            b.complete = e || c.complete;
            return b;
        }
        function c(b) {
            // Valid standard speeds (nothing, number, named speed)
            if (!b || typeof b === "number" || a.fx.speeds[b]) {
                return true;
            }
            // Invalid strings - treat as "normal" speed
            if (typeof b === "string" && !a.effects.effect[b]) {
                return true;
            }
            // Complete callback
            if (a.isFunction(b)) {
                return true;
            }
            // Options hash (but not naming an effect)
            if (typeof b === "object" && !b.effect) {
                return true;
            }
            // Didn't match any standard API
            return false;
        }
        a.fn.extend({
            effect: function() {
                var c = b.apply(this, arguments), d = c.mode, e = c.queue, f = a.effects.effect[c.effect];
                if (a.fx.off || !f) {
                    // delegate to the original method (e.g., .show()) if possible
                    if (d) {
                        return this[d](c.duration, c.complete);
                    } else {
                        return this.each(function() {
                            if (c.complete) {
                                c.complete.call(this);
                            }
                        });
                    }
                }
                function g(b) {
                    var d = a(this), e = c.complete, g = c.mode;
                    function h() {
                        if (a.isFunction(e)) {
                            e.call(d[0]);
                        }
                        if (a.isFunction(b)) {
                            b();
                        }
                    }
                    // If the element already has the correct final state, delegate to
                    // the core methods so the internal tracking of "olddisplay" works.
                    if (d.is(":hidden") ? g === "hide" : g === "show") {
                        d[g]();
                        h();
                    } else {
                        f.call(d[0], c, h);
                    }
                }
                return e === false ? this.each(g) : this.queue(e || "fx", g);
            },
            show: function(a) {
                return function(d) {
                    if (c(d)) {
                        return a.apply(this, arguments);
                    } else {
                        var e = b.apply(this, arguments);
                        e.mode = "show";
                        return this.effect.call(this, e);
                    }
                };
            }(a.fn.show),
            hide: function(a) {
                return function(d) {
                    if (c(d)) {
                        return a.apply(this, arguments);
                    } else {
                        var e = b.apply(this, arguments);
                        e.mode = "hide";
                        return this.effect.call(this, e);
                    }
                };
            }(a.fn.hide),
            toggle: function(a) {
                return function(d) {
                    if (c(d) || typeof d === "boolean") {
                        return a.apply(this, arguments);
                    } else {
                        var e = b.apply(this, arguments);
                        e.mode = "toggle";
                        return this.effect.call(this, e);
                    }
                };
            }(a.fn.toggle),
            // helper functions
            cssUnit: function(b) {
                var c = this.css(b), d = [];
                a.each([ "em", "px", "%", "pt" ], function(a, b) {
                    if (c.indexOf(b) > 0) {
                        d = [ parseFloat(c), b ];
                    }
                });
                return d;
            }
        });
    })();
    /******************************************************************************/
    /*********************************** EASING ***********************************/
    /******************************************************************************/
    (function() {
        // based on easing equations from Robert Penner (http://www.robertpenner.com/easing)
        var b = {};
        a.each([ "Quad", "Cubic", "Quart", "Quint", "Expo" ], function(a, c) {
            b[c] = function(b) {
                return Math.pow(b, a + 2);
            };
        });
        a.extend(b, {
            Sine: function(a) {
                return 1 - Math.cos(a * Math.PI / 2);
            },
            Circ: function(a) {
                return 1 - Math.sqrt(1 - a * a);
            },
            Elastic: function(a) {
                return a === 0 || a === 1 ? a : -Math.pow(2, 8 * (a - 1)) * Math.sin(((a - 1) * 80 - 7.5) * Math.PI / 15);
            },
            Back: function(a) {
                return a * a * (3 * a - 2);
            },
            Bounce: function(a) {
                var b, c = 4;
                while (a < ((b = Math.pow(2, --c)) - 1) / 11) {}
                return 1 / Math.pow(4, 3 - c) - 7.5625 * Math.pow((b * 3 - 2) / 22 - a, 2);
            }
        });
        a.each(b, function(b, c) {
            a.easing["easeIn" + b] = c;
            a.easing["easeOut" + b] = function(a) {
                return 1 - c(1 - a);
            };
            a.easing["easeInOut" + b] = function(a) {
                return a < .5 ? c(a * 2) / 2 : 1 - c(a * -2 + 2) / 2;
            };
        });
    })();
    var F = a.effects;
    /*!
 * jQuery UI Effects Blind 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/blind-effect/
 */
    var G = a.effects.effect.blind = function(b, c) {
        // Create element
        var d = a(this), e = /up|down|vertical/, f = /up|left|vertical|horizontal/, g = [ "position", "top", "bottom", "left", "right", "height", "width" ], h = a.effects.setMode(d, b.mode || "hide"), i = b.direction || "up", j = e.test(i), k = j ? "height" : "width", l = j ? "top" : "left", m = f.test(i), n = {}, o = h === "show", p, q, r;
        // if already wrapped, the wrapper's properties are my property. #6245
        if (d.parent().is(".ui-effects-wrapper")) {
            a.effects.save(d.parent(), g);
        } else {
            a.effects.save(d, g);
        }
        d.show();
        p = a.effects.createWrapper(d).css({
            overflow: "hidden"
        });
        q = p[k]();
        r = parseFloat(p.css(l)) || 0;
        n[k] = o ? q : 0;
        if (!m) {
            d.css(j ? "bottom" : "right", 0).css(j ? "top" : "left", "auto").css({
                position: "absolute"
            });
            n[l] = o ? r : q + r;
        }
        // start at 0 if we are showing
        if (o) {
            p.css(k, 0);
            if (!m) {
                p.css(l, r + q);
            }
        }
        // Animate
        p.animate(n, {
            duration: b.duration,
            easing: b.easing,
            queue: false,
            complete: function() {
                if (h === "hide") {
                    d.hide();
                }
                a.effects.restore(d, g);
                a.effects.removeWrapper(d);
                c();
            }
        });
    };
    /*!
 * jQuery UI Effects Bounce 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/bounce-effect/
 */
    var H = a.effects.effect.bounce = function(b, c) {
        var d = a(this), e = [ "position", "top", "bottom", "left", "right", "height", "width" ], // defaults:
        f = a.effects.setMode(d, b.mode || "effect"), g = f === "hide", h = f === "show", i = b.direction || "up", j = b.distance, k = b.times || 5, // number of internal animations
        l = k * 2 + (h || g ? 1 : 0), m = b.duration / l, n = b.easing, // utility:
        o = i === "up" || i === "down" ? "top" : "left", p = i === "up" || i === "left", q, r, s, // we will need to re-assemble the queue to stack our animations in place
        t = d.queue(), u = t.length;
        // Avoid touching opacity to prevent clearType and PNG issues in IE
        if (h || g) {
            e.push("opacity");
        }
        a.effects.save(d, e);
        d.show();
        a.effects.createWrapper(d);
        // Create Wrapper
        // default distance for the BIGGEST bounce is the outer Distance / 3
        if (!j) {
            j = d[o === "top" ? "outerHeight" : "outerWidth"]() / 3;
        }
        if (h) {
            s = {
                opacity: 1
            };
            s[o] = 0;
            // if we are showing, force opacity 0 and set the initial position
            // then do the "first" animation
            d.css("opacity", 0).css(o, p ? -j * 2 : j * 2).animate(s, m, n);
        }
        // start at the smallest distance if we are hiding
        if (g) {
            j = j / Math.pow(2, k - 1);
        }
        s = {};
        s[o] = 0;
        // Bounces up/down/left/right then back to 0 -- times * 2 animations happen here
        for (q = 0; q < k; q++) {
            r = {};
            r[o] = (p ? "-=" : "+=") + j;
            d.animate(r, m, n).animate(s, m, n);
            j = g ? j * 2 : j / 2;
        }
        // Last Bounce when Hiding
        if (g) {
            r = {
                opacity: 0
            };
            r[o] = (p ? "-=" : "+=") + j;
            d.animate(r, m, n);
        }
        d.queue(function() {
            if (g) {
                d.hide();
            }
            a.effects.restore(d, e);
            a.effects.removeWrapper(d);
            c();
        });
        // inject all the animations we just queued to be first in line (after "inprogress")
        if (u > 1) {
            t.splice.apply(t, [ 1, 0 ].concat(t.splice(u, l + 1)));
        }
        d.dequeue();
    };
    /*!
 * jQuery UI Effects Clip 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/clip-effect/
 */
    var I = a.effects.effect.clip = function(b, c) {
        // Create element
        var d = a(this), e = [ "position", "top", "bottom", "left", "right", "height", "width" ], f = a.effects.setMode(d, b.mode || "hide"), g = f === "show", h = b.direction || "vertical", i = h === "vertical", j = i ? "height" : "width", k = i ? "top" : "left", l = {}, m, n, o;
        // Save & Show
        a.effects.save(d, e);
        d.show();
        // Create Wrapper
        m = a.effects.createWrapper(d).css({
            overflow: "hidden"
        });
        n = d[0].tagName === "IMG" ? m : d;
        o = n[j]();
        // Shift
        if (g) {
            n.css(j, 0);
            n.css(k, o / 2);
        }
        // Create Animation Object:
        l[j] = g ? o : 0;
        l[k] = g ? 0 : o / 2;
        // Animate
        n.animate(l, {
            queue: false,
            duration: b.duration,
            easing: b.easing,
            complete: function() {
                if (!g) {
                    d.hide();
                }
                a.effects.restore(d, e);
                a.effects.removeWrapper(d);
                c();
            }
        });
    };
    /*!
 * jQuery UI Effects Drop 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/drop-effect/
 */
    var J = a.effects.effect.drop = function(b, c) {
        var d = a(this), e = [ "position", "top", "bottom", "left", "right", "opacity", "height", "width" ], f = a.effects.setMode(d, b.mode || "hide"), g = f === "show", h = b.direction || "left", i = h === "up" || h === "down" ? "top" : "left", j = h === "up" || h === "left" ? "pos" : "neg", k = {
            opacity: g ? 1 : 0
        }, l;
        // Adjust
        a.effects.save(d, e);
        d.show();
        a.effects.createWrapper(d);
        l = b.distance || d[i === "top" ? "outerHeight" : "outerWidth"](true) / 2;
        if (g) {
            d.css("opacity", 0).css(i, j === "pos" ? -l : l);
        }
        // Animation
        k[i] = (g ? j === "pos" ? "+=" : "-=" : j === "pos" ? "-=" : "+=") + l;
        // Animate
        d.animate(k, {
            queue: false,
            duration: b.duration,
            easing: b.easing,
            complete: function() {
                if (f === "hide") {
                    d.hide();
                }
                a.effects.restore(d, e);
                a.effects.removeWrapper(d);
                c();
            }
        });
    };
    /*!
 * jQuery UI Effects Explode 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/explode-effect/
 */
    var K = a.effects.effect.explode = function(b, c) {
        var d = b.pieces ? Math.round(Math.sqrt(b.pieces)) : 3, e = d, f = a(this), g = a.effects.setMode(f, b.mode || "hide"), h = g === "show", // show and then visibility:hidden the element before calculating offset
        i = f.show().css("visibility", "hidden").offset(), // width and height of a piece
        j = Math.ceil(f.outerWidth() / e), k = Math.ceil(f.outerHeight() / d), l = [], // loop
        m, n, o, p, q, r;
        // children animate complete:
        function s() {
            l.push(this);
            if (l.length === d * e) {
                t();
            }
        }
        // clone the element for each row and cell.
        for (m = 0; m < d; m++) {
            // ===>
            p = i.top + m * k;
            r = m - (d - 1) / 2;
            for (n = 0; n < e; n++) {
                // |||
                o = i.left + n * j;
                q = n - (e - 1) / 2;
                // Create a clone of the now hidden main element that will be absolute positioned
                // within a wrapper div off the -left and -top equal to size of our pieces
                f.clone().appendTo("body").wrap("<div></div>").css({
                    position: "absolute",
                    visibility: "visible",
                    left: -n * j,
                    top: -m * k
                }).parent().addClass("ui-effects-explode").css({
                    position: "absolute",
                    overflow: "hidden",
                    width: j,
                    height: k,
                    left: o + (h ? q * j : 0),
                    top: p + (h ? r * k : 0),
                    opacity: h ? 0 : 1
                }).animate({
                    left: o + (h ? 0 : q * j),
                    top: p + (h ? 0 : r * k),
                    opacity: h ? 1 : 0
                }, b.duration || 500, b.easing, s);
            }
        }
        function t() {
            f.css({
                visibility: "visible"
            });
            a(l).remove();
            if (!h) {
                f.hide();
            }
            c();
        }
    };
    /*!
 * jQuery UI Effects Fade 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/fade-effect/
 */
    var L = a.effects.effect.fade = function(b, c) {
        var d = a(this), e = a.effects.setMode(d, b.mode || "toggle");
        d.animate({
            opacity: e
        }, {
            queue: false,
            duration: b.duration,
            easing: b.easing,
            complete: c
        });
    };
    /*!
 * jQuery UI Effects Fold 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/fold-effect/
 */
    var M = a.effects.effect.fold = function(b, c) {
        // Create element
        var d = a(this), e = [ "position", "top", "bottom", "left", "right", "height", "width" ], f = a.effects.setMode(d, b.mode || "hide"), g = f === "show", h = f === "hide", i = b.size || 15, j = /([0-9]+)%/.exec(i), k = !!b.horizFirst, l = g !== k, m = l ? [ "width", "height" ] : [ "height", "width" ], n = b.duration / 2, o, p, q = {}, r = {};
        a.effects.save(d, e);
        d.show();
        // Create Wrapper
        o = a.effects.createWrapper(d).css({
            overflow: "hidden"
        });
        p = l ? [ o.width(), o.height() ] : [ o.height(), o.width() ];
        if (j) {
            i = parseInt(j[1], 10) / 100 * p[h ? 0 : 1];
        }
        if (g) {
            o.css(k ? {
                height: 0,
                width: i
            } : {
                height: i,
                width: 0
            });
        }
        // Animation
        q[m[0]] = g ? p[0] : i;
        r[m[1]] = g ? p[1] : 0;
        // Animate
        o.animate(q, n, b.easing).animate(r, n, b.easing, function() {
            if (h) {
                d.hide();
            }
            a.effects.restore(d, e);
            a.effects.removeWrapper(d);
            c();
        });
    };
    /*!
 * jQuery UI Effects Highlight 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/highlight-effect/
 */
    var N = a.effects.effect.highlight = function(b, c) {
        var d = a(this), e = [ "backgroundImage", "backgroundColor", "opacity" ], f = a.effects.setMode(d, b.mode || "show"), g = {
            backgroundColor: d.css("backgroundColor")
        };
        if (f === "hide") {
            g.opacity = 0;
        }
        a.effects.save(d, e);
        d.show().css({
            backgroundImage: "none",
            backgroundColor: b.color || "#ffff99"
        }).animate(g, {
            queue: false,
            duration: b.duration,
            easing: b.easing,
            complete: function() {
                if (f === "hide") {
                    d.hide();
                }
                a.effects.restore(d, e);
                c();
            }
        });
    };
    /*!
 * jQuery UI Effects Size 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/size-effect/
 */
    var O = a.effects.effect.size = function(b, c) {
        // Create element
        var d, e, f, g = a(this), h = [ "position", "top", "bottom", "left", "right", "width", "height", "overflow", "opacity" ], // Always restore
        i = [ "position", "top", "bottom", "left", "right", "overflow", "opacity" ], // Copy for children
        j = [ "width", "height", "overflow" ], k = [ "fontSize" ], l = [ "borderTopWidth", "borderBottomWidth", "paddingTop", "paddingBottom" ], m = [ "borderLeftWidth", "borderRightWidth", "paddingLeft", "paddingRight" ], // Set options
        n = a.effects.setMode(g, b.mode || "effect"), o = b.restore || n !== "effect", p = b.scale || "both", q = b.origin || [ "middle", "center" ], r = g.css("position"), s = o ? h : i, t = {
            height: 0,
            width: 0,
            outerHeight: 0,
            outerWidth: 0
        };
        if (n === "show") {
            g.show();
        }
        d = {
            height: g.height(),
            width: g.width(),
            outerHeight: g.outerHeight(),
            outerWidth: g.outerWidth()
        };
        if (b.mode === "toggle" && n === "show") {
            g.from = b.to || t;
            g.to = b.from || d;
        } else {
            g.from = b.from || (n === "show" ? t : d);
            g.to = b.to || (n === "hide" ? t : d);
        }
        // Set scaling factor
        f = {
            from: {
                y: g.from.height / d.height,
                x: g.from.width / d.width
            },
            to: {
                y: g.to.height / d.height,
                x: g.to.width / d.width
            }
        };
        // Scale the css box
        if (p === "box" || p === "both") {
            // Vertical props scaling
            if (f.from.y !== f.to.y) {
                s = s.concat(l);
                g.from = a.effects.setTransition(g, l, f.from.y, g.from);
                g.to = a.effects.setTransition(g, l, f.to.y, g.to);
            }
            // Horizontal props scaling
            if (f.from.x !== f.to.x) {
                s = s.concat(m);
                g.from = a.effects.setTransition(g, m, f.from.x, g.from);
                g.to = a.effects.setTransition(g, m, f.to.x, g.to);
            }
        }
        // Scale the content
        if (p === "content" || p === "both") {
            // Vertical props scaling
            if (f.from.y !== f.to.y) {
                s = s.concat(k).concat(j);
                g.from = a.effects.setTransition(g, k, f.from.y, g.from);
                g.to = a.effects.setTransition(g, k, f.to.y, g.to);
            }
        }
        a.effects.save(g, s);
        g.show();
        a.effects.createWrapper(g);
        g.css("overflow", "hidden").css(g.from);
        // Adjust
        if (q) {
            // Calculate baseline shifts
            e = a.effects.getBaseline(q, d);
            g.from.top = (d.outerHeight - g.outerHeight()) * e.y;
            g.from.left = (d.outerWidth - g.outerWidth()) * e.x;
            g.to.top = (d.outerHeight - g.to.outerHeight) * e.y;
            g.to.left = (d.outerWidth - g.to.outerWidth) * e.x;
        }
        g.css(g.from);
        // set top & left
        // Animate
        if (p === "content" || p === "both") {
            // Scale the children
            // Add margins/font-size
            l = l.concat([ "marginTop", "marginBottom" ]).concat(k);
            m = m.concat([ "marginLeft", "marginRight" ]);
            j = h.concat(l).concat(m);
            g.find("*[width]").each(function() {
                var c = a(this), d = {
                    height: c.height(),
                    width: c.width(),
                    outerHeight: c.outerHeight(),
                    outerWidth: c.outerWidth()
                };
                if (o) {
                    a.effects.save(c, j);
                }
                c.from = {
                    height: d.height * f.from.y,
                    width: d.width * f.from.x,
                    outerHeight: d.outerHeight * f.from.y,
                    outerWidth: d.outerWidth * f.from.x
                };
                c.to = {
                    height: d.height * f.to.y,
                    width: d.width * f.to.x,
                    outerHeight: d.height * f.to.y,
                    outerWidth: d.width * f.to.x
                };
                // Vertical props scaling
                if (f.from.y !== f.to.y) {
                    c.from = a.effects.setTransition(c, l, f.from.y, c.from);
                    c.to = a.effects.setTransition(c, l, f.to.y, c.to);
                }
                // Horizontal props scaling
                if (f.from.x !== f.to.x) {
                    c.from = a.effects.setTransition(c, m, f.from.x, c.from);
                    c.to = a.effects.setTransition(c, m, f.to.x, c.to);
                }
                // Animate children
                c.css(c.from);
                c.animate(c.to, b.duration, b.easing, function() {
                    // Restore children
                    if (o) {
                        a.effects.restore(c, j);
                    }
                });
            });
        }
        // Animate
        g.animate(g.to, {
            queue: false,
            duration: b.duration,
            easing: b.easing,
            complete: function() {
                if (g.to.opacity === 0) {
                    g.css("opacity", g.from.opacity);
                }
                if (n === "hide") {
                    g.hide();
                }
                a.effects.restore(g, s);
                if (!o) {
                    // we need to calculate our new positioning based on the scaling
                    if (r === "static") {
                        g.css({
                            position: "relative",
                            top: g.to.top,
                            left: g.to.left
                        });
                    } else {
                        a.each([ "top", "left" ], function(a, b) {
                            g.css(b, function(b, c) {
                                var d = parseInt(c, 10), e = a ? g.to.left : g.to.top;
                                // if original was "auto", recalculate the new value from wrapper
                                if (c === "auto") {
                                    return e + "px";
                                }
                                return d + e + "px";
                            });
                        });
                    }
                }
                a.effects.removeWrapper(g);
                c();
            }
        });
    };
    /*!
 * jQuery UI Effects Scale 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/scale-effect/
 */
    var P = a.effects.effect.scale = function(b, c) {
        // Create element
        var d = a(this), e = a.extend(true, {}, b), f = a.effects.setMode(d, b.mode || "effect"), g = parseInt(b.percent, 10) || (parseInt(b.percent, 10) === 0 ? 0 : f === "hide" ? 0 : 100), h = b.direction || "both", i = b.origin, j = {
            height: d.height(),
            width: d.width(),
            outerHeight: d.outerHeight(),
            outerWidth: d.outerWidth()
        }, k = {
            y: h !== "horizontal" ? g / 100 : 1,
            x: h !== "vertical" ? g / 100 : 1
        };
        // We are going to pass this effect to the size effect:
        e.effect = "size";
        e.queue = false;
        e.complete = c;
        // Set default origin and restore for show/hide
        if (f !== "effect") {
            e.origin = i || [ "middle", "center" ];
            e.restore = true;
        }
        e.from = b.from || (f === "show" ? {
            height: 0,
            width: 0,
            outerHeight: 0,
            outerWidth: 0
        } : j);
        e.to = {
            height: j.height * k.y,
            width: j.width * k.x,
            outerHeight: j.outerHeight * k.y,
            outerWidth: j.outerWidth * k.x
        };
        // Fade option to support puff
        if (e.fade) {
            if (f === "show") {
                e.from.opacity = 0;
                e.to.opacity = 1;
            }
            if (f === "hide") {
                e.from.opacity = 1;
                e.to.opacity = 0;
            }
        }
        // Animate
        d.effect(e);
    };
    /*!
 * jQuery UI Effects Puff 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/puff-effect/
 */
    var Q = a.effects.effect.puff = function(b, c) {
        var d = a(this), e = a.effects.setMode(d, b.mode || "hide"), f = e === "hide", g = parseInt(b.percent, 10) || 150, h = g / 100, i = {
            height: d.height(),
            width: d.width(),
            outerHeight: d.outerHeight(),
            outerWidth: d.outerWidth()
        };
        a.extend(b, {
            effect: "scale",
            queue: false,
            fade: true,
            mode: e,
            complete: c,
            percent: f ? g : 100,
            from: f ? i : {
                height: i.height * h,
                width: i.width * h,
                outerHeight: i.outerHeight * h,
                outerWidth: i.outerWidth * h
            }
        });
        d.effect(b);
    };
    /*!
 * jQuery UI Effects Pulsate 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/pulsate-effect/
 */
    var R = a.effects.effect.pulsate = function(b, c) {
        var d = a(this), e = a.effects.setMode(d, b.mode || "show"), f = e === "show", g = e === "hide", h = f || e === "hide", // showing or hiding leaves of the "last" animation
        i = (b.times || 5) * 2 + (h ? 1 : 0), j = b.duration / i, k = 0, l = d.queue(), m = l.length, n;
        if (f || !d.is(":visible")) {
            d.css("opacity", 0).show();
            k = 1;
        }
        // anims - 1 opacity "toggles"
        for (n = 1; n < i; n++) {
            d.animate({
                opacity: k
            }, j, b.easing);
            k = 1 - k;
        }
        d.animate({
            opacity: k
        }, j, b.easing);
        d.queue(function() {
            if (g) {
                d.hide();
            }
            c();
        });
        // We just queued up "anims" animations, we need to put them next in the queue
        if (m > 1) {
            l.splice.apply(l, [ 1, 0 ].concat(l.splice(m, i + 1)));
        }
        d.dequeue();
    };
    /*!
 * jQuery UI Effects Shake 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/shake-effect/
 */
    var S = a.effects.effect.shake = function(b, c) {
        var d = a(this), e = [ "position", "top", "bottom", "left", "right", "height", "width" ], f = a.effects.setMode(d, b.mode || "effect"), g = b.direction || "left", h = b.distance || 20, i = b.times || 3, j = i * 2 + 1, k = Math.round(b.duration / j), l = g === "up" || g === "down" ? "top" : "left", m = g === "up" || g === "left", n = {}, o = {}, p = {}, q, // we will need to re-assemble the queue to stack our animations in place
        r = d.queue(), s = r.length;
        a.effects.save(d, e);
        d.show();
        a.effects.createWrapper(d);
        // Animation
        n[l] = (m ? "-=" : "+=") + h;
        o[l] = (m ? "+=" : "-=") + h * 2;
        p[l] = (m ? "-=" : "+=") + h * 2;
        // Animate
        d.animate(n, k, b.easing);
        // Shakes
        for (q = 1; q < i; q++) {
            d.animate(o, k, b.easing).animate(p, k, b.easing);
        }
        d.animate(o, k, b.easing).animate(n, k / 2, b.easing).queue(function() {
            if (f === "hide") {
                d.hide();
            }
            a.effects.restore(d, e);
            a.effects.removeWrapper(d);
            c();
        });
        // inject all the animations we just queued to be first in line (after "inprogress")
        if (s > 1) {
            r.splice.apply(r, [ 1, 0 ].concat(r.splice(s, j + 1)));
        }
        d.dequeue();
    };
    /*!
 * jQuery UI Effects Slide 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/slide-effect/
 */
    var T = a.effects.effect.slide = function(b, c) {
        // Create element
        var d = a(this), e = [ "position", "top", "bottom", "left", "right", "width", "height" ], f = a.effects.setMode(d, b.mode || "show"), g = f === "show", h = b.direction || "left", i = h === "up" || h === "down" ? "top" : "left", j = h === "up" || h === "left", k, l = {};
        // Adjust
        a.effects.save(d, e);
        d.show();
        k = b.distance || d[i === "top" ? "outerHeight" : "outerWidth"](true);
        a.effects.createWrapper(d).css({
            overflow: "hidden"
        });
        if (g) {
            d.css(i, j ? isNaN(k) ? "-" + k : -k : k);
        }
        // Animation
        l[i] = (g ? j ? "+=" : "-=" : j ? "-=" : "+=") + k;
        // Animate
        d.animate(l, {
            queue: false,
            duration: b.duration,
            easing: b.easing,
            complete: function() {
                if (f === "hide") {
                    d.hide();
                }
                a.effects.restore(d, e);
                a.effects.removeWrapper(d);
                c();
            }
        });
    };
    /*!
 * jQuery UI Effects Transfer 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/transfer-effect/
 */
    var U = a.effects.effect.transfer = function(b, c) {
        var d = a(this), e = a(b.to), f = e.css("position") === "fixed", g = a("body"), h = f ? g.scrollTop() : 0, i = f ? g.scrollLeft() : 0, j = e.offset(), k = {
            top: j.top - h,
            left: j.left - i,
            height: e.innerHeight(),
            width: e.innerWidth()
        }, l = d.offset(), m = a("<div class='ui-effects-transfer'></div>").appendTo(document.body).addClass(b.className).css({
            top: l.top - h,
            left: l.left - i,
            height: d.innerHeight(),
            width: d.innerWidth(),
            position: f ? "fixed" : "absolute"
        }).animate(k, b.duration, b.easing, function() {
            m.remove();
            c();
        });
    };
    /*!
 * jQuery UI Progressbar 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/progressbar/
 */
    var V = a.widget("ui.progressbar", {
        version: "1.11.4",
        options: {
            max: 100,
            value: 0,
            change: null,
            complete: null
        },
        min: 0,
        _create: function() {
            // Constrain initial value
            this.oldValue = this.options.value = this._constrainedValue();
            this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({
                // Only set static values, aria-valuenow and aria-valuemax are
                // set inside _refreshValue()
                role: "progressbar",
                "aria-valuemin": this.min
            });
            this.valueDiv = a("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element);
            this._refreshValue();
        },
        _destroy: function() {
            this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow");
            this.valueDiv.remove();
        },
        value: function(a) {
            if (a === undefined) {
                return this.options.value;
            }
            this.options.value = this._constrainedValue(a);
            this._refreshValue();
        },
        _constrainedValue: function(a) {
            if (a === undefined) {
                a = this.options.value;
            }
            this.indeterminate = a === false;
            // sanitize value
            if (typeof a !== "number") {
                a = 0;
            }
            return this.indeterminate ? false : Math.min(this.options.max, Math.max(this.min, a));
        },
        _setOptions: function(a) {
            // Ensure "value" option is set after other values (like max)
            var b = a.value;
            delete a.value;
            this._super(a);
            this.options.value = this._constrainedValue(b);
            this._refreshValue();
        },
        _setOption: function(a, b) {
            if (a === "max") {
                // Don't allow a max less than min
                b = Math.max(this.min, b);
            }
            if (a === "disabled") {
                this.element.toggleClass("ui-state-disabled", !!b).attr("aria-disabled", b);
            }
            this._super(a, b);
        },
        _percentage: function() {
            return this.indeterminate ? 100 : 100 * (this.options.value - this.min) / (this.options.max - this.min);
        },
        _refreshValue: function() {
            var b = this.options.value, c = this._percentage();
            this.valueDiv.toggle(this.indeterminate || b > this.min).toggleClass("ui-corner-right", b === this.options.max).width(c.toFixed(0) + "%");
            this.element.toggleClass("ui-progressbar-indeterminate", this.indeterminate);
            if (this.indeterminate) {
                this.element.removeAttr("aria-valuenow");
                if (!this.overlayDiv) {
                    this.overlayDiv = a("<div class='ui-progressbar-overlay'></div>").appendTo(this.valueDiv);
                }
            } else {
                this.element.attr({
                    "aria-valuemax": this.options.max,
                    "aria-valuenow": b
                });
                if (this.overlayDiv) {
                    this.overlayDiv.remove();
                    this.overlayDiv = null;
                }
            }
            if (this.oldValue !== b) {
                this.oldValue = b;
                this._trigger("change");
            }
            if (b === this.options.max) {
                this._trigger("complete");
            }
        }
    });
    /*!
 * jQuery UI Selectable 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/selectable/
 */
    var W = a.widget("ui.selectable", a.ui.mouse, {
        version: "1.11.4",
        options: {
            appendTo: "body",
            autoRefresh: true,
            distance: 0,
            filter: "*",
            tolerance: "touch",
            // callbacks
            selected: null,
            selecting: null,
            start: null,
            stop: null,
            unselected: null,
            unselecting: null
        },
        _create: function() {
            var b, c = this;
            this.element.addClass("ui-selectable");
            this.dragged = false;
            // cache selectee children based on filter
            this.refresh = function() {
                b = a(c.options.filter, c.element[0]);
                b.addClass("ui-selectee");
                b.each(function() {
                    var b = a(this), c = b.offset();
                    a.data(this, "selectable-item", {
                        element: this,
                        $element: b,
                        left: c.left,
                        top: c.top,
                        right: c.left + b.outerWidth(),
                        bottom: c.top + b.outerHeight(),
                        startselected: false,
                        selected: b.hasClass("ui-selected"),
                        selecting: b.hasClass("ui-selecting"),
                        unselecting: b.hasClass("ui-unselecting")
                    });
                });
            };
            this.refresh();
            this.selectees = b.addClass("ui-selectee");
            this._mouseInit();
            this.helper = a("<div class='ui-selectable-helper'></div>");
        },
        _destroy: function() {
            this.selectees.removeClass("ui-selectee").removeData("selectable-item");
            this.element.removeClass("ui-selectable ui-selectable-disabled");
            this._mouseDestroy();
        },
        _mouseStart: function(b) {
            var c = this, d = this.options;
            this.opos = [ b.pageX, b.pageY ];
            if (this.options.disabled) {
                return;
            }
            this.selectees = a(d.filter, this.element[0]);
            this._trigger("start", b);
            a(d.appendTo).append(this.helper);
            // position helper (lasso)
            this.helper.css({
                left: b.pageX,
                top: b.pageY,
                width: 0,
                height: 0
            });
            if (d.autoRefresh) {
                this.refresh();
            }
            this.selectees.filter(".ui-selected").each(function() {
                var d = a.data(this, "selectable-item");
                d.startselected = true;
                if (!b.metaKey && !b.ctrlKey) {
                    d.$element.removeClass("ui-selected");
                    d.selected = false;
                    d.$element.addClass("ui-unselecting");
                    d.unselecting = true;
                    // selectable UNSELECTING callback
                    c._trigger("unselecting", b, {
                        unselecting: d.element
                    });
                }
            });
            a(b.target).parents().addBack().each(function() {
                var d, e = a.data(this, "selectable-item");
                if (e) {
                    d = !b.metaKey && !b.ctrlKey || !e.$element.hasClass("ui-selected");
                    e.$element.removeClass(d ? "ui-unselecting" : "ui-selected").addClass(d ? "ui-selecting" : "ui-unselecting");
                    e.unselecting = !d;
                    e.selecting = d;
                    e.selected = d;
                    // selectable (UN)SELECTING callback
                    if (d) {
                        c._trigger("selecting", b, {
                            selecting: e.element
                        });
                    } else {
                        c._trigger("unselecting", b, {
                            unselecting: e.element
                        });
                    }
                    return false;
                }
            });
        },
        _mouseDrag: function(b) {
            this.dragged = true;
            if (this.options.disabled) {
                return;
            }
            var c, d = this, e = this.options, f = this.opos[0], g = this.opos[1], h = b.pageX, i = b.pageY;
            if (f > h) {
                c = h;
                h = f;
                f = c;
            }
            if (g > i) {
                c = i;
                i = g;
                g = c;
            }
            this.helper.css({
                left: f,
                top: g,
                width: h - f,
                height: i - g
            });
            this.selectees.each(function() {
                var c = a.data(this, "selectable-item"), j = false;
                //prevent helper from being selected if appendTo: selectable
                if (!c || c.element === d.element[0]) {
                    return;
                }
                if (e.tolerance === "touch") {
                    j = !(c.left > h || c.right < f || c.top > i || c.bottom < g);
                } else if (e.tolerance === "fit") {
                    j = c.left > f && c.right < h && c.top > g && c.bottom < i;
                }
                if (j) {
                    // SELECT
                    if (c.selected) {
                        c.$element.removeClass("ui-selected");
                        c.selected = false;
                    }
                    if (c.unselecting) {
                        c.$element.removeClass("ui-unselecting");
                        c.unselecting = false;
                    }
                    if (!c.selecting) {
                        c.$element.addClass("ui-selecting");
                        c.selecting = true;
                        // selectable SELECTING callback
                        d._trigger("selecting", b, {
                            selecting: c.element
                        });
                    }
                } else {
                    // UNSELECT
                    if (c.selecting) {
                        if ((b.metaKey || b.ctrlKey) && c.startselected) {
                            c.$element.removeClass("ui-selecting");
                            c.selecting = false;
                            c.$element.addClass("ui-selected");
                            c.selected = true;
                        } else {
                            c.$element.removeClass("ui-selecting");
                            c.selecting = false;
                            if (c.startselected) {
                                c.$element.addClass("ui-unselecting");
                                c.unselecting = true;
                            }
                            // selectable UNSELECTING callback
                            d._trigger("unselecting", b, {
                                unselecting: c.element
                            });
                        }
                    }
                    if (c.selected) {
                        if (!b.metaKey && !b.ctrlKey && !c.startselected) {
                            c.$element.removeClass("ui-selected");
                            c.selected = false;
                            c.$element.addClass("ui-unselecting");
                            c.unselecting = true;
                            // selectable UNSELECTING callback
                            d._trigger("unselecting", b, {
                                unselecting: c.element
                            });
                        }
                    }
                }
            });
            return false;
        },
        _mouseStop: function(b) {
            var c = this;
            this.dragged = false;
            a(".ui-unselecting", this.element[0]).each(function() {
                var d = a.data(this, "selectable-item");
                d.$element.removeClass("ui-unselecting");
                d.unselecting = false;
                d.startselected = false;
                c._trigger("unselected", b, {
                    unselected: d.element
                });
            });
            a(".ui-selecting", this.element[0]).each(function() {
                var d = a.data(this, "selectable-item");
                d.$element.removeClass("ui-selecting").addClass("ui-selected");
                d.selecting = false;
                d.selected = true;
                d.startselected = true;
                c._trigger("selected", b, {
                    selected: d.element
                });
            });
            this._trigger("stop", b);
            this.helper.remove();
            return false;
        }
    });
    /*!
 * jQuery UI Selectmenu 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/selectmenu
 */
    var X = a.widget("ui.selectmenu", {
        version: "1.11.4",
        defaultElement: "<select>",
        options: {
            appendTo: null,
            disabled: null,
            icons: {
                button: "ui-icon-triangle-1-s"
            },
            position: {
                my: "left top",
                at: "left bottom",
                collision: "none"
            },
            width: null,
            // callbacks
            change: null,
            close: null,
            focus: null,
            open: null,
            select: null
        },
        _create: function() {
            var a = this.element.uniqueId().attr("id");
            this.ids = {
                element: a,
                button: a + "-button",
                menu: a + "-menu"
            };
            this._drawButton();
            this._drawMenu();
            if (this.options.disabled) {
                this.disable();
            }
        },
        _drawButton: function() {
            var b = this;
            // Associate existing label with the new button
            this.label = a("label[for='" + this.ids.element + "']").attr("for", this.ids.button);
            this._on(this.label, {
                click: function(a) {
                    this.button.focus();
                    a.preventDefault();
                }
            });
            // Hide original select element
            this.element.hide();
            // Create button
            this.button = a("<span>", {
                "class": "ui-selectmenu-button ui-widget ui-state-default ui-corner-all",
                tabindex: this.options.disabled ? -1 : 0,
                id: this.ids.button,
                role: "combobox",
                "aria-expanded": "false",
                "aria-autocomplete": "list",
                "aria-owns": this.ids.menu,
                "aria-haspopup": "true"
            }).insertAfter(this.element);
            a("<span>", {
                "class": "ui-icon " + this.options.icons.button
            }).prependTo(this.button);
            this.buttonText = a("<span>", {
                "class": "ui-selectmenu-text"
            }).appendTo(this.button);
            this._setText(this.buttonText, this.element.find("option:selected").text());
            this._resizeButton();
            this._on(this.button, this._buttonEvents);
            this.button.one("focusin", function() {
                // Delay rendering the menu items until the button receives focus.
                // The menu may have already been rendered via a programmatic open.
                if (!b.menuItems) {
                    b._refreshMenu();
                }
            });
            this._hoverable(this.button);
            this._focusable(this.button);
        },
        _drawMenu: function() {
            var b = this;
            // Create menu
            this.menu = a("<ul>", {
                "aria-hidden": "true",
                "aria-labelledby": this.ids.button,
                id: this.ids.menu
            });
            // Wrap menu
            this.menuWrap = a("<div>", {
                "class": "ui-selectmenu-menu ui-front"
            }).append(this.menu).appendTo(this._appendTo());
            // Initialize menu widget
            this.menuInstance = this.menu.menu({
                role: "listbox",
                select: function(a, c) {
                    a.preventDefault();
                    // support: IE8
                    // If the item was selected via a click, the text selection
                    // will be destroyed in IE
                    b._setSelection();
                    b._select(c.item.data("ui-selectmenu-item"), a);
                },
                focus: function(a, c) {
                    var d = c.item.data("ui-selectmenu-item");
                    // Prevent inital focus from firing and check if its a newly focused item
                    if (b.focusIndex != null && d.index !== b.focusIndex) {
                        b._trigger("focus", a, {
                            item: d
                        });
                        if (!b.isOpen) {
                            b._select(d, a);
                        }
                    }
                    b.focusIndex = d.index;
                    b.button.attr("aria-activedescendant", b.menuItems.eq(d.index).attr("id"));
                }
            }).menu("instance");
            // Adjust menu styles to dropdown
            this.menu.addClass("ui-corner-bottom").removeClass("ui-corner-all");
            // Don't close the menu on mouseleave
            this.menuInstance._off(this.menu, "mouseleave");
            // Cancel the menu's collapseAll on document click
            this.menuInstance._closeOnDocumentClick = function() {
                return false;
            };
            // Selects often contain empty items, but never contain dividers
            this.menuInstance._isDivider = function() {
                return false;
            };
        },
        refresh: function() {
            this._refreshMenu();
            this._setText(this.buttonText, this._getSelectedItem().text());
            if (!this.options.width) {
                this._resizeButton();
            }
        },
        _refreshMenu: function() {
            this.menu.empty();
            var a, b = this.element.find("option");
            if (!b.length) {
                return;
            }
            this._parseOptions(b);
            this._renderMenu(this.menu, this.items);
            this.menuInstance.refresh();
            this.menuItems = this.menu.find("li").not(".ui-selectmenu-optgroup");
            a = this._getSelectedItem();
            // Update the menu to have the correct item focused
            this.menuInstance.focus(null, a);
            this._setAria(a.data("ui-selectmenu-item"));
            // Set disabled state
            this._setOption("disabled", this.element.prop("disabled"));
        },
        open: function(a) {
            if (this.options.disabled) {
                return;
            }
            // If this is the first time the menu is being opened, render the items
            if (!this.menuItems) {
                this._refreshMenu();
            } else {
                // Menu clears focus on close, reset focus to selected item
                this.menu.find(".ui-state-focus").removeClass("ui-state-focus");
                this.menuInstance.focus(null, this._getSelectedItem());
            }
            this.isOpen = true;
            this._toggleAttr();
            this._resizeMenu();
            this._position();
            this._on(this.document, this._documentClick);
            this._trigger("open", a);
        },
        _position: function() {
            this.menuWrap.position(a.extend({
                of: this.button
            }, this.options.position));
        },
        close: function(a) {
            if (!this.isOpen) {
                return;
            }
            this.isOpen = false;
            this._toggleAttr();
            this.range = null;
            this._off(this.document);
            this._trigger("close", a);
        },
        widget: function() {
            return this.button;
        },
        menuWidget: function() {
            return this.menu;
        },
        _renderMenu: function(b, c) {
            var d = this, e = "";
            a.each(c, function(c, f) {
                if (f.optgroup !== e) {
                    a("<li>", {
                        "class": "ui-selectmenu-optgroup ui-menu-divider" + (f.element.parent("optgroup").prop("disabled") ? " ui-state-disabled" : ""),
                        text: f.optgroup
                    }).appendTo(b);
                    e = f.optgroup;
                }
                d._renderItemData(b, f);
            });
        },
        _renderItemData: function(a, b) {
            return this._renderItem(a, b).data("ui-selectmenu-item", b);
        },
        _renderItem: function(b, c) {
            var d = a("<li>");
            if (c.disabled) {
                d.addClass("ui-state-disabled");
            }
            this._setText(d, c.label);
            return d.appendTo(b);
        },
        _setText: function(a, b) {
            if (b) {
                a.text(b);
            } else {
                a.html("&#160;");
            }
        },
        _move: function(a, b) {
            var c, d, e = ".ui-menu-item";
            if (this.isOpen) {
                c = this.menuItems.eq(this.focusIndex);
            } else {
                c = this.menuItems.eq(this.element[0].selectedIndex);
                e += ":not(.ui-state-disabled)";
            }
            if (a === "first" || a === "last") {
                d = c[a === "first" ? "prevAll" : "nextAll"](e).eq(-1);
            } else {
                d = c[a + "All"](e).eq(0);
            }
            if (d.length) {
                this.menuInstance.focus(b, d);
            }
        },
        _getSelectedItem: function() {
            return this.menuItems.eq(this.element[0].selectedIndex);
        },
        _toggle: function(a) {
            this[this.isOpen ? "close" : "open"](a);
        },
        _setSelection: function() {
            var a;
            if (!this.range) {
                return;
            }
            if (window.getSelection) {
                a = window.getSelection();
                a.removeAllRanges();
                a.addRange(this.range);
            } else {
                this.range.select();
            }
            // support: IE
            // Setting the text selection kills the button focus in IE, but
            // restoring the focus doesn't kill the selection.
            this.button.focus();
        },
        _documentClick: {
            mousedown: function(b) {
                if (!this.isOpen) {
                    return;
                }
                if (!a(b.target).closest(".ui-selectmenu-menu, #" + this.ids.button).length) {
                    this.close(b);
                }
            }
        },
        _buttonEvents: {
            // Prevent text selection from being reset when interacting with the selectmenu (#10144)
            mousedown: function() {
                var a;
                if (window.getSelection) {
                    a = window.getSelection();
                    if (a.rangeCount) {
                        this.range = a.getRangeAt(0);
                    }
                } else {
                    this.range = document.selection.createRange();
                }
            },
            click: function(a) {
                this._setSelection();
                this._toggle(a);
            },
            keydown: function(b) {
                var c = true;
                switch (b.keyCode) {
                  case a.ui.keyCode.TAB:
                  case a.ui.keyCode.ESCAPE:
                    this.close(b);
                    c = false;
                    break;

                  case a.ui.keyCode.ENTER:
                    if (this.isOpen) {
                        this._selectFocusedItem(b);
                    }
                    break;

                  case a.ui.keyCode.UP:
                    if (b.altKey) {
                        this._toggle(b);
                    } else {
                        this._move("prev", b);
                    }
                    break;

                  case a.ui.keyCode.DOWN:
                    if (b.altKey) {
                        this._toggle(b);
                    } else {
                        this._move("next", b);
                    }
                    break;

                  case a.ui.keyCode.SPACE:
                    if (this.isOpen) {
                        this._selectFocusedItem(b);
                    } else {
                        this._toggle(b);
                    }
                    break;

                  case a.ui.keyCode.LEFT:
                    this._move("prev", b);
                    break;

                  case a.ui.keyCode.RIGHT:
                    this._move("next", b);
                    break;

                  case a.ui.keyCode.HOME:
                  case a.ui.keyCode.PAGE_UP:
                    this._move("first", b);
                    break;

                  case a.ui.keyCode.END:
                  case a.ui.keyCode.PAGE_DOWN:
                    this._move("last", b);
                    break;

                  default:
                    this.menu.trigger(b);
                    c = false;
                }
                if (c) {
                    b.preventDefault();
                }
            }
        },
        _selectFocusedItem: function(a) {
            var b = this.menuItems.eq(this.focusIndex);
            if (!b.hasClass("ui-state-disabled")) {
                this._select(b.data("ui-selectmenu-item"), a);
            }
        },
        _select: function(a, b) {
            var c = this.element[0].selectedIndex;
            // Change native select element
            this.element[0].selectedIndex = a.index;
            this._setText(this.buttonText, a.label);
            this._setAria(a);
            this._trigger("select", b, {
                item: a
            });
            if (a.index !== c) {
                this._trigger("change", b, {
                    item: a
                });
            }
            this.close(b);
        },
        _setAria: function(a) {
            var b = this.menuItems.eq(a.index).attr("id");
            this.button.attr({
                "aria-labelledby": b,
                "aria-activedescendant": b
            });
            this.menu.attr("aria-activedescendant", b);
        },
        _setOption: function(a, b) {
            if (a === "icons") {
                this.button.find("span.ui-icon").removeClass(this.options.icons.button).addClass(b.button);
            }
            this._super(a, b);
            if (a === "appendTo") {
                this.menuWrap.appendTo(this._appendTo());
            }
            if (a === "disabled") {
                this.menuInstance.option("disabled", b);
                this.button.toggleClass("ui-state-disabled", b).attr("aria-disabled", b);
                this.element.prop("disabled", b);
                if (b) {
                    this.button.attr("tabindex", -1);
                    this.close();
                } else {
                    this.button.attr("tabindex", 0);
                }
            }
            if (a === "width") {
                this._resizeButton();
            }
        },
        _appendTo: function() {
            var b = this.options.appendTo;
            if (b) {
                b = b.jquery || b.nodeType ? a(b) : this.document.find(b).eq(0);
            }
            if (!b || !b[0]) {
                b = this.element.closest(".ui-front");
            }
            if (!b.length) {
                b = this.document[0].body;
            }
            return b;
        },
        _toggleAttr: function() {
            this.button.toggleClass("ui-corner-top", this.isOpen).toggleClass("ui-corner-all", !this.isOpen).attr("aria-expanded", this.isOpen);
            this.menuWrap.toggleClass("ui-selectmenu-open", this.isOpen);
            this.menu.attr("aria-hidden", !this.isOpen);
        },
        _resizeButton: function() {
            var a = this.options.width;
            if (!a) {
                a = this.element.show().outerWidth();
                this.element.hide();
            }
            this.button.outerWidth(a);
        },
        _resizeMenu: function() {
            this.menu.outerWidth(Math.max(this.button.outerWidth(), // support: IE10
            // IE10 wraps long text (possibly a rounding bug)
            // so we add 1px to avoid the wrapping
            this.menu.width("").outerWidth() + 1));
        },
        _getCreateOptions: function() {
            return {
                disabled: this.element.prop("disabled")
            };
        },
        _parseOptions: function(b) {
            var c = [];
            b.each(function(b, d) {
                var e = a(d), f = e.parent("optgroup");
                c.push({
                    element: e,
                    index: b,
                    value: e.val(),
                    label: e.text(),
                    optgroup: f.attr("label") || "",
                    disabled: f.prop("disabled") || e.prop("disabled")
                });
            });
            this.items = c;
        },
        _destroy: function() {
            this.menuWrap.remove();
            this.button.remove();
            this.element.show();
            this.element.removeUniqueId();
            this.label.attr("for", this.ids.element);
        }
    });
    /*!
 * jQuery UI Slider 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/slider/
 */
    var Y = a.widget("ui.slider", a.ui.mouse, {
        version: "1.11.4",
        widgetEventPrefix: "slide",
        options: {
            animate: false,
            distance: 0,
            max: 100,
            min: 0,
            orientation: "horizontal",
            range: false,
            step: 1,
            value: 0,
            values: null,
            // callbacks
            change: null,
            slide: null,
            start: null,
            stop: null
        },
        // number of pages in a slider
        // (how many times can you page up/down to go through the whole range)
        numPages: 5,
        _create: function() {
            this._keySliding = false;
            this._mouseSliding = false;
            this._animateOff = true;
            this._handleIndex = null;
            this._detectOrientation();
            this._mouseInit();
            this._calculateNewMax();
            this.element.addClass("ui-slider" + " ui-slider-" + this.orientation + " ui-widget" + " ui-widget-content" + " ui-corner-all");
            this._refresh();
            this._setOption("disabled", this.options.disabled);
            this._animateOff = false;
        },
        _refresh: function() {
            this._createRange();
            this._createHandles();
            this._setupEvents();
            this._refreshValue();
        },
        _createHandles: function() {
            var b, c, d = this.options, e = this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"), f = "<span class='ui-slider-handle ui-state-default ui-corner-all' tabindex='0'></span>", g = [];
            c = d.values && d.values.length || 1;
            if (e.length > c) {
                e.slice(c).remove();
                e = e.slice(0, c);
            }
            for (b = e.length; b < c; b++) {
                g.push(f);
            }
            this.handles = e.add(a(g.join("")).appendTo(this.element));
            this.handle = this.handles.eq(0);
            this.handles.each(function(b) {
                a(this).data("ui-slider-handle-index", b);
            });
        },
        _createRange: function() {
            var b = this.options, c = "";
            if (b.range) {
                if (b.range === true) {
                    if (!b.values) {
                        b.values = [ this._valueMin(), this._valueMin() ];
                    } else if (b.values.length && b.values.length !== 2) {
                        b.values = [ b.values[0], b.values[0] ];
                    } else if (a.isArray(b.values)) {
                        b.values = b.values.slice(0);
                    }
                }
                if (!this.range || !this.range.length) {
                    this.range = a("<div></div>").appendTo(this.element);
                    c = "ui-slider-range" + // note: this isn't the most fittingly semantic framework class for this element,
                    // but worked best visually with a variety of themes
                    " ui-widget-header ui-corner-all";
                } else {
                    this.range.removeClass("ui-slider-range-min ui-slider-range-max").css({
                        left: "",
                        bottom: ""
                    });
                }
                this.range.addClass(c + (b.range === "min" || b.range === "max" ? " ui-slider-range-" + b.range : ""));
            } else {
                if (this.range) {
                    this.range.remove();
                }
                this.range = null;
            }
        },
        _setupEvents: function() {
            this._off(this.handles);
            this._on(this.handles, this._handleEvents);
            this._hoverable(this.handles);
            this._focusable(this.handles);
        },
        _destroy: function() {
            this.handles.remove();
            if (this.range) {
                this.range.remove();
            }
            this.element.removeClass("ui-slider" + " ui-slider-horizontal" + " ui-slider-vertical" + " ui-widget" + " ui-widget-content" + " ui-corner-all");
            this._mouseDestroy();
        },
        _mouseCapture: function(b) {
            var c, d, e, f, g, h, i, j, k = this, l = this.options;
            if (l.disabled) {
                return false;
            }
            this.elementSize = {
                width: this.element.outerWidth(),
                height: this.element.outerHeight()
            };
            this.elementOffset = this.element.offset();
            c = {
                x: b.pageX,
                y: b.pageY
            };
            d = this._normValueFromMouse(c);
            e = this._valueMax() - this._valueMin() + 1;
            this.handles.each(function(b) {
                var c = Math.abs(d - k.values(b));
                if (e > c || e === c && (b === k._lastChangedValue || k.values(b) === l.min)) {
                    e = c;
                    f = a(this);
                    g = b;
                }
            });
            h = this._start(b, g);
            if (h === false) {
                return false;
            }
            this._mouseSliding = true;
            this._handleIndex = g;
            f.addClass("ui-state-active").focus();
            i = f.offset();
            j = !a(b.target).parents().addBack().is(".ui-slider-handle");
            this._clickOffset = j ? {
                left: 0,
                top: 0
            } : {
                left: b.pageX - i.left - f.width() / 2,
                top: b.pageY - i.top - f.height() / 2 - (parseInt(f.css("borderTopWidth"), 10) || 0) - (parseInt(f.css("borderBottomWidth"), 10) || 0) + (parseInt(f.css("marginTop"), 10) || 0)
            };
            if (!this.handles.hasClass("ui-state-hover")) {
                this._slide(b, g, d);
            }
            this._animateOff = true;
            return true;
        },
        _mouseStart: function() {
            return true;
        },
        _mouseDrag: function(a) {
            var b = {
                x: a.pageX,
                y: a.pageY
            }, c = this._normValueFromMouse(b);
            this._slide(a, this._handleIndex, c);
            return false;
        },
        _mouseStop: function(a) {
            this.handles.removeClass("ui-state-active");
            this._mouseSliding = false;
            this._stop(a, this._handleIndex);
            this._change(a, this._handleIndex);
            this._handleIndex = null;
            this._clickOffset = null;
            this._animateOff = false;
            return false;
        },
        _detectOrientation: function() {
            this.orientation = this.options.orientation === "vertical" ? "vertical" : "horizontal";
        },
        _normValueFromMouse: function(a) {
            var b, c, d, e, f;
            if (this.orientation === "horizontal") {
                b = this.elementSize.width;
                c = a.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0);
            } else {
                b = this.elementSize.height;
                c = a.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0);
            }
            d = c / b;
            if (d > 1) {
                d = 1;
            }
            if (d < 0) {
                d = 0;
            }
            if (this.orientation === "vertical") {
                d = 1 - d;
            }
            e = this._valueMax() - this._valueMin();
            f = this._valueMin() + d * e;
            return this._trimAlignValue(f);
        },
        _start: function(a, b) {
            var c = {
                handle: this.handles[b],
                value: this.value()
            };
            if (this.options.values && this.options.values.length) {
                c.value = this.values(b);
                c.values = this.values();
            }
            return this._trigger("start", a, c);
        },
        _slide: function(a, b, c) {
            var d, e, f;
            if (this.options.values && this.options.values.length) {
                d = this.values(b ? 0 : 1);
                if (this.options.values.length === 2 && this.options.range === true && (b === 0 && c > d || b === 1 && c < d)) {
                    c = d;
                }
                if (c !== this.values(b)) {
                    e = this.values();
                    e[b] = c;
                    // A slide can be canceled by returning false from the slide callback
                    f = this._trigger("slide", a, {
                        handle: this.handles[b],
                        value: c,
                        values: e
                    });
                    d = this.values(b ? 0 : 1);
                    if (f !== false) {
                        this.values(b, c);
                    }
                }
            } else {
                if (c !== this.value()) {
                    // A slide can be canceled by returning false from the slide callback
                    f = this._trigger("slide", a, {
                        handle: this.handles[b],
                        value: c
                    });
                    if (f !== false) {
                        this.value(c);
                    }
                }
            }
        },
        _stop: function(a, b) {
            var c = {
                handle: this.handles[b],
                value: this.value()
            };
            if (this.options.values && this.options.values.length) {
                c.value = this.values(b);
                c.values = this.values();
            }
            this._trigger("stop", a, c);
        },
        _change: function(a, b) {
            if (!this._keySliding && !this._mouseSliding) {
                var c = {
                    handle: this.handles[b],
                    value: this.value()
                };
                if (this.options.values && this.options.values.length) {
                    c.value = this.values(b);
                    c.values = this.values();
                }
                //store the last changed value index for reference when handles overlap
                this._lastChangedValue = b;
                this._trigger("change", a, c);
            }
        },
        value: function(a) {
            if (arguments.length) {
                this.options.value = this._trimAlignValue(a);
                this._refreshValue();
                this._change(null, 0);
                return;
            }
            return this._value();
        },
        values: function(b, c) {
            var d, e, f;
            if (arguments.length > 1) {
                this.options.values[b] = this._trimAlignValue(c);
                this._refreshValue();
                this._change(null, b);
                return;
            }
            if (arguments.length) {
                if (a.isArray(arguments[0])) {
                    d = this.options.values;
                    e = arguments[0];
                    for (f = 0; f < d.length; f += 1) {
                        d[f] = this._trimAlignValue(e[f]);
                        this._change(null, f);
                    }
                    this._refreshValue();
                } else {
                    if (this.options.values && this.options.values.length) {
                        return this._values(b);
                    } else {
                        return this.value();
                    }
                }
            } else {
                return this._values();
            }
        },
        _setOption: function(b, c) {
            var d, e = 0;
            if (b === "range" && this.options.range === true) {
                if (c === "min") {
                    this.options.value = this._values(0);
                    this.options.values = null;
                } else if (c === "max") {
                    this.options.value = this._values(this.options.values.length - 1);
                    this.options.values = null;
                }
            }
            if (a.isArray(this.options.values)) {
                e = this.options.values.length;
            }
            if (b === "disabled") {
                this.element.toggleClass("ui-state-disabled", !!c);
            }
            this._super(b, c);
            switch (b) {
              case "orientation":
                this._detectOrientation();
                this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-" + this.orientation);
                this._refreshValue();
                // Reset positioning from previous orientation
                this.handles.css(c === "horizontal" ? "bottom" : "left", "");
                break;

              case "value":
                this._animateOff = true;
                this._refreshValue();
                this._change(null, 0);
                this._animateOff = false;
                break;

              case "values":
                this._animateOff = true;
                this._refreshValue();
                for (d = 0; d < e; d += 1) {
                    this._change(null, d);
                }
                this._animateOff = false;
                break;

              case "step":
              case "min":
              case "max":
                this._animateOff = true;
                this._calculateNewMax();
                this._refreshValue();
                this._animateOff = false;
                break;

              case "range":
                this._animateOff = true;
                this._refresh();
                this._animateOff = false;
                break;
            }
        },
        //internal value getter
        // _value() returns value trimmed by min and max, aligned by step
        _value: function() {
            var a = this.options.value;
            a = this._trimAlignValue(a);
            return a;
        },
        //internal values getter
        // _values() returns array of values trimmed by min and max, aligned by step
        // _values( index ) returns single value trimmed by min and max, aligned by step
        _values: function(a) {
            var b, c, d;
            if (arguments.length) {
                b = this.options.values[a];
                b = this._trimAlignValue(b);
                return b;
            } else if (this.options.values && this.options.values.length) {
                // .slice() creates a copy of the array
                // this copy gets trimmed by min and max and then returned
                c = this.options.values.slice();
                for (d = 0; d < c.length; d += 1) {
                    c[d] = this._trimAlignValue(c[d]);
                }
                return c;
            } else {
                return [];
            }
        },
        // returns the step-aligned value that val is closest to, between (inclusive) min and max
        _trimAlignValue: function(a) {
            if (a <= this._valueMin()) {
                return this._valueMin();
            }
            if (a >= this._valueMax()) {
                return this._valueMax();
            }
            var b = this.options.step > 0 ? this.options.step : 1, c = (a - this._valueMin()) % b, d = a - c;
            if (Math.abs(c) * 2 >= b) {
                d += c > 0 ? b : -b;
            }
            // Since JavaScript has problems with large floats, round
            // the final value to 5 digits after the decimal point (see #4124)
            return parseFloat(d.toFixed(5));
        },
        _calculateNewMax: function() {
            var a = this.options.max, b = this._valueMin(), c = this.options.step, d = Math.floor(+(a - b).toFixed(this._precision()) / c) * c;
            a = d + b;
            this.max = parseFloat(a.toFixed(this._precision()));
        },
        _precision: function() {
            var a = this._precisionOf(this.options.step);
            if (this.options.min !== null) {
                a = Math.max(a, this._precisionOf(this.options.min));
            }
            return a;
        },
        _precisionOf: function(a) {
            var b = a.toString(), c = b.indexOf(".");
            return c === -1 ? 0 : b.length - c - 1;
        },
        _valueMin: function() {
            return this.options.min;
        },
        _valueMax: function() {
            return this.max;
        },
        _refreshValue: function() {
            var b, c, d, e, f, g = this.options.range, h = this.options, i = this, j = !this._animateOff ? h.animate : false, k = {};
            if (this.options.values && this.options.values.length) {
                this.handles.each(function(d) {
                    c = (i.values(d) - i._valueMin()) / (i._valueMax() - i._valueMin()) * 100;
                    k[i.orientation === "horizontal" ? "left" : "bottom"] = c + "%";
                    a(this).stop(1, 1)[j ? "animate" : "css"](k, h.animate);
                    if (i.options.range === true) {
                        if (i.orientation === "horizontal") {
                            if (d === 0) {
                                i.range.stop(1, 1)[j ? "animate" : "css"]({
                                    left: c + "%"
                                }, h.animate);
                            }
                            if (d === 1) {
                                i.range[j ? "animate" : "css"]({
                                    width: c - b + "%"
                                }, {
                                    queue: false,
                                    duration: h.animate
                                });
                            }
                        } else {
                            if (d === 0) {
                                i.range.stop(1, 1)[j ? "animate" : "css"]({
                                    bottom: c + "%"
                                }, h.animate);
                            }
                            if (d === 1) {
                                i.range[j ? "animate" : "css"]({
                                    height: c - b + "%"
                                }, {
                                    queue: false,
                                    duration: h.animate
                                });
                            }
                        }
                    }
                    b = c;
                });
            } else {
                d = this.value();
                e = this._valueMin();
                f = this._valueMax();
                c = f !== e ? (d - e) / (f - e) * 100 : 0;
                k[this.orientation === "horizontal" ? "left" : "bottom"] = c + "%";
                this.handle.stop(1, 1)[j ? "animate" : "css"](k, h.animate);
                if (g === "min" && this.orientation === "horizontal") {
                    this.range.stop(1, 1)[j ? "animate" : "css"]({
                        width: c + "%"
                    }, h.animate);
                }
                if (g === "max" && this.orientation === "horizontal") {
                    this.range[j ? "animate" : "css"]({
                        width: 100 - c + "%"
                    }, {
                        queue: false,
                        duration: h.animate
                    });
                }
                if (g === "min" && this.orientation === "vertical") {
                    this.range.stop(1, 1)[j ? "animate" : "css"]({
                        height: c + "%"
                    }, h.animate);
                }
                if (g === "max" && this.orientation === "vertical") {
                    this.range[j ? "animate" : "css"]({
                        height: 100 - c + "%"
                    }, {
                        queue: false,
                        duration: h.animate
                    });
                }
            }
        },
        _handleEvents: {
            keydown: function(b) {
                var c, d, e, f, g = a(b.target).data("ui-slider-handle-index");
                switch (b.keyCode) {
                  case a.ui.keyCode.HOME:
                  case a.ui.keyCode.END:
                  case a.ui.keyCode.PAGE_UP:
                  case a.ui.keyCode.PAGE_DOWN:
                  case a.ui.keyCode.UP:
                  case a.ui.keyCode.RIGHT:
                  case a.ui.keyCode.DOWN:
                  case a.ui.keyCode.LEFT:
                    b.preventDefault();
                    if (!this._keySliding) {
                        this._keySliding = true;
                        a(b.target).addClass("ui-state-active");
                        c = this._start(b, g);
                        if (c === false) {
                            return;
                        }
                    }
                    break;
                }
                f = this.options.step;
                if (this.options.values && this.options.values.length) {
                    d = e = this.values(g);
                } else {
                    d = e = this.value();
                }
                switch (b.keyCode) {
                  case a.ui.keyCode.HOME:
                    e = this._valueMin();
                    break;

                  case a.ui.keyCode.END:
                    e = this._valueMax();
                    break;

                  case a.ui.keyCode.PAGE_UP:
                    e = this._trimAlignValue(d + (this._valueMax() - this._valueMin()) / this.numPages);
                    break;

                  case a.ui.keyCode.PAGE_DOWN:
                    e = this._trimAlignValue(d - (this._valueMax() - this._valueMin()) / this.numPages);
                    break;

                  case a.ui.keyCode.UP:
                  case a.ui.keyCode.RIGHT:
                    if (d === this._valueMax()) {
                        return;
                    }
                    e = this._trimAlignValue(d + f);
                    break;

                  case a.ui.keyCode.DOWN:
                  case a.ui.keyCode.LEFT:
                    if (d === this._valueMin()) {
                        return;
                    }
                    e = this._trimAlignValue(d - f);
                    break;
                }
                this._slide(b, g, e);
            },
            keyup: function(b) {
                var c = a(b.target).data("ui-slider-handle-index");
                if (this._keySliding) {
                    this._keySliding = false;
                    this._stop(b, c);
                    this._change(b, c);
                    a(b.target).removeClass("ui-state-active");
                }
            }
        }
    });
    /*!
 * jQuery UI Sortable 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/sortable/
 */
    var Z = a.widget("ui.sortable", a.ui.mouse, {
        version: "1.11.4",
        widgetEventPrefix: "sort",
        ready: false,
        options: {
            appendTo: "parent",
            axis: false,
            connectWith: false,
            containment: false,
            cursor: "auto",
            cursorAt: false,
            dropOnEmpty: true,
            forcePlaceholderSize: false,
            forceHelperSize: false,
            grid: false,
            handle: false,
            helper: "original",
            items: "> *",
            opacity: false,
            placeholder: false,
            revert: false,
            scroll: true,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            scope: "default",
            tolerance: "intersect",
            zIndex: 1e3,
            // callbacks
            activate: null,
            beforeStop: null,
            change: null,
            deactivate: null,
            out: null,
            over: null,
            receive: null,
            remove: null,
            sort: null,
            start: null,
            stop: null,
            update: null
        },
        _isOverAxis: function(a, b, c) {
            return a >= b && a < b + c;
        },
        _isFloating: function(a) {
            return /left|right/.test(a.css("float")) || /inline|table-cell/.test(a.css("display"));
        },
        _create: function() {
            this.containerCache = {};
            this.element.addClass("ui-sortable");
            //Get the items
            this.refresh();
            //Let's determine the parent's offset
            this.offset = this.element.offset();
            //Initialize mouse events for interaction
            this._mouseInit();
            this._setHandleClassName();
            //We're ready to go
            this.ready = true;
        },
        _setOption: function(a, b) {
            this._super(a, b);
            if (a === "handle") {
                this._setHandleClassName();
            }
        },
        _setHandleClassName: function() {
            this.element.find(".ui-sortable-handle").removeClass("ui-sortable-handle");
            a.each(this.items, function() {
                (this.instance.options.handle ? this.item.find(this.instance.options.handle) : this.item).addClass("ui-sortable-handle");
            });
        },
        _destroy: function() {
            this.element.removeClass("ui-sortable ui-sortable-disabled").find(".ui-sortable-handle").removeClass("ui-sortable-handle");
            this._mouseDestroy();
            for (var a = this.items.length - 1; a >= 0; a--) {
                this.items[a].item.removeData(this.widgetName + "-item");
            }
            return this;
        },
        _mouseCapture: function(b, c) {
            var d = null, e = false, f = this;
            if (this.reverting) {
                return false;
            }
            if (this.options.disabled || this.options.type === "static") {
                return false;
            }
            //We have to refresh the items data once first
            this._refreshItems(b);
            //Find out if the clicked node (or one of its parents) is a actual item in this.items
            a(b.target).parents().each(function() {
                if (a.data(this, f.widgetName + "-item") === f) {
                    d = a(this);
                    return false;
                }
            });
            if (a.data(b.target, f.widgetName + "-item") === f) {
                d = a(b.target);
            }
            if (!d) {
                return false;
            }
            if (this.options.handle && !c) {
                a(this.options.handle, d).find("*").addBack().each(function() {
                    if (this === b.target) {
                        e = true;
                    }
                });
                if (!e) {
                    return false;
                }
            }
            this.currentItem = d;
            this._removeCurrentsFromItems();
            return true;
        },
        _mouseStart: function(b, c, d) {
            var e, f, g = this.options;
            this.currentContainer = this;
            //We only need to call refreshPositions, because the refreshItems call has been moved to mouseCapture
            this.refreshPositions();
            //Create and append the visible helper
            this.helper = this._createHelper(b);
            //Cache the helper size
            this._cacheHelperProportions();
            /*
		 * - Position generation -
		 * This block generates everything position related - it's the core of draggables.
		 */
            //Cache the margins of the original element
            this._cacheMargins();
            //Get the next scrolling parent
            this.scrollParent = this.helper.scrollParent();
            //The element's absolute position on the page minus margins
            this.offset = this.currentItem.offset();
            this.offset = {
                top: this.offset.top - this.margins.top,
                left: this.offset.left - this.margins.left
            };
            a.extend(this.offset, {
                click: {
                    //Where the click happened, relative to the element
                    left: b.pageX - this.offset.left,
                    top: b.pageY - this.offset.top
                },
                parent: this._getParentOffset(),
                relative: this._getRelativeOffset()
            });
            // Only after we got the offset, we can change the helper's position to absolute
            // TODO: Still need to figure out a way to make relative sorting possible
            this.helper.css("position", "absolute");
            this.cssPosition = this.helper.css("position");
            //Generate the original position
            this.originalPosition = this._generatePosition(b);
            this.originalPageX = b.pageX;
            this.originalPageY = b.pageY;
            //Adjust the mouse offset relative to the helper if "cursorAt" is supplied
            g.cursorAt && this._adjustOffsetFromHelper(g.cursorAt);
            //Cache the former DOM position
            this.domPosition = {
                prev: this.currentItem.prev()[0],
                parent: this.currentItem.parent()[0]
            };
            //If the helper is not the original, hide the original so it's not playing any role during the drag, won't cause anything bad this way
            if (this.helper[0] !== this.currentItem[0]) {
                this.currentItem.hide();
            }
            //Create the placeholder
            this._createPlaceholder();
            //Set a containment if given in the options
            if (g.containment) {
                this._setContainment();
            }
            if (g.cursor && g.cursor !== "auto") {
                // cursor option
                f = this.document.find("body");
                // support: IE
                this.storedCursor = f.css("cursor");
                f.css("cursor", g.cursor);
                this.storedStylesheet = a("<style>*{ cursor: " + g.cursor + " !important; }</style>").appendTo(f);
            }
            if (g.opacity) {
                // opacity option
                if (this.helper.css("opacity")) {
                    this._storedOpacity = this.helper.css("opacity");
                }
                this.helper.css("opacity", g.opacity);
            }
            if (g.zIndex) {
                // zIndex option
                if (this.helper.css("zIndex")) {
                    this._storedZIndex = this.helper.css("zIndex");
                }
                this.helper.css("zIndex", g.zIndex);
            }
            //Prepare scrolling
            if (this.scrollParent[0] !== this.document[0] && this.scrollParent[0].tagName !== "HTML") {
                this.overflowOffset = this.scrollParent.offset();
            }
            //Call callbacks
            this._trigger("start", b, this._uiHash());
            //Recache the helper size
            if (!this._preserveHelperProportions) {
                this._cacheHelperProportions();
            }
            //Post "activate" events to possible containers
            if (!d) {
                for (e = this.containers.length - 1; e >= 0; e--) {
                    this.containers[e]._trigger("activate", b, this._uiHash(this));
                }
            }
            //Prepare possible droppables
            if (a.ui.ddmanager) {
                a.ui.ddmanager.current = this;
            }
            if (a.ui.ddmanager && !g.dropBehaviour) {
                a.ui.ddmanager.prepareOffsets(this, b);
            }
            this.dragging = true;
            this.helper.addClass("ui-sortable-helper");
            this._mouseDrag(b);
            //Execute the drag once - this causes the helper not to be visible before getting its correct position
            return true;
        },
        _mouseDrag: function(b) {
            var c, d, e, f, g = this.options, h = false;
            //Compute the helpers position
            this.position = this._generatePosition(b);
            this.positionAbs = this._convertPositionTo("absolute");
            if (!this.lastPositionAbs) {
                this.lastPositionAbs = this.positionAbs;
            }
            //Do scrolling
            if (this.options.scroll) {
                if (this.scrollParent[0] !== this.document[0] && this.scrollParent[0].tagName !== "HTML") {
                    if (this.overflowOffset.top + this.scrollParent[0].offsetHeight - b.pageY < g.scrollSensitivity) {
                        this.scrollParent[0].scrollTop = h = this.scrollParent[0].scrollTop + g.scrollSpeed;
                    } else if (b.pageY - this.overflowOffset.top < g.scrollSensitivity) {
                        this.scrollParent[0].scrollTop = h = this.scrollParent[0].scrollTop - g.scrollSpeed;
                    }
                    if (this.overflowOffset.left + this.scrollParent[0].offsetWidth - b.pageX < g.scrollSensitivity) {
                        this.scrollParent[0].scrollLeft = h = this.scrollParent[0].scrollLeft + g.scrollSpeed;
                    } else if (b.pageX - this.overflowOffset.left < g.scrollSensitivity) {
                        this.scrollParent[0].scrollLeft = h = this.scrollParent[0].scrollLeft - g.scrollSpeed;
                    }
                } else {
                    if (b.pageY - this.document.scrollTop() < g.scrollSensitivity) {
                        h = this.document.scrollTop(this.document.scrollTop() - g.scrollSpeed);
                    } else if (this.window.height() - (b.pageY - this.document.scrollTop()) < g.scrollSensitivity) {
                        h = this.document.scrollTop(this.document.scrollTop() + g.scrollSpeed);
                    }
                    if (b.pageX - this.document.scrollLeft() < g.scrollSensitivity) {
                        h = this.document.scrollLeft(this.document.scrollLeft() - g.scrollSpeed);
                    } else if (this.window.width() - (b.pageX - this.document.scrollLeft()) < g.scrollSensitivity) {
                        h = this.document.scrollLeft(this.document.scrollLeft() + g.scrollSpeed);
                    }
                }
                if (h !== false && a.ui.ddmanager && !g.dropBehaviour) {
                    a.ui.ddmanager.prepareOffsets(this, b);
                }
            }
            //Regenerate the absolute position used for position checks
            this.positionAbs = this._convertPositionTo("absolute");
            //Set the helper position
            if (!this.options.axis || this.options.axis !== "y") {
                this.helper[0].style.left = this.position.left + "px";
            }
            if (!this.options.axis || this.options.axis !== "x") {
                this.helper[0].style.top = this.position.top + "px";
            }
            //Rearrange
            for (c = this.items.length - 1; c >= 0; c--) {
                //Cache variables and intersection, continue if no intersection
                d = this.items[c];
                e = d.item[0];
                f = this._intersectsWithPointer(d);
                if (!f) {
                    continue;
                }
                // Only put the placeholder inside the current Container, skip all
                // items from other containers. This works because when moving
                // an item from one container to another the
                // currentContainer is switched before the placeholder is moved.
                //
                // Without this, moving items in "sub-sortables" can cause
                // the placeholder to jitter between the outer and inner container.
                if (d.instance !== this.currentContainer) {
                    continue;
                }
                // cannot intersect with itself
                // no useless actions that have been done before
                // no action if the item moved is the parent of the item checked
                if (e !== this.currentItem[0] && this.placeholder[f === 1 ? "next" : "prev"]()[0] !== e && !a.contains(this.placeholder[0], e) && (this.options.type === "semi-dynamic" ? !a.contains(this.element[0], e) : true)) {
                    this.direction = f === 1 ? "down" : "up";
                    if (this.options.tolerance === "pointer" || this._intersectsWithSides(d)) {
                        this._rearrange(b, d);
                    } else {
                        break;
                    }
                    this._trigger("change", b, this._uiHash());
                    break;
                }
            }
            //Post events to containers
            this._contactContainers(b);
            //Interconnect with droppables
            if (a.ui.ddmanager) {
                a.ui.ddmanager.drag(this, b);
            }
            //Call callbacks
            this._trigger("sort", b, this._uiHash());
            this.lastPositionAbs = this.positionAbs;
            return false;
        },
        _mouseStop: function(b, c) {
            if (!b) {
                return;
            }
            //If we are using droppables, inform the manager about the drop
            if (a.ui.ddmanager && !this.options.dropBehaviour) {
                a.ui.ddmanager.drop(this, b);
            }
            if (this.options.revert) {
                var d = this, e = this.placeholder.offset(), f = this.options.axis, g = {};
                if (!f || f === "x") {
                    g.left = e.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === this.document[0].body ? 0 : this.offsetParent[0].scrollLeft);
                }
                if (!f || f === "y") {
                    g.top = e.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === this.document[0].body ? 0 : this.offsetParent[0].scrollTop);
                }
                this.reverting = true;
                a(this.helper).animate(g, parseInt(this.options.revert, 10) || 500, function() {
                    d._clear(b);
                });
            } else {
                this._clear(b, c);
            }
            return false;
        },
        cancel: function() {
            if (this.dragging) {
                this._mouseUp({
                    target: null
                });
                if (this.options.helper === "original") {
                    this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper");
                } else {
                    this.currentItem.show();
                }
                //Post deactivating events to containers
                for (var b = this.containers.length - 1; b >= 0; b--) {
                    this.containers[b]._trigger("deactivate", null, this._uiHash(this));
                    if (this.containers[b].containerCache.over) {
                        this.containers[b]._trigger("out", null, this._uiHash(this));
                        this.containers[b].containerCache.over = 0;
                    }
                }
            }
            if (this.placeholder) {
                //$(this.placeholder[0]).remove(); would have been the jQuery way - unfortunately, it unbinds ALL events from the original node!
                if (this.placeholder[0].parentNode) {
                    this.placeholder[0].parentNode.removeChild(this.placeholder[0]);
                }
                if (this.options.helper !== "original" && this.helper && this.helper[0].parentNode) {
                    this.helper.remove();
                }
                a.extend(this, {
                    helper: null,
                    dragging: false,
                    reverting: false,
                    _noFinalSort: null
                });
                if (this.domPosition.prev) {
                    a(this.domPosition.prev).after(this.currentItem);
                } else {
                    a(this.domPosition.parent).prepend(this.currentItem);
                }
            }
            return this;
        },
        serialize: function(b) {
            var c = this._getItemsAsjQuery(b && b.connected), d = [];
            b = b || {};
            a(c).each(function() {
                var c = (a(b.item || this).attr(b.attribute || "id") || "").match(b.expression || /(.+)[\-=_](.+)/);
                if (c) {
                    d.push((b.key || c[1] + "[]") + "=" + (b.key && b.expression ? c[1] : c[2]));
                }
            });
            if (!d.length && b.key) {
                d.push(b.key + "=");
            }
            return d.join("&");
        },
        toArray: function(b) {
            var c = this._getItemsAsjQuery(b && b.connected), d = [];
            b = b || {};
            c.each(function() {
                d.push(a(b.item || this).attr(b.attribute || "id") || "");
            });
            return d;
        },
        /* Be careful with the following core functions */
        _intersectsWith: function(a) {
            var b = this.positionAbs.left, c = b + this.helperProportions.width, d = this.positionAbs.top, e = d + this.helperProportions.height, f = a.left, g = f + a.width, h = a.top, i = h + a.height, j = this.offset.click.top, k = this.offset.click.left, l = this.options.axis === "x" || d + j > h && d + j < i, m = this.options.axis === "y" || b + k > f && b + k < g, n = l && m;
            if (this.options.tolerance === "pointer" || this.options.forcePointerForContainers || this.options.tolerance !== "pointer" && this.helperProportions[this.floating ? "width" : "height"] > a[this.floating ? "width" : "height"]) {
                return n;
            } else {
                // Right Half
                // Left Half
                // Bottom Half
                return f < b + this.helperProportions.width / 2 && c - this.helperProportions.width / 2 < g && h < d + this.helperProportions.height / 2 && e - this.helperProportions.height / 2 < i;
            }
        },
        _intersectsWithPointer: function(a) {
            var b = this.options.axis === "x" || this._isOverAxis(this.positionAbs.top + this.offset.click.top, a.top, a.height), c = this.options.axis === "y" || this._isOverAxis(this.positionAbs.left + this.offset.click.left, a.left, a.width), d = b && c, e = this._getDragVerticalDirection(), f = this._getDragHorizontalDirection();
            if (!d) {
                return false;
            }
            return this.floating ? f && f === "right" || e === "down" ? 2 : 1 : e && (e === "down" ? 2 : 1);
        },
        _intersectsWithSides: function(a) {
            var b = this._isOverAxis(this.positionAbs.top + this.offset.click.top, a.top + a.height / 2, a.height), c = this._isOverAxis(this.positionAbs.left + this.offset.click.left, a.left + a.width / 2, a.width), d = this._getDragVerticalDirection(), e = this._getDragHorizontalDirection();
            if (this.floating && e) {
                return e === "right" && c || e === "left" && !c;
            } else {
                return d && (d === "down" && b || d === "up" && !b);
            }
        },
        _getDragVerticalDirection: function() {
            var a = this.positionAbs.top - this.lastPositionAbs.top;
            return a !== 0 && (a > 0 ? "down" : "up");
        },
        _getDragHorizontalDirection: function() {
            var a = this.positionAbs.left - this.lastPositionAbs.left;
            return a !== 0 && (a > 0 ? "right" : "left");
        },
        refresh: function(a) {
            this._refreshItems(a);
            this._setHandleClassName();
            this.refreshPositions();
            return this;
        },
        _connectWith: function() {
            var a = this.options;
            return a.connectWith.constructor === String ? [ a.connectWith ] : a.connectWith;
        },
        _getItemsAsjQuery: function(b) {
            var c, d, e, f, g = [], h = [], i = this._connectWith();
            if (i && b) {
                for (c = i.length - 1; c >= 0; c--) {
                    e = a(i[c], this.document[0]);
                    for (d = e.length - 1; d >= 0; d--) {
                        f = a.data(e[d], this.widgetFullName);
                        if (f && f !== this && !f.options.disabled) {
                            h.push([ a.isFunction(f.options.items) ? f.options.items.call(f.element) : a(f.options.items, f.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), f ]);
                        }
                    }
                }
            }
            h.push([ a.isFunction(this.options.items) ? this.options.items.call(this.element, null, {
                options: this.options,
                item: this.currentItem
            }) : a(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this ]);
            function j() {
                g.push(this);
            }
            for (c = h.length - 1; c >= 0; c--) {
                h[c][0].each(j);
            }
            return a(g);
        },
        _removeCurrentsFromItems: function() {
            var b = this.currentItem.find(":data(" + this.widgetName + "-item)");
            this.items = a.grep(this.items, function(a) {
                for (var c = 0; c < b.length; c++) {
                    if (b[c] === a.item[0]) {
                        return false;
                    }
                }
                return true;
            });
        },
        _refreshItems: function(b) {
            this.items = [];
            this.containers = [ this ];
            var c, d, e, f, g, h, i, j, k = this.items, l = [ [ a.isFunction(this.options.items) ? this.options.items.call(this.element[0], b, {
                item: this.currentItem
            }) : a(this.options.items, this.element), this ] ], m = this._connectWith();
            if (m && this.ready) {
                //Shouldn't be run the first time through due to massive slow-down
                for (c = m.length - 1; c >= 0; c--) {
                    e = a(m[c], this.document[0]);
                    for (d = e.length - 1; d >= 0; d--) {
                        f = a.data(e[d], this.widgetFullName);
                        if (f && f !== this && !f.options.disabled) {
                            l.push([ a.isFunction(f.options.items) ? f.options.items.call(f.element[0], b, {
                                item: this.currentItem
                            }) : a(f.options.items, f.element), f ]);
                            this.containers.push(f);
                        }
                    }
                }
            }
            for (c = l.length - 1; c >= 0; c--) {
                g = l[c][1];
                h = l[c][0];
                for (d = 0, j = h.length; d < j; d++) {
                    i = a(h[d]);
                    i.data(this.widgetName + "-item", g);
                    // Data for target checking (mouse manager)
                    k.push({
                        item: i,
                        instance: g,
                        width: 0,
                        height: 0,
                        left: 0,
                        top: 0
                    });
                }
            }
        },
        refreshPositions: function(b) {
            // Determine whether items are being displayed horizontally
            this.floating = this.items.length ? this.options.axis === "x" || this._isFloating(this.items[0].item) : false;
            //This has to be redone because due to the item being moved out/into the offsetParent, the offsetParent's position will change
            if (this.offsetParent && this.helper) {
                this.offset.parent = this._getParentOffset();
            }
            var c, d, e, f;
            for (c = this.items.length - 1; c >= 0; c--) {
                d = this.items[c];
                //We ignore calculating positions of all connected containers when we're not over them
                if (d.instance !== this.currentContainer && this.currentContainer && d.item[0] !== this.currentItem[0]) {
                    continue;
                }
                e = this.options.toleranceElement ? a(this.options.toleranceElement, d.item) : d.item;
                if (!b) {
                    d.width = e.outerWidth();
                    d.height = e.outerHeight();
                }
                f = e.offset();
                d.left = f.left;
                d.top = f.top;
            }
            if (this.options.custom && this.options.custom.refreshContainers) {
                this.options.custom.refreshContainers.call(this);
            } else {
                for (c = this.containers.length - 1; c >= 0; c--) {
                    f = this.containers[c].element.offset();
                    this.containers[c].containerCache.left = f.left;
                    this.containers[c].containerCache.top = f.top;
                    this.containers[c].containerCache.width = this.containers[c].element.outerWidth();
                    this.containers[c].containerCache.height = this.containers[c].element.outerHeight();
                }
            }
            return this;
        },
        _createPlaceholder: function(b) {
            b = b || this;
            var c, d = b.options;
            if (!d.placeholder || d.placeholder.constructor === String) {
                c = d.placeholder;
                d.placeholder = {
                    element: function() {
                        var d = b.currentItem[0].nodeName.toLowerCase(), e = a("<" + d + ">", b.document[0]).addClass(c || b.currentItem[0].className + " ui-sortable-placeholder").removeClass("ui-sortable-helper");
                        if (d === "tbody") {
                            b._createTrPlaceholder(b.currentItem.find("tr").eq(0), a("<tr>", b.document[0]).appendTo(e));
                        } else if (d === "tr") {
                            b._createTrPlaceholder(b.currentItem, e);
                        } else if (d === "img") {
                            e.attr("src", b.currentItem.attr("src"));
                        }
                        if (!c) {
                            e.css("visibility", "hidden");
                        }
                        return e;
                    },
                    update: function(a, e) {
                        // 1. If a className is set as 'placeholder option, we don't force sizes - the class is responsible for that
                        // 2. The option 'forcePlaceholderSize can be enabled to force it even if a class name is specified
                        if (c && !d.forcePlaceholderSize) {
                            return;
                        }
                        //If the element doesn't have a actual height by itself (without styles coming from a stylesheet), it receives the inline height from the dragged item
                        if (!e.height()) {
                            e.height(b.currentItem.innerHeight() - parseInt(b.currentItem.css("paddingTop") || 0, 10) - parseInt(b.currentItem.css("paddingBottom") || 0, 10));
                        }
                        if (!e.width()) {
                            e.width(b.currentItem.innerWidth() - parseInt(b.currentItem.css("paddingLeft") || 0, 10) - parseInt(b.currentItem.css("paddingRight") || 0, 10));
                        }
                    }
                };
            }
            //Create the placeholder
            b.placeholder = a(d.placeholder.element.call(b.element, b.currentItem));
            //Append it after the actual current item
            b.currentItem.after(b.placeholder);
            //Update the size of the placeholder (TODO: Logic to fuzzy, see line 316/317)
            d.placeholder.update(b, b.placeholder);
        },
        _createTrPlaceholder: function(b, c) {
            var d = this;
            b.children().each(function() {
                a("<td>&#160;</td>", d.document[0]).attr("colspan", a(this).attr("colspan") || 1).appendTo(c);
            });
        },
        _contactContainers: function(b) {
            var c, d, e, f, g, h, i, j, k, l, m = null, n = null;
            // get innermost container that intersects with item
            for (c = this.containers.length - 1; c >= 0; c--) {
                // never consider a container that's located within the item itself
                if (a.contains(this.currentItem[0], this.containers[c].element[0])) {
                    continue;
                }
                if (this._intersectsWith(this.containers[c].containerCache)) {
                    // if we've already found a container and it's more "inner" than this, then continue
                    if (m && a.contains(this.containers[c].element[0], m.element[0])) {
                        continue;
                    }
                    m = this.containers[c];
                    n = c;
                } else {
                    // container doesn't intersect. trigger "out" event if necessary
                    if (this.containers[c].containerCache.over) {
                        this.containers[c]._trigger("out", b, this._uiHash(this));
                        this.containers[c].containerCache.over = 0;
                    }
                }
            }
            // if no intersecting containers found, return
            if (!m) {
                return;
            }
            // move the item into the container if it's not there already
            if (this.containers.length === 1) {
                if (!this.containers[n].containerCache.over) {
                    this.containers[n]._trigger("over", b, this._uiHash(this));
                    this.containers[n].containerCache.over = 1;
                }
            } else {
                //When entering a new container, we will find the item with the least distance and append our item near it
                e = 1e4;
                f = null;
                k = m.floating || this._isFloating(this.currentItem);
                g = k ? "left" : "top";
                h = k ? "width" : "height";
                l = k ? "clientX" : "clientY";
                for (d = this.items.length - 1; d >= 0; d--) {
                    if (!a.contains(this.containers[n].element[0], this.items[d].item[0])) {
                        continue;
                    }
                    if (this.items[d].item[0] === this.currentItem[0]) {
                        continue;
                    }
                    i = this.items[d].item.offset()[g];
                    j = false;
                    if (b[l] - i > this.items[d][h] / 2) {
                        j = true;
                    }
                    if (Math.abs(b[l] - i) < e) {
                        e = Math.abs(b[l] - i);
                        f = this.items[d];
                        this.direction = j ? "up" : "down";
                    }
                }
                //Check if dropOnEmpty is enabled
                if (!f && !this.options.dropOnEmpty) {
                    return;
                }
                if (this.currentContainer === this.containers[n]) {
                    if (!this.currentContainer.containerCache.over) {
                        this.containers[n]._trigger("over", b, this._uiHash());
                        this.currentContainer.containerCache.over = 1;
                    }
                    return;
                }
                f ? this._rearrange(b, f, null, true) : this._rearrange(b, null, this.containers[n].element, true);
                this._trigger("change", b, this._uiHash());
                this.containers[n]._trigger("change", b, this._uiHash(this));
                this.currentContainer = this.containers[n];
                //Update the placeholder
                this.options.placeholder.update(this.currentContainer, this.placeholder);
                this.containers[n]._trigger("over", b, this._uiHash(this));
                this.containers[n].containerCache.over = 1;
            }
        },
        _createHelper: function(b) {
            var c = this.options, d = a.isFunction(c.helper) ? a(c.helper.apply(this.element[0], [ b, this.currentItem ])) : c.helper === "clone" ? this.currentItem.clone() : this.currentItem;
            //Add the helper to the DOM if that didn't happen already
            if (!d.parents("body").length) {
                a(c.appendTo !== "parent" ? c.appendTo : this.currentItem[0].parentNode)[0].appendChild(d[0]);
            }
            if (d[0] === this.currentItem[0]) {
                this._storedCSS = {
                    width: this.currentItem[0].style.width,
                    height: this.currentItem[0].style.height,
                    position: this.currentItem.css("position"),
                    top: this.currentItem.css("top"),
                    left: this.currentItem.css("left")
                };
            }
            if (!d[0].style.width || c.forceHelperSize) {
                d.width(this.currentItem.width());
            }
            if (!d[0].style.height || c.forceHelperSize) {
                d.height(this.currentItem.height());
            }
            return d;
        },
        _adjustOffsetFromHelper: function(b) {
            if (typeof b === "string") {
                b = b.split(" ");
            }
            if (a.isArray(b)) {
                b = {
                    left: +b[0],
                    top: +b[1] || 0
                };
            }
            if ("left" in b) {
                this.offset.click.left = b.left + this.margins.left;
            }
            if ("right" in b) {
                this.offset.click.left = this.helperProportions.width - b.right + this.margins.left;
            }
            if ("top" in b) {
                this.offset.click.top = b.top + this.margins.top;
            }
            if ("bottom" in b) {
                this.offset.click.top = this.helperProportions.height - b.bottom + this.margins.top;
            }
        },
        _getParentOffset: function() {
            //Get the offsetParent and cache its position
            this.offsetParent = this.helper.offsetParent();
            var b = this.offsetParent.offset();
            // This is a special case where we need to modify a offset calculated on start, since the following happened:
            // 1. The position of the helper is absolute, so it's position is calculated based on the next positioned parent
            // 2. The actual offset parent is a child of the scroll parent, and the scroll parent isn't the document, which means that
            //    the scroll is included in the initial calculation of the offset of the parent, and never recalculated upon drag
            if (this.cssPosition === "absolute" && this.scrollParent[0] !== this.document[0] && a.contains(this.scrollParent[0], this.offsetParent[0])) {
                b.left += this.scrollParent.scrollLeft();
                b.top += this.scrollParent.scrollTop();
            }
            // This needs to be actually done for all browsers, since pageX/pageY includes this information
            // with an ugly IE fix
            if (this.offsetParent[0] === this.document[0].body || this.offsetParent[0].tagName && this.offsetParent[0].tagName.toLowerCase() === "html" && a.ui.ie) {
                b = {
                    top: 0,
                    left: 0
                };
            }
            return {
                top: b.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: b.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            };
        },
        _getRelativeOffset: function() {
            if (this.cssPosition === "relative") {
                var a = this.currentItem.position();
                return {
                    top: a.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
                    left: a.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
                };
            } else {
                return {
                    top: 0,
                    left: 0
                };
            }
        },
        _cacheMargins: function() {
            this.margins = {
                left: parseInt(this.currentItem.css("marginLeft"), 10) || 0,
                top: parseInt(this.currentItem.css("marginTop"), 10) || 0
            };
        },
        _cacheHelperProportions: function() {
            this.helperProportions = {
                width: this.helper.outerWidth(),
                height: this.helper.outerHeight()
            };
        },
        _setContainment: function() {
            var b, c, d, e = this.options;
            if (e.containment === "parent") {
                e.containment = this.helper[0].parentNode;
            }
            if (e.containment === "document" || e.containment === "window") {
                this.containment = [ 0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, e.containment === "document" ? this.document.width() : this.window.width() - this.helperProportions.width - this.margins.left, (e.containment === "document" ? this.document.width() : this.window.height() || this.document[0].body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top ];
            }
            if (!/^(document|window|parent)$/.test(e.containment)) {
                b = a(e.containment)[0];
                c = a(e.containment).offset();
                d = a(b).css("overflow") !== "hidden";
                this.containment = [ c.left + (parseInt(a(b).css("borderLeftWidth"), 10) || 0) + (parseInt(a(b).css("paddingLeft"), 10) || 0) - this.margins.left, c.top + (parseInt(a(b).css("borderTopWidth"), 10) || 0) + (parseInt(a(b).css("paddingTop"), 10) || 0) - this.margins.top, c.left + (d ? Math.max(b.scrollWidth, b.offsetWidth) : b.offsetWidth) - (parseInt(a(b).css("borderLeftWidth"), 10) || 0) - (parseInt(a(b).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, c.top + (d ? Math.max(b.scrollHeight, b.offsetHeight) : b.offsetHeight) - (parseInt(a(b).css("borderTopWidth"), 10) || 0) - (parseInt(a(b).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top ];
            }
        },
        _convertPositionTo: function(b, c) {
            if (!c) {
                c = this.position;
            }
            var d = b === "absolute" ? 1 : -1, e = this.cssPosition === "absolute" && !(this.scrollParent[0] !== this.document[0] && a.contains(this.scrollParent[0], this.offsetParent[0])) ? this.offsetParent : this.scrollParent, f = /(html|body)/i.test(e[0].tagName);
            return {
                top: c.top + // The absolute mouse position
                this.offset.relative.top * d + // Only for relative positioned nodes: Relative offset from element to offset parent
                this.offset.parent.top * d - // The offsetParent's offset without borders (offset + border)
                (this.cssPosition === "fixed" ? -this.scrollParent.scrollTop() : f ? 0 : e.scrollTop()) * d,
                left: c.left + // The absolute mouse position
                this.offset.relative.left * d + // Only for relative positioned nodes: Relative offset from element to offset parent
                this.offset.parent.left * d - // The offsetParent's offset without borders (offset + border)
                (this.cssPosition === "fixed" ? -this.scrollParent.scrollLeft() : f ? 0 : e.scrollLeft()) * d
            };
        },
        _generatePosition: function(b) {
            var c, d, e = this.options, f = b.pageX, g = b.pageY, h = this.cssPosition === "absolute" && !(this.scrollParent[0] !== this.document[0] && a.contains(this.scrollParent[0], this.offsetParent[0])) ? this.offsetParent : this.scrollParent, i = /(html|body)/i.test(h[0].tagName);
            // This is another very weird special case that only happens for relative elements:
            // 1. If the css position is relative
            // 2. and the scroll parent is the document or similar to the offset parent
            // we have to refresh the relative offset during the scroll so there are no jumps
            if (this.cssPosition === "relative" && !(this.scrollParent[0] !== this.document[0] && this.scrollParent[0] !== this.offsetParent[0])) {
                this.offset.relative = this._getRelativeOffset();
            }
            /*
		 * - Position constraining -
		 * Constrain the position to a mix of grid, containment.
		 */
            if (this.originalPosition) {
                //If we are not dragging yet, we won't check for options
                if (this.containment) {
                    if (b.pageX - this.offset.click.left < this.containment[0]) {
                        f = this.containment[0] + this.offset.click.left;
                    }
                    if (b.pageY - this.offset.click.top < this.containment[1]) {
                        g = this.containment[1] + this.offset.click.top;
                    }
                    if (b.pageX - this.offset.click.left > this.containment[2]) {
                        f = this.containment[2] + this.offset.click.left;
                    }
                    if (b.pageY - this.offset.click.top > this.containment[3]) {
                        g = this.containment[3] + this.offset.click.top;
                    }
                }
                if (e.grid) {
                    c = this.originalPageY + Math.round((g - this.originalPageY) / e.grid[1]) * e.grid[1];
                    g = this.containment ? c - this.offset.click.top >= this.containment[1] && c - this.offset.click.top <= this.containment[3] ? c : c - this.offset.click.top >= this.containment[1] ? c - e.grid[1] : c + e.grid[1] : c;
                    d = this.originalPageX + Math.round((f - this.originalPageX) / e.grid[0]) * e.grid[0];
                    f = this.containment ? d - this.offset.click.left >= this.containment[0] && d - this.offset.click.left <= this.containment[2] ? d : d - this.offset.click.left >= this.containment[0] ? d - e.grid[0] : d + e.grid[0] : d;
                }
            }
            return {
                top: g - // The absolute mouse position
                this.offset.click.top - // Click offset (relative to the element)
                this.offset.relative.top - // Only for relative positioned nodes: Relative offset from element to offset parent
                this.offset.parent.top + (// The offsetParent's offset without borders (offset + border)
                this.cssPosition === "fixed" ? -this.scrollParent.scrollTop() : i ? 0 : h.scrollTop()),
                left: f - // The absolute mouse position
                this.offset.click.left - // Click offset (relative to the element)
                this.offset.relative.left - // Only for relative positioned nodes: Relative offset from element to offset parent
                this.offset.parent.left + (// The offsetParent's offset without borders (offset + border)
                this.cssPosition === "fixed" ? -this.scrollParent.scrollLeft() : i ? 0 : h.scrollLeft())
            };
        },
        _rearrange: function(a, b, c, d) {
            c ? c[0].appendChild(this.placeholder[0]) : b.item[0].parentNode.insertBefore(this.placeholder[0], this.direction === "down" ? b.item[0] : b.item[0].nextSibling);
            //Various things done here to improve the performance:
            // 1. we create a setTimeout, that calls refreshPositions
            // 2. on the instance, we have a counter variable, that get's higher after every append
            // 3. on the local scope, we copy the counter variable, and check in the timeout, if it's still the same
            // 4. this lets only the last addition to the timeout stack through
            this.counter = this.counter ? ++this.counter : 1;
            var e = this.counter;
            this._delay(function() {
                if (e === this.counter) {
                    this.refreshPositions(!d);
                }
            });
        },
        _clear: function(a, b) {
            this.reverting = false;
            // We delay all events that have to be triggered to after the point where the placeholder has been removed and
            // everything else normalized again
            var c, d = [];
            // We first have to update the dom position of the actual currentItem
            // Note: don't do it if the current item is already removed (by a user), or it gets reappended (see #4088)
            if (!this._noFinalSort && this.currentItem.parent().length) {
                this.placeholder.before(this.currentItem);
            }
            this._noFinalSort = null;
            if (this.helper[0] === this.currentItem[0]) {
                for (c in this._storedCSS) {
                    if (this._storedCSS[c] === "auto" || this._storedCSS[c] === "static") {
                        this._storedCSS[c] = "";
                    }
                }
                this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper");
            } else {
                this.currentItem.show();
            }
            if (this.fromOutside && !b) {
                d.push(function(a) {
                    this._trigger("receive", a, this._uiHash(this.fromOutside));
                });
            }
            if ((this.fromOutside || this.domPosition.prev !== this.currentItem.prev().not(".ui-sortable-helper")[0] || this.domPosition.parent !== this.currentItem.parent()[0]) && !b) {
                d.push(function(a) {
                    this._trigger("update", a, this._uiHash());
                });
            }
            // Check if the items Container has Changed and trigger appropriate
            // events.
            if (this !== this.currentContainer) {
                if (!b) {
                    d.push(function(a) {
                        this._trigger("remove", a, this._uiHash());
                    });
                    d.push(function(a) {
                        return function(b) {
                            a._trigger("receive", b, this._uiHash(this));
                        };
                    }.call(this, this.currentContainer));
                    d.push(function(a) {
                        return function(b) {
                            a._trigger("update", b, this._uiHash(this));
                        };
                    }.call(this, this.currentContainer));
                }
            }
            //Post events to containers
            function e(a, b, c) {
                return function(d) {
                    c._trigger(a, d, b._uiHash(b));
                };
            }
            for (c = this.containers.length - 1; c >= 0; c--) {
                if (!b) {
                    d.push(e("deactivate", this, this.containers[c]));
                }
                if (this.containers[c].containerCache.over) {
                    d.push(e("out", this, this.containers[c]));
                    this.containers[c].containerCache.over = 0;
                }
            }
            //Do what was originally in plugins
            if (this.storedCursor) {
                this.document.find("body").css("cursor", this.storedCursor);
                this.storedStylesheet.remove();
            }
            if (this._storedOpacity) {
                this.helper.css("opacity", this._storedOpacity);
            }
            if (this._storedZIndex) {
                this.helper.css("zIndex", this._storedZIndex === "auto" ? "" : this._storedZIndex);
            }
            this.dragging = false;
            if (!b) {
                this._trigger("beforeStop", a, this._uiHash());
            }
            //$(this.placeholder[0]).remove(); would have been the jQuery way - unfortunately, it unbinds ALL events from the original node!
            this.placeholder[0].parentNode.removeChild(this.placeholder[0]);
            if (!this.cancelHelperRemoval) {
                if (this.helper[0] !== this.currentItem[0]) {
                    this.helper.remove();
                }
                this.helper = null;
            }
            if (!b) {
                for (c = 0; c < d.length; c++) {
                    d[c].call(this, a);
                }
                //Trigger all delayed events
                this._trigger("stop", a, this._uiHash());
            }
            this.fromOutside = false;
            return !this.cancelHelperRemoval;
        },
        _trigger: function() {
            if (a.Widget.prototype._trigger.apply(this, arguments) === false) {
                this.cancel();
            }
        },
        _uiHash: function(b) {
            var c = b || this;
            return {
                helper: c.helper,
                placeholder: c.placeholder || a([]),
                position: c.position,
                originalPosition: c.originalPosition,
                offset: c.positionAbs,
                item: c.currentItem,
                sender: b ? b.element : null
            };
        }
    });
    /*!
 * jQuery UI Spinner 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/spinner/
 */
    function $(a) {
        return function() {
            var b = this.element.val();
            a.apply(this, arguments);
            this._refresh();
            if (b !== this.element.val()) {
                this._trigger("change");
            }
        };
    }
    var _ = a.widget("ui.spinner", {
        version: "1.11.4",
        defaultElement: "<input>",
        widgetEventPrefix: "spin",
        options: {
            culture: null,
            icons: {
                down: "ui-icon-triangle-1-s",
                up: "ui-icon-triangle-1-n"
            },
            incremental: true,
            max: null,
            min: null,
            numberFormat: null,
            page: 10,
            step: 1,
            change: null,
            spin: null,
            start: null,
            stop: null
        },
        _create: function() {
            // handle string values that need to be parsed
            this._setOption("max", this.options.max);
            this._setOption("min", this.options.min);
            this._setOption("step", this.options.step);
            // Only format if there is a value, prevents the field from being marked
            // as invalid in Firefox, see #9573.
            if (this.value() !== "") {
                // Format the value, but don't constrain.
                this._value(this.element.val(), true);
            }
            this._draw();
            this._on(this._events);
            this._refresh();
            // turning off autocomplete prevents the browser from remembering the
            // value when navigating through history, so we re-enable autocomplete
            // if the page is unloaded before the widget is destroyed. #7790
            this._on(this.window, {
                beforeunload: function() {
                    this.element.removeAttr("autocomplete");
                }
            });
        },
        _getCreateOptions: function() {
            var b = {}, c = this.element;
            a.each([ "min", "max", "step" ], function(a, d) {
                var e = c.attr(d);
                if (e !== undefined && e.length) {
                    b[d] = e;
                }
            });
            return b;
        },
        _events: {
            keydown: function(a) {
                if (this._start(a) && this._keydown(a)) {
                    a.preventDefault();
                }
            },
            keyup: "_stop",
            focus: function() {
                this.previous = this.element.val();
            },
            blur: function(a) {
                if (this.cancelBlur) {
                    delete this.cancelBlur;
                    return;
                }
                this._stop();
                this._refresh();
                if (this.previous !== this.element.val()) {
                    this._trigger("change", a);
                }
            },
            mousewheel: function(a, b) {
                if (!b) {
                    return;
                }
                if (!this.spinning && !this._start(a)) {
                    return false;
                }
                this._spin((b > 0 ? 1 : -1) * this.options.step, a);
                clearTimeout(this.mousewheelTimer);
                this.mousewheelTimer = this._delay(function() {
                    if (this.spinning) {
                        this._stop(a);
                    }
                }, 100);
                a.preventDefault();
            },
            "mousedown .ui-spinner-button": function(b) {
                var c;
                // We never want the buttons to have focus; whenever the user is
                // interacting with the spinner, the focus should be on the input.
                // If the input is focused then this.previous is properly set from
                // when the input first received focus. If the input is not focused
                // then we need to set this.previous based on the value before spinning.
                c = this.element[0] === this.document[0].activeElement ? this.previous : this.element.val();
                function d() {
                    var a = this.element[0] === this.document[0].activeElement;
                    if (!a) {
                        this.element.focus();
                        this.previous = c;
                        // support: IE
                        // IE sets focus asynchronously, so we need to check if focus
                        // moved off of the input because the user clicked on the button.
                        this._delay(function() {
                            this.previous = c;
                        });
                    }
                }
                // ensure focus is on (or stays on) the text field
                b.preventDefault();
                d.call(this);
                // support: IE
                // IE doesn't prevent moving focus even with event.preventDefault()
                // so we set a flag to know when we should ignore the blur event
                // and check (again) if focus moved off of the input.
                this.cancelBlur = true;
                this._delay(function() {
                    delete this.cancelBlur;
                    d.call(this);
                });
                if (this._start(b) === false) {
                    return;
                }
                this._repeat(null, a(b.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, b);
            },
            "mouseup .ui-spinner-button": "_stop",
            "mouseenter .ui-spinner-button": function(b) {
                // button will add ui-state-active if mouse was down while mouseleave and kept down
                if (!a(b.currentTarget).hasClass("ui-state-active")) {
                    return;
                }
                if (this._start(b) === false) {
                    return false;
                }
                this._repeat(null, a(b.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, b);
            },
            // TODO: do we really want to consider this a stop?
            // shouldn't we just stop the repeater and wait until mouseup before
            // we trigger the stop event?
            "mouseleave .ui-spinner-button": "_stop"
        },
        _draw: function() {
            var a = this.uiSpinner = this.element.addClass("ui-spinner-input").attr("autocomplete", "off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml());
            this.element.attr("role", "spinbutton");
            // button bindings
            this.buttons = a.find(".ui-spinner-button").attr("tabIndex", -1).button().removeClass("ui-corner-all");
            // IE 6 doesn't understand height: 50% for the buttons
            // unless the wrapper has an explicit height
            if (this.buttons.height() > Math.ceil(a.height() * .5) && a.height() > 0) {
                a.height(a.height());
            }
            // disable spinner if element was already disabled
            if (this.options.disabled) {
                this.disable();
            }
        },
        _keydown: function(b) {
            var c = this.options, d = a.ui.keyCode;
            switch (b.keyCode) {
              case d.UP:
                this._repeat(null, 1, b);
                return true;

              case d.DOWN:
                this._repeat(null, -1, b);
                return true;

              case d.PAGE_UP:
                this._repeat(null, c.page, b);
                return true;

              case d.PAGE_DOWN:
                this._repeat(null, -c.page, b);
                return true;
            }
            return false;
        },
        _uiSpinnerHtml: function() {
            return "<span class='ui-spinner ui-widget ui-widget-content ui-corner-all'></span>";
        },
        _buttonHtml: function() {
            return "" + "<a class='ui-spinner-button ui-spinner-up ui-corner-tr'>" + "<span class='ui-icon " + this.options.icons.up + "'>&#9650;</span>" + "</a>" + "<a class='ui-spinner-button ui-spinner-down ui-corner-br'>" + "<span class='ui-icon " + this.options.icons.down + "'>&#9660;</span>" + "</a>";
        },
        _start: function(a) {
            if (!this.spinning && this._trigger("start", a) === false) {
                return false;
            }
            if (!this.counter) {
                this.counter = 1;
            }
            this.spinning = true;
            return true;
        },
        _repeat: function(a, b, c) {
            a = a || 500;
            clearTimeout(this.timer);
            this.timer = this._delay(function() {
                this._repeat(40, b, c);
            }, a);
            this._spin(b * this.options.step, c);
        },
        _spin: function(a, b) {
            var c = this.value() || 0;
            if (!this.counter) {
                this.counter = 1;
            }
            c = this._adjustValue(c + a * this._increment(this.counter));
            if (!this.spinning || this._trigger("spin", b, {
                value: c
            }) !== false) {
                this._value(c);
                this.counter++;
            }
        },
        _increment: function(b) {
            var c = this.options.incremental;
            if (c) {
                return a.isFunction(c) ? c(b) : Math.floor(b * b * b / 5e4 - b * b / 500 + 17 * b / 200 + 1);
            }
            return 1;
        },
        _precision: function() {
            var a = this._precisionOf(this.options.step);
            if (this.options.min !== null) {
                a = Math.max(a, this._precisionOf(this.options.min));
            }
            return a;
        },
        _precisionOf: function(a) {
            var b = a.toString(), c = b.indexOf(".");
            return c === -1 ? 0 : b.length - c - 1;
        },
        _adjustValue: function(a) {
            var b, c, d = this.options;
            // make sure we're at a valid step
            // - find out where we are relative to the base (min or 0)
            b = d.min !== null ? d.min : 0;
            c = a - b;
            // - round to the nearest step
            c = Math.round(c / d.step) * d.step;
            // - rounding is based on 0, so adjust back to our base
            a = b + c;
            // fix precision from bad JS floating point math
            a = parseFloat(a.toFixed(this._precision()));
            // clamp the value
            if (d.max !== null && a > d.max) {
                return d.max;
            }
            if (d.min !== null && a < d.min) {
                return d.min;
            }
            return a;
        },
        _stop: function(a) {
            if (!this.spinning) {
                return;
            }
            clearTimeout(this.timer);
            clearTimeout(this.mousewheelTimer);
            this.counter = 0;
            this.spinning = false;
            this._trigger("stop", a);
        },
        _setOption: function(a, b) {
            if (a === "culture" || a === "numberFormat") {
                var c = this._parse(this.element.val());
                this.options[a] = b;
                this.element.val(this._format(c));
                return;
            }
            if (a === "max" || a === "min" || a === "step") {
                if (typeof b === "string") {
                    b = this._parse(b);
                }
            }
            if (a === "icons") {
                this.buttons.first().find(".ui-icon").removeClass(this.options.icons.up).addClass(b.up);
                this.buttons.last().find(".ui-icon").removeClass(this.options.icons.down).addClass(b.down);
            }
            this._super(a, b);
            if (a === "disabled") {
                this.widget().toggleClass("ui-state-disabled", !!b);
                this.element.prop("disabled", !!b);
                this.buttons.button(b ? "disable" : "enable");
            }
        },
        _setOptions: $(function(a) {
            this._super(a);
        }),
        _parse: function(a) {
            if (typeof a === "string" && a !== "") {
                a = window.Globalize && this.options.numberFormat ? Globalize.parseFloat(a, 10, this.options.culture) : +a;
            }
            return a === "" || isNaN(a) ? null : a;
        },
        _format: function(a) {
            if (a === "") {
                return "";
            }
            return window.Globalize && this.options.numberFormat ? Globalize.format(a, this.options.numberFormat, this.options.culture) : a;
        },
        _refresh: function() {
            this.element.attr({
                "aria-valuemin": this.options.min,
                "aria-valuemax": this.options.max,
                // TODO: what should we do with values that can't be parsed?
                "aria-valuenow": this._parse(this.element.val())
            });
        },
        isValid: function() {
            var a = this.value();
            // null is invalid
            if (a === null) {
                return false;
            }
            // if value gets adjusted, it's invalid
            return a === this._adjustValue(a);
        },
        // update the value without triggering change
        _value: function(a, b) {
            var c;
            if (a !== "") {
                c = this._parse(a);
                if (c !== null) {
                    if (!b) {
                        c = this._adjustValue(c);
                    }
                    a = this._format(c);
                }
            }
            this.element.val(a);
            this._refresh();
        },
        _destroy: function() {
            this.element.removeClass("ui-spinner-input").prop("disabled", false).removeAttr("autocomplete").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow");
            this.uiSpinner.replaceWith(this.element);
        },
        stepUp: $(function(a) {
            this._stepUp(a);
        }),
        _stepUp: function(a) {
            if (this._start()) {
                this._spin((a || 1) * this.options.step);
                this._stop();
            }
        },
        stepDown: $(function(a) {
            this._stepDown(a);
        }),
        _stepDown: function(a) {
            if (this._start()) {
                this._spin((a || 1) * -this.options.step);
                this._stop();
            }
        },
        pageUp: $(function(a) {
            this._stepUp((a || 1) * this.options.page);
        }),
        pageDown: $(function(a) {
            this._stepDown((a || 1) * this.options.page);
        }),
        value: function(a) {
            if (!arguments.length) {
                return this._parse(this.element.val());
            }
            $(this._value).call(this, a);
        },
        widget: function() {
            return this.uiSpinner;
        }
    });
    /*!
 * jQuery UI Tabs 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/tabs/
 */
    var aa = a.widget("ui.tabs", {
        version: "1.11.4",
        delay: 300,
        options: {
            active: null,
            collapsible: false,
            event: "click",
            heightStyle: "content",
            hide: null,
            show: null,
            // callbacks
            activate: null,
            beforeActivate: null,
            beforeLoad: null,
            load: null
        },
        _isLocal: function() {
            var a = /#.*$/;
            return function(b) {
                var c, d;
                // support: IE7
                // IE7 doesn't normalize the href property when set via script (#9317)
                b = b.cloneNode(false);
                c = b.href.replace(a, "");
                d = location.href.replace(a, "");
                // decoding may throw an error if the URL isn't UTF-8 (#9518)
                try {
                    c = decodeURIComponent(c);
                } catch (e) {}
                try {
                    d = decodeURIComponent(d);
                } catch (e) {}
                return b.hash.length > 1 && c === d;
            };
        }(),
        _create: function() {
            var b = this, c = this.options;
            this.running = false;
            this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all").toggleClass("ui-tabs-collapsible", c.collapsible);
            this._processTabs();
            c.active = this._initialActive();
            // Take disabling tabs via class attribute from HTML
            // into account and update option properly.
            if (a.isArray(c.disabled)) {
                c.disabled = a.unique(c.disabled.concat(a.map(this.tabs.filter(".ui-state-disabled"), function(a) {
                    return b.tabs.index(a);
                }))).sort();
            }
            // check for length avoids error when initializing empty list
            if (this.options.active !== false && this.anchors.length) {
                this.active = this._findActive(c.active);
            } else {
                this.active = a();
            }
            this._refresh();
            if (this.active.length) {
                this.load(c.active);
            }
        },
        _initialActive: function() {
            var b = this.options.active, c = this.options.collapsible, d = location.hash.substring(1);
            if (b === null) {
                // check the fragment identifier in the URL
                if (d) {
                    this.tabs.each(function(c, e) {
                        if (a(e).attr("aria-controls") === d) {
                            b = c;
                            return false;
                        }
                    });
                }
                // check for a tab marked active via a class
                if (b === null) {
                    b = this.tabs.index(this.tabs.filter(".ui-tabs-active"));
                }
                // no active tab, set to false
                if (b === null || b === -1) {
                    b = this.tabs.length ? 0 : false;
                }
            }
            // handle numbers: negative, out of range
            if (b !== false) {
                b = this.tabs.index(this.tabs.eq(b));
                if (b === -1) {
                    b = c ? false : 0;
                }
            }
            // don't allow collapsible: false and active: false
            if (!c && b === false && this.anchors.length) {
                b = 0;
            }
            return b;
        },
        _getCreateEventData: function() {
            return {
                tab: this.active,
                panel: !this.active.length ? a() : this._getPanelForTab(this.active)
            };
        },
        _tabKeydown: function(b) {
            var c = a(this.document[0].activeElement).closest("li"), d = this.tabs.index(c), e = true;
            if (this._handlePageNav(b)) {
                return;
            }
            switch (b.keyCode) {
              case a.ui.keyCode.RIGHT:
              case a.ui.keyCode.DOWN:
                d++;
                break;

              case a.ui.keyCode.UP:
              case a.ui.keyCode.LEFT:
                e = false;
                d--;
                break;

              case a.ui.keyCode.END:
                d = this.anchors.length - 1;
                break;

              case a.ui.keyCode.HOME:
                d = 0;
                break;

              case a.ui.keyCode.SPACE:
                // Activate only, no collapsing
                b.preventDefault();
                clearTimeout(this.activating);
                this._activate(d);
                return;

              case a.ui.keyCode.ENTER:
                // Toggle (cancel delayed activation, allow collapsing)
                b.preventDefault();
                clearTimeout(this.activating);
                // Determine if we should collapse or activate
                this._activate(d === this.options.active ? false : d);
                return;

              default:
                return;
            }
            // Focus the appropriate tab, based on which key was pressed
            b.preventDefault();
            clearTimeout(this.activating);
            d = this._focusNextTab(d, e);
            // Navigating with control/command key will prevent automatic activation
            if (!b.ctrlKey && !b.metaKey) {
                // Update aria-selected immediately so that AT think the tab is already selected.
                // Otherwise AT may confuse the user by stating that they need to activate the tab,
                // but the tab will already be activated by the time the announcement finishes.
                c.attr("aria-selected", "false");
                this.tabs.eq(d).attr("aria-selected", "true");
                this.activating = this._delay(function() {
                    this.option("active", d);
                }, this.delay);
            }
        },
        _panelKeydown: function(b) {
            if (this._handlePageNav(b)) {
                return;
            }
            // Ctrl+up moves focus to the current tab
            if (b.ctrlKey && b.keyCode === a.ui.keyCode.UP) {
                b.preventDefault();
                this.active.focus();
            }
        },
        // Alt+page up/down moves focus to the previous/next tab (and activates)
        _handlePageNav: function(b) {
            if (b.altKey && b.keyCode === a.ui.keyCode.PAGE_UP) {
                this._activate(this._focusNextTab(this.options.active - 1, false));
                return true;
            }
            if (b.altKey && b.keyCode === a.ui.keyCode.PAGE_DOWN) {
                this._activate(this._focusNextTab(this.options.active + 1, true));
                return true;
            }
        },
        _findNextTab: function(b, c) {
            var d = this.tabs.length - 1;
            function e() {
                if (b > d) {
                    b = 0;
                }
                if (b < 0) {
                    b = d;
                }
                return b;
            }
            while (a.inArray(e(), this.options.disabled) !== -1) {
                b = c ? b + 1 : b - 1;
            }
            return b;
        },
        _focusNextTab: function(a, b) {
            a = this._findNextTab(a, b);
            this.tabs.eq(a).focus();
            return a;
        },
        _setOption: function(a, b) {
            if (a === "active") {
                // _activate() will handle invalid values and update this.options
                this._activate(b);
                return;
            }
            if (a === "disabled") {
                // don't use the widget factory's disabled handling
                this._setupDisabled(b);
                return;
            }
            this._super(a, b);
            if (a === "collapsible") {
                this.element.toggleClass("ui-tabs-collapsible", b);
                // Setting collapsible: false while collapsed; open first panel
                if (!b && this.options.active === false) {
                    this._activate(0);
                }
            }
            if (a === "event") {
                this._setupEvents(b);
            }
            if (a === "heightStyle") {
                this._setupHeightStyle(b);
            }
        },
        _sanitizeSelector: function(a) {
            return a ? a.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g, "\\$&") : "";
        },
        refresh: function() {
            var b = this.options, c = this.tablist.children(":has(a[href])");
            // get disabled tabs from class attribute from HTML
            // this will get converted to a boolean if needed in _refresh()
            b.disabled = a.map(c.filter(".ui-state-disabled"), function(a) {
                return c.index(a);
            });
            this._processTabs();
            // was collapsed or no tabs
            if (b.active === false || !this.anchors.length) {
                b.active = false;
                this.active = a();
            } else if (this.active.length && !a.contains(this.tablist[0], this.active[0])) {
                // all remaining tabs are disabled
                if (this.tabs.length === b.disabled.length) {
                    b.active = false;
                    this.active = a();
                } else {
                    this._activate(this._findNextTab(Math.max(0, b.active - 1), false));
                }
            } else {
                // make sure active index is correct
                b.active = this.tabs.index(this.active);
            }
            this._refresh();
        },
        _refresh: function() {
            this._setupDisabled(this.options.disabled);
            this._setupEvents(this.options.event);
            this._setupHeightStyle(this.options.heightStyle);
            this.tabs.not(this.active).attr({
                "aria-selected": "false",
                "aria-expanded": "false",
                tabIndex: -1
            });
            this.panels.not(this._getPanelForTab(this.active)).hide().attr({
                "aria-hidden": "true"
            });
            // Make sure one tab is in the tab order
            if (!this.active.length) {
                this.tabs.eq(0).attr("tabIndex", 0);
            } else {
                this.active.addClass("ui-tabs-active ui-state-active").attr({
                    "aria-selected": "true",
                    "aria-expanded": "true",
                    tabIndex: 0
                });
                this._getPanelForTab(this.active).show().attr({
                    "aria-hidden": "false"
                });
            }
        },
        _processTabs: function() {
            var b = this, c = this.tabs, d = this.anchors, e = this.panels;
            this.tablist = this._getList().addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").attr("role", "tablist").delegate("> li", "mousedown" + this.eventNamespace, function(b) {
                if (a(this).is(".ui-state-disabled")) {
                    b.preventDefault();
                }
            }).delegate(".ui-tabs-anchor", "focus" + this.eventNamespace, function() {
                if (a(this).closest("li").is(".ui-state-disabled")) {
                    this.blur();
                }
            });
            this.tabs = this.tablist.find("> li:has(a[href])").addClass("ui-state-default ui-corner-top").attr({
                role: "tab",
                tabIndex: -1
            });
            this.anchors = this.tabs.map(function() {
                return a("a", this)[0];
            }).addClass("ui-tabs-anchor").attr({
                role: "presentation",
                tabIndex: -1
            });
            this.panels = a();
            this.anchors.each(function(c, d) {
                var e, f, g, h = a(d).uniqueId().attr("id"), i = a(d).closest("li"), j = i.attr("aria-controls");
                // inline tab
                if (b._isLocal(d)) {
                    e = d.hash;
                    g = e.substring(1);
                    f = b.element.find(b._sanitizeSelector(e));
                } else {
                    // If the tab doesn't already have aria-controls,
                    // generate an id by using a throw-away element
                    g = i.attr("aria-controls") || a({}).uniqueId()[0].id;
                    e = "#" + g;
                    f = b.element.find(e);
                    if (!f.length) {
                        f = b._createPanel(g);
                        f.insertAfter(b.panels[c - 1] || b.tablist);
                    }
                    f.attr("aria-live", "polite");
                }
                if (f.length) {
                    b.panels = b.panels.add(f);
                }
                if (j) {
                    i.data("ui-tabs-aria-controls", j);
                }
                i.attr({
                    "aria-controls": g,
                    "aria-labelledby": h
                });
                f.attr("aria-labelledby", h);
            });
            this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").attr("role", "tabpanel");
            // Avoid memory leaks (#10056)
            if (c) {
                this._off(c.not(this.tabs));
                this._off(d.not(this.anchors));
                this._off(e.not(this.panels));
            }
        },
        // allow overriding how to find the list for rare usage scenarios (#7715)
        _getList: function() {
            return this.tablist || this.element.find("ol,ul").eq(0);
        },
        _createPanel: function(b) {
            return a("<div>").attr("id", b).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy", true);
        },
        _setupDisabled: function(b) {
            if (a.isArray(b)) {
                if (!b.length) {
                    b = false;
                } else if (b.length === this.anchors.length) {
                    b = true;
                }
            }
            // disable tabs
            for (var c = 0, d; d = this.tabs[c]; c++) {
                if (b === true || a.inArray(c, b) !== -1) {
                    a(d).addClass("ui-state-disabled").attr("aria-disabled", "true");
                } else {
                    a(d).removeClass("ui-state-disabled").removeAttr("aria-disabled");
                }
            }
            this.options.disabled = b;
        },
        _setupEvents: function(b) {
            var c = {};
            if (b) {
                a.each(b.split(" "), function(a, b) {
                    c[b] = "_eventHandler";
                });
            }
            this._off(this.anchors.add(this.tabs).add(this.panels));
            // Always prevent the default action, even when disabled
            this._on(true, this.anchors, {
                click: function(a) {
                    a.preventDefault();
                }
            });
            this._on(this.anchors, c);
            this._on(this.tabs, {
                keydown: "_tabKeydown"
            });
            this._on(this.panels, {
                keydown: "_panelKeydown"
            });
            this._focusable(this.tabs);
            this._hoverable(this.tabs);
        },
        _setupHeightStyle: function(b) {
            var c, d = this.element.parent();
            if (b === "fill") {
                c = d.height();
                c -= this.element.outerHeight() - this.element.height();
                this.element.siblings(":visible").each(function() {
                    var b = a(this), d = b.css("position");
                    if (d === "absolute" || d === "fixed") {
                        return;
                    }
                    c -= b.outerHeight(true);
                });
                this.element.children().not(this.panels).each(function() {
                    c -= a(this).outerHeight(true);
                });
                this.panels.each(function() {
                    a(this).height(Math.max(0, c - a(this).innerHeight() + a(this).height()));
                }).css("overflow", "auto");
            } else if (b === "auto") {
                c = 0;
                this.panels.each(function() {
                    c = Math.max(c, a(this).height("").height());
                }).height(c);
            }
        },
        _eventHandler: function(b) {
            var c = this.options, d = this.active, e = a(b.currentTarget), f = e.closest("li"), g = f[0] === d[0], h = g && c.collapsible, i = h ? a() : this._getPanelForTab(f), j = !d.length ? a() : this._getPanelForTab(d), k = {
                oldTab: d,
                oldPanel: j,
                newTab: h ? a() : f,
                newPanel: i
            };
            b.preventDefault();
            if (f.hasClass("ui-state-disabled") || // tab is already loading
            f.hasClass("ui-tabs-loading") || // can't switch durning an animation
            this.running || // click on active header, but not collapsible
            g && !c.collapsible || // allow canceling activation
            this._trigger("beforeActivate", b, k) === false) {
                return;
            }
            c.active = h ? false : this.tabs.index(f);
            this.active = g ? a() : f;
            if (this.xhr) {
                this.xhr.abort();
            }
            if (!j.length && !i.length) {
                a.error("jQuery UI Tabs: Mismatching fragment identifier.");
            }
            if (i.length) {
                this.load(this.tabs.index(f), b);
            }
            this._toggle(b, k);
        },
        // handles show/hide for selecting tabs
        _toggle: function(b, c) {
            var d = this, e = c.newPanel, f = c.oldPanel;
            this.running = true;
            function g() {
                d.running = false;
                d._trigger("activate", b, c);
            }
            function h() {
                c.newTab.closest("li").addClass("ui-tabs-active ui-state-active");
                if (e.length && d.options.show) {
                    d._show(e, d.options.show, g);
                } else {
                    e.show();
                    g();
                }
            }
            // start out by hiding, then showing, then completing
            if (f.length && this.options.hide) {
                this._hide(f, this.options.hide, function() {
                    c.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active");
                    h();
                });
            } else {
                c.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active");
                f.hide();
                h();
            }
            f.attr("aria-hidden", "true");
            c.oldTab.attr({
                "aria-selected": "false",
                "aria-expanded": "false"
            });
            // If we're switching tabs, remove the old tab from the tab order.
            // If we're opening from collapsed state, remove the previous tab from the tab order.
            // If we're collapsing, then keep the collapsing tab in the tab order.
            if (e.length && f.length) {
                c.oldTab.attr("tabIndex", -1);
            } else if (e.length) {
                this.tabs.filter(function() {
                    return a(this).attr("tabIndex") === 0;
                }).attr("tabIndex", -1);
            }
            e.attr("aria-hidden", "false");
            c.newTab.attr({
                "aria-selected": "true",
                "aria-expanded": "true",
                tabIndex: 0
            });
        },
        _activate: function(b) {
            var c, d = this._findActive(b);
            // trying to activate the already active panel
            if (d[0] === this.active[0]) {
                return;
            }
            // trying to collapse, simulate a click on the current active header
            if (!d.length) {
                d = this.active;
            }
            c = d.find(".ui-tabs-anchor")[0];
            this._eventHandler({
                target: c,
                currentTarget: c,
                preventDefault: a.noop
            });
        },
        _findActive: function(b) {
            return b === false ? a() : this.tabs.eq(b);
        },
        _getIndex: function(a) {
            // meta-function to give users option to provide a href string instead of a numerical index.
            if (typeof a === "string") {
                a = this.anchors.index(this.anchors.filter("[href$='" + a + "']"));
            }
            return a;
        },
        _destroy: function() {
            if (this.xhr) {
                this.xhr.abort();
            }
            this.element.removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible");
            this.tablist.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").removeAttr("role");
            this.anchors.removeClass("ui-tabs-anchor").removeAttr("role").removeAttr("tabIndex").removeUniqueId();
            this.tablist.unbind(this.eventNamespace);
            this.tabs.add(this.panels).each(function() {
                if (a.data(this, "ui-tabs-destroy")) {
                    a(this).remove();
                } else {
                    a(this).removeClass("ui-state-default ui-state-active ui-state-disabled " + "ui-corner-top ui-corner-bottom ui-widget-content ui-tabs-active ui-tabs-panel").removeAttr("tabIndex").removeAttr("aria-live").removeAttr("aria-busy").removeAttr("aria-selected").removeAttr("aria-labelledby").removeAttr("aria-hidden").removeAttr("aria-expanded").removeAttr("role");
                }
            });
            this.tabs.each(function() {
                var b = a(this), c = b.data("ui-tabs-aria-controls");
                if (c) {
                    b.attr("aria-controls", c).removeData("ui-tabs-aria-controls");
                } else {
                    b.removeAttr("aria-controls");
                }
            });
            this.panels.show();
            if (this.options.heightStyle !== "content") {
                this.panels.css("height", "");
            }
        },
        enable: function(b) {
            var c = this.options.disabled;
            if (c === false) {
                return;
            }
            if (b === undefined) {
                c = false;
            } else {
                b = this._getIndex(b);
                if (a.isArray(c)) {
                    c = a.map(c, function(a) {
                        return a !== b ? a : null;
                    });
                } else {
                    c = a.map(this.tabs, function(a, c) {
                        return c !== b ? c : null;
                    });
                }
            }
            this._setupDisabled(c);
        },
        disable: function(b) {
            var c = this.options.disabled;
            if (c === true) {
                return;
            }
            if (b === undefined) {
                c = true;
            } else {
                b = this._getIndex(b);
                if (a.inArray(b, c) !== -1) {
                    return;
                }
                if (a.isArray(c)) {
                    c = a.merge([ b ], c).sort();
                } else {
                    c = [ b ];
                }
            }
            this._setupDisabled(c);
        },
        load: function(b, c) {
            b = this._getIndex(b);
            var d = this, e = this.tabs.eq(b), f = e.find(".ui-tabs-anchor"), g = this._getPanelForTab(e), h = {
                tab: e,
                panel: g
            }, i = function(a, b) {
                if (b === "abort") {
                    d.panels.stop(false, true);
                }
                e.removeClass("ui-tabs-loading");
                g.removeAttr("aria-busy");
                if (a === d.xhr) {
                    delete d.xhr;
                }
            };
            // not remote
            if (this._isLocal(f[0])) {
                return;
            }
            this.xhr = a.ajax(this._ajaxSettings(f, c, h));
            // support: jQuery <1.8
            // jQuery <1.8 returns false if the request is canceled in beforeSend,
            // but as of 1.8, $.ajax() always returns a jqXHR object.
            if (this.xhr && this.xhr.statusText !== "canceled") {
                e.addClass("ui-tabs-loading");
                g.attr("aria-busy", "true");
                this.xhr.done(function(a, b, e) {
                    // support: jQuery <1.8
                    // http://bugs.jquery.com/ticket/11778
                    setTimeout(function() {
                        g.html(a);
                        d._trigger("load", c, h);
                        i(e, b);
                    }, 1);
                }).fail(function(a, b) {
                    // support: jQuery <1.8
                    // http://bugs.jquery.com/ticket/11778
                    setTimeout(function() {
                        i(a, b);
                    }, 1);
                });
            }
        },
        _ajaxSettings: function(b, c, d) {
            var e = this;
            return {
                url: b.attr("href"),
                beforeSend: function(b, f) {
                    return e._trigger("beforeLoad", c, a.extend({
                        jqXHR: b,
                        ajaxSettings: f
                    }, d));
                }
            };
        },
        _getPanelForTab: function(b) {
            var c = a(b).attr("aria-controls");
            return this.element.find(this._sanitizeSelector("#" + c));
        }
    });
    /*!
 * jQuery UI Tooltip 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/tooltip/
 */
    var ba = a.widget("ui.tooltip", {
        version: "1.11.4",
        options: {
            content: function() {
                // support: IE<9, Opera in jQuery <1.7
                // .text() can't accept undefined, so coerce to a string
                var b = a(this).attr("title") || "";
                // Escape title, since we're going from an attribute to raw HTML
                return a("<a>").text(b).html();
            },
            hide: true,
            // Disabled elements have inconsistent behavior across browsers (#8661)
            items: "[title]:not([disabled])",
            position: {
                my: "left top+15",
                at: "left bottom",
                collision: "flipfit flip"
            },
            show: true,
            tooltipClass: null,
            track: false,
            // callbacks
            close: null,
            open: null
        },
        _addDescribedBy: function(b, c) {
            var d = (b.attr("aria-describedby") || "").split(/\s+/);
            d.push(c);
            b.data("ui-tooltip-id", c).attr("aria-describedby", a.trim(d.join(" ")));
        },
        _removeDescribedBy: function(b) {
            var c = b.data("ui-tooltip-id"), d = (b.attr("aria-describedby") || "").split(/\s+/), e = a.inArray(c, d);
            if (e !== -1) {
                d.splice(e, 1);
            }
            b.removeData("ui-tooltip-id");
            d = a.trim(d.join(" "));
            if (d) {
                b.attr("aria-describedby", d);
            } else {
                b.removeAttr("aria-describedby");
            }
        },
        _create: function() {
            this._on({
                mouseover: "open",
                focusin: "open"
            });
            // IDs of generated tooltips, needed for destroy
            this.tooltips = {};
            // IDs of parent tooltips where we removed the title attribute
            this.parents = {};
            if (this.options.disabled) {
                this._disable();
            }
            // Append the aria-live region so tooltips announce correctly
            this.liveRegion = a("<div>").attr({
                role: "log",
                "aria-live": "assertive",
                "aria-relevant": "additions"
            }).addClass("ui-helper-hidden-accessible").appendTo(this.document[0].body);
        },
        _setOption: function(b, c) {
            var d = this;
            if (b === "disabled") {
                this[c ? "_disable" : "_enable"]();
                this.options[b] = c;
                // disable element style changes
                return;
            }
            this._super(b, c);
            if (b === "content") {
                a.each(this.tooltips, function(a, b) {
                    d._updateContent(b.element);
                });
            }
        },
        _disable: function() {
            var b = this;
            // close open tooltips
            a.each(this.tooltips, function(c, d) {
                var e = a.Event("blur");
                e.target = e.currentTarget = d.element[0];
                b.close(e, true);
            });
            // remove title attributes to prevent native tooltips
            this.element.find(this.options.items).addBack().each(function() {
                var b = a(this);
                if (b.is("[title]")) {
                    b.data("ui-tooltip-title", b.attr("title")).removeAttr("title");
                }
            });
        },
        _enable: function() {
            // restore title attributes
            this.element.find(this.options.items).addBack().each(function() {
                var b = a(this);
                if (b.data("ui-tooltip-title")) {
                    b.attr("title", b.data("ui-tooltip-title"));
                }
            });
        },
        open: function(b) {
            var c = this, d = a(b ? b.target : this.element).closest(this.options.items);
            // No element to show a tooltip for or the tooltip is already open
            if (!d.length || d.data("ui-tooltip-id")) {
                return;
            }
            if (d.attr("title")) {
                d.data("ui-tooltip-title", d.attr("title"));
            }
            d.data("ui-tooltip-open", true);
            // kill parent tooltips, custom or native, for hover
            if (b && b.type === "mouseover") {
                d.parents().each(function() {
                    var b = a(this), d;
                    if (b.data("ui-tooltip-open")) {
                        d = a.Event("blur");
                        d.target = d.currentTarget = this;
                        c.close(d, true);
                    }
                    if (b.attr("title")) {
                        b.uniqueId();
                        c.parents[this.id] = {
                            element: this,
                            title: b.attr("title")
                        };
                        b.attr("title", "");
                    }
                });
            }
            this._registerCloseHandlers(b, d);
            this._updateContent(d, b);
        },
        _updateContent: function(a, b) {
            var c, d = this.options.content, e = this, f = b ? b.type : null;
            if (typeof d === "string") {
                return this._open(b, a, d);
            }
            c = d.call(a[0], function(c) {
                // IE may instantly serve a cached response for ajax requests
                // delay this call to _open so the other call to _open runs first
                e._delay(function() {
                    // Ignore async response if tooltip was closed already
                    if (!a.data("ui-tooltip-open")) {
                        return;
                    }
                    // jQuery creates a special event for focusin when it doesn't
                    // exist natively. To improve performance, the native event
                    // object is reused and the type is changed. Therefore, we can't
                    // rely on the type being correct after the event finished
                    // bubbling, so we set it back to the previous value. (#8740)
                    if (b) {
                        b.type = f;
                    }
                    this._open(b, a, c);
                });
            });
            if (c) {
                this._open(b, a, c);
            }
        },
        _open: function(b, c, d) {
            var e, f, g, h, i = a.extend({}, this.options.position);
            if (!d) {
                return;
            }
            // Content can be updated multiple times. If the tooltip already
            // exists, then just update the content and bail.
            e = this._find(c);
            if (e) {
                e.tooltip.find(".ui-tooltip-content").html(d);
                return;
            }
            // if we have a title, clear it to prevent the native tooltip
            // we have to check first to avoid defining a title if none exists
            // (we don't want to cause an element to start matching [title])
            //
            // We use removeAttr only for key events, to allow IE to export the correct
            // accessible attributes. For mouse events, set to empty string to avoid
            // native tooltip showing up (happens only when removing inside mouseover).
            if (c.is("[title]")) {
                if (b && b.type === "mouseover") {
                    c.attr("title", "");
                } else {
                    c.removeAttr("title");
                }
            }
            e = this._tooltip(c);
            f = e.tooltip;
            this._addDescribedBy(c, f.attr("id"));
            f.find(".ui-tooltip-content").html(d);
            // Support: Voiceover on OS X, JAWS on IE <= 9
            // JAWS announces deletions even when aria-relevant="additions"
            // Voiceover will sometimes re-read the entire log region's contents from the beginning
            this.liveRegion.children().hide();
            if (d.clone) {
                h = d.clone();
                h.removeAttr("id").find("[id]").removeAttr("id");
            } else {
                h = d;
            }
            a("<div>").html(h).appendTo(this.liveRegion);
            function j(a) {
                i.of = a;
                if (f.is(":hidden")) {
                    return;
                }
                f.position(i);
            }
            if (this.options.track && b && /^mouse/.test(b.type)) {
                this._on(this.document, {
                    mousemove: j
                });
                // trigger once to override element-relative positioning
                j(b);
            } else {
                f.position(a.extend({
                    of: c
                }, this.options.position));
            }
            f.hide();
            this._show(f, this.options.show);
            // Handle tracking tooltips that are shown with a delay (#8644). As soon
            // as the tooltip is visible, position the tooltip using the most recent
            // event.
            if (this.options.show && this.options.show.delay) {
                g = this.delayedShow = setInterval(function() {
                    if (f.is(":visible")) {
                        j(i.of);
                        clearInterval(g);
                    }
                }, a.fx.interval);
            }
            this._trigger("open", b, {
                tooltip: f
            });
        },
        _registerCloseHandlers: function(b, c) {
            var d = {
                keyup: function(b) {
                    if (b.keyCode === a.ui.keyCode.ESCAPE) {
                        var d = a.Event(b);
                        d.currentTarget = c[0];
                        this.close(d, true);
                    }
                }
            };
            // Only bind remove handler for delegated targets. Non-delegated
            // tooltips will handle this in destroy.
            if (c[0] !== this.element[0]) {
                d.remove = function() {
                    this._removeTooltip(this._find(c).tooltip);
                };
            }
            if (!b || b.type === "mouseover") {
                d.mouseleave = "close";
            }
            if (!b || b.type === "focusin") {
                d.focusout = "close";
            }
            this._on(true, c, d);
        },
        close: function(b) {
            var c, d = this, e = a(b ? b.currentTarget : this.element), f = this._find(e);
            // The tooltip may already be closed
            if (!f) {
                // We set ui-tooltip-open immediately upon open (in open()), but only set the
                // additional data once there's actually content to show (in _open()). So even if the
                // tooltip doesn't have full data, we always remove ui-tooltip-open in case we're in
                // the period between open() and _open().
                e.removeData("ui-tooltip-open");
                return;
            }
            c = f.tooltip;
            // disabling closes the tooltip, so we need to track when we're closing
            // to avoid an infinite loop in case the tooltip becomes disabled on close
            if (f.closing) {
                return;
            }
            // Clear the interval for delayed tracking tooltips
            clearInterval(this.delayedShow);
            // only set title if we had one before (see comment in _open())
            // If the title attribute has changed since open(), don't restore
            if (e.data("ui-tooltip-title") && !e.attr("title")) {
                e.attr("title", e.data("ui-tooltip-title"));
            }
            this._removeDescribedBy(e);
            f.hiding = true;
            c.stop(true);
            this._hide(c, this.options.hide, function() {
                d._removeTooltip(a(this));
            });
            e.removeData("ui-tooltip-open");
            this._off(e, "mouseleave focusout keyup");
            // Remove 'remove' binding only on delegated targets
            if (e[0] !== this.element[0]) {
                this._off(e, "remove");
            }
            this._off(this.document, "mousemove");
            if (b && b.type === "mouseleave") {
                a.each(this.parents, function(b, c) {
                    a(c.element).attr("title", c.title);
                    delete d.parents[b];
                });
            }
            f.closing = true;
            this._trigger("close", b, {
                tooltip: c
            });
            if (!f.hiding) {
                f.closing = false;
            }
        },
        _tooltip: function(b) {
            var c = a("<div>").attr("role", "tooltip").addClass("ui-tooltip ui-widget ui-corner-all ui-widget-content " + (this.options.tooltipClass || "")), d = c.uniqueId().attr("id");
            a("<div>").addClass("ui-tooltip-content").appendTo(c);
            c.appendTo(this.document[0].body);
            return this.tooltips[d] = {
                element: b,
                tooltip: c
            };
        },
        _find: function(a) {
            var b = a.data("ui-tooltip-id");
            return b ? this.tooltips[b] : null;
        },
        _removeTooltip: function(a) {
            a.remove();
            delete this.tooltips[a.attr("id")];
        },
        _destroy: function() {
            var b = this;
            // close open tooltips
            a.each(this.tooltips, function(c, d) {
                // Delegate to close method to handle common cleanup
                var e = a.Event("blur"), f = d.element;
                e.target = e.currentTarget = f[0];
                b.close(e, true);
                // Remove immediately; destroying an open tooltip doesn't use the
                // hide animation
                a("#" + c).remove();
                // Restore the title
                if (f.data("ui-tooltip-title")) {
                    // If the title attribute has changed since open(), don't restore
                    if (!f.attr("title")) {
                        f.attr("title", f.data("ui-tooltip-title"));
                    }
                    f.removeData("ui-tooltip-title");
                }
            });
            this.liveRegion.remove();
        }
    });
});

/*!
 * jQuery Validation Plugin v1.14.0
 *
 * http://jqueryvalidation.org/
 *
 * Copyright (c) 2015 Jörn Zaefferer
 * Released under the MIT license
 */
(function(a) {
    if (typeof define === "function" && define.amd) {
        define([ "jquery" ], a);
    } else {
        a(jQuery);
    }
})(function(a) {
    a.extend(a.fn, {
        // http://jqueryvalidation.org/validate/
        validate: function(b) {
            // if nothing is selected, return nothing; can't chain anyway
            if (!this.length) {
                if (b && b.debug && window.console) {
                    console.warn("Nothing selected, can't validate, returning nothing.");
                }
                return;
            }
            // check if a validator for this form was already created
            var c = a.data(this[0], "validator");
            if (c) {
                return c;
            }
            // Add novalidate tag if HTML5.
            this.attr("novalidate", "novalidate");
            c = new a.validator(b, this[0]);
            a.data(this[0], "validator", c);
            if (c.settings.onsubmit) {
                this.on("click.validate", ":submit", function(b) {
                    if (c.settings.submitHandler) {
                        c.submitButton = b.target;
                    }
                    // allow suppressing validation by adding a cancel class to the submit button
                    if (a(this).hasClass("cancel")) {
                        c.cancelSubmit = true;
                    }
                    // allow suppressing validation by adding the html5 formnovalidate attribute to the submit button
                    if (a(this).attr("formnovalidate") !== undefined) {
                        c.cancelSubmit = true;
                    }
                });
                // validate the form on submit
                this.on("submit.validate", function(b) {
                    if (c.settings.debug) {
                        // prevent form submit to be able to see console output
                        b.preventDefault();
                    }
                    function d() {
                        var d, e;
                        if (c.settings.submitHandler) {
                            if (c.submitButton) {
                                // insert a hidden input as a replacement for the missing submit button
                                d = a("<input type='hidden'/>").attr("name", c.submitButton.name).val(a(c.submitButton).val()).appendTo(c.currentForm);
                            }
                            e = c.settings.submitHandler.call(c, c.currentForm, b);
                            if (c.submitButton) {
                                // and clean up afterwards; thanks to no-block-scope, hidden can be referenced
                                d.remove();
                            }
                            if (e !== undefined) {
                                return e;
                            }
                            return false;
                        }
                        return true;
                    }
                    // prevent submit for invalid forms or custom submit handlers
                    if (c.cancelSubmit) {
                        c.cancelSubmit = false;
                        return d();
                    }
                    if (c.form()) {
                        if (c.pendingRequest) {
                            c.formSubmitted = true;
                            return false;
                        }
                        return d();
                    } else {
                        c.focusInvalid();
                        return false;
                    }
                });
            }
            return c;
        },
        // http://jqueryvalidation.org/valid/
        valid: function() {
            var b, c, d;
            if (a(this[0]).is("form")) {
                b = this.validate().form();
            } else {
                d = [];
                b = true;
                c = a(this[0].form).validate();
                this.each(function() {
                    b = c.element(this) && b;
                    d = d.concat(c.errorList);
                });
                c.errorList = d;
            }
            return b;
        },
        // http://jqueryvalidation.org/rules/
        rules: function(b, c) {
            var d = this[0], e, f, g, h, i, j;
            if (b) {
                e = a.data(d.form, "validator").settings;
                f = e.rules;
                g = a.validator.staticRules(d);
                switch (b) {
                  case "add":
                    a.extend(g, a.validator.normalizeRule(c));
                    // remove messages from rules, but allow them to be set separately
                    delete g.messages;
                    f[d.name] = g;
                    if (c.messages) {
                        e.messages[d.name] = a.extend(e.messages[d.name], c.messages);
                    }
                    break;

                  case "remove":
                    if (!c) {
                        delete f[d.name];
                        return g;
                    }
                    j = {};
                    a.each(c.split(/\s/), function(b, c) {
                        j[c] = g[c];
                        delete g[c];
                        if (c === "required") {
                            a(d).removeAttr("aria-required");
                        }
                    });
                    return j;
                }
            }
            h = a.validator.normalizeRules(a.extend({}, a.validator.classRules(d), a.validator.attributeRules(d), a.validator.dataRules(d), a.validator.staticRules(d)), d);
            // make sure required is at front
            if (h.required) {
                i = h.required;
                delete h.required;
                h = a.extend({
                    required: i
                }, h);
                a(d).attr("aria-required", "true");
            }
            // make sure remote is at back
            if (h.remote) {
                i = h.remote;
                delete h.remote;
                h = a.extend(h, {
                    remote: i
                });
            }
            return h;
        }
    });
    // Custom selectors
    a.extend(a.expr[":"], {
        // http://jqueryvalidation.org/blank-selector/
        blank: function(b) {
            return !a.trim("" + a(b).val());
        },
        // http://jqueryvalidation.org/filled-selector/
        filled: function(b) {
            return !!a.trim("" + a(b).val());
        },
        // http://jqueryvalidation.org/unchecked-selector/
        unchecked: function(b) {
            return !a(b).prop("checked");
        }
    });
    // constructor for validator
    a.validator = function(b, c) {
        this.settings = a.extend(true, {}, a.validator.defaults, b);
        this.currentForm = c;
        this.init();
    };
    // http://jqueryvalidation.org/jQuery.validator.format/
    a.validator.format = function(b, c) {
        if (arguments.length === 1) {
            return function() {
                var c = a.makeArray(arguments);
                c.unshift(b);
                return a.validator.format.apply(this, c);
            };
        }
        if (arguments.length > 2 && c.constructor !== Array) {
            c = a.makeArray(arguments).slice(1);
        }
        if (c.constructor !== Array) {
            c = [ c ];
        }
        a.each(c, function(a, c) {
            b = b.replace(new RegExp("\\{" + a + "\\}", "g"), function() {
                return c;
            });
        });
        return b;
    };
    a.extend(a.validator, {
        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            validClass: "valid",
            errorElement: "label",
            focusCleanup: false,
            focusInvalid: true,
            errorContainer: a([]),
            errorLabelContainer: a([]),
            onsubmit: true,
            ignore: ":hidden",
            ignoreTitle: false,
            onfocusin: function(a) {
                this.lastActive = a;
                // Hide error label and remove error class on focus if enabled
                if (this.settings.focusCleanup) {
                    if (this.settings.unhighlight) {
                        this.settings.unhighlight.call(this, a, this.settings.errorClass, this.settings.validClass);
                    }
                    this.hideThese(this.errorsFor(a));
                }
            },
            onfocusout: function(a) {
                if (!this.checkable(a) && (a.name in this.submitted || !this.optional(a))) {
                    this.element(a);
                }
            },
            onkeyup: function(b, c) {
                // Avoid revalidate the field when pressing one of the following keys
                // Shift       => 16
                // Ctrl        => 17
                // Alt         => 18
                // Caps lock   => 20
                // End         => 35
                // Home        => 36
                // Left arrow  => 37
                // Up arrow    => 38
                // Right arrow => 39
                // Down arrow  => 40
                // Insert      => 45
                // Num lock    => 144
                // AltGr key   => 225
                var d = [ 16, 17, 18, 20, 35, 36, 37, 38, 39, 40, 45, 144, 225 ];
                if (c.which === 9 && this.elementValue(b) === "" || a.inArray(c.keyCode, d) !== -1) {
                    return;
                } else if (b.name in this.submitted || b === this.lastElement) {
                    this.element(b);
                }
            },
            onclick: function(a) {
                // click on selects, radiobuttons and checkboxes
                if (a.name in this.submitted) {
                    this.element(a);
                } else if (a.parentNode.name in this.submitted) {
                    this.element(a.parentNode);
                }
            },
            highlight: function(b, c, d) {
                if (b.type === "radio") {
                    this.findByName(b.name).addClass(c).removeClass(d);
                } else {
                    a(b).addClass(c).removeClass(d);
                }
            },
            unhighlight: function(b, c, d) {
                if (b.type === "radio") {
                    this.findByName(b.name).removeClass(c).addClass(d);
                } else {
                    a(b).removeClass(c).addClass(d);
                }
            }
        },
        // http://jqueryvalidation.org/jQuery.validator.setDefaults/
        setDefaults: function(b) {
            a.extend(a.validator.defaults, b);
        },
        messages: {
            required: "This field is required.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date ( ISO ).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            maxlength: a.validator.format("Please enter no more than {0} characters."),
            minlength: a.validator.format("Please enter at least {0} characters."),
            rangelength: a.validator.format("Please enter a value between {0} and {1} characters long."),
            range: a.validator.format("Please enter a value between {0} and {1}."),
            max: a.validator.format("Please enter a value less than or equal to {0}."),
            min: a.validator.format("Please enter a value greater than or equal to {0}.")
        },
        autoCreateRanges: false,
        prototype: {
            init: function() {
                this.labelContainer = a(this.settings.errorLabelContainer);
                this.errorContext = this.labelContainer.length && this.labelContainer || a(this.currentForm);
                this.containers = a(this.settings.errorContainer).add(this.settings.errorLabelContainer);
                this.submitted = {};
                this.valueCache = {};
                this.pendingRequest = 0;
                this.pending = {};
                this.invalid = {};
                this.reset();
                var b = this.groups = {}, c;
                a.each(this.settings.groups, function(c, d) {
                    if (typeof d === "string") {
                        d = d.split(/\s/);
                    }
                    a.each(d, function(a, d) {
                        b[d] = c;
                    });
                });
                c = this.settings.rules;
                a.each(c, function(b, d) {
                    c[b] = a.validator.normalizeRule(d);
                });
                function d(b) {
                    var c = a.data(this.form, "validator"), d = "on" + b.type.replace(/^validate/, ""), e = c.settings;
                    if (e[d] && !a(this).is(e.ignore)) {
                        e[d].call(c, this, b);
                    }
                }
                a(this.currentForm).on("focusin.validate focusout.validate keyup.validate", ":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], " + "[type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], " + "[type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], " + "[type='radio'], [type='checkbox']", d).on("click.validate", "select, option, [type='radio'], [type='checkbox']", d);
                if (this.settings.invalidHandler) {
                    a(this.currentForm).on("invalid-form.validate", this.settings.invalidHandler);
                }
                // Add aria-required to any Static/Data/Class required fields before first validation
                // Screen readers require this attribute to be present before the initial submission http://www.w3.org/TR/WCAG-TECHS/ARIA2.html
                a(this.currentForm).find("[required], [data-rule-required], .required").attr("aria-required", "true");
            },
            // http://jqueryvalidation.org/Validator.form/
            form: function() {
                this.checkForm();
                a.extend(this.submitted, this.errorMap);
                this.invalid = a.extend({}, this.errorMap);
                if (!this.valid()) {
                    a(this.currentForm).triggerHandler("invalid-form", [ this ]);
                }
                this.showErrors();
                return this.valid();
            },
            checkForm: function() {
                this.prepareForm();
                for (var a = 0, b = this.currentElements = this.elements(); b[a]; a++) {
                    this.check(b[a]);
                }
                return this.valid();
            },
            // http://jqueryvalidation.org/Validator.element/
            element: function(b) {
                var c = this.clean(b), d = this.validationTargetFor(c), e = true;
                this.lastElement = d;
                if (d === undefined) {
                    delete this.invalid[c.name];
                } else {
                    this.prepareElement(d);
                    this.currentElements = a(d);
                    e = this.check(d) !== false;
                    if (e) {
                        delete this.invalid[d.name];
                    } else {
                        this.invalid[d.name] = true;
                    }
                }
                // Add aria-invalid status for screen readers
                a(b).attr("aria-invalid", !e);
                if (!this.numberOfInvalids()) {
                    // Hide error containers on last error
                    this.toHide = this.toHide.add(this.containers);
                }
                this.showErrors();
                return e;
            },
            // http://jqueryvalidation.org/Validator.showErrors/
            showErrors: function(b) {
                if (b) {
                    // add items to error list and map
                    a.extend(this.errorMap, b);
                    this.errorList = [];
                    for (var c in b) {
                        this.errorList.push({
                            message: b[c],
                            element: this.findByName(c)[0]
                        });
                    }
                    // remove items from success list
                    this.successList = a.grep(this.successList, function(a) {
                        return !(a.name in b);
                    });
                }
                if (this.settings.showErrors) {
                    this.settings.showErrors.call(this, this.errorMap, this.errorList);
                } else {
                    this.defaultShowErrors();
                }
            },
            // http://jqueryvalidation.org/Validator.resetForm/
            resetForm: function() {
                if (a.fn.resetForm) {
                    a(this.currentForm).resetForm();
                }
                this.submitted = {};
                this.lastElement = null;
                this.prepareForm();
                this.hideErrors();
                var b, c = this.elements().removeData("previousValue").removeAttr("aria-invalid");
                if (this.settings.unhighlight) {
                    for (b = 0; c[b]; b++) {
                        this.settings.unhighlight.call(this, c[b], this.settings.errorClass, "");
                    }
                } else {
                    c.removeClass(this.settings.errorClass);
                }
            },
            numberOfInvalids: function() {
                return this.objectLength(this.invalid);
            },
            objectLength: function(a) {
                /* jshint unused: false */
                var b = 0, c;
                for (c in a) {
                    b++;
                }
                return b;
            },
            hideErrors: function() {
                this.hideThese(this.toHide);
            },
            hideThese: function(a) {
                a.not(this.containers).text("");
                this.addWrapper(a).hide();
            },
            valid: function() {
                return this.size() === 0;
            },
            size: function() {
                return this.errorList.length;
            },
            focusInvalid: function() {
                if (this.settings.focusInvalid) {
                    try {
                        a(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").focus().trigger("focusin");
                    } catch (b) {}
                }
            },
            findLastActive: function() {
                var b = this.lastActive;
                return b && a.grep(this.errorList, function(a) {
                    return a.element.name === b.name;
                }).length === 1 && b;
            },
            elements: function() {
                var b = this, c = {};
                // select all valid inputs inside the form (no submit or reset buttons)
                return a(this.currentForm).find("input, select, textarea").not(":submit, :reset, :image, :disabled").not(this.settings.ignore).filter(function() {
                    if (!this.name && b.settings.debug && window.console) {
                        console.error("%o has no name assigned", this);
                    }
                    // select only the first element for each name, and only those with rules specified
                    if (this.name in c || !b.objectLength(a(this).rules())) {
                        return false;
                    }
                    c[this.name] = true;
                    return true;
                });
            },
            clean: function(b) {
                return a(b)[0];
            },
            errors: function() {
                var b = this.settings.errorClass.split(" ").join(".");
                return a(this.settings.errorElement + "." + b, this.errorContext);
            },
            reset: function() {
                this.successList = [];
                this.errorList = [];
                this.errorMap = {};
                this.toShow = a([]);
                this.toHide = a([]);
                this.currentElements = a([]);
            },
            prepareForm: function() {
                this.reset();
                this.toHide = this.errors().add(this.containers);
            },
            prepareElement: function(a) {
                this.reset();
                this.toHide = this.errorsFor(a);
            },
            elementValue: function(b) {
                var c, d = a(b), e = b.type;
                if (e === "radio" || e === "checkbox") {
                    return this.findByName(b.name).filter(":checked").val();
                } else if (e === "number" && typeof b.validity !== "undefined") {
                    return b.validity.badInput ? false : d.val();
                }
                c = d.val();
                if (typeof c === "string") {
                    return c.replace(/\r/g, "");
                }
                return c;
            },
            check: function(b) {
                b = this.validationTargetFor(this.clean(b));
                var c = a(b).rules(), d = a.map(c, function(a, b) {
                    return b;
                }).length, e = false, f = this.elementValue(b), g, h, i;
                for (h in c) {
                    i = {
                        method: h,
                        parameters: c[h]
                    };
                    try {
                        g = a.validator.methods[h].call(this, f, b, i.parameters);
                        // if a method indicates that the field is optional and therefore valid,
                        // don't mark it as valid when there are no other rules
                        if (g === "dependency-mismatch" && d === 1) {
                            e = true;
                            continue;
                        }
                        e = false;
                        if (g === "pending") {
                            this.toHide = this.toHide.not(this.errorsFor(b));
                            return;
                        }
                        if (!g) {
                            this.formatAndAdd(b, i);
                            return false;
                        }
                    } catch (j) {
                        if (this.settings.debug && window.console) {
                            console.log("Exception occurred when checking element " + b.id + ", check the '" + i.method + "' method.", j);
                        }
                        if (j instanceof TypeError) {
                            j.message += ".  Exception occurred when checking element " + b.id + ", check the '" + i.method + "' method.";
                        }
                        throw j;
                    }
                }
                if (e) {
                    return;
                }
                if (this.objectLength(c)) {
                    this.successList.push(b);
                }
                return true;
            },
            // return the custom message for the given element and validation method
            // specified in the element's HTML5 data attribute
            // return the generic message if present and no method specific message is present
            customDataMessage: function(b, c) {
                return a(b).data("msg" + c.charAt(0).toUpperCase() + c.substring(1).toLowerCase()) || a(b).data("msg");
            },
            // return the custom message for the given element name and validation method
            customMessage: function(a, b) {
                var c = this.settings.messages[a];
                return c && (c.constructor === String ? c : c[b]);
            },
            // return the first defined argument, allowing empty strings
            findDefined: function() {
                for (var a = 0; a < arguments.length; a++) {
                    if (arguments[a] !== undefined) {
                        return arguments[a];
                    }
                }
                return undefined;
            },
            defaultMessage: function(b, c) {
                // title is never undefined, so handle empty string as undefined
                return this.findDefined(this.customMessage(b.name, c), this.customDataMessage(b, c), !this.settings.ignoreTitle && b.title || undefined, a.validator.messages[c], "<strong>Warning: No message defined for " + b.name + "</strong>");
            },
            formatAndAdd: function(b, c) {
                var d = this.defaultMessage(b, c.method), e = /\$?\{(\d+)\}/g;
                if (typeof d === "function") {
                    d = d.call(this, c.parameters, b);
                } else if (e.test(d)) {
                    d = a.validator.format(d.replace(e, "{$1}"), c.parameters);
                }
                this.errorList.push({
                    message: d,
                    element: b,
                    method: c.method
                });
                this.errorMap[b.name] = d;
                this.submitted[b.name] = d;
            },
            addWrapper: function(a) {
                if (this.settings.wrapper) {
                    a = a.add(a.parent(this.settings.wrapper));
                }
                return a;
            },
            defaultShowErrors: function() {
                var a, b, c;
                for (a = 0; this.errorList[a]; a++) {
                    c = this.errorList[a];
                    if (this.settings.highlight) {
                        this.settings.highlight.call(this, c.element, this.settings.errorClass, this.settings.validClass);
                    }
                    this.showLabel(c.element, c.message);
                }
                if (this.errorList.length) {
                    this.toShow = this.toShow.add(this.containers);
                }
                if (this.settings.success) {
                    for (a = 0; this.successList[a]; a++) {
                        this.showLabel(this.successList[a]);
                    }
                }
                if (this.settings.unhighlight) {
                    for (a = 0, b = this.validElements(); b[a]; a++) {
                        this.settings.unhighlight.call(this, b[a], this.settings.errorClass, this.settings.validClass);
                    }
                }
                this.toHide = this.toHide.not(this.toShow);
                this.hideErrors();
                this.addWrapper(this.toShow).show();
            },
            validElements: function() {
                return this.currentElements.not(this.invalidElements());
            },
            invalidElements: function() {
                return a(this.errorList).map(function() {
                    return this.element;
                });
            },
            showLabel: function(b, c) {
                var d, e, f, g = this.errorsFor(b), h = this.idOrName(b), i = a(b).attr("aria-describedby");
                if (g.length) {
                    // refresh error/success class
                    g.removeClass(this.settings.validClass).addClass(this.settings.errorClass);
                    // replace message on existing label
                    g.html(c);
                } else {
                    // create error element
                    g = a("<" + this.settings.errorElement + ">").attr("id", h + "-error").addClass(this.settings.errorClass).html(c || "");
                    // Maintain reference to the element to be placed into the DOM
                    d = g;
                    if (this.settings.wrapper) {
                        // make sure the element is visible, even in IE
                        // actually showing the wrapped element is handled elsewhere
                        d = g.hide().show().wrap("<" + this.settings.wrapper + "/>").parent();
                    }
                    if (this.labelContainer.length) {
                        this.labelContainer.append(d);
                    } else if (this.settings.errorPlacement) {
                        this.settings.errorPlacement(d, a(b));
                    } else {
                        d.insertAfter(b);
                    }
                    // Link error back to the element
                    if (g.is("label")) {
                        // If the error is a label, then associate using 'for'
                        g.attr("for", h);
                    } else if (g.parents("label[for='" + h + "']").length === 0) {
                        // If the element is not a child of an associated label, then it's necessary
                        // to explicitly apply aria-describedby
                        f = g.attr("id").replace(/(:|\.|\[|\]|\$)/g, "\\$1");
                        // Respect existing non-error aria-describedby
                        if (!i) {
                            i = f;
                        } else if (!i.match(new RegExp("\\b" + f + "\\b"))) {
                            // Add to end of list if not already present
                            i += " " + f;
                        }
                        a(b).attr("aria-describedby", i);
                        // If this element is grouped, then assign to all elements in the same group
                        e = this.groups[b.name];
                        if (e) {
                            a.each(this.groups, function(b, c) {
                                if (c === e) {
                                    a("[name='" + b + "']", this.currentForm).attr("aria-describedby", g.attr("id"));
                                }
                            });
                        }
                    }
                }
                if (!c && this.settings.success) {
                    g.text("");
                    if (typeof this.settings.success === "string") {
                        g.addClass(this.settings.success);
                    } else {
                        this.settings.success(g, b);
                    }
                }
                this.toShow = this.toShow.add(g);
            },
            errorsFor: function(b) {
                var c = this.idOrName(b), d = a(b).attr("aria-describedby"), e = "label[for='" + c + "'], label[for='" + c + "'] *";
                // aria-describedby should directly reference the error element
                if (d) {
                    e = e + ", #" + d.replace(/\s+/g, ", #");
                }
                return this.errors().filter(e);
            },
            idOrName: function(a) {
                return this.groups[a.name] || (this.checkable(a) ? a.name : a.id || a.name);
            },
            validationTargetFor: function(b) {
                // If radio/checkbox, validate first element in group instead
                if (this.checkable(b)) {
                    b = this.findByName(b.name);
                }
                // Always apply ignore filter
                return a(b).not(this.settings.ignore)[0];
            },
            checkable: function(a) {
                return /radio|checkbox/i.test(a.type);
            },
            findByName: function(b) {
                return a(this.currentForm).find("[name='" + b + "']");
            },
            getLength: function(b, c) {
                switch (c.nodeName.toLowerCase()) {
                  case "select":
                    return a("option:selected", c).length;

                  case "input":
                    if (this.checkable(c)) {
                        return this.findByName(c.name).filter(":checked").length;
                    }
                }
                return b.length;
            },
            depend: function(a, b) {
                return this.dependTypes[typeof a] ? this.dependTypes[typeof a](a, b) : true;
            },
            dependTypes: {
                "boolean": function(a) {
                    return a;
                },
                string: function(b, c) {
                    return !!a(b, c.form).length;
                },
                "function": function(a, b) {
                    return a(b);
                }
            },
            optional: function(b) {
                var c = this.elementValue(b);
                return !a.validator.methods.required.call(this, c, b) && "dependency-mismatch";
            },
            startRequest: function(a) {
                if (!this.pending[a.name]) {
                    this.pendingRequest++;
                    this.pending[a.name] = true;
                }
            },
            stopRequest: function(b, c) {
                this.pendingRequest--;
                // sometimes synchronization fails, make sure pendingRequest is never < 0
                if (this.pendingRequest < 0) {
                    this.pendingRequest = 0;
                }
                delete this.pending[b.name];
                if (c && this.pendingRequest === 0 && this.formSubmitted && this.form()) {
                    a(this.currentForm).submit();
                    this.formSubmitted = false;
                } else if (!c && this.pendingRequest === 0 && this.formSubmitted) {
                    a(this.currentForm).triggerHandler("invalid-form", [ this ]);
                    this.formSubmitted = false;
                }
            },
            previousValue: function(b) {
                return a.data(b, "previousValue") || a.data(b, "previousValue", {
                    old: null,
                    valid: true,
                    message: this.defaultMessage(b, "remote")
                });
            },
            // cleans up all forms and elements, removes validator-specific events
            destroy: function() {
                this.resetForm();
                a(this.currentForm).off(".validate").removeData("validator");
            }
        },
        classRuleSettings: {
            required: {
                required: true
            },
            email: {
                email: true
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            dateISO: {
                dateISO: true
            },
            number: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            }
        },
        addClassRules: function(b, c) {
            if (b.constructor === String) {
                this.classRuleSettings[b] = c;
            } else {
                a.extend(this.classRuleSettings, b);
            }
        },
        classRules: function(b) {
            var c = {}, d = a(b).attr("class");
            if (d) {
                a.each(d.split(" "), function() {
                    if (this in a.validator.classRuleSettings) {
                        a.extend(c, a.validator.classRuleSettings[this]);
                    }
                });
            }
            return c;
        },
        normalizeAttributeRule: function(a, b, c, d) {
            // convert the value to a number for number inputs, and for text for backwards compability
            // allows type="date" and others to be compared as strings
            if (/min|max/.test(c) && (b === null || /number|range|text/.test(b))) {
                d = Number(d);
                // Support Opera Mini, which returns NaN for undefined minlength
                if (isNaN(d)) {
                    d = undefined;
                }
            }
            if (d || d === 0) {
                a[c] = d;
            } else if (b === c && b !== "range") {
                // exception: the jquery validate 'range' method
                // does not test for the html5 'range' type
                a[c] = true;
            }
        },
        attributeRules: function(b) {
            var c = {}, d = a(b), e = b.getAttribute("type"), f, g;
            for (f in a.validator.methods) {
                // support for <input required> in both html5 and older browsers
                if (f === "required") {
                    g = b.getAttribute(f);
                    // Some browsers return an empty string for the required attribute
                    // and non-HTML5 browsers might have required="" markup
                    if (g === "") {
                        g = true;
                    }
                    // force non-HTML5 browsers to return bool
                    g = !!g;
                } else {
                    g = d.attr(f);
                }
                this.normalizeAttributeRule(c, e, f, g);
            }
            // maxlength may be returned as -1, 2147483647 ( IE ) and 524288 ( safari ) for text inputs
            if (c.maxlength && /-1|2147483647|524288/.test(c.maxlength)) {
                delete c.maxlength;
            }
            return c;
        },
        dataRules: function(b) {
            var c = {}, d = a(b), e = b.getAttribute("type"), f, g;
            for (f in a.validator.methods) {
                g = d.data("rule" + f.charAt(0).toUpperCase() + f.substring(1).toLowerCase());
                this.normalizeAttributeRule(c, e, f, g);
            }
            return c;
        },
        staticRules: function(b) {
            var c = {}, d = a.data(b.form, "validator");
            if (d.settings.rules) {
                c = a.validator.normalizeRule(d.settings.rules[b.name]) || {};
            }
            return c;
        },
        normalizeRules: function(b, c) {
            // handle dependency check
            a.each(b, function(d, e) {
                // ignore rule when param is explicitly false, eg. required:false
                if (e === false) {
                    delete b[d];
                    return;
                }
                if (e.param || e.depends) {
                    var f = true;
                    switch (typeof e.depends) {
                      case "string":
                        f = !!a(e.depends, c.form).length;
                        break;

                      case "function":
                        f = e.depends.call(c, c);
                        break;
                    }
                    if (f) {
                        b[d] = e.param !== undefined ? e.param : true;
                    } else {
                        delete b[d];
                    }
                }
            });
            // evaluate parameters
            a.each(b, function(d, e) {
                b[d] = a.isFunction(e) ? e(c) : e;
            });
            // clean number parameters
            a.each([ "minlength", "maxlength" ], function() {
                if (b[this]) {
                    b[this] = Number(b[this]);
                }
            });
            a.each([ "rangelength", "range" ], function() {
                var c;
                if (b[this]) {
                    if (a.isArray(b[this])) {
                        b[this] = [ Number(b[this][0]), Number(b[this][1]) ];
                    } else if (typeof b[this] === "string") {
                        c = b[this].replace(/[\[\]]/g, "").split(/[\s,]+/);
                        b[this] = [ Number(c[0]), Number(c[1]) ];
                    }
                }
            });
            if (a.validator.autoCreateRanges) {
                // auto-create ranges
                if (b.min != null && b.max != null) {
                    b.range = [ b.min, b.max ];
                    delete b.min;
                    delete b.max;
                }
                if (b.minlength != null && b.maxlength != null) {
                    b.rangelength = [ b.minlength, b.maxlength ];
                    delete b.minlength;
                    delete b.maxlength;
                }
            }
            return b;
        },
        // Converts a simple string to a {string: true} rule, e.g., "required" to {required:true}
        normalizeRule: function(b) {
            if (typeof b === "string") {
                var c = {};
                a.each(b.split(/\s/), function() {
                    c[this] = true;
                });
                b = c;
            }
            return b;
        },
        // http://jqueryvalidation.org/jQuery.validator.addMethod/
        addMethod: function(b, c, d) {
            a.validator.methods[b] = c;
            a.validator.messages[b] = d !== undefined ? d : a.validator.messages[b];
            if (c.length < 3) {
                a.validator.addClassRules(b, a.validator.normalizeRule(b));
            }
        },
        methods: {
            // http://jqueryvalidation.org/required-method/
            required: function(b, c, d) {
                // check if dependency is met
                if (!this.depend(d, c)) {
                    return "dependency-mismatch";
                }
                if (c.nodeName.toLowerCase() === "select") {
                    // could be an array for select-multiple or a string, both are fine this way
                    var e = a(c).val();
                    return e && e.length > 0;
                }
                if (this.checkable(c)) {
                    return this.getLength(b, c) > 0;
                }
                return b.length > 0;
            },
            // http://jqueryvalidation.org/email-method/
            email: function(a, b) {
                // From https://html.spec.whatwg.org/multipage/forms.html#valid-e-mail-address
                // Retrieved 2014-01-14
                // If you have a problem with this implementation, report a bug against the above spec
                // Or use custom methods to implement your own email validation
                return this.optional(b) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(a);
            },
            // http://jqueryvalidation.org/url-method/
            url: function(a, b) {
                // Copyright (c) 2010-2013 Diego Perini, MIT licensed
                // https://gist.github.com/dperini/729294
                // see also https://mathiasbynens.be/demo/url-regex
                // modified to allow protocol-relative URLs
                return this.optional(b) || /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[\/?#]\S*)?$/i.test(a);
            },
            // http://jqueryvalidation.org/date-method/
            date: function(a, b) {
                return this.optional(b) || !/Invalid|NaN/.test(new Date(a).toString());
            },
            // http://jqueryvalidation.org/dateISO-method/
            dateISO: function(a, b) {
                return this.optional(b) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(a);
            },
            // http://jqueryvalidation.org/number-method/
            number: function(a, b) {
                return this.optional(b) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(a);
            },
            // http://jqueryvalidation.org/digits-method/
            digits: function(a, b) {
                return this.optional(b) || /^\d+$/.test(a);
            },
            // http://jqueryvalidation.org/creditcard-method/
            // based on http://en.wikipedia.org/wiki/Luhn_algorithm
            creditcard: function(a, b) {
                if (this.optional(b)) {
                    return "dependency-mismatch";
                }
                // accept only spaces, digits and dashes
                if (/[^0-9 \-]+/.test(a)) {
                    return false;
                }
                var c = 0, d = 0, e = false, f, g;
                a = a.replace(/\D/g, "");
                // Basing min and max length on
                // http://developer.ean.com/general_info/Valid_Credit_Card_Types
                if (a.length < 13 || a.length > 19) {
                    return false;
                }
                for (f = a.length - 1; f >= 0; f--) {
                    g = a.charAt(f);
                    d = parseInt(g, 10);
                    if (e) {
                        if ((d *= 2) > 9) {
                            d -= 9;
                        }
                    }
                    c += d;
                    e = !e;
                }
                return c % 10 === 0;
            },
            // http://jqueryvalidation.org/minlength-method/
            minlength: function(b, c, d) {
                var e = a.isArray(b) ? b.length : this.getLength(b, c);
                return this.optional(c) || e >= d;
            },
            // http://jqueryvalidation.org/maxlength-method/
            maxlength: function(b, c, d) {
                var e = a.isArray(b) ? b.length : this.getLength(b, c);
                return this.optional(c) || e <= d;
            },
            // http://jqueryvalidation.org/rangelength-method/
            rangelength: function(b, c, d) {
                var e = a.isArray(b) ? b.length : this.getLength(b, c);
                return this.optional(c) || e >= d[0] && e <= d[1];
            },
            // http://jqueryvalidation.org/min-method/
            min: function(a, b, c) {
                return this.optional(b) || a >= c;
            },
            // http://jqueryvalidation.org/max-method/
            max: function(a, b, c) {
                return this.optional(b) || a <= c;
            },
            // http://jqueryvalidation.org/range-method/
            range: function(a, b, c) {
                return this.optional(b) || a >= c[0] && a <= c[1];
            },
            // http://jqueryvalidation.org/equalTo-method/
            equalTo: function(b, c, d) {
                // bind to the blur event of the target in order to revalidate whenever the target field is updated
                // TODO find a way to bind the event just once, avoiding the unbind-rebind overhead
                var e = a(d);
                if (this.settings.onfocusout) {
                    e.off(".validate-equalTo").on("blur.validate-equalTo", function() {
                        a(c).valid();
                    });
                }
                return b === e.val();
            },
            // http://jqueryvalidation.org/remote-method/
            remote: function(b, c, d) {
                if (this.optional(c)) {
                    return "dependency-mismatch";
                }
                var e = this.previousValue(c), f, g;
                if (!this.settings.messages[c.name]) {
                    this.settings.messages[c.name] = {};
                }
                e.originalMessage = this.settings.messages[c.name].remote;
                this.settings.messages[c.name].remote = e.message;
                d = typeof d === "string" && {
                    url: d
                } || d;
                if (e.old === b) {
                    return e.valid;
                }
                e.old = b;
                f = this;
                this.startRequest(c);
                g = {};
                g[c.name] = b;
                a.ajax(a.extend(true, {
                    mode: "abort",
                    port: "validate" + c.name,
                    dataType: "json",
                    data: g,
                    context: f.currentForm,
                    success: function(d) {
                        var g = d === true || d === "true", h, i, j;
                        f.settings.messages[c.name].remote = e.originalMessage;
                        if (g) {
                            j = f.formSubmitted;
                            f.prepareElement(c);
                            f.formSubmitted = j;
                            f.successList.push(c);
                            delete f.invalid[c.name];
                            f.showErrors();
                        } else {
                            h = {};
                            i = d || f.defaultMessage(c, "remote");
                            h[c.name] = e.message = a.isFunction(i) ? i(b) : i;
                            f.invalid[c.name] = true;
                            f.showErrors(h);
                        }
                        e.valid = g;
                        f.stopRequest(c, g);
                    }
                }, d));
                return "pending";
            }
        }
    });
    // ajax mode: abort
    // usage: $.ajax({ mode: "abort"[, port: "uniqueport"]});
    // if mode:"abort" is used, the previous request on that port (port can be undefined) is aborted via XMLHttpRequest.abort()
    var b = {}, c;
    // Use a prefilter if available (1.5+)
    if (a.ajaxPrefilter) {
        a.ajaxPrefilter(function(a, c, d) {
            var e = a.port;
            if (a.mode === "abort") {
                if (b[e]) {
                    b[e].abort();
                }
                b[e] = d;
            }
        });
    } else {
        // Proxy ajax
        c = a.ajax;
        a.ajax = function(d) {
            var e = ("mode" in d ? d : a.ajaxSettings).mode, f = ("port" in d ? d : a.ajaxSettings).port;
            if (e === "abort") {
                if (b[f]) {
                    b[f].abort();
                }
                b[f] = c.apply(this, arguments);
                return b[f];
            }
            return c.apply(this, arguments);
        };
    }
});

/*!
 * jQuery Validation Plugin v1.14.0
 *
 * http://jqueryvalidation.org/
 *
 * Copyright (c) 2015 Jörn Zaefferer
 * Released under the MIT license
 */
(function(a) {
    if (typeof define === "function" && define.amd) {
        define([ "jquery", "./jquery.validate" ], a);
    } else {
        a(jQuery);
    }
})(function(a) {
    (function() {
        function b(a) {
            // remove html tags and space chars
            return a.replace(/<.[^<>]*?>/g, " ").replace(/&nbsp;|&#160;/gi, " ").replace(/[.(),;:!?%#$'\"_+=\/\-“”’]*/g, "");
        }
        a.validator.addMethod("maxWords", function(a, c, d) {
            return this.optional(c) || b(a).match(/\b\w+\b/g).length <= d;
        }, a.validator.format("Please enter {0} words or less."));
        a.validator.addMethod("minWords", function(a, c, d) {
            return this.optional(c) || b(a).match(/\b\w+\b/g).length >= d;
        }, a.validator.format("Please enter at least {0} words."));
        a.validator.addMethod("rangeWords", function(a, c, d) {
            var e = b(a), f = /\b\w+\b/g;
            return this.optional(c) || e.match(f).length >= d[0] && e.match(f).length <= d[1];
        }, a.validator.format("Please enter between {0} and {1} words."));
    })();
    // Accept a value from a file input based on a required mimetype
    a.validator.addMethod("accept", function(b, c, d) {
        // Split mime on commas in case we have multiple types we can accept
        var e = typeof d === "string" ? d.replace(/\s/g, "").replace(/,/g, "|") : "image/*", f = this.optional(c), g, h;
        // Element is optional
        if (f) {
            return f;
        }
        if (a(c).attr("type") === "file") {
            // If we are using a wildcard, make it regex friendly
            e = e.replace(/\*/g, ".*");
            // Check if the element has a FileList before checking each file
            if (c.files && c.files.length) {
                for (g = 0; g < c.files.length; g++) {
                    h = c.files[g];
                    // Grab the mimetype from the loaded file, verify it matches
                    if (!h.type.match(new RegExp("\\.?(" + e + ")$", "i"))) {
                        return false;
                    }
                }
            }
        }
        // Either return true because we've validated each file, or because the
        // browser does not support element.files and the FileList feature
        return true;
    }, a.validator.format("Please enter a value with a valid mimetype."));
    a.validator.addMethod("alphanumeric", function(a, b) {
        return this.optional(b) || /^\w+$/i.test(a);
    }, "Letters, numbers, and underscores only please");
    /*
 * Dutch bank account numbers (not 'giro' numbers) have 9 digits
 * and pass the '11 check'.
 * We accept the notation with spaces, as that is common.
 * acceptable: 123456789 or 12 34 56 789
 */
    a.validator.addMethod("bankaccountNL", function(a, b) {
        if (this.optional(b)) {
            return true;
        }
        if (!/^[0-9]{9}|([0-9]{2} ){3}[0-9]{3}$/.test(a)) {
            return false;
        }
        // now '11 check'
        var c = a.replace(/ /g, ""), // remove spaces
        d = 0, e = c.length, f, g, h;
        for (f = 0; f < e; f++) {
            g = e - f;
            h = c.substring(f, f + 1);
            d = d + g * h;
        }
        return d % 11 === 0;
    }, "Please specify a valid bank account number");
    a.validator.addMethod("bankorgiroaccountNL", function(b, c) {
        return this.optional(c) || a.validator.methods.bankaccountNL.call(this, b, c) || a.validator.methods.giroaccountNL.call(this, b, c);
    }, "Please specify a valid bank or giro account number");
    /**
 * BIC is the business identifier code (ISO 9362). This BIC check is not a guarantee for authenticity.
 *
 * BIC pattern: BBBBCCLLbbb (8 or 11 characters long; bbb is optional)
 *
 * BIC definition in detail:
 * - First 4 characters - bank code (only letters)
 * - Next 2 characters - ISO 3166-1 alpha-2 country code (only letters)
 * - Next 2 characters - location code (letters and digits)
 *   a. shall not start with '0' or '1'
 *   b. second character must be a letter ('O' is not allowed) or one of the following digits ('0' for test (therefore not allowed), '1' for passive participant and '2' for active participant)
 * - Last 3 characters - branch code, optional (shall not start with 'X' except in case of 'XXX' for primary office) (letters and digits)
 */
    a.validator.addMethod("bic", function(a, b) {
        return this.optional(b) || /^([A-Z]{6}[A-Z2-9][A-NP-Z1-2])(X{3}|[A-WY-Z0-9][A-Z0-9]{2})?$/.test(a);
    }, "Please specify a valid BIC code");
    /*
 * Código de identificación fiscal ( CIF ) is the tax identification code for Spanish legal entities
 * Further rules can be found in Spanish on http://es.wikipedia.org/wiki/C%C3%B3digo_de_identificaci%C3%B3n_fiscal
 */
    a.validator.addMethod("cifES", function(a) {
        "use strict";
        var b = [], c, d, e, f, g, h;
        a = a.toUpperCase();
        // Quick format test
        if (!a.match("((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)")) {
            return false;
        }
        for (e = 0; e < 9; e++) {
            b[e] = parseInt(a.charAt(e), 10);
        }
        // Algorithm for checking CIF codes
        d = b[2] + b[4] + b[6];
        for (f = 1; f < 8; f += 2) {
            g = (2 * b[f]).toString();
            h = g.charAt(1);
            d += parseInt(g.charAt(0), 10) + (h === "" ? 0 : parseInt(h, 10));
        }
        /* The first (position 1) is a letter following the following criteria:
	 *	A. Corporations
	 *	B. LLCs
	 *	C. General partnerships
	 *	D. Companies limited partnerships
	 *	E. Communities of goods
	 *	F. Cooperative Societies
	 *	G. Associations
	 *	H. Communities of homeowners in horizontal property regime
	 *	J. Civil Societies
	 *	K. Old format
	 *	L. Old format
	 *	M. Old format
	 *	N. Nonresident entities
	 *	P. Local authorities
	 *	Q. Autonomous bodies, state or not, and the like, and congregations and religious institutions
	 *	R. Congregations and religious institutions (since 2008 ORDER EHA/451/2008)
	 *	S. Organs of State Administration and regions
	 *	V. Agrarian Transformation
	 *	W. Permanent establishments of non-resident in Spain
	 */
        if (/^[ABCDEFGHJNPQRSUVW]{1}/.test(a)) {
            d += "";
            c = 10 - parseInt(d.charAt(d.length - 1), 10);
            a += c;
            return b[8].toString() === String.fromCharCode(64 + c) || b[8].toString() === a.charAt(a.length - 1);
        }
        return false;
    }, "Please specify a valid CIF number.");
    /*
 * Brazillian CPF number (Cadastrado de Pessoas Físicas) is the equivalent of a Brazilian tax registration number.
 * CPF numbers have 11 digits in total: 9 numbers followed by 2 check numbers that are being used for validation.
 */
    a.validator.addMethod("cpfBR", function(a) {
        // Removing special characters from value
        a = a.replace(/([~!@#$%^&*()_+=`{}\[\]\-|\\:;'<>,.\/? ])+/g, "");
        // Checking value to have 11 digits only
        if (a.length !== 11) {
            return false;
        }
        var b = 0, c, d, e, f;
        c = parseInt(a.substring(9, 10), 10);
        d = parseInt(a.substring(10, 11), 10);
        e = function(a, b) {
            var c = a * 10 % 11;
            if (c === 10 || c === 11) {
                c = 0;
            }
            return c === b;
        };
        // Checking for dump data
        if (a === "" || a === "00000000000" || a === "11111111111" || a === "22222222222" || a === "33333333333" || a === "44444444444" || a === "55555555555" || a === "66666666666" || a === "77777777777" || a === "88888888888" || a === "99999999999") {
            return false;
        }
        // Step 1 - using first Check Number:
        for (f = 1; f <= 9; f++) {
            b = b + parseInt(a.substring(f - 1, f), 10) * (11 - f);
        }
        // If first Check Number (CN) is valid, move to Step 2 - using second Check Number:
        if (e(b, c)) {
            b = 0;
            for (f = 1; f <= 10; f++) {
                b = b + parseInt(a.substring(f - 1, f), 10) * (12 - f);
            }
            return e(b, d);
        }
        return false;
    }, "Please specify a valid CPF number");
    /* NOTICE: Modified version of Castle.Components.Validator.CreditCardValidator
 * Redistributed under the the Apache License 2.0 at http://www.apache.org/licenses/LICENSE-2.0
 * Valid Types: mastercard, visa, amex, dinersclub, enroute, discover, jcb, unknown, all (overrides all other settings)
 */
    a.validator.addMethod("creditcardtypes", function(a, b, c) {
        if (/[^0-9\-]+/.test(a)) {
            return false;
        }
        a = a.replace(/\D/g, "");
        var d = 0;
        if (c.mastercard) {
            d |= 1;
        }
        if (c.visa) {
            d |= 2;
        }
        if (c.amex) {
            d |= 4;
        }
        if (c.dinersclub) {
            d |= 8;
        }
        if (c.enroute) {
            d |= 16;
        }
        if (c.discover) {
            d |= 32;
        }
        if (c.jcb) {
            d |= 64;
        }
        if (c.unknown) {
            d |= 128;
        }
        if (c.all) {
            d = 1 | 2 | 4 | 8 | 16 | 32 | 64 | 128;
        }
        if (d & 1 && /^(5[12345])/.test(a)) {
            //mastercard
            return a.length === 16;
        }
        if (d & 2 && /^(4)/.test(a)) {
            //visa
            return a.length === 16;
        }
        if (d & 4 && /^(3[47])/.test(a)) {
            //amex
            return a.length === 15;
        }
        if (d & 8 && /^(3(0[012345]|[68]))/.test(a)) {
            //dinersclub
            return a.length === 14;
        }
        if (d & 16 && /^(2(014|149))/.test(a)) {
            //enroute
            return a.length === 15;
        }
        if (d & 32 && /^(6011)/.test(a)) {
            //discover
            return a.length === 16;
        }
        if (d & 64 && /^(3)/.test(a)) {
            //jcb
            return a.length === 16;
        }
        if (d & 64 && /^(2131|1800)/.test(a)) {
            //jcb
            return a.length === 15;
        }
        if (d & 128) {
            //unknown
            return true;
        }
        return false;
    }, "Please enter a valid credit card number.");
    /**
 * Validates currencies with any given symbols by @jameslouiz
 * Symbols can be optional or required. Symbols required by default
 *
 * Usage examples:
 *  currency: ["£", false] - Use false for soft currency validation
 *  currency: ["$", false]
 *  currency: ["RM", false] - also works with text based symbols such as "RM" - Malaysia Ringgit etc
 *
 *  <input class="currencyInput" name="currencyInput">
 *
 * Soft symbol checking
 *  currencyInput: {
 *     currency: ["$", false]
 *  }
 *
 * Strict symbol checking (default)
 *  currencyInput: {
 *     currency: "$"
 *     //OR
 *     currency: ["$", true]
 *  }
 *
 * Multiple Symbols
 *  currencyInput: {
 *     currency: "$,£,¢"
 *  }
 */
    a.validator.addMethod("currency", function(a, b, c) {
        var d = typeof c === "string", e = d ? c : c[0], f = d ? true : c[1], g;
        e = e.replace(/,/g, "");
        e = f ? e + "]" : e + "]?";
        g = "^[" + e + "([1-9]{1}[0-9]{0,2}(\\,[0-9]{3})*(\\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\\.[0-9]{0,2})?|0(\\.[0-9]{0,2})?|(\\.[0-9]{1,2})?)$";
        g = new RegExp(g);
        return this.optional(b) || g.test(a);
    }, "Please specify a valid currency");
    a.validator.addMethod("dateFA", function(a, b) {
        return this.optional(b) || /^[1-4]\d{3}\/((0?[1-6]\/((3[0-1])|([1-2][0-9])|(0?[1-9])))|((1[0-2]|(0?[7-9]))\/(30|([1-2][0-9])|(0?[1-9]))))$/.test(a);
    }, a.validator.messages.date);
    /**
 * Return true, if the value is a valid date, also making this formal check dd/mm/yyyy.
 *
 * @example $.validator.methods.date("01/01/1900")
 * @result true
 *
 * @example $.validator.methods.date("01/13/1990")
 * @result false
 *
 * @example $.validator.methods.date("01.01.1900")
 * @result false
 *
 * @example <input name="pippo" class="{dateITA:true}" />
 * @desc Declares an optional input element whose value must be a valid date.
 *
 * @name $.validator.methods.dateITA
 * @type Boolean
 * @cat Plugins/Validate/Methods
 */
    a.validator.addMethod("dateITA", function(a, b) {
        var c = false, d = /^\d{1,2}\/\d{1,2}\/\d{4}$/, e, f, g, h, i;
        if (d.test(a)) {
            e = a.split("/");
            f = parseInt(e[0], 10);
            g = parseInt(e[1], 10);
            h = parseInt(e[2], 10);
            i = new Date(Date.UTC(h, g - 1, f, 12, 0, 0, 0));
            if (i.getUTCFullYear() === h && i.getUTCMonth() === g - 1 && i.getUTCDate() === f) {
                c = true;
            } else {
                c = false;
            }
        } else {
            c = false;
        }
        return this.optional(b) || c;
    }, a.validator.messages.date);
    a.validator.addMethod("dateNL", function(a, b) {
        return this.optional(b) || /^(0?[1-9]|[12]\d|3[01])[\.\/\-](0?[1-9]|1[012])[\.\/\-]([12]\d)?(\d\d)$/.test(a);
    }, a.validator.messages.date);
    // Older "accept" file extension method. Old docs: http://docs.jquery.com/Plugins/Validation/Methods/accept
    a.validator.addMethod("extension", function(a, b, c) {
        c = typeof c === "string" ? c.replace(/,/g, "|") : "png|jpe?g|gif";
        return this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"));
    }, a.validator.format("Please enter a value with a valid extension."));
    /**
 * Dutch giro account numbers (not bank numbers) have max 7 digits
 */
    a.validator.addMethod("giroaccountNL", function(a, b) {
        return this.optional(b) || /^[0-9]{1,7}$/.test(a);
    }, "Please specify a valid giro account number");
    /**
 * IBAN is the international bank account number.
 * It has a country - specific format, that is checked here too
 */
    a.validator.addMethod("iban", function(a, b) {
        // some quick simple tests to prevent needless work
        if (this.optional(b)) {
            return true;
        }
        // remove spaces and to upper case
        var c = a.replace(/ /g, "").toUpperCase(), d = "", e = true, f = "", g = "", h, i, j, k, l, m, n, o, p;
        // check the country code and find the country specific format
        h = c.substring(0, 2);
        m = {
            AL: "\\d{8}[\\dA-Z]{16}",
            AD: "\\d{8}[\\dA-Z]{12}",
            AT: "\\d{16}",
            AZ: "[\\dA-Z]{4}\\d{20}",
            BE: "\\d{12}",
            BH: "[A-Z]{4}[\\dA-Z]{14}",
            BA: "\\d{16}",
            BR: "\\d{23}[A-Z][\\dA-Z]",
            BG: "[A-Z]{4}\\d{6}[\\dA-Z]{8}",
            CR: "\\d{17}",
            HR: "\\d{17}",
            CY: "\\d{8}[\\dA-Z]{16}",
            CZ: "\\d{20}",
            DK: "\\d{14}",
            DO: "[A-Z]{4}\\d{20}",
            EE: "\\d{16}",
            FO: "\\d{14}",
            FI: "\\d{14}",
            FR: "\\d{10}[\\dA-Z]{11}\\d{2}",
            GE: "[\\dA-Z]{2}\\d{16}",
            DE: "\\d{18}",
            GI: "[A-Z]{4}[\\dA-Z]{15}",
            GR: "\\d{7}[\\dA-Z]{16}",
            GL: "\\d{14}",
            GT: "[\\dA-Z]{4}[\\dA-Z]{20}",
            HU: "\\d{24}",
            IS: "\\d{22}",
            IE: "[\\dA-Z]{4}\\d{14}",
            IL: "\\d{19}",
            IT: "[A-Z]\\d{10}[\\dA-Z]{12}",
            KZ: "\\d{3}[\\dA-Z]{13}",
            KW: "[A-Z]{4}[\\dA-Z]{22}",
            LV: "[A-Z]{4}[\\dA-Z]{13}",
            LB: "\\d{4}[\\dA-Z]{20}",
            LI: "\\d{5}[\\dA-Z]{12}",
            LT: "\\d{16}",
            LU: "\\d{3}[\\dA-Z]{13}",
            MK: "\\d{3}[\\dA-Z]{10}\\d{2}",
            MT: "[A-Z]{4}\\d{5}[\\dA-Z]{18}",
            MR: "\\d{23}",
            MU: "[A-Z]{4}\\d{19}[A-Z]{3}",
            MC: "\\d{10}[\\dA-Z]{11}\\d{2}",
            MD: "[\\dA-Z]{2}\\d{18}",
            ME: "\\d{18}",
            NL: "[A-Z]{4}\\d{10}",
            NO: "\\d{11}",
            PK: "[\\dA-Z]{4}\\d{16}",
            PS: "[\\dA-Z]{4}\\d{21}",
            PL: "\\d{24}",
            PT: "\\d{21}",
            RO: "[A-Z]{4}[\\dA-Z]{16}",
            SM: "[A-Z]\\d{10}[\\dA-Z]{12}",
            SA: "\\d{2}[\\dA-Z]{18}",
            RS: "\\d{18}",
            SK: "\\d{20}",
            SI: "\\d{15}",
            ES: "\\d{20}",
            SE: "\\d{20}",
            CH: "\\d{5}[\\dA-Z]{12}",
            TN: "\\d{20}",
            TR: "\\d{5}[\\dA-Z]{17}",
            AE: "\\d{3}\\d{16}",
            GB: "[A-Z]{4}\\d{14}",
            VG: "[\\dA-Z]{4}\\d{16}"
        };
        l = m[h];
        // As new countries will start using IBAN in the
        // future, we only check if the countrycode is known.
        // This prevents false negatives, while almost all
        // false positives introduced by this, will be caught
        // by the checksum validation below anyway.
        // Strict checking should return FALSE for unknown
        // countries.
        if (typeof l !== "undefined") {
            n = new RegExp("^[A-Z]{2}\\d{2}" + l + "$", "");
            if (!n.test(c)) {
                return false;
            }
        }
        // now check the checksum, first convert to digits
        i = c.substring(4, c.length) + c.substring(0, 4);
        for (o = 0; o < i.length; o++) {
            j = i.charAt(o);
            if (j !== "0") {
                e = false;
            }
            if (!e) {
                d += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(j);
            }
        }
        // calculate the result of: ibancheckdigits % 97
        for (p = 0; p < d.length; p++) {
            k = d.charAt(p);
            g = "" + f + "" + k;
            f = g % 97;
        }
        return f === 1;
    }, "Please specify a valid IBAN");
    a.validator.addMethod("integer", function(a, b) {
        return this.optional(b) || /^-?\d+$/.test(a);
    }, "A positive or negative non-decimal number please");
    a.validator.addMethod("ipv4", function(a, b) {
        return this.optional(b) || /^(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)$/i.test(a);
    }, "Please enter a valid IP v4 address.");
    a.validator.addMethod("ipv6", function(a, b) {
        return this.optional(b) || /^((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(([0-9A-Fa-f]{1,4}:){0,5}:((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(::([0-9A-Fa-f]{1,4}:){0,5}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|([0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})|(::([0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:))$/i.test(a);
    }, "Please enter a valid IP v6 address.");
    a.validator.addMethod("lettersonly", function(a, b) {
        return this.optional(b) || /^[a-z]+$/i.test(a);
    }, "Letters only please");
    a.validator.addMethod("letterswithbasicpunc", function(a, b) {
        return this.optional(b) || /^[a-z\-.,()'"\s]+$/i.test(a);
    }, "Letters or punctuation only please");
    a.validator.addMethod("mobileNL", function(a, b) {
        return this.optional(b) || /^((\+|00(\s|\s?\-\s?)?)31(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)6((\s|\s?\-\s?)?[0-9]){8}$/.test(a);
    }, "Please specify a valid mobile number");
    /* For UK phone functions, do the following server side processing:
 * Compare original input with this RegEx pattern:
 * ^\(?(?:(?:00\)?[\s\-]?\(?|\+)(44)\)?[\s\-]?\(?(?:0\)?[\s\-]?\(?)?|0)([1-9]\d{1,4}\)?[\s\d\-]+)$
 * Extract $1 and set $prefix to '+44<space>' if $1 is '44', otherwise set $prefix to '0'
 * Extract $2 and remove hyphens, spaces and parentheses. Phone number is combined $prefix and $2.
 * A number of very detailed GB telephone number RegEx patterns can also be found at:
 * http://www.aa-asterisk.org.uk/index.php/Regular_Expressions_for_Validating_and_Formatting_GB_Telephone_Numbers
 */
    a.validator.addMethod("mobileUK", function(a, b) {
        a = a.replace(/\(|\)|\s+|-/g, "");
        return this.optional(b) || a.length > 9 && a.match(/^(?:(?:(?:00\s?|\+)44\s?|0)7(?:[1345789]\d{2}|624)\s?\d{3}\s?\d{3})$/);
    }, "Please specify a valid mobile number");
    /*
 * The número de identidad de extranjero ( NIE )is a code used to identify the non-nationals in Spain
 */
    a.validator.addMethod("nieES", function(a) {
        "use strict";
        a = a.toUpperCase();
        // Basic format test
        if (!a.match("((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)")) {
            return false;
        }
        // Test NIE
        //T
        if (/^[T]{1}/.test(a)) {
            return a[8] === /^[T]{1}[A-Z0-9]{8}$/.test(a);
        }
        //XYZ
        if (/^[XYZ]{1}/.test(a)) {
            return a[8] === "TRWAGMYFPDXBNJZSQVHLCKE".charAt(a.replace("X", "0").replace("Y", "1").replace("Z", "2").substring(0, 8) % 23);
        }
        return false;
    }, "Please specify a valid NIE number.");
    /*
 * The Número de Identificación Fiscal ( NIF ) is the way tax identification used in Spain for individuals
 */
    a.validator.addMethod("nifES", function(a) {
        "use strict";
        a = a.toUpperCase();
        // Basic format test
        if (!a.match("((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)")) {
            return false;
        }
        // Test NIF
        if (/^[0-9]{8}[A-Z]{1}$/.test(a)) {
            return "TRWAGMYFPDXBNJZSQVHLCKE".charAt(a.substring(8, 0) % 23) === a.charAt(8);
        }
        // Test specials NIF (starts with K, L or M)
        if (/^[KLM]{1}/.test(a)) {
            return a[8] === String.fromCharCode(64);
        }
        return false;
    }, "Please specify a valid NIF number.");
    jQuery.validator.addMethod("notEqualTo", function(b, c, d) {
        return this.optional(c) || !a.validator.methods.equalTo.call(this, b, c, d);
    }, "Please enter a different value, values must not be the same.");
    a.validator.addMethod("nowhitespace", function(a, b) {
        return this.optional(b) || /^\S+$/i.test(a);
    }, "No white space please");
    /**
* Return true if the field value matches the given format RegExp
*
* @example $.validator.methods.pattern("AR1004",element,/^AR\d{4}$/)
* @result true
*
* @example $.validator.methods.pattern("BR1004",element,/^AR\d{4}$/)
* @result false
*
* @name $.validator.methods.pattern
* @type Boolean
* @cat Plugins/Validate/Methods
*/
    a.validator.addMethod("pattern", function(a, b, c) {
        if (this.optional(b)) {
            return true;
        }
        if (typeof c === "string") {
            c = new RegExp("^(?:" + c + ")$");
        }
        return c.test(a);
    }, "Invalid format.");
    /**
 * Dutch phone numbers have 10 digits (or 11 and start with +31).
 */
    a.validator.addMethod("phoneNL", function(a, b) {
        return this.optional(b) || /^((\+|00(\s|\s?\-\s?)?)31(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)[1-9]((\s|\s?\-\s?)?[0-9]){8}$/.test(a);
    }, "Please specify a valid phone number.");
    /* For UK phone functions, do the following server side processing:
 * Compare original input with this RegEx pattern:
 * ^\(?(?:(?:00\)?[\s\-]?\(?|\+)(44)\)?[\s\-]?\(?(?:0\)?[\s\-]?\(?)?|0)([1-9]\d{1,4}\)?[\s\d\-]+)$
 * Extract $1 and set $prefix to '+44<space>' if $1 is '44', otherwise set $prefix to '0'
 * Extract $2 and remove hyphens, spaces and parentheses. Phone number is combined $prefix and $2.
 * A number of very detailed GB telephone number RegEx patterns can also be found at:
 * http://www.aa-asterisk.org.uk/index.php/Regular_Expressions_for_Validating_and_Formatting_GB_Telephone_Numbers
 */
    a.validator.addMethod("phoneUK", function(a, b) {
        a = a.replace(/\(|\)|\s+|-/g, "");
        return this.optional(b) || a.length > 9 && a.match(/^(?:(?:(?:00\s?|\+)44\s?)|(?:\(?0))(?:\d{2}\)?\s?\d{4}\s?\d{4}|\d{3}\)?\s?\d{3}\s?\d{3,4}|\d{4}\)?\s?(?:\d{5}|\d{3}\s?\d{3})|\d{5}\)?\s?\d{4,5})$/);
    }, "Please specify a valid phone number");
    /**
 * matches US phone number format
 *
 * where the area code may not start with 1 and the prefix may not start with 1
 * allows '-' or ' ' as a separator and allows parens around area code
 * some people may want to put a '1' in front of their number
 *
 * 1(212)-999-2345 or
 * 212 999 2344 or
 * 212-999-0983
 *
 * but not
 * 111-123-5434
 * and not
 * 212 123 4567
 */
    a.validator.addMethod("phoneUS", function(a, b) {
        a = a.replace(/\s+/g, "");
        return this.optional(b) || a.length > 9 && a.match(/^(\+?1-?)?(\([2-9]([02-9]\d|1[02-9])\)|[2-9]([02-9]\d|1[02-9]))-?[2-9]([02-9]\d|1[02-9])-?\d{4}$/);
    }, "Please specify a valid phone number");
    /* For UK phone functions, do the following server side processing:
 * Compare original input with this RegEx pattern:
 * ^\(?(?:(?:00\)?[\s\-]?\(?|\+)(44)\)?[\s\-]?\(?(?:0\)?[\s\-]?\(?)?|0)([1-9]\d{1,4}\)?[\s\d\-]+)$
 * Extract $1 and set $prefix to '+44<space>' if $1 is '44', otherwise set $prefix to '0'
 * Extract $2 and remove hyphens, spaces and parentheses. Phone number is combined $prefix and $2.
 * A number of very detailed GB telephone number RegEx patterns can also be found at:
 * http://www.aa-asterisk.org.uk/index.php/Regular_Expressions_for_Validating_and_Formatting_GB_Telephone_Numbers
 */
    //Matches UK landline + mobile, accepting only 01-3 for landline or 07 for mobile to exclude many premium numbers
    a.validator.addMethod("phonesUK", function(a, b) {
        a = a.replace(/\(|\)|\s+|-/g, "");
        return this.optional(b) || a.length > 9 && a.match(/^(?:(?:(?:00\s?|\+)44\s?|0)(?:1\d{8,9}|[23]\d{9}|7(?:[1345789]\d{8}|624\d{6})))$/);
    }, "Please specify a valid uk phone number");
    /**
 * Matches a valid Canadian Postal Code
 *
 * @example jQuery.validator.methods.postalCodeCA( "H0H 0H0", element )
 * @result true
 *
 * @example jQuery.validator.methods.postalCodeCA( "H0H0H0", element )
 * @result false
 *
 * @name jQuery.validator.methods.postalCodeCA
 * @type Boolean
 * @cat Plugins/Validate/Methods
 */
    a.validator.addMethod("postalCodeCA", function(a, b) {
        return this.optional(b) || /^[ABCEGHJKLMNPRSTVXY]\d[A-Z] \d[A-Z]\d$/.test(a);
    }, "Please specify a valid postal code");
    /*
* Valida CEPs do brasileiros:
*
* Formatos aceitos:
* 99999-999
* 99.999-999
* 99999999
*/
    a.validator.addMethod("postalcodeBR", function(a, b) {
        return this.optional(b) || /^\d{2}.\d{3}-\d{3}?$|^\d{5}-?\d{3}?$/.test(a);
    }, "Informe um CEP válido.");
    /* Matches Italian postcode (CAP) */
    a.validator.addMethod("postalcodeIT", function(a, b) {
        return this.optional(b) || /^\d{5}$/.test(a);
    }, "Please specify a valid postal code");
    a.validator.addMethod("postalcodeNL", function(a, b) {
        return this.optional(b) || /^[1-9][0-9]{3}\s?[a-zA-Z]{2}$/.test(a);
    }, "Please specify a valid postal code");
    // Matches UK postcode. Does not match to UK Channel Islands that have their own postcodes (non standard UK)
    a.validator.addMethod("postcodeUK", function(a, b) {
        return this.optional(b) || /^((([A-PR-UWYZ][0-9])|([A-PR-UWYZ][0-9][0-9])|([A-PR-UWYZ][A-HK-Y][0-9])|([A-PR-UWYZ][A-HK-Y][0-9][0-9])|([A-PR-UWYZ][0-9][A-HJKSTUW])|([A-PR-UWYZ][A-HK-Y][0-9][ABEHMNPRVWXY]))\s?([0-9][ABD-HJLNP-UW-Z]{2})|(GIR)\s?(0AA))$/i.test(a);
    }, "Please specify a valid UK postcode");
    /*
 * Lets you say "at least X inputs that match selector Y must be filled."
 *
 * The end result is that neither of these inputs:
 *
 *	<input class="productinfo" name="partnumber">
 *	<input class="productinfo" name="description">
 *
 *	...will validate unless at least one of them is filled.
 *
 * partnumber:	{require_from_group: [1,".productinfo"]},
 * description: {require_from_group: [1,".productinfo"]}
 *
 * options[0]: number of fields that must be filled in the group
 * options[1]: CSS selector that defines the group of conditionally required fields
 */
    a.validator.addMethod("require_from_group", function(b, c, d) {
        var e = a(d[1], c.form), f = e.eq(0), g = f.data("valid_req_grp") ? f.data("valid_req_grp") : a.extend({}, this), h = e.filter(function() {
            return g.elementValue(this);
        }).length >= d[0];
        // Store the cloned validator for future validation
        f.data("valid_req_grp", g);
        // If element isn't being validated, run each require_from_group field's validation rules
        if (!a(c).data("being_validated")) {
            e.data("being_validated", true);
            e.each(function() {
                g.element(this);
            });
            e.data("being_validated", false);
        }
        return h;
    }, a.validator.format("Please fill at least {0} of these fields."));
    /*
 * Lets you say "either at least X inputs that match selector Y must be filled,
 * OR they must all be skipped (left blank)."
 *
 * The end result, is that none of these inputs:
 *
 *	<input class="productinfo" name="partnumber">
 *	<input class="productinfo" name="description">
 *	<input class="productinfo" name="color">
 *
 *	...will validate unless either at least two of them are filled,
 *	OR none of them are.
 *
 * partnumber:	{skip_or_fill_minimum: [2,".productinfo"]},
 * description: {skip_or_fill_minimum: [2,".productinfo"]},
 * color:		{skip_or_fill_minimum: [2,".productinfo"]}
 *
 * options[0]: number of fields that must be filled in the group
 * options[1]: CSS selector that defines the group of conditionally required fields
 *
 */
    a.validator.addMethod("skip_or_fill_minimum", function(b, c, d) {
        var e = a(d[1], c.form), f = e.eq(0), g = f.data("valid_skip") ? f.data("valid_skip") : a.extend({}, this), h = e.filter(function() {
            return g.elementValue(this);
        }).length, i = h === 0 || h >= d[0];
        // Store the cloned validator for future validation
        f.data("valid_skip", g);
        // If element isn't being validated, run each skip_or_fill_minimum field's validation rules
        if (!a(c).data("being_validated")) {
            e.data("being_validated", true);
            e.each(function() {
                g.element(this);
            });
            e.data("being_validated", false);
        }
        return i;
    }, a.validator.format("Please either skip these fields or fill at least {0} of them."));
    /* Validates US States and/or Territories by @jdforsythe
 * Can be case insensitive or require capitalization - default is case insensitive
 * Can include US Territories or not - default does not
 * Can include US Military postal abbreviations (AA, AE, AP) - default does not
 *
 * Note: "States" always includes DC (District of Colombia)
 *
 * Usage examples:
 *
 *  This is the default - case insensitive, no territories, no military zones
 *  stateInput: {
 *     caseSensitive: false,
 *     includeTerritories: false,
 *     includeMilitary: false
 *  }
 *
 *  Only allow capital letters, no territories, no military zones
 *  stateInput: {
 *     caseSensitive: false
 *  }
 *
 *  Case insensitive, include territories but not military zones
 *  stateInput: {
 *     includeTerritories: true
 *  }
 *
 *  Only allow capital letters, include territories and military zones
 *  stateInput: {
 *     caseSensitive: true,
 *     includeTerritories: true,
 *     includeMilitary: true
 *  }
 *
 *
 *
 */
    a.validator.addMethod("stateUS", function(a, b, c) {
        var d = typeof c === "undefined", e = d || typeof c.caseSensitive === "undefined" ? false : c.caseSensitive, f = d || typeof c.includeTerritories === "undefined" ? false : c.includeTerritories, g = d || typeof c.includeMilitary === "undefined" ? false : c.includeMilitary, h;
        if (!f && !g) {
            h = "^(A[KLRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|PA|RI|S[CD]|T[NX]|UT|V[AT]|W[AIVY])$";
        } else if (f && g) {
            h = "^(A[AEKLPRSZ]|C[AOT]|D[CE]|FL|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEINOPST]|N[CDEHJMVY]|O[HKR]|P[AR]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$";
        } else if (f) {
            h = "^(A[KLRSZ]|C[AOT]|D[CE]|FL|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEINOPST]|N[CDEHJMVY]|O[HKR]|P[AR]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$";
        } else {
            h = "^(A[AEKLPRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|PA|RI|S[CD]|T[NX]|UT|V[AT]|W[AIVY])$";
        }
        h = e ? new RegExp(h) : new RegExp(h, "i");
        return this.optional(b) || h.test(a);
    }, "Please specify a valid state");
    // TODO check if value starts with <, otherwise don't try stripping anything
    a.validator.addMethod("strippedminlength", function(b, c, d) {
        return a(b).text().length >= d;
    }, a.validator.format("Please enter at least {0} characters"));
    a.validator.addMethod("time", function(a, b) {
        return this.optional(b) || /^([01]\d|2[0-3]|[0-9])(:[0-5]\d){1,2}$/.test(a);
    }, "Please enter a valid time, between 00:00 and 23:59");
    a.validator.addMethod("time12h", function(a, b) {
        return this.optional(b) || /^((0?[1-9]|1[012])(:[0-5]\d){1,2}(\ ?[AP]M))$/i.test(a);
    }, "Please enter a valid time in 12-hour am/pm format");
    // same as url, but TLD is optional
    a.validator.addMethod("url2", function(a, b) {
        return this.optional(b) || /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)*(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(a);
    }, a.validator.messages.url);
    /**
 * Return true, if the value is a valid vehicle identification number (VIN).
 *
 * Works with all kind of text inputs.
 *
 * @example <input type="text" size="20" name="VehicleID" class="{required:true,vinUS:true}" />
 * @desc Declares a required input element whose value must be a valid vehicle identification number.
 *
 * @name $.validator.methods.vinUS
 * @type Boolean
 * @cat Plugins/Validate/Methods
 */
    a.validator.addMethod("vinUS", function(a) {
        if (a.length !== 17) {
            return false;
        }
        var b = [ "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" ], c = [ 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 7, 9, 2, 3, 4, 5, 6, 7, 8, 9 ], d = [ 8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2 ], e = 0, f, g, h, i, j, k;
        for (f = 0; f < 17; f++) {
            i = d[f];
            h = a.slice(f, f + 1);
            if (f === 8) {
                k = h;
            }
            if (!isNaN(h)) {
                h *= i;
            } else {
                for (g = 0; g < b.length; g++) {
                    if (h.toUpperCase() === b[g]) {
                        h = c[g];
                        h *= i;
                        if (isNaN(k) && g === 8) {
                            k = b[g];
                        }
                        break;
                    }
                }
            }
            e += h;
        }
        j = e % 11;
        if (j === 10) {
            j = "X";
        }
        if (j === k) {
            return true;
        }
        return false;
    }, "The specified vehicle identification number (VIN) is invalid.");
    a.validator.addMethod("zipcodeUS", function(a, b) {
        return this.optional(b) || /^\d{5}(-\d{4})?$/.test(a);
    }, "The specified US ZIP Code is invalid");
    a.validator.addMethod("ziprange", function(a, b) {
        return this.optional(b) || /^90[2-5]\d\{2\}-\d{4}$/.test(a);
    }, "Your ZIP-code must be in the range 902xx-xxxx to 905xx-xxxx");
});

$(document).ready(function() {
    var a = false;
    var b = function() {
        if ($(window).width() < 860 && !a) {
            a = true;
            $("table.responsive").each(function(a, b) {
                c($(b));
            });
            return true;
        } else if (a && $(window).width() > 860) {
            a = false;
            $("table.responsive").each(function(a, b) {
                d($(b));
            });
        }
    };
    $(window).load(b);
    $(window).on("redraw", function() {
        a = false;
        b();
    });
    // An event to listen for
    $(window).on("resize", b);
    function c(a) {
        a.wrap("<div class='table-wrapper' />");
        var b = a.clone();
        b.find("td:not(:first-child), th:not(:first-child)").css("display", "none");
        b.removeClass("responsive");
        a.closest(".table-wrapper").append(b);
        b.wrap("<div class='pinned' />");
        a.wrap("<div class='scrollable' />");
        e(a, b);
    }
    function d(a) {
        a.closest(".table-wrapper").find(".pinned").remove();
        a.unwrap();
        a.unwrap();
    }
    function e(a, b) {
        var c = a.find("tr"), d = b.find("tr"), e = [];
        c.each(function(a) {
            var b = $(this), c = b.find("th, td");
            c.each(function() {
                var b = $(this).outerHeight(true);
                b = b - 1;
                e[a] = e[a] || 0;
                if (b > e[a]) e[a] = b;
            });
        });
        d.each(function(a) {
            $(this).height(e[a]);
        });
    }
});

//
// Page
//
// Menu Setup
(function() {
    $(".navigation-menu-button").on("click", function() {
        $(".nav").toggleClass("show");
        if ($(".nav").hasClass("show")) {
            $(".nav").insertAfter(".navigation-wrapper");
        } else {
            $(".nav").appendTo(".navigation-wrapper");
        }
    });
})();

//
// Window Resize Event w/ Delay
//
$(window).resize(function() {
    waitForFinalEvent(function() {
        // console.log('Resize...');
        changeLayout();
    }, 100, "Change Layout");
});

// limit the amount of resize calls by calling the function after the resize event
var waitForFinalEvent = function() {
    var a = {};
    return function(b, c, d) {
        if (!d) {
            d = "Don't call this twice without a uniqueId";
        }
        if (a[d]) {
            clearTimeout(a[d]);
        }
        a[d] = setTimeout(b, c);
    };
}();

// Move Sidebar depending on browser size
function changeLayout() {
    if ($(window).width() >= 860) {
        $(".nav.show").appendTo(".navigation-wrapper");
        $(".nav.show").toggleClass("show");
    }
}

function initTabs() {
    $("ul.tabs li").on("click", function(a) {
        var b = $(this).attr("data-tab");
        $("ul.tabs li").removeClass("current");
        $(".tab-content").removeClass("current");
        $(this).addClass("current");
        $("#" + b).addClass("current");
        $("#" + b).focus();
    });
}

//
// Validate
//
// Login Page Rules
function validateLoginForm() {
    var a = $("#loginForm").validate({
        onsubmit: false,
        onkeyup: function(a) {
            $(a).valid();
            checkForValid("#loginForm");
        },
        rules: {
            // Step 1
            loginName: {
                email: true,
                required: true
            },
            loginPW: {
                minlength: 6,
                required: true
            }
        }
    });
    return a;
}

// Estimator Section Rules
function validateEstimatorForm() {
    var a = $("#estimatorForm").validate({
        onsubmit: false,
        onkeyup: function(a) {
            $(a).valid();
            checkForValidEstimator();
        },
        onclick: function(a) {
            var b = $(a).prop("type");
            if (b === "radio" || b === "checkbox") {
                $(a).valid();
            }
            if (b === "select-one") {
                checkForSelectChange(a);
                $(a).valid();
                checkForValidEstimator();
                if (a.id === "step5CompareRate") {
                    compareRateTo();
                }
            }
        },
        rules: {
            // Step 2
            step2FirstName: "required",
            step2LastName: "required",
            step2Email: {
                email: true,
                required: true
            },
            // Step 3
            step3RadioUserType: "required",
            step3State: "required",
            step3Utility: "required",
            step3RateClass: "required",
            // Step 4
            step4PlanStart: "required",
            step4PlanLength: "required",
            // Step 5
            step5_month1_usage: {
                required: true,
                number: true
            },
            step5Month: {
                number: true
            },
            // Step 6
            step6PlanStrategy: "required",
            step6RetailAdderRate: "required"
        },
        messages: {
            step3State: "Please select a valid state."
        }
    });
    return a;
}

//
// Run Validation, these are called on the appropriate page
//
// Run Validation for Login Form
function pageLoginValidation() {
    "use strict";
    validateLoginForm();
    preventClick(".btn");
    checkForValid("#loginForm");
    // AutoFill / AutoComplete
    checkAutoFillEvent();
}

// Run Validation for Estimator Form
function pageEstimatorValidation() {
    "use strict";
    validateEstimatorForm();
    preventClick(".btn");
    checkForValidEstimator();
    // Next Event ( activeStep, targetStep )
    changeSectionNextEvent("step-two", "step-three");
    changeSectionNextEvent("step-three", "step-four");
    changeSectionNextEvent("step-four", "step-five");
    changeSectionNextEvent("step-five", "step-six");
    // Prev Event
    changeSectionBackEvent("step-three", "step-two");
    changeSectionBackEvent("step-four", "step-three");
    changeSectionBackEvent("step-five", "step-four");
    changeSectionBackEvent("step-six", "step-five");
    //
    // Special Cases
    //
    // AutoFill / AutoComplete
    checkAutoFillEvent();
    // Datepicker
    $(".date-picker").datepicker();
    // Step 3 - Radio Input Event Listeners
    $("#radio02").click(function() {
        if ($(this).is(":checked")) {
            a();
        }
    });
    // REPLACE -------- //
    // -----------------//
    // -----------------//
    // State Select
    $("#step3State").change(function(a) {
        if (this.value !== "MA" && this.value !== "CT" && this.value !== "NH") {
            $(".modal-2").toggleClass("hide");
            this.selectedIndex = 0;
        }
    });
    // Close Modal 1 --- commercial / residence
    $(".modal-1 .close").click(function() {
        b();
    });
    // CLose Modal 2 --- states
    $(".modal-2 .close").click(function() {
        $(".modal-2").toggleClass("hide");
    });
    // Step 3 - Modal 1 Toggle
    function a() {
        $(".modal-1").toggleClass("hide");
    }
    // -----------------//
    // Step 3 - Close Modal 1
    function b() {
        a();
        $("#radio01").prop("checked", true);
        $("#radio01").valid();
    }
    // Step 5 - Repeat Field Values Event Listener
    $("#duplicateUsage").click(function() {
        c(".input-field.usage");
    });
    $("#duplicateRate").click(function() {
        c(".input-field.rate");
    });
    // Step 5 - Duplicate Field Values
    function c(a) {
        var b = [];
        // Get Values
        $(a).each(function() {
            if ($(this).val() !== "") {
                b.push($(this).val());
            }
        });
        // if No Values
        if (b.length <= 0) {
            alert("There are no values entered");
        }
        // if Single Value
        if (b.length == 1) {
            $(a).each(function() {
                $(this).val(b[0]);
                $(this).valid();
            });
        }
        // if Too Many Values
        if (b.length > 1) {
            if (confirm("Alert you have multiple values that will be replaced, Do you want to proceed?")) {
                $(a).each(function() {
                    $(this).val(b[0]);
                    $(this).valid();
                });
            }
        }
    }
}

//
// Check if Form/Section is Valid
//
// If form is valid enable Button
function checkForValid(a) {
    var b = a + " .inputRequired";
    var c = a + " .inputRequired.valid";
    var d = $(a).find("button.next");
    if ($(b).length === $(c).length) {
        enableButton(d);
        return true;
    } else {
        disableButton(d);
        return false;
    }
}

// Check Entire Estimator Form for changes
function checkForValidEstimator() {
    checkForValid("#step-two");
    checkForValid("#step-three");
    checkForValid("#step-four");
    checkForValid("#step-five");
    checkForValid("#step-six");
}

// Check For Select Change
function checkForSelectChange(a) {
    $(a).change(function() {
        $(this).valid();
        checkForValidEstimator();
        if (a.id === "step5CompareRate") {
            compareRateTo();
        }
    });
}

// Check For AutoFill / AutoComplete
function checkAutoFillEvent() {
    $(".inputRequired").on("change blur", function() {
        $(this).valid();
        checkForValidEstimator();
    });
}

//
// Next / Prev Controls
//
// Next
function changeSectionNextEvent(a, b) {
    var c = $("#" + a).find(".next");
    $(c).click(function() {
        if (!$(c).hasClass("disabled")) {
            // Hide Current
            $("#" + a).addClass("hide");
            $("#" + b).removeClass("hide");
            // Set Breadcrumb
            $("." + a).addClass("val");
            // we know its valid
            $("." + a).removeClass("active");
            $("." + b).addClass("active");
        }
    });
}

// Prev
function changeSectionBackEvent(a, b) {
    var c = $("#" + a).find(".prev");
    $(c).click(function() {
        if (!$(c).hasClass("disabled")) {
            // Hide Current
            $("#" + a).addClass("hide");
            $("#" + b).removeClass("hide");
            // Set Breadcrumb
            if (checkForValid("#" + a)) {
                $("." + a).addClass("val");
            } else {
                $("." + a).removeClass("val");
            }
            $("." + a).removeClass("active");
            $("." + b).addClass("active");
        }
    });
}

// Prevent Clicks on Disabled Button Elements
function preventClick(a) {
    $(a).click(function(a) {
        if ($(this).hasClass("disabled")) {
            a.preventDefault();
            var b = "#" + $(this).closest("form").attr("id");
            //.get(0) added
            // Will enable error messages immediately before the button element.
            $(this).prev().show();
            $(b).valid();
        }
    });
}

// Disable Button
function disableButton(a) {
    $(a).addClass("disabled");
}

// Enable Button
function enableButton(a) {
    $(a).removeClass("disabled");
}

//
// Misc Validation Rules
//
var compareRateTo = function(a) {
    if ($("#step5CompareRate").val() == "Service Competitor") {
        $("#step-five .input.rate").each(function() {
            $(this).removeClass("hide");
        });
        $("#step-five").removeClass("stretch");
    } else {
        $("#step-five .input.rate").each(function() {
            $(this).addClass("hide");
        });
        $("#step-five").addClass("stretch");
    }
};