<!doctype html>
<html class="no-js" lang="">

<?php include 'templates/_head.php'; ?>

<body>

  <?php include 'templates/_header.php'; ?>

  <section>
    <div class="content">
      <div class="row">

        <div class="main-content">

          <div class="progress-bar">
            <a href="javascript:void(0);" class="step-1 val">1</a>
            <a href="javascript:void(0);" class="step-two active">2</a>
            <a href="javascript:void(0);" class="step-three">3</a>
            <a href="javascript:void(0);" class="step-four">4</a>
            <a href="javascript:void(0);" class="step-five">5</a>
            <a href="javascript:void(0);" class="step-six">6</a>
          </div>

          <div class="estimator-tool">

            <form id="estimatorForm" autocomplete="off" action="">

              <!-- Step 1 LOGIN PAGE - index.php -->

              <!-- Step 2 -->
              <fieldset id="step-two">
                <h2 class="title">CLIENT INFO</h2>
                <span class="input">
                  <label class="input-label" for="step2FirstName">
                    <span class="input-label-content">First Name</span>
                  </label>
                  <input class="inputRequired input-field" type="text" id="step2FirstName" name="step2FirstName" />
                </span>
                <span class="input">
                  <label class="input-label" for="step2LastName">
                    <span class="input-label-content">Last Name</span>
                  </label>
                  <input class="inputRequired input-field" type="text" id="step2LastName" name="step2LastName" />
                </span>
                <span class="input">
                  <label class="input-label" for="step2Email">
                    <span class="input-label-content">Email Address</span>
                  </label>
                  <input class="inputRequired input-field" type="text" id="step2Email" name="step2Email" />
                </span>
                <span class="input btn-container">
                  <button id="button-step2" type="button" class="btn next disabled">NEXT &#9654;</button>
                </span>
              </fieldset>

              <!-- Step 3 -->
              <fieldset id="step-three" class="hide">
                <h2 class="title">NEEDS TITLE</h2>

                <span class="input">
                  <label class="input-label" for="step3RadioUserType">
                    <span class="input-label-content">Select what type of energy user you are</span>
                    <br />
                  </label>
                  <span>
                    <input type="radio" id="radio01" class="inputRequired" name="step3RadioUserType" value="commercial" />
                    <label class="radio-label" for="radio01"><span><!--Bullet--></span>Commercial/Industrial</label>
                    <input type="radio" id="radio02" class="inputRequired" name="step3RadioUserType" value="commercial" />
                    <label class="radio-label" for="radio02"><span><!--Bullet--></span>Residential</label>
                  </span>
                  <label id="idCheck-error" class="error" for="step3RadioUserType"></label>

                </span>

                <span class="input">
                  <label class="input-label" for="step3State">
                    <span class="input-label-content">Select Your State</span>
                  </label>
                  <div class="select-container">
                    <select class="inputRequired input-field" id="step3State" name="step3State">
                      <option></option>
                      <option value="AL" class="">Alabama</option>
                      <option value="AK" class="">Alaska</option>
                      <option value="AZ" class="">Arizona</option>
                      <option value="AR" class="">Arkansas</option>
                      <option>---</option>
                      <option value="CA" class="">California</option>
                      <option value="CO" class="">Colorado</option>
                      <option value="CT" class="validStateOption">Connecticut</option>
                      <option value="DE" class="">Delaware</option>
                      <option value="FL" class="">Florida</option>
                      <option value="GA" class="">Georgia</option>
                      <option value="HI" class="">Hawaii</option>
                      <option value="ID" class="">Idaho</option>
                      <option value="IL" class="">Illinois</option>
                      <option value="IN" class="">Indiana</option>
                      <option value="IA" class="">Iowa</option>
                      <option value="KS" class="">Kansas</option>
                      <option value="KY" class="">Kentucky</option>
                      <option value="LA" class="">Louisiana</option>
                      <option value="ME" class="validStateOption">Maine</option>
                      <option value="MD" class="">Maryland</option>
                      <option value="MA" class="validStateOption">Massachusetts</option>
                      <option value="MI" class="">Michigan</option>
                      <option value="MN" class="">Minnesota</option>
                      <option value="MS" class="">Mississippi</option>
                      <option value="MO" class="">Missouri</option>
                      <option value="MT" class="">Montana</option>
                      <option value="NE" class="">Nebraska</option>
                      <option value="NV" class="">Nevada</option>
                      <option value="NH" class="validStateOption">New Hampshire</option>
                      <option value="NJ" class="">New Jersey</option>
                      <option value="NM" class="">New Mexico</option>
                      <option value="NY" class="">New York</option>
                      <option value="NC" class="">North Carolina</option>
                      <option value="ND" class="">North Dakota</option>
                      <option value="OH" class="">Ohio</option>
                      <option value="OK" class="">Oklahoma</option>
                      <option value="OR" class="">Oregon</option>
                      <option value="PA" class="validStateOption">Pennsylvania</option>
                      <option value="RI" class="validStateOption">Rhode Island</option>
                      <option value="SC" class="">South Carolina</option>
                      <option value="SD" class="">South Dakota</option>
                      <option value="TN" class="">Tennessee</option>
                      <option value="TX" class="">Texas</option>
                      <option value="UT" class="">Utah</option>
                      <option value="VT" class="">Vermont</option>
                      <option value="VA" class="">Virginia</option>
                      <option value="WA" class="">Washington</option>
                      <option value="WV" class="">West Virginia</option>
                      <option value="WI" class="">Wisconsin</option>
                      <option value="WY" class="">Wyoming</option>
                    </select>
                    <span class="select-arrow">&#9660;</span>
                  </div>
                </span>
                <span class="input">
                  <label class="input-label" for="step3Utility">
                    <span class="input-label-content">Select Your Utility</span>
                  </label>
                  <div class="select-container">
                    <select class="inputRequired input-field" id="step3Utility" name="step3Utility">
                      <option></option>
                      <option>Item 1</option>
                      <option>Item 2</option>
                      <option>Item 3</option>
                      <option>Item 4</option>
                    </select>
                    <span class="select-arrow">&#9660;</span>
                  </div>
                </span>
                <span class="input">
                  <label class="input-label" for="step3RateClass">
                    <span class="input-label-content">Select Your Rate Class</span>
                  </label>
                  <div class="select-container">
                    <select class="inputRequired input-field" id="step3RateClass" name="step3RateClass">
                      <option></option>
                      <option>Item 1</option>
                      <option>Item 2</option>
                      <option>Item 3</option>
                      <option>Item 4</option>
                    </select>
                    <span class="select-arrow">&#9660;</span>
                  </div>
                </span>
                <span class="input btn-container">
                  <button id="button-back-step3" type="button" class="btn prev">&#9664; BACK</button>
                  <button id="button-step3" type="button" class="btn next disabled">NEXT &#9654;</button>
                </span>

                <div class="modal modal-1 hide">
                  <div class="container">
                    <div class="message">
                      <div class="close" style="cursor: pointer;">Close</div>
                      <p>Sorry, we only serve commercial and industrial customers at this time.</p>
                      <p>Please Visit our site for more informaiton</p>
                      <a href="http://www.mintenergy.net/" target="_blank" id="learn-more" class="btn learn-more">LEARN MORE</a>
                    </div>
                  </div>
                </div>

                <div class="modal modal-2 hide">
                  <div class="container">
                    <div class="message">
                      <div class="close" style="cursor: pointer;">Close</div>
                      <p>Sorry, we only serve Connecticut, Massachusetts, Maine, New Hampshire, Pennsylvania, Rhode Island at this time.</p>
                      <p>Please Visit our site for more informaiton</p>
                      <a href="http://www.mintenergy.net/" target="_blank" id="learn-more" class="btn learn-more">LEARN MORE</a>
                    </div>
                  </div>
                </div>

              </fieldset>

              <!-- Step 4 -->
              <fieldset id="step-four" class="hide">
                <h2 class="title">Plan Information</h2>

                <!-- date picker -->
                <span class="input">
                  <label class="input-label" for="step4PlanStart">
                    <span class="input-label-content">When would you like your plan to start?</span>
                  </label>
                  <input class="inputRequired input-field date-picker" type="text" id="step4PlanStart" name="step4PlanStart" />
                  <span class="input-date">&#x1f4c5;</span>
                </span>

                <span class="input">
                  <label class="input-label" for="step4PlanLength">
                    <span class="input-label-content">Desired Term</span>
                  </label>
                  <!-- This field is in question -->
                  <div class="select-container">
                    <select class="inputRequired input-field" id="step4PlanLength" name="step4PlanLength">
                      <option></option>
                      <option>Item 1</option>
                      <option>Item 2</option>
                      <option>Item 3</option>
                      <option>Item 4</option>
                    </select>
                    <span class="select-arrow">&#9660;</span>
                  </div>
                </span>

                <span class="input btn-container">
                  <button id="button-back_step4" type="button" class="btn prev">&#9664; BACK</button>
                  <button id="button-step4" type="button" class="btn next disabled">NEXT &#9654;</button>
                </span>

              </fieldset>

              <!-- Step 5 -->
              <fieldset id="step-five" class="hide stretch">
                <h2 class="title">Needs Title</h2>

                <span class="input">
                  <label class="input-label" for="step5CompareRate">
                    <span class="input-label-content">Compare Rate to:</span>
                  </label>
                  <div class="select-container">
                    <select class="input-field" id="step5CompareRate" name="step5CompareRate">
                      <option></option>
                      <option>Service Competitor</option>
                    </select>
                    <span class="select-arrow">&#9660;</span>
                  </div>
                </span>
                <div class="form-row">
                  <span class="column-heading">
                    <span class="input-label-content">Enter your kWh Usage and Rates for the past 12 months</span>
                  </span>
                </div>
                <div class="form-row">
                  <span class="input segmented">
                    <span class="column-heading">
                      <span class="input-label-content">Duplicate Entries</span>
                    </span>
                  </span>
                  <span class="input segmented usage">
                    <a id="duplicateUsage" class="utility gradient">Same Usage for all months</a>
                  </span>
                  <span class="input segmented rate hide">
                   <a id="duplicateRate" class="utility gradient">Same Rate for all months</a>
                  </span>
                </div>
                <!-- Month 1 -->
                <div class="form-row">
                  <span class="input segmented">
                    <span class="column-heading">
                      <span class="input-label-content">MONTH</span>
                    </span>
                    <h3 class="section">August 2015</h3>
                  </span>
                  <span class="input segmented usage">
                    <label class="input-label" for="step5_month1_usage">
                      <span class="input-label-content">KWH USAGE</span>
                    </label>
                    <input class="inputRequired input-field usage" type="text" id="step5_month1_usage" name="step5_month1_usage" />
                  </span>
                  <span class="input segmented rate hide">
                    <label class="input-label" for="step5_month1_rate">
                      <span class="input-label-content">RATE/KWH</span>
                    </label>
                    <input class="input-field rate" type="text" id="step5_month1_rate" name="step5Month" />
                  </span>
                </div>
                <!-- Month 2 -->
                <div class="form-row">
                  <span class="input segmented">
                    <span class="column-heading">
                      <span class="input-label-content">MONTH</span>
                    </span>
                    <h3 class="section">July 2015</h3>
                  </span>
                  <span class="input segmented usage">
                    <label class="input-label" for="step5_month2_usage">
                      <span class="input-label-content">KWH USAGE</span>
                    </label>
                    <input class="input-field usage" type="text" id="step5_month2_usage" name="step5Month" />
                  </span>
                  <span class="input segmented rate hide">
                    <label class="input-label" for="step5_month2_rate">
                      <span class="input-label-content">RATE/KWH</span>
                    </label>
                    <input class="input-field rate" type="text" id="step5_month2_rate" name="step5Month" />
                  </span>
                </div>
                <!-- Month 3 -->
                <div class="form-row">
                  <span class="input segmented">
                    <span class="column-heading">
                      <span class="input-label-content">MONTH</span>
                    </span>
                    <h3 class="section">June 2015</h3>
                  </span>
                  <span class="input segmented usage">
                    <label class="input-label" for="step5_month3_usage">
                      <span class="input-label-content">KWH USAGE</span>
                    </label>
                    <input class="input-field usage" type="text" id="step5_month3_usage" name="step5Month" />
                  </span>
                  <span class="input segmented rate hide">
                    <label class="input-label" for="step5_month3_rate">
                      <span class="input-label-content">RATE/KWH</span>
                    </label>
                    <input class="input-field rate" type="text" id="step5_month3_rate" name="step5Month" />
                  </span>
                </div>
                <!-- Month 4 -->
                <div class="form-row">
                  <span class="input segmented">
                    <span class="column-heading">
                      <span class="input-label-content">MONTH</span>
                    </span>
                    <h3 class="section">May 2015</h3>
                  </span>
                  <span class="input segmented usage">
                    <label class="input-label" for="step5_month4_usage">
                      <span class="input-label-content">KWH USAGE</span>
                    </label>
                    <input class="input-field usage" type="text" id="step5_month4_usage" name="step5Month" />
                  </span>
                  <span class="input segmented rate hide">
                    <label class="input-label" for="step5_month4_rate">
                      <span class="input-label-content">RATE/KWH</span>
                    </label>
                    <input class="input-field rate" type="text" id="step5_month4_rate" name="step5Month" />
                  </span>
                </div>
                <!-- Month 5 -->
                <div class="form-row">
                  <span class="input segmented">
                    <span class="column-heading">
                      <span class="input-label-content">MONTH</span>
                    </span>
                    <h3 class="section">April 2015</h3>
                  </span>
                  <span class="input segmented usage">
                    <label class="input-label" for="step5_month5_usage">
                      <span class="input-label-content">KWH USAGE</span>
                    </label>
                    <input class="input-field usage" type="text" id="step5_month5_usage" name="step5Month" />
                  </span>
                  <span class="input segmented rate hide">
                    <label class="input-label" for="step5_month5_rate">
                      <span class="input-label-content">RATE/KWH</span>
                    </label>
                    <input class="input-field rate" type="text" id="step5_month5_rate" name="step5Month" />
                  </span>
                </div>
                <!-- Month 6 -->
                <div class="form-row">
                  <span class="input segmented">
                    <span class="column-heading">
                      <span class="input-label-content">MONTH</span>
                    </span>
                    <h3 class="section">March 2015</h3>
                  </span>
                  <span class="input segmented usage">
                    <label class="input-label" for="step5_month6_usage">
                      <span class="input-label-content">KWH USAGE</span>
                    </label>
                    <input class="input-field usage" type="text" id="step5_month6_usage" name="step5Month" />
                  </span>
                  <span class="input segmented rate hide">
                    <label class="input-label" for="step5_month6_rate">
                      <span class="input-label-content">RATE/KWH</span>
                    </label>
                    <input class="input-field rate" type="text" id="step5_month6_rate" name="step5Month" />
                  </span>
                </div>
                <!-- Month 7 -->
                <div class="form-row">
                  <span class="input segmented">
                    <span class="column-heading">
                      <span class="input-label-content">MONTH</span>
                    </span>
                    <h3 class="section">February 2015</h3>
                  </span>
                  <span class="input segmented usage">
                    <label class="input-label" for="step5_month7_usage">
                      <span class="input-label-content">KWH USAGE</span>
                    </label>
                    <input class="input-field usage" type="text" id="step5_month7_usage" name="step5Month" />
                  </span>
                  <span class="input segmented rate hide">
                    <label class="input-label" for="step5_month7_rate">
                      <span class="input-label-content">RATE/KWH</span>
                    </label>
                    <input class="input-field rate" type="text" id="step5_month7_rate" name="step5Month" />
                  </span>
                </div>
                <!-- Month 8 -->
                <div class="form-row">
                  <span class="input segmented">
                    <span class="column-heading">
                      <span class="input-label-content">MONTH</span>
                    </span>
                    <h3 class="section">January 2015</h3>
                  </span>
                  <span class="input segmented usage">
                    <label class="input-label" for="step5_month8_usage">
                      <span class="input-label-content">KWH USAGE</span>
                    </label>
                    <input class="input-field usage" type="text" id="step5_month8_usage" name="step5Month" />
                  </span>
                  <span class="input segmented rate hide">
                    <label class="input-label" for="step5_month8_rate">
                      <span class="input-label-content">RATE/KWH</span>
                    </label>
                    <input class="input-field rate" type="text" id="step5_month8_rate" name="step5Month" />
                  </span>
                </div>
                <!-- Month 9 -->
                <div class="form-row">
                  <span class="input segmented">
                    <span class="column-heading">
                      <span class="input-label-content">MONTH</span>
                    </span>
                    <h3 class="section">December 2015</h3>
                  </span>
                  <span class="input segmented usage">
                    <label class="input-label" for="step5_month9_usage">
                      <span class="input-label-content">KWH USAGE</span>
                    </label>
                    <input class="input-field usage" type="text" id="step5_month9_usage" name="step5Month" />
                  </span>
                  <span class="input segmented rate hide">
                    <label class="input-label" for="step5_month9_rate">
                      <span class="input-label-content">RATE/KWH</span>
                    </label>
                    <input class="input-field rate" type="text" id="step5_month9_rate" name="step5Month" />
                  </span>
                </div>
                <!-- Month 10 -->
                <div class="form-row">
                  <span class="input segmented">
                    <span class="column-heading">
                      <span class="input-label-content">MONTH</span>
                    </span>
                    <h3 class="section">November 2015</h3>
                  </span>
                  <span class="input segmented usage">
                    <label class="input-label" for="step5_month10_usage">
                      <span class="input-label-content">KWH USAGE</span>
                    </label>
                    <input class="input-field usage" type="text" id="step5_month10_usage" name="step5Month" />
                  </span>
                  <span class="input segmented rate hide">
                    <label class="input-label" for="step5_month10_rate">
                      <span class="input-label-content">RATE/KWH</span>
                    </label>
                    <input class="input-field rate" type="text" id="step5_month10_rate" name="step5Month" />
                  </span>
                </div>
                <!-- Month 11 -->
                <div class="form-row">
                  <span class="input segmented">
                    <span class="column-heading">
                      <span class="input-label-content">MONTH</span>
                    </span>
                    <h3 class="section">October 2015</h3>
                  </span>
                  <span class="input segmented usage">
                    <label class="input-label" for="step5_month11_usage">
                      <span class="input-label-content">KWH USAGE</span>
                    </label>
                    <input class="input-field usage" type="text" id="step5_month11_usage" name="step5Month" />
                  </span>
                  <span class="input segmented rate hide">
                    <label class="input-label" for="step5_month11_rate">
                      <span class="input-label-content">RATE/KWH</span>
                    </label>
                    <input class="input-field rate" type="text" id="step5_month11_rate" name="step5Month" />
                  </span>
                </div>
                <!-- Month 12 -->
                <div class="form-row">
                  <span class="input segmented">
                    <span class="column-heading">
                      <span class="input-label-content">MONTH</span>
                    </span>
                    <h3 class="section">September 2015</h3>
                  </span>
                  <span class="input segmented usage">
                    <label class="input-label" for="step5_month12_usage">
                      <span class="input-label-content">KWH USAGE</span>
                    </label>
                    <input class="input-field usage" type="text" id="step5_month12_usage" name="step5Month" />
                  </span>
                  <span class="input segmented rate hide">
                    <label class="input-label" for="step5_month12_rate">
                      <span class="input-label-content">RATE/KWH</span>
                    </label>
                    <input class="input-field rate" type="text" id="step5_month12_rate" name="step5Month" />
                  </span>
                </div>

                <span class="input btn-container">
                  <button id="button-back-step4" type="button" class="btn prev">&#9664; BACK</button>
                  <button id="button-step5" type="button" class="btn next disabled">NEXT &#9654;</button>
                </span>

              </fieldset>


              <!-- Step 6 -->
              <fieldset id="step-six" class="hide">
                <h2 class="title">Needs Title</h2>

                <span class="input">
                  <label class="input-label" for="step6PlanStrategy">
                    <span class="input-label-content">Select Your Strategy</span>
                  </label>
                  <div class="select-container">
                    <select class="inputRequired input-field" id="step6PlanStrategy" name="step6PlanStrategy">
                      <option></option>
                      <option>Item 1</option>
                      <option>Item 2</option>
                      <option>Item 3</option>
                      <option>Item 4</option>
                    </select>
                    <span class="select-arrow">&#9660;</span>
                  </div>
                </span>
                <span class="input">
                  <label class="input-label" for="step6RetailAdderRate">
                    <span class="input-label-content">Enter Retail Adder Rate</span>
                  </label>
                  <input class="inputRequired input-field" type="text" id="step6RetailAdderRate" />
                </span>

                <span class="input btn-container">
                  <button id="button-back-step5" type="button" class="btn prev">&#9664; BACK</button>
                  <button id="button-results" type="submit" class="btn next results disabled">RESULTS</button>
                </span>

              </fieldset>

            </form>
            <!-- End Form -->

          </div>
        </div>

      </div>
    </div>
  </section>

  <?php include 'templates/_footer.php'; ?>

  <script src="dist/js/scripts.js"></script>

  <script type="text/javascript">

    pageEstimatorValidation();

  </script>

</body>

</html>
