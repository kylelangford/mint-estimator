<header class="navigation" role="banner">
  <div class="navigation-wrapper">
    <a href="javascript:void(0)" class="logo">
      <img src="https://raw.githubusercontent.com/thoughtbot/refills/master/source/images/placeholder_logo_1.png" alt="Logo Image">
    </a>
    <div class="menu-btn-container">
      <a href="javascript:void(0)" class="navigation-menu-button" id="js-mobile-menu">MENU</a>
    </div>
    <nav class="nav" role="navigation">
      <ul id="js-navigation-menu" class="navigation-menu">
        <li class="nav-link"><a href="http://mintenergy.net/faq.php">Help/FAQ</a></li>
        <li class="nav-link"><a href="http://mintenergy.net/contact.php">Contact Us</a></li>
        <li class="nav-link"><a href="/">Main Site</a></li>
      </ul>
    </nav>
  </div>
</header>
